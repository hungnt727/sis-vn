﻿alter table [Order]
add [OrderTypeId] int
go
alter table [Order]
add [CustomerCreateId] int
FOREIGN KEY ([CustomerCreateId]) REFERENCES Customer(Id)
go
create table [SISVN_OrderPaymentBalance]
(
	[Id] int identity(1,1) primary key,
	[OrderId] int not null,
	[PaymentDateUtc] datetime not null,
	[PaymentAmount] decimal not null,
	[PaymentMethodId] int not null,
	FOREIGN KEY (OrderId) REFERENCES [Order](Id)
)
go
create table [SISVN_ProductUnitCombo]
(
	[Id] int identity(1,1) primary key,
	[Name] int not null,
	[Deleted] bool default 0,
	[DisplayOrder] int not null,
	[CreatedOnUtc] datetime not null,
	[UpdatedOnUtc] datetime not null,
)
go
create table [SISVN_ProductUnitComboItem]
(
	[Id] int identity(1,1) primary key,
	[ProductUnitComboId] int not null,
	[Name] int not null,
	[IsBase] bit not null,
	[ExchangeQuantity] int not null,
	[Deleted] bool default 0,
	[DisplayOrder] int not null,
	[CreatedOnUtc] datetime not null,
	[UpdatedOnUtc] datetime not null,
	FOREIGN KEY (ProductUnitComboId) REFERENCES [SISVN_ProductUnitCombo](Id)
)
go
alter table [Product]
add [WholePrice] decimal
go
alter table [Product]
add [SpecialPrice] decimal
go
alter table [Product]
add [PublishedPOS] bit
go
alter table [Order]
add [VendorId] int 
FOREIGN KEY ([VendorId]) REFERENCES Vendor(Id)
go


alter table [Order]
add [WareHouseId] int 
FOREIGN KEY ([WareHouseId]) REFERENCES WareHouse(Id)
go