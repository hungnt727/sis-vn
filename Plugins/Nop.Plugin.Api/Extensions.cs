﻿using Nop.Plugin.Api.Models.Common;
using Nop.Services.Catalog;
using Nop.Services.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api
{
    public static class Extensions
    {
        public static DecimalModel ToModel(this decimal value, IPriceFormatter priceFormatter)
        {
            if (priceFormatter == null) throw new ArgumentNullException("priceFormatter");
            var model = new DecimalModel
            {
                Value = value,
                Text = priceFormatter.FormatPrice(value)
            };
            return model;
        }

        public static string Format(this DateTime date, IDateTimeHelper _dateTimeHelper, string format = "dd/MM/yyyy hh:mm:ss")
        {
            var userDate = _dateTimeHelper.ConvertToUserTime(date);
            var result = userDate.ToString(format);
            return result;
        }
    }
}
