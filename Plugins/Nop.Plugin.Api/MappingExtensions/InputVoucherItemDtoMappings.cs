﻿using Nop.Core.Domain.Orders;
using Nop.Plugin.Api.AutoMapper;
using Nop.Plugin.Api.DTOs.InputVoucherItems;
using Nop.Plugin.Api.DTOs.OrderItems;

namespace Nop.Plugin.Api.MappingExtensions
{
    public static class InputVoucherItemDtoMappings
    {
        public static InputVoucherItemDto ToInputVoucherItemDto(this OrderItem orderItem)
        {
            return orderItem.MapTo<OrderItem, InputVoucherItemDto>();
        }
    }
}