﻿using Nop.Plugin.Api.AutoMapper;

namespace Nop.Plugin.Api.MappingExtensions
{
    using IdentityServer4.EntityFramework.Entities;
    using Nop.Plugin.Api.Models;

    public static class ClientMappings
    {
        public static ClientApiModel ToApiModel(this Client client)
        {
            var model = client.MapTo<Client, ClientApiModel>();
            model.AccessRole = client.Properties != null && client.Properties.Count == 1 ? client.Properties[0].Value : "";
            return model;
        }
    }
}