﻿using Nop.Plugin.Api.AutoMapper;
using Nop.Core.Domain.Discounts;
using Nop.Plugin.Api.DTOs.Customers;
using Nop.Plugin.Api.DTOs.Discounts;

namespace Nop.Plugin.Api.MappingExtensions
{
    public static class DiscountDtoMapping
    {
        public static DiscountDto ToDto(this Discount discount)
        {
            return discount.MapTo<Discount, DiscountDto>();
        }

        public static DiscountDto ToOrderCustomerDto(this Discount discount)
        {
            return discount.MapTo<Discount, DiscountDto>();
        }
       
    }
}