﻿using Nop.Core.Domain.Orders;
using Nop.Plugin.Api.AutoMapper;
using Nop.Plugin.Api.DTOs.InputVouchers;

namespace Nop.Plugin.Api.MappingExtensions
{
    public static class InputVoucherDtoMappings
    {
        public static InputVoucherDto ToInputVoucherDto(this Order order)
        {
            return order.MapTo<Order, InputVoucherDto>();
        }
    }
}