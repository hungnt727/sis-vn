﻿namespace Nop.Plugin.Api.Authorization.Requirements
{
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Http;
    using Nop.Core.Infrastructure;
    using Nop.Plugin.Api.Models;
    using Nop.Plugin.Api.Services;
    using System;

    public class AccessRoleMatchRequirement : IAuthorizationRequirement
    {
        private string role;

        public AccessRoleMatchRequirement(string systemNameRole)
        {
            this.role = systemNameRole;
        }
        public bool IsMatch()
        {
            var httpContextAccessor = EngineContext.Current.Resolve<IHttpContextAccessor>();

            var clientId =
                httpContextAccessor.HttpContext.User.FindFirst("client_id")?.Value;

            if (clientId != null)
            {
                var clientService = EngineContext.Current.Resolve<IClientService>();
                var client = clientService.FindClientByClientId(clientId);

                if (client != null && string.Equals(client.AccessRole, this.role, StringComparison.InvariantCultureIgnoreCase))
                {
                    return true;
                }
            }

            return false;
        }
    }
}