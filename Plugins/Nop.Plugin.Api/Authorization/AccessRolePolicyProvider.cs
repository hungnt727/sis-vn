﻿using Microsoft.AspNetCore.Authorization;
using Nop.Core.Domain.Customers;
using Nop.Core.Infrastructure;
using Nop.Plugin.Api.Authorization.Requirements;
using Nop.Services.Customers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.Authorization
{
    internal class AccessRolePolicyProvider : IAuthorizationPolicyProvider
    {
        private IList<CustomerRole> _cachedRoles;

        public AccessRolePolicyProvider()
        {
            var customerService = EngineContext.Current.Resolve<ICustomerService>();
            _cachedRoles = customerService.GetAllCustomerRoles();
        }

        public Task<AuthorizationPolicy> GetDefaultPolicyAsync() => Task.FromResult(new AuthorizationPolicyBuilder().RequireAuthenticatedUser().Build());

        // Policies are looked up by string name, so expect 'parameters' (like age)
        // to be embedded in the policy names. This is abstracted away from developers
        // by the more strongly-typed attributes derived from AuthorizeAttribute
        // (like [MinimumAgeAuthorize()] in this sample)
        public Task<AuthorizationPolicy> GetPolicyAsync(string policyName)
        {
            if (string.Equals(policyName, "Admin", StringComparison.InvariantCultureIgnoreCase))
            {
                policyName = "Administrators";
            }
            var role = _cachedRoles.FirstOrDefault(r => string.Equals(r.Name, policyName));
            if (role != null)
            {
                var policy = new AuthorizationPolicyBuilder();
                policy.AddRequirements(new AccessRoleMatchRequirement(role.SystemName));
                return Task.FromResult(policy.Build());
            }

            return Task.FromResult<AuthorizationPolicy>(null);
        }
    }
}
