﻿namespace Nop.Plugin.Api.Authorization.Policies
{
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Authorization;
    using Nop.Plugin.Api.Authorization.Requirements;

    public class AccessRoleMatchAuthorizationPolicy : AuthorizationHandler<AccessRoleMatchRequirement>
    {
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, AccessRoleMatchRequirement requirement)
        {
            if (requirement.IsMatch())
            {
                context.Succeed(requirement);
            }
            else
            {
                context.Fail();
            }

            return Task.CompletedTask;
        }
    }
}