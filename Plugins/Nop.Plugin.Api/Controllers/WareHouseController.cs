﻿using Nop.Plugin.Api.Attributes;
using Nop.Plugin.Api.Delta;
using Nop.Plugin.Api.Factories;
using Nop.Plugin.Api.Helpers;
using Nop.Plugin.Api.JSON.ActionResults;
using Nop.Plugin.Api.ModelBinders;
using Nop.Plugin.Api.Services;
using Nop.Services.Common;
using Nop.Services.Directory;
using Nop.Services.Discounts;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Media;
using Nop.Services.Messages;
using Nop.Services.Security;
using Nop.Services.Stores;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace Nop.Plugin.Api.Controllers
{
    using Microsoft.AspNetCore.Mvc;
    using Newtonsoft.Json;
    using Nop.Core.Domain.Catalog;
    using Nop.Core.Domain.Discounts;
    using Nop.Core.Domain.Shipping;
    using Nop.Plugin.Api.DTOs.Errors;
    using Nop.Plugin.Api.DTOs.LiquidationStock;
    using Nop.Plugin.Api.DTOs.ProductWarehouseInventorys;
    using Nop.Plugin.Api.DTOs.StockQuantityHistorys;
    using Nop.Plugin.Api.DTOs.WareHouses;
    using Nop.Plugin.Api.JSON.Serializers;
    using Nop.Plugin.Api.Models.InventoryStockParameters;
    using Nop.Services.Customers;
    using Nop.Services.Orders;
    using Nop.Services.Shipping;

    public class WareHouseController : BaseApiController
    {
        private IShippingService _shippingService;

        public WareHouseController(
            IJsonFieldsSerializer jsonFieldsSerializer,
            IAclService aclService,
            ICustomerService customerService,
            IStoreMappingService storeMappingService,
            IStoreService storeService,
            IDiscountService discountService,
            ICustomerActivityService customerActivityService,
            ILocalizationService localizationService,
            IGenericAttributeService genericAttributeService,
            IEncryptionService encryptionService,
            IFactory<Discount> factory,
            ICountryService countryService,
            IMappingHelper mappingHelper,
            INewsLetterSubscriptionService newsLetterSubscriptionService,
            IPictureService pictureService,
            IShippingService shippingService

            )
        {
            _shippingService = shippingService;
        }

        #region Custom Method
        /// <summary>
        /// [TuanAnh][23/09/2018][Lấy danh sách kho]
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("/api/get_ware_house")]
        [ProducesResponseType(typeof(ProductWarehouseInventoryDtoRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [GetRequestsErrorInterceptorActionFilter]
        public virtual IActionResult GetWareHouse(WareHouseSearchParametersModel parameters)
        {
            if (!ModelState.IsValid)
            {
                return Error();
            }

           var wareHouses = _shippingService.GetWarehouses(parameters.KeySearch, parameters.PageIndex, parameters.PageSize);

            IList<WareHouseDto> wareHousesDto = wareHouses.Select(s => new WareHouseDto()
            {
                Id = s.Id,
                Name = s.Name,
                Note = s.AdminComment

             }).ToList();

            var wareHousesRootObject = new WareHousesRootObject()
            {
                WareHouses = wareHousesDto,
                TotalRow = wareHouses.TotalCount
            };
            var json = _jsonFieldsSerializer.Serialize(wareHousesRootObject, string.Empty);
            return new RawJsonActionResult(json);
        }

        /// <summary>
        /// [TuanAnh][23/09/2018][Lấy thông tin kho hàng]
        /// </summary>
        /// <param name="warehouseid"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("/api/get_ware_house_by_id/{wareHouseId}")]
        [ProducesResponseType(typeof(WareHouseDto), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        public virtual IActionResult GetWareHouseById(int wareHouseId)
        {
            if (!ModelState.IsValid)
            {
                return Error();
            }

            var wareHouse = _shippingService.GetWarehouseById(wareHouseId);
            if (wareHouse ==  null )
            {
                return Error(HttpStatusCode.NotFound, "wareHouse", "not found");
            }

            WareHouseDto wareHousesDto = new WareHouseDto()
            {
                Id = wareHouse.Id,
                Name = wareHouse.Name,
                Note = wareHouse.AdminComment

            };

            var json = JsonConvert.SerializeObject(wareHousesDto);
            return new RawJsonActionResult(json);
        }



        /// <summary>
        /// [TuanAnh][03/09/2018][Hàm tạo mới kho]
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("/api/create_ware_house")]
        [ProducesResponseType(typeof(WareHouseDto), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), 422)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        public virtual IActionResult CreateWareHouse([ModelBinder(typeof(JsonModelBinder<WareHouseDto>))] Delta<WareHouseDto> wareHouseDelta)
        {
            if (!ModelState.IsValid)
            {
                return Error();
            }
            var wareHouse = new Warehouse();
            wareHouse.Name = wareHouseDelta.Dto.Name;
            wareHouse.AdminComment = wareHouseDelta.Dto.Note;

            _shippingService.InsertWarehouse(wareHouse);

            wareHouseDelta.Dto.Id = wareHouse.Id;

            var productWarehouseInventoryDto = wareHouse;

            _customerActivityService.InsertActivity("CreateWareHouse", _localizationService.GetResource("ActivityLog.CreateWareHouse"), wareHouse.Id);

            var json = JsonConvert.SerializeObject(wareHouse);

            return new RawJsonActionResult(json);
        }

        /// <summary>
        /// [TuanAnh][03/09/2018][Hàm cập nhập kho]
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("/api/update_ware_house")]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), 422)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        public virtual IActionResult UpdateWareHouse([ModelBinder(typeof(JsonModelBinder<WareHouseDto>))] Delta<WareHouseDto> wareHouseDelta)
        {
            if (!ModelState.IsValid)
            {
                return Error();
            }

            var wareHouse = _shippingService.GetWarehouseById(wareHouseDelta.Dto.Id);

            if (wareHouse == null )
            {
                return Error(HttpStatusCode.NotFound, "wareHouse", "not found");
            }

            wareHouse.Name = wareHouseDelta.Dto.Name;
            wareHouse.AdminComment = wareHouseDelta.Dto.Note;

            _shippingService.UpdateWarehouse(wareHouse);

            _customerActivityService.InsertActivity("UpdateWareHouse", _localizationService.GetResource("ActivityLog.UpdateWareHouse"), wareHouse.Id);

            var json = JsonConvert.SerializeObject(wareHouse);

            return new RawJsonActionResult(json);

        }


        /// <summary>
        /// [TuanAnh][03/09/2018][Hàm xóa kho]
        /// </summary>
        /// <param name="discountDelta"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("/api/ware_house/{id}")]
        [GetRequestsErrorInterceptorActionFilter]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        public IActionResult Delete(int id)
        {
            if (!ModelState.IsValid)
            {
                return Error();
            }
            if (id <= 0)
            {
                return Error(HttpStatusCode.BadRequest, "id", "invalid id");
            }
            var wareHouse = _shippingService.GetWarehouseById(id);
            if (wareHouse == null)
            {
                return Error(HttpStatusCode.NotFound, "warehouse", "not found");
            }
            _shippingService.DeleteWarehouse(wareHouse);
            _customerActivityService.InsertActivity("DeleteWarehouse", _localizationService.GetResource("ActivityLog.DeleteWarehouse"), wareHouse.Id);
            return new RawJsonActionResult("{}");
        }


        #endregion
    }
}