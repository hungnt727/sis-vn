﻿using Nop.Plugin.Api.Attributes;
using Nop.Plugin.Api.Delta;
using Nop.Plugin.Api.Factories;
using Nop.Plugin.Api.Helpers;
using Nop.Plugin.Api.JSON.ActionResults;
using Nop.Plugin.Api.ModelBinders;
using Nop.Plugin.Api.Services;
using Nop.Services.Common;
using Nop.Services.Directory;
using Nop.Services.Discounts;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Media;
using Nop.Services.Messages;
using Nop.Services.Security;
using Nop.Services.Stores;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace Nop.Plugin.Api.Controllers
{
    using Microsoft.AspNetCore.Mvc;
    using Newtonsoft.Json;
    using Nop.Core.Domain.Catalog;
    using Nop.Core.Domain.Discounts;
    using Nop.Plugin.Api.DTOs.Errors;
    using Nop.Plugin.Api.DTOs.LiquidationStock;
    using Nop.Plugin.Api.DTOs.ProductWarehouseInventorys;
    using Nop.Plugin.Api.DTOs.StockQuantityHistorys;
    using Nop.Plugin.Api.JSON.Serializers;
    using Nop.Plugin.Api.Models.InventoryStockParameters;
    using Nop.Services.Customers;
    using Nop.Services.Orders;

    public class InventoryStockController : BaseApiController
    {

        private IProductWarehouseInventoryApiService _productWarehouseInventoryApiService;
        private IStockQuantityHistoryApiService _stockQuantityHistoryApiService;
        private IOrderService _orderService;

        public InventoryStockController(
            IJsonFieldsSerializer jsonFieldsSerializer,
            IAclService aclService,
            ICustomerService customerService,
            IStoreMappingService storeMappingService,
            IStoreService storeService,
            IDiscountService discountService,
            ICustomerActivityService customerActivityService,
            ILocalizationService localizationService,
            IGenericAttributeService genericAttributeService,
            IEncryptionService encryptionService,
            IFactory<Discount> factory,
            ICountryService countryService,
            IMappingHelper mappingHelper,
            INewsLetterSubscriptionService newsLetterSubscriptionService,
            IPictureService pictureService,
            IProductWarehouseInventoryApiService productWarehouseInventoryApiService,
            IStockQuantityHistoryApiService stockQuantityHistoryApiService,
            IOrderService orderService

            )
        {
            _productWarehouseInventoryApiService = productWarehouseInventoryApiService;
            _stockQuantityHistoryApiService = stockQuantityHistoryApiService;
            _orderService = orderService;
        }

        #region Custom Method
        /// <summary>
        /// [TuanAnh][03/09/2018][Hàm lấy thông tin tồn kho hiện tại.]
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("/api/get_inventory_stock")]
        [ProducesResponseType(typeof(ProductWarehouseInventoryDtoRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [GetRequestsErrorInterceptorActionFilter]
        public virtual IActionResult GetInventoryStock(GetInventoryStockParametersModel parameters)
        {
            if (!ModelState.IsValid)
            {
                return Error();
            }

           var productWarehouseInventorys = _productWarehouseInventoryApiService.GetInventoryStock(parameters.KeySearchProduct, parameters.ProductTypeId, parameters.VendorId, parameters.WarehouseId, parameters.PageIndex, parameters.PageSize);

            IList<ProductWarehouseInventoryDto> productWarehouseInventoryDtos = productWarehouseInventorys.Select(s =>
               ProductWarehouseInventoryDto.CopyData(s)
            ).ToList();

            var productWarehouseInventoryRootObject = new ProductWarehouseInventoryDtoRootObject()
            {
                ProductWarehouseInventorys = productWarehouseInventoryDtos,
                TotalRow = productWarehouseInventorys.TotalCount
            };

            var json = _jsonFieldsSerializer.Serialize(productWarehouseInventoryRootObject, string.Empty);

            return new RawJsonActionResult(json);
        }

        /// <summary>
        /// [TuanAnh][03/09/2018][Hàm lấy thông tin tồn kho hiện tại theo sản phẩm]
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("/api/get_inventory_stock_by_product")]
        [ProducesResponseType(typeof(ProductWarehouseInventoryDto), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [GetRequestsErrorInterceptorActionFilter]
        public virtual IActionResult GetInventoryStockByProduct(InventoryStockByProductParametersModel parameter)
        {
            if (!ModelState.IsValid)
            {
                return Error();
            }
            var productWarehouseInventory = _productWarehouseInventoryApiService.GetInventoryStockByProduct(parameter.ProductId, parameter.WarehouseId);
            var productWarehouseInventoryDto = ProductWarehouseInventoryDto.CopyData(productWarehouseInventory);
            var json = JsonConvert.SerializeObject(productWarehouseInventoryDto);

            return new RawJsonActionResult(json);
        }

        /// <summary>
        /// [TuanAnh][03/09/2018][Hàm lấy thông tin tồn kho theo time line]
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("/api/get_inventory_stock_time_line")]
        [ProducesResponseType(typeof(ProductWarehouseInventoryDtoRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [GetRequestsErrorInterceptorActionFilter]
        public virtual IActionResult GetInventoryStockTimeLine(StockQuantityHistoryParametersModel parameters)
        {
            if (!ModelState.IsValid)
            {
                return Error();
            }

            var stockQuantityHistorys = _stockQuantityHistoryApiService.GetInventoryStockByTimeLine(parameters.DateFrom, parameters.DateTo, parameters.KeySearchProduct, parameters.ProductTypeId, parameters.WarehouseId, parameters.VendorId, parameters.PageIndex, parameters.PageSize);

         

            var productWarehouseInventoryRootObject = new StockQuantityHistoryDtoRootObject()
            {
                StockQuantityHistorys = stockQuantityHistorys,
                TotalRow = stockQuantityHistorys.TotalCount
            };

            var json = _jsonFieldsSerializer.Serialize(productWarehouseInventoryRootObject, string.Empty);

            return new RawJsonActionResult(json);
        }


        /// <summary>
        /// [TuanAnh][03/09/2018][Hàm lấy lịch sử tồn kho của sản phẩm.]
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("/api/get_inventory_stock_time_line_by_product")]
        [ProducesResponseType(typeof(StockQuantityHistoryParametersByProductModel), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [GetRequestsErrorInterceptorActionFilter]
        public virtual IActionResult GetInventoryStockTimeLineByProduct(StockQuantityHistoryParametersByProductModel parameters)
        {
            if (!ModelState.IsValid)
            {
                return Error();
            }
            var stockQuantityHistorys = _stockQuantityHistoryApiService.GetStockQuantityHistoryByProduct(parameters.ProductId, parameters.WarehouseId, parameters.DateFrom, parameters.DateTo, parameters.PageIndex, parameters.PageSize);

            IList<StockQuantityHistoryDtoModel> StockQuantityHistoryModelDtos = stockQuantityHistorys.Select(s =>
               StockQuantityHistoryDtoModel.CopyData(s)
            ).ToList();

            var productWarehouseInventoryModelRootObject = new StockQuantityHistoryDtoModelRootObject()
            {
                StockQuantityHistorys = StockQuantityHistoryModelDtos,
                TotalRow = stockQuantityHistorys.TotalCount
            };

            var json = _jsonFieldsSerializer.Serialize(productWarehouseInventoryModelRootObject, string.Empty);

            return new RawJsonActionResult(json);
        }

        /// <summary>
        /// [TuanAnh][08/09/2018][Hàm lấy thông tin tồn kho dưới mức tồn min]
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("/api/get_inventory_stock_under_min_stock/{warehouseid}")]
        [ProducesResponseType(typeof(ProductWarehouseInventoryDtoRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [GetRequestsErrorInterceptorActionFilter]
        public virtual IActionResult GetInventoryStockUnderMinStock(int? WareHouseid)
        {
            if (!ModelState.IsValid)
            {
                return Error();
            }

            var productWarehouseInventorys = _productWarehouseInventoryApiService.GetInventoryStockUnderMinStock(WareHouseid,1, int.MaxValue -1);

            IList<ProductWarehouseInventoryDto> productWarehouseInventoryDtos = productWarehouseInventorys.Select(s =>
               ProductWarehouseInventoryDto.CopyData(s)
            ).ToList();

            var productWarehouseInventoryRootObject = new ProductWarehouseInventoryDtoRootObject()
            {
                ProductWarehouseInventorys = productWarehouseInventoryDtos,
                TotalRow = productWarehouseInventorys.TotalCount
            };

            var json = _jsonFieldsSerializer.Serialize(productWarehouseInventoryRootObject, string.Empty);

            return new RawJsonActionResult(json);
        }


        /// <summary>
        /// [TuanAnh][20/09/2018][Hàm Kiểm kê tồn kho]
        /// </summary>
        /// <param name="liquidationDelta"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("/api/liquidation_stock")]
        [ProducesResponseType(typeof(ErrorsRootObject), 422)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        public  IActionResult LiquidationStock([ModelBinder(typeof(JsonModelBinder<LiquidationStockDto>))] Delta<LiquidationStockDto> liquidationDelta)
        {
            if (!ModelState.IsValid)
            {
                return Error();
            }
            if (liquidationDelta.Dto.WarehouseId == null)
            {
                return Error(HttpStatusCode.NotFound, "warehouseid", "not found");
            }

            if (liquidationDelta.Dto.LiquidationStockItems != null )
            {
                foreach (var item in liquidationDelta.Dto.LiquidationStockItems)
                {
                    var objStockQuantityHistory = new StockQuantityHistory();
                    objStockQuantityHistory.WarehouseId = liquidationDelta.Dto.WarehouseId;
                    objStockQuantityHistory.ProductId = item.ProductId;
                    objStockQuantityHistory.StockQuantity = item.CurrentStockQuantity;
                    objStockQuantityHistory.QuantityAdjustment = item.CurrentStockQuantity - item.StockQuantity ;
                    objStockQuantityHistory.Message = item.Description;
                    objStockQuantityHistory.CreatedOnUtc = liquidationDelta.Dto.LiquidationDate;
                    _stockQuantityHistoryApiService.CreateStockQuantityHistory(objStockQuantityHistory);

                    var objProductWarehouseInventory = new ProductWarehouseInventory();
                    objProductWarehouseInventory.WarehouseId = liquidationDelta.Dto.WarehouseId.Value;
                    objProductWarehouseInventory.ProductId = item.ProductId;
                    objProductWarehouseInventory.StockQuantity = item.CurrentStockQuantity;
                    _productWarehouseInventoryApiService.UpdateProductWarehouseInventory(objProductWarehouseInventory);
                }
            }
            else
            {
                if (liquidationDelta.Dto.WarehouseId == null)
                {
                    return Error(HttpStatusCode.NotFound, "liquidation_stock_items", "not found");
                }
            }
            return new RawJsonActionResult("{}");

        }

        #endregion
    }
}