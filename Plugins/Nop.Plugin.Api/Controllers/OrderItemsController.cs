﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Orders;
using Nop.Plugin.Api.Attributes;
using Nop.Plugin.Api.Constants;
using Nop.Plugin.Api.Delta;
using Nop.Plugin.Api.DTOs.OrderItems;
using Nop.Plugin.Api.Helpers;
using Nop.Plugin.Api.JSON.ActionResults;
using Nop.Plugin.Api.MappingExtensions;
using Nop.Plugin.Api.ModelBinders;
using Nop.Plugin.Api.Models.OrderItemsParameters;
using Nop.Plugin.Api.Services;
using Nop.Services.Catalog;
using Nop.Services.Customers;
using Nop.Services.Discounts;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Media;
using Nop.Services.Orders;
using Nop.Services.Security;
using Nop.Services.Stores;
using Nop.Services.Tax;

namespace Nop.Plugin.Api.Controllers
{
    using Microsoft.AspNetCore.Authentication.JwtBearer;
    using Microsoft.AspNetCore.Mvc;
    using Nop.Core;
    using Nop.Plugin.Api.DTOs.Errors;
    using Nop.Plugin.Api.DTOs.Orders;
    using Nop.Plugin.Api.JSON.Serializers;

    [ApiAuthorize(Policy = JwtBearerDefaults.AuthenticationScheme, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class OrderItemsController : BaseApiController
    {
        private readonly IOrderItemApiService _orderItemApiService;
        private readonly IOrderApiService _orderApiService;
        private readonly IOrderService _orderService;
        private readonly IProductApiService _productApiService;
        private readonly IPriceCalculationService _priceCalculationService;
        private readonly ITaxService _taxService;
        private readonly IDTOHelper _dtoHelper;
        private readonly IProductAttributeConverter _productAttributeConverter;

        public OrderItemsController(IJsonFieldsSerializer jsonFieldsSerializer,
            IAclService aclService,
            ICustomerService customerService,
            IStoreMappingService storeMappingService,
            IStoreService storeService,
            IDiscountService discountService,
            ICustomerActivityService customerActivityService,
            ILocalizationService localizationService,
            IOrderItemApiService orderItemApiService,
            IOrderApiService orderApiService,
            IOrderService orderService,
            IProductApiService productApiService,
            IPriceCalculationService priceCalculationService,
            ITaxService taxService,
            IPictureService pictureService, IDTOHelper dtoHelper)
        {
            _orderItemApiService = orderItemApiService;
            _orderApiService = orderApiService;
            _orderService = orderService;
            _productApiService = productApiService;
            _priceCalculationService = priceCalculationService;
            _taxService = taxService;
            _dtoHelper = dtoHelper;
        }

        #region Custom Method

        /// <summary>
        /// [HienNguyen][29/07/2018][Hàm lấy dữ liệu chi tiết đơn hàng]
        /// </summary>
        /// <param name="orderId"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("/api/get_order_items/{id}")]
        [ProducesResponseType(typeof(OrderItemsRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult GetOrderItems(int id)
        {
            //if (parameters.PageSize < Configurations.MinLimit || parameters.PageSize > Configurations.MaxLimit)
            //{
            //    return Error(HttpStatusCode.BadRequest, "PageSize", "Invalid limit parameter");
            //}

            //if (parameters.PageIndex < Configurations.DefaultPageValue)
            //{
            //    return Error(HttpStatusCode.BadRequest, "PageIndex", "Invalid request parameters");
            //}

            //Order order = _orderApiService.GetOrderById(id);

            //if (order == null)
            //{
            //    return Error(HttpStatusCode.NotFound, "order", "not found");
            //}

            IPagedList<OrderItem> allOrderItemsForOrder = _orderService.GetOrderItemByOrderId(id, 1, int.MaxValue - 1);

            List<OrderItemDtoModel> listOrderItemModel = allOrderItemsForOrder.Select(item => OrderItemDtoModel.CopyData(item)).ToList();

            var orderItemsRootObject = new OrderItemsRootObject()
            {
                OrderItems = listOrderItemModel,
                SubTotalAmount = listOrderItemModel.Sum(m => m.SalePrice * m.Quantity),
                TotalAmount = listOrderItemModel.Sum(m => (m.SalePrice - m.Discount) * m.Quantity),
                TotalAmountPaid = 0,
                TotalDiscount = listOrderItemModel.Sum(m => m.Discount),
                TotalRow = allOrderItemsForOrder.TotalCount
            };

            var json = _jsonFieldsSerializer.Serialize(orderItemsRootObject, string.Empty);

            return new RawJsonActionResult(json);
        }

        /// <summary>
        /// [HienNguyen][31/07/2018][Hàm lấy lịch sử thanh toán đơn hàng]
        /// </summary>
        /// <param name="orderId"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("/api/get_payment_balance_history/{orderId}")]
        [ProducesResponseType(typeof(OrderPaymentBalanceRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult GetPaymentBalanceHistory(int orderId)
        {
            var orderItemsRootObject = new OrderPaymentBalanceRootObject()
            {
                OrderPaymentBalance = _orderService.GetOrderPaymentBalance(orderId).Select(m => OrderPaymentDto.CopyData(m)).ToList(),
            };

            var json = _jsonFieldsSerializer.Serialize(orderItemsRootObject, string.Empty);

            return new RawJsonActionResult(json);
        }
        #endregion

        //[HttpGet]
        //[Route("/api/getorderitems/{orderId}/items")]
        //[ProducesResponseType(typeof(OrderItemsRootObject), (int)HttpStatusCode.OK)]
        //[ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        //[ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        //[ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        //[GetRequestsErrorInterceptorActionFilter]
        //public IActionResult GetOrderItems(int orderId, OrderItemsParametersModel parameters)
        //{
        //    if (parameters.PageSize < Configurations.MinLimit || parameters.PageSize > Configurations.MaxLimit)
        //    {
        //        return Error(HttpStatusCode.BadRequest, "PageSize", "Invalid limit parameter");
        //    }

        //    if (parameters.PageIndex < Configurations.DefaultPageValue)
        //    {
        //        return Error(HttpStatusCode.BadRequest, "PageIndex", "Invalid request parameters");
        //    }

        //    Order order = _orderApiService.GetOrderById(orderId);

        //    if (order == null)
        //    {
        //        return Error(HttpStatusCode.NotFound, "order", "not found");
        //    }

        //    IPagedList<OrderItem> allOrderItemsForOrder = _orderService.GetOrderItemByOrderId(orderId, parameters.PageSize, parameters.PageIndex);

        //    var orderItemsRootObject = new OrderItemsRootObject()
        //    {
        //        OrderItems = allOrderItemsForOrder.Select(item => _dtoHelper.PrepareOrderItemDTO(item)).ToList(),
        //        TotalRow = allOrderItemsForOrder.TotalCount
        //    };

        //    var json = _jsonFieldsSerializer.Serialize(orderItemsRootObject, parameters.Fields);

        //    return new RawJsonActionResult(json);
        //}

        [HttpGet]
        [Route("/api/orders/{orderId}/items/count")]
        [ProducesResponseType(typeof(OrderItemsCountRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult GetOrderItemsCount(int orderId)
        {
            Order order = _orderApiService.GetOrderById(orderId);

            if (order == null)
            {
                return Error(HttpStatusCode.NotFound, "order", "not found");
            }

            int orderItemsCountForOrder = _orderItemApiService.GetOrderItemsCount(order);

            var orderItemsCountRootObject = new OrderItemsCountRootObject()
            {
                Count = orderItemsCountForOrder
            };

            return Ok(orderItemsCountRootObject);
        }

        [HttpGet]
        [Route("/api/orders/{orderId}/items/{orderItemId}")]
        [ProducesResponseType(typeof(OrderItemsRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult GetOrderItemByIdForOrder(int orderId, int orderItemId, string fields = "")
        {
            Order order = _orderApiService.GetOrderById(orderId);

            if (order == null)
            {
                return Error(HttpStatusCode.NotFound, "order", "not found");
            }

            OrderItem orderItem = _orderService.GetOrderItemById(orderItemId);

            if (orderItem == null)
            {
                return Error(HttpStatusCode.NotFound, "order_item", "not found");
            }

            var orderItemDtos = new List<OrderItemDto>();
            orderItemDtos.Add(_dtoHelper.PrepareOrderItemDTO(orderItem));

            var orderItemsRootObject = new OrderItemsRootObject()
            {
                //OrderItems = orderItemDtos
            };

            var json = _jsonFieldsSerializer.Serialize(orderItemsRootObject, fields);

            return new RawJsonActionResult(json);
        }

        [HttpPost]
        [Route("/api/orders/{orderId}/items")]
        [ProducesResponseType(typeof(OrderItemsRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), 422)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        public IActionResult CreateOrderItem(int orderId,
            [ModelBinder(typeof(JsonModelBinder<OrderItemDto>))] Delta<OrderItemDto> orderItemDelta)
        {
            // Here we display the errors if the validation has failed at some point.
            if (!ModelState.IsValid)
            {
                return Error();
            }

            Order order = _orderApiService.GetOrderById(orderId);

            if (order == null)
            {
                return Error(HttpStatusCode.NotFound, "order", "not found");
            }

            Product product = GetProduct(orderItemDelta.Dto.ProductId);

            if (product == null)
            {
                return Error(HttpStatusCode.NotFound, "product", "not found");
            }

            if (product.IsRental)
            {
                if (orderItemDelta.Dto.RentalStartDateUtc == null)
                {
                    return Error(HttpStatusCode.BadRequest, "rental_start_date_utc", "required");
                }

                if (orderItemDelta.Dto.RentalEndDateUtc == null)
                {
                    return Error(HttpStatusCode.BadRequest, "rental_end_date_utc", "required");
                }

                if (orderItemDelta.Dto.RentalStartDateUtc > orderItemDelta.Dto.RentalEndDateUtc)
                {
                    return Error(HttpStatusCode.BadRequest, "rental_start_date_utc", "should be before rental_end_date_utc");
                }

                if (orderItemDelta.Dto.RentalStartDateUtc < DateTime.UtcNow)
                {
                    return Error(HttpStatusCode.BadRequest, "rental_start_date_utc", "should be a future date");
                }
            }

            OrderItem newOrderItem = PrepareDefaultOrderItemFromProduct(order, product);
            orderItemDelta.Merge(newOrderItem);

            order.OrderItems.Add(newOrderItem);

            _orderService.UpdateOrder(order);

            _customerActivityService.InsertActivity("AddNewOrderItem",
               _localizationService.GetResource("ActivityLog.AddNewOrderItem"), newOrderItem.Id);

            var orderItemsRootObject = new OrderItemsRootObject();

            //orderItemsRootObject.OrderItems.Add(newOrderItem.ToDto());

            var json = _jsonFieldsSerializer.Serialize(orderItemsRootObject, string.Empty);

            return new RawJsonActionResult(json);
        }

        [HttpPut]
        [Route("/api/orders/{orderId}/items/{orderItemId}")]
        [ProducesResponseType(typeof(OrderItemsRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), 422)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        public IActionResult UpdateOrderItem(int orderId, int orderItemId,
          [ModelBinder(typeof(JsonModelBinder<OrderItemDto>))] Delta<OrderItemDto> orderItemDelta)
        {
            // Here we display the errors if the validation has failed at some point.
            if (!ModelState.IsValid)
            {
                return Error();
            }

            OrderItem orderItemToUpdate = _orderService.GetOrderItemById(orderItemId);

            if (orderItemToUpdate == null)
            {
                return Error(HttpStatusCode.NotFound, "order_item", "not found");
            }

            Order order = _orderApiService.GetOrderById(orderId);

            if (order == null)
            {
                return Error(HttpStatusCode.NotFound, "order", "not found");
            }

            // This is needed because those fields shouldn't be updatable. That is why we save them and after the merge set them back.
            int? productId = orderItemToUpdate.ProductId;
            DateTime? rentalStartDate = orderItemToUpdate.RentalStartDateUtc;
            DateTime? rentalEndDate = orderItemToUpdate.RentalEndDateUtc;

            orderItemDelta.Merge(orderItemToUpdate);

            orderItemToUpdate.ProductId = productId ?? 0;
            orderItemToUpdate.RentalStartDateUtc = rentalStartDate;
            orderItemToUpdate.RentalEndDateUtc = rentalEndDate;

            _orderService.UpdateOrder(order);

            _customerActivityService.InsertActivity("UpdateOrderItem",
               _localizationService.GetResource("ActivityLog.UpdateOrderItem"), orderItemToUpdate.Id);

            var orderItemsRootObject = new OrderItemsRootObject();

            //orderItemsRootObject.OrderItems.Add(orderItemToUpdate.ToDto());

            var json = _jsonFieldsSerializer.Serialize(orderItemsRootObject, string.Empty);

            return new RawJsonActionResult(json);
        }

        [HttpDelete]
        [Route("/api/orders/{orderId}/items/{orderItemId}")]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult DeleteOrderItemById(int orderId, int orderItemId)
        {
            Order order = _orderApiService.GetOrderById(orderId);

            if (order == null)
            {
                return Error(HttpStatusCode.NotFound, "order", "not found");
            }

            OrderItem orderItem = _orderService.GetOrderItemById(orderItemId);
            _orderService.DeleteOrderItem(orderItem);

            return new RawJsonActionResult("{}");
        }

        [HttpDelete]
        [Route("/api/orders/{orderId}/items")]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult DeleteAllOrderItemsForOrder(int orderId)
        {
            Order order = _orderApiService.GetOrderById(orderId);

            if (order == null)
            {
                return Error(HttpStatusCode.NotFound, "order", "not found");
            }

            var orderItemsList = order.OrderItems.ToList();

            for (int i = 0; i < orderItemsList.Count; i++)
            {
                _orderService.DeleteOrderItem(orderItemsList[i]);
            }

            return new RawJsonActionResult("{}");
        }

        private Product GetProduct(int? productId)
        {
            Product product = null;

            if (productId.HasValue)
            {
                int id = productId.Value;

                product = _productApiService.GetProductById(id);
            }
            return product;
        }

        private OrderItem PrepareDefaultOrderItemFromProduct(Order order, Product product)
        {
            var presetQty = 1;
            var presetPrice = _priceCalculationService.GetFinalPrice(product, order.Customer, decimal.Zero, true, presetQty);

            decimal taxRate;
            decimal presetPriceInclTax = _taxService.GetProductPrice(product, presetPrice, true, order.Customer, out taxRate);
            decimal presetPriceExclTax = _taxService.GetProductPrice(product, presetPrice, false, order.Customer, out taxRate);

            OrderItem orderItem = new OrderItem()
            {
                OrderItemGuid = new Guid(),
                UnitPriceExclTax = presetPriceExclTax,
                UnitPriceInclTax = presetPriceInclTax,
                PriceInclTax = presetPriceInclTax,
                PriceExclTax = presetPriceExclTax,
                OriginalProductCost = _priceCalculationService.GetProductCost(product, null),
                Quantity = presetQty,
                Product = product,
                Order = order
            };

            return orderItem;
        }
    }
}