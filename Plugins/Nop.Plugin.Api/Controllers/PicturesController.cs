﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Discounts;
using Nop.Core.Domain.Media;
using Nop.Plugin.Api.Attributes;
using Nop.Plugin.Api.Constants;
using Nop.Plugin.Api.Delta;
using Nop.Plugin.Api.DTOs.Images;
using Nop.Plugin.Api.DTOs.Products;
using Nop.Plugin.Api.Factories;
using Nop.Plugin.Api.JSON.ActionResults;
using Nop.Plugin.Api.ModelBinders;
using Nop.Plugin.Api.Models.ProductsParameters;
using Nop.Plugin.Api.Services;
using Nop.Services.Catalog;
using Nop.Services.Customers;
using Nop.Services.Discounts;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Media;
using Nop.Services.Security;
using Nop.Services.Seo;
using Nop.Services.Stores;
using Nop.Plugin.Api.Helpers;

namespace Nop.Plugin.Api.Controllers
{
    using Microsoft.AspNetCore.Authentication.JwtBearer;
    using Microsoft.AspNetCore.Mvc;
    using Nop.Core;
    using Nop.Plugin.Api.DTOs.Errors;
    using Nop.Plugin.Api.DTOs.Pictures;
    using Nop.Plugin.Api.JSON.Serializers;
    using System.Text.RegularExpressions;

    /// <summary>
    /// [HienNguyen][21/08/2018][Xử lý hình ảnh]
    /// </summary>
    [ApiAuthorize(Policy = JwtBearerDefaults.AuthenticationScheme, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class PicturesController : BaseApiController
    {
        private readonly IProductApiService _productApiService;
        private readonly IProductService _productService;
        private readonly IUrlRecordService _urlRecordService;
        private readonly IPictureService _pictureService;
        private readonly IManufacturerService _manufacturerService;
        private readonly IFactory<ProductPicture> _factory;
        private readonly IProductTagService _productTagService;
        private readonly IProductAttributeService _productAttributeService;
        private readonly IDTOHelper _dtoHelper;

        public PicturesController(IProductApiService productApiService,
                                  IJsonFieldsSerializer jsonFieldsSerializer,
                                  IProductService productService,
                                  IUrlRecordService urlRecordService,
                                  ICustomerActivityService customerActivityService,
                                  ILocalizationService localizationService,
                                  IFactory<ProductPicture> factory,
                                  IAclService aclService,
                                  IStoreMappingService storeMappingService,
                                  IStoreService storeService,
                                  ICustomerService customerService,
                                  IDiscountService discountService,
                                  IPictureService pictureService,
                                  IManufacturerService manufacturerService,
                                  IProductTagService productTagService,
                                  IProductAttributeService productAttributeService,
                                  IDTOHelper dtoHelper)
        {
            _productApiService = productApiService;
            _factory = factory;
            _manufacturerService = manufacturerService;
            _productTagService = productTagService;
            _urlRecordService = urlRecordService;
            _productService = productService;
            _productAttributeService = productAttributeService;
            _dtoHelper = dtoHelper;
            _pictureService = pictureService;
        }

        #region Custom Method

        /// <summary>
        /// [HienNguyen][07/08/2018][Get hình ảnh by id]
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("/api/get_picture_by_id/{id}")]
        [ProducesResponseType(typeof(PicturesRootObjectDtoModel), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult GetPictureById(int id)
        {
            var picturesAsDtos = _pictureService.GetPictureById(id);

            var picturesRootObject = new PicturesRootObjectDtoModel()
            {
                Pictures = new List<PictureDtoModel>() { _dtoHelper.PreparePictureDTOModel(picturesAsDtos) },
            };

            var json = _jsonFieldsSerializer.Serialize(picturesRootObject, string.Empty);

            return new RawJsonActionResult(json);
        }

        /// <summary>
        /// [HienNguyen][07/08/2018][Get hình ảnh sản phẩm theo mã sản phẩm]
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("/api/get_picture_by_product_id/{id}")]
        [ProducesResponseType(typeof(PicturesRootObjectDtoModel), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult GetPictureByProductId(int id)
        {
            var picturesAsDtos = _pictureService.GetPicturesByProductId(id);

            var picturesRootObject = new PicturesRootObjectDtoModel()
            {
                Pictures = picturesAsDtos.Select(m => _dtoHelper.PreparePictureDTOModel(m)).ToList(),
                TotalRow = picturesAsDtos.Count
            };

            var json = _jsonFieldsSerializer.Serialize(picturesRootObject, string.Empty);

            return new RawJsonActionResult(json);
        }


        //[HttpPost]
        //[Route("/api/pictures")]
        //public Picture InsertPicture(string data, string seName)
        //{
        //    if (!string.IsNullOrEmpty(data))
        //    {
        //        var regex = Regex.Match(data, @"data:image/(?<type>.+?),(?<data>.+)");
        //        var base64Data = regex.Groups["data"].Value;
        //        var binData = Convert.FromBase64String(base64Data);
        //        var picture = _pictureService.InsertPicture(binData, "image/png", _pictureService.GetPictureSeName(seName), isNew: true);
        //        return picture;
        //    };
        //    return null;
        //}

        /// <summary>
        /// [HienNguyen][22/08/2018][Thêm mới hình ảnh]
        /// </summary>
        /// <param name="productDelta"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("/api/create_picture")]
        [ProducesResponseType(typeof(PicturesRootObjectDtoModel), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), 422)]
        public IActionResult CreatePicture([ModelBinder(typeof(JsonModelBinder<PictureDtoModel>))] Delta<PictureDtoModel> pictureDelta)
        {
            // Here we display the errors if the validation has failed at some point.
            if (!ModelState.IsValid)
            {
                return Error();
            }

            Picture picture = _pictureService.InsertPicture(pictureDelta.Dto.PictureBinary, pictureDelta.Dto.MimeType, pictureDelta.Dto.SeoFilename);

            _customerActivityService.InsertActivity("AddNewPicture",
                _localizationService.GetResource("ActivityLog.AddNewPicture"), picture.Id);

            // Preparing the result dto of the new product
            PictureDtoModel productDto = _dtoHelper.PreparePictureDTOModel(picture);

            var picturesRootObject = new PicturesRootObjectDtoModel();

            picturesRootObject.Pictures.Add(productDto);

            var json = _jsonFieldsSerializer.Serialize(picturesRootObject, string.Empty);

            return new RawJsonActionResult(json);
        }

        /// <summary>
        /// [HienNguyen][22/08/2018][Thêm mới hình ảnh cho sản phẩm]
        /// </summary>
        /// <param name="productDelta"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("/api/create_product_picture")]
        [ProducesResponseType(typeof(ProductPictureObjectDtoModel), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), 422)]
        public IActionResult CreateProductPicture([ModelBinder(typeof(JsonModelBinder<ProductPictureDtoModel>))] Delta<ProductPictureDtoModel> productPictureDelta)
        {
            // Here we display the errors if the validation has failed at some point.
            if (!ModelState.IsValid)
            {
                return Error();
            }

            // Inserting the new product
            ProductPicture productPicture = _factory.Initialize();
            productPictureDelta.Merge(productPicture);

            _productService.InsertProductPicture(productPicture);

            ProductPictureDtoModel productDto = _dtoHelper.PrepareProductPictureDTOModel(productPicture);

            var picturesRootObject = new ProductPictureObjectDtoModel();

            picturesRootObject.ProductPicture.Add(productDto);

            var json = _jsonFieldsSerializer.Serialize(picturesRootObject, string.Empty);

            return new RawJsonActionResult(json);
        }

        /// <summary>
        /// [HienNguyen][22/08/2018][Thêm mới hình ảnh cho sản phẩm]
        /// </summary>
        /// <param name="productDelta"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("/api/delete_product_picture/{id}")]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), 422)]
        public IActionResult DeleteProductPicture(int id)
        {
            // Here we display the errors if the validation has failed at some point.
            if (!ModelState.IsValid)
            {
                return Error();
            }

            // Inserting the new product
            ProductPicture productPicture = _productService.GetProductPictureById(id);

            _productService.DeleteProductPicture(productPicture);

            //activity log
            _customerActivityService.InsertActivity("DeleteProductPicture", _localizationService.GetResource("ActivityLog.DeleteProductPicture"), productPicture.Id);

            return new RawJsonActionResult("{}");
        }

        /// <summary>
        /// [HienNguyen][07/08/2018][Get hình ảnh sản phẩm theo mã sản phẩm]
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("/api/delete_picture/{id}")]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult DeletePicture(int id)
        {
            if (id <= 0)
            {
                return Error(HttpStatusCode.BadRequest, "productId", "invalid id");
            }

            Picture picture = _pictureService.GetPictureById(id);
            _pictureService.DeletePicture(picture);

            //activity log
            _customerActivityService.InsertActivity("DeletePicture", _localizationService.GetResource("ActivityLog.DeletePicture"), picture.Id);

            return new RawJsonActionResult("{}");
        }


        #endregion

    }
}