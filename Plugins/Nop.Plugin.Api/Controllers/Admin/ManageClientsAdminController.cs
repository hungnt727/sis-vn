﻿namespace Nop.Plugin.Api.Controllers.Admin
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Nop.Plugin.Api.Constants;
    using Nop.Services.Localization;
    using Nop.Web.Framework;
    using Nop.Web.Framework.Controllers;
    using Nop.Web.Framework.Kendoui;
    using Nop.Web.Framework.Mvc.Filters;
    using Nop.Plugin.Api.Models;
    using Nop.Plugin.Api.Services;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Nop.Services.Customers;
    using Nop.Core.Caching;
    using IdentityServer4.EntityFramework.Entities;
    using Nop.Core.Domain.Customers;
    using Nop.Plugin.Api.Domain;

    [AuthorizeAdmin]
    [Area(AreaNames.Admin)]
    [Route("admin/manageClientsAdmin/")]
    public class ManageClientsAdminController : BasePluginController
    {
        private readonly ICustomerService _customerService;
        private readonly IClientService _clientService;
        private readonly ILocalizationService _localizationService;
        private readonly CustomerSettings _customerSettings;

        public ManageClientsAdminController(CustomerSettings customerSettings, ILocalizationService localizationService, IClientService clientService, ICustomerService customerService)
        {
            _customerSettings = customerSettings;
            _customerService = customerService;
            _localizationService = localizationService;
            _clientService = clientService;
        }

        [HttpGet]
        [Route("list")]
        public ActionResult List()
        {
            return View(ViewNames.AdminApiClientsList);
        }

        [HttpPost]
        [Route("list")]
        public ActionResult List(DataSourceRequest command)
        {
            IList<ClientApiModel> gridModel = _clientService.GetAllClients();

            var grids = new DataSourceResult()
            {
                Data = gridModel,
                Total = gridModel.Count()
            };

            return Json(grids);
        }

        [HttpGet]
        [Route("create")]
        public ActionResult Create()
        {
            var clientModel = new ClientApiModel
            {
                Enabled = true
            };
            PrepareAccessRoles(clientModel);
            PrepareCustomerMappingModel(clientModel);
            return View(ViewNames.AdminApiClientsCreate, clientModel);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        [Route("create")]
        public ActionResult Create(ClientApiModel model, bool continueEditing)
        {
            if (ModelState.IsValid)
            {
                int clientId = _clientService.InsertClient(model);
                //customer
                SaveCustomerMappings(clientId, model);

                SuccessNotification(_localizationService.GetResource("Plugins.Api.Admin.Client.Created"));
                return continueEditing ? RedirectToAction("Edit", new { id = clientId }) : RedirectToAction("List");
            }
            return RedirectToAction("List");
        }

        [HttpGet]
        [Route("edit/{id}")]
        public IActionResult Edit(int id)
        {
            ClientApiModel clientModel = _clientService.FindClientByIdAsync(id);
            PrepareAccessRoles(clientModel);
            PrepareCustomerMappingModel(clientModel, id);
            return View(ViewNames.AdminApiClientsEdit, clientModel);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        [Route("edit/{id}")]
        public async Task<IActionResult> Edit(ClientApiModel model, bool continueEditing)
        {
            if (ModelState.IsValid)
            {
                _clientService.UpdateClient(model);

                SaveCustomerMappings(model.Id, model);

                SuccessNotification(_localizationService.GetResource("Plugins.Api.Admin.Client.Edit"));
                return continueEditing ? RedirectToAction("Edit", new { id = model.Id }) : RedirectToAction("List");
            }

            return RedirectToAction("List");
        }

        [HttpPost, ActionName("Delete")]
        [Route("delete/{id}")]
        public IActionResult DeleteConfirmed(int id)
        {
            var clientCustomers = _clientService.GetAllClientCustomers()
                .Where(c => c.ClientId == id).ToList();
            foreach(var cc in clientCustomers)
            {
                _clientService.DeleteClientCustomer(cc.Id);
            }
            _clientService.DeleteClient(id);

            SuccessNotification(_localizationService.GetResource("Plugins.Api.Admin.Client.Deleted"));
            return RedirectToAction("List");
        }


        protected virtual void PrepareAccessRoles(ClientApiModel model)
        {
            if (model == null)
                throw new ArgumentNullException(nameof(model));

            model.AvailableRoles.Add(new SelectListItem
            {
                Text = _localizationService.GetResource("Admin.Catalog.Categories.Fields.Parent.None"),
                Value = ""
            });
            var categories = SelectListHelper.GetCustomerRoles(_customerService);
            foreach (var c in categories)
                model.AvailableRoles.Add(c);
        }
        protected virtual void PrepareCustomerMappingModel(ClientApiModel model, int clientid = 0)
        {
            if (model == null)
                throw new ArgumentNullException(nameof(model));

            if (clientid > 0)
                model.SelectedCustomerIds = _clientService.GetAllClientCustomers()
                    .Where(c => c.ClientId == clientid)
                    .Select(c => c.CustomerId)
                    .ToList();

            var allCustomers = SelectListHelper.GetCustomers(_customerService, _customerSettings);
            foreach (var c in allCustomers)
            {
                c.Selected = model.SelectedCustomerIds.Contains(int.Parse(c.Value));
                model.AvailableCustomers.Add(c);
            }
        }

        protected virtual void SaveCustomerMappings(int clientId, ClientApiModel model)
        {
            var existingClientCustomers = _clientService.GetAllClientCustomers()
                .Where(c => c.ClientId == clientId);

            //delete categories
            foreach (var cc in existingClientCustomers)
                if (!model.SelectedCustomerIds.Contains(cc.CustomerId))
                    _clientService.DeleteClientCustomer(cc.Id);

            //add categories
            foreach (var customerId in model.SelectedCustomerIds)
                if (existingClientCustomers.FirstOrDefault(c => c.CustomerId == customerId && c.ClientId == clientId) == null)
                {
                    _clientService.InsertClientCustomer(new ClientCustomer
                    {
                        ClientId = clientId,
                        CustomerId = customerId
                    });
                }
        }
    }

    public static class SelectListHelper
    {
        public static List<SelectListItem> GetCustomerRoles(ICustomerService customerService)
        {
            if (customerService == null)
                throw new ArgumentNullException(nameof(customerService));

            var listItems = customerService.GetAllCustomerRoles().Select(c => new SelectListItem
            {
                Text = c.Name,
                Value = c.SystemName
            });

            var result = new List<SelectListItem>();
            //clone the list to ensure that "selected" property is not set
            foreach (var item in listItems)
            {
                result.Add(new SelectListItem
                {
                    Text = item.Text,
                    Value = item.Value
                });
            }

            return result;
        }

        public static List<SelectListItem> GetCustomers(ICustomerService customerService, CustomerSettings settings)
        {
            if (customerService == null)
                throw new ArgumentNullException(nameof(customerService));

            var listItems = customerService.GetAllCustomers(customerRoleIds: new[] { customerService.GetCustomerRoleBySystemName(SystemCustomerRoleNames.Registered).Id })
                .Select(c => new SelectListItem
                {
                    Text = settings.UsernamesEnabled ? c.Username : c.Email,
                    Value = c.Id.ToString()
                });

            var result = new List<SelectListItem>();
            //clone the list to ensure that "selected" property is not set
            foreach (var item in listItems)
            {
                result.Add(new SelectListItem
                {
                    Text = item.Text,
                    Value = item.Value
                });
            }

            return result;
        }
    }
}