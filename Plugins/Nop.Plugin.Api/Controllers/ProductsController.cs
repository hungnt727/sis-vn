﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Discounts;
using Nop.Core.Domain.Media;
using Nop.Plugin.Api.Attributes;
using Nop.Plugin.Api.Constants;
using Nop.Plugin.Api.Delta;
using Nop.Plugin.Api.DTOs.Images;
using Nop.Plugin.Api.DTOs.Products;
using Nop.Plugin.Api.Factories;
using Nop.Plugin.Api.JSON.ActionResults;
using Nop.Plugin.Api.ModelBinders;
using Nop.Plugin.Api.Models.ProductsParameters;
using Nop.Plugin.Api.Services;
using Nop.Services.Catalog;
using Nop.Services.Customers;
using Nop.Services.Discounts;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Media;
using Nop.Services.Security;
using Nop.Services.Seo;
using Nop.Services.Stores;
using Nop.Plugin.Api.Helpers;

namespace Nop.Plugin.Api.Controllers
{
    using Microsoft.AspNetCore.Authentication.JwtBearer;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Nop.Core;
    using Nop.Plugin.Api.DTOs.Errors;
    using Nop.Plugin.Api.JSON.Serializers;
    using Nop.Plugin.Api.Models;
    using System.Text.RegularExpressions;

    [ApiAuthorize(Policy = JwtBearerDefaults.AuthenticationScheme, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class ProductsController : BaseApiController
    {
        #region Fields
        private readonly MediaSettings _mediaSettings;
        private readonly IProductApiService _productApiService;
        private readonly ICategoryService _categoryService;
        private readonly IProductService _productService;
        private readonly IUrlRecordService _urlRecordService;
        private readonly IManufacturerService _manufacturerService;
        private readonly IFactory<Product> _factory;
        private readonly IProductTagService _productTagService;
        private readonly IProductAttributeService _productAttributeService;
        private readonly IDTOHelper _dtoHelper;
        private readonly Regex _imageDataRegex = new Regex(@"data:(?<type>.+?);[a-zA-Z0-6]+,(?<data>.+)"); 
        #endregion

        #region Ctor
        public ProductsController(
            MediaSettings mediaSettings,
            ICategoryService categoryService,
            IProductApiService productApiService,
            IProductService productService,
            IUrlRecordService urlRecordService,
            IFactory<Product> factory,
            IManufacturerService manufacturerService,
            IProductTagService productTagService,
            IProductAttributeService productAttributeService,
            IDTOHelper dtoHelper)
        {
            _mediaSettings = mediaSettings;
            _categoryService = categoryService;
            _productApiService = productApiService;
            _factory = factory;
            _manufacturerService = manufacturerService;
            _productTagService = productTagService;
            _urlRecordService = urlRecordService;
            _productService = productService;
            _productAttributeService = productAttributeService;
            _dtoHelper = dtoHelper;
        } 
        #endregion

        #region API
        /// <summary>
        /// Receive a list of all products
        /// </summary>
        /// <response code="200">OK</response>
        /// <response code="400">Bad Request</response>
        /// <response code="401">Unauthorized</response>
        [HttpGet]
        [Route("/api/products")]
        [ProducesResponseType(typeof(ProductsRootObjectDto), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult GetProducts(ProductsParametersModel parameters)
        {
            if (!Authorize(StandardPermissionProvider.ManageProducts))
            {
                return Error(HttpStatusCode.BadRequest, "", "Unauthorized products");
            }
            if (parameters.PageSize < Configurations.MinLimit || parameters.PageSize > Configurations.MaxLimit)
            {
                return Error(HttpStatusCode.BadRequest, "limit", "invalid limit parameter");
            }

            if (parameters.PageNumber < Configurations.DefaultPageValue)
            {
                return Error(HttpStatusCode.BadRequest, "page", "invalid page parameter");
            }
            //get role of client
            List<int> roleIds = null;
            var clientId = _httpContextAccessor.HttpContext.User.FindFirst("client_id")?.Value;
            if (clientId != null)
            {
                var client = _clientService.FindClientByClientId(clientId);
                var role = _customerService.GetCustomerRoleBySystemName(client.AccessRole);
                if (role != null)
                {
                    roleIds = new List<int> { role.Id };
                }
            }
            //category ids
            var categoryIds = parameters.CategoryId.HasValue ? new List<int> { parameters.CategoryId.Value } : null;
            //output argument
            IList<int> output = new List<int>();
            //published
            bool? published = true;
            switch (parameters.ProductStatus)
            {
                case ProductStatusEnum.Published:
                    published = true;
                    break;
                case ProductStatusEnum.UnPublished:
                    published = false;
                    break;
                default:
                    published = null;
                    break;
            }
            var allProducts = _productService.SearchProducts(out output,
                keywords: parameters.Keyword, categoryIds: categoryIds, vendorId: parameters.VendorId ?? 0,
                showHidden: true,
                overridePublished: published,
                pageIndex: parameters.PageNumber - 1, pageSize: parameters.PageSize,
                orderBy: parameters.OrderBy,
                roleIds: roleIds);

            IList<ProductDtoModel> productsAsDtos = allProducts.Select(product =>
            {
                return _dtoHelper.PrepareProductDTOModel(product, true, parameters.ImageSize >= 0 ? parameters.ImageSize : _mediaSettings.ProductThumbPictureSize);

            }).ToList();

            var productsRootObject = new ProductsRootObjectDtoModel()
            {
                Products = productsAsDtos,
                PageIndex = allProducts.PageIndex,
                PageSize = allProducts.PageSize,
                TotalPage = allProducts.TotalPages,
                TotalRow = allProducts.TotalCount
            };

            var json = _jsonFieldsSerializer.Serialize(productsRootObject, parameters.Fields);

            return new RawJsonActionResult(json);
        }

        [HttpGet]
        [Route("/api/products/{id:int}")]
        [ProducesResponseType(typeof(ProductDto), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult GetProductById(int id)
        {
            if (!Authorize(StandardPermissionProvider.ManageProducts))
            {
                return Error(HttpStatusCode.BadRequest, "", "Unauthorized products");
            }
            var product = _productService.GetProductById(id);
            if (product == null || product.Deleted)
            {
                return Error(HttpStatusCode.BadRequest, "id", "Cannot found product");
            }
            if (!_storeMappingService.Authorize(product))
            {
                return Error(HttpStatusCode.Unauthorized, "id", "Unauthorized specified product");
            }
            var model = _dtoHelper.PrepareProductDTO(product);

            //allow over sale
            //when manage stock quantity
            if (product.ManageInventoryMethodId == (int)ManageInventoryMethod.ManageStock)
            {
                if (product.LowStockActivity == LowStockActivity.Nothing)
                {
                    model.AllowOverSale = true;
                }
                else
                {
                    model.AllowOverSale = false;
                }
            }
            else
            {
                model.AllowOverSale = false;
            }

            var json = _jsonFieldsSerializer.Serialize(model, null);

            return new RawJsonActionResult(json);
        }


        /// <summary>
        /// [HienNguyen][09/08/2018][Thêm mới sản phẩm]
        /// </summary>
        /// <param name="productDelta"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("/api/products")]
        [ProducesResponseType(typeof(ProductsRootObjectDto), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), 422)]
        public IActionResult CreateProduct([ModelBinder(typeof(JsonModelBinder<ProductDto>))] Delta<ProductDto> productDelta)
        {
            if (!Authorize(StandardPermissionProvider.ManageProducts))
            {
                return Error(HttpStatusCode.BadRequest, "", "Unauthorized products");
            }
            // Here we display the errors if the validation has failed at some point.
            if (!ModelState.IsValid)
            {
                return Error();
            }

            // Inserting the new product
            Product product = _factory.Initialize();
            productDelta.Merge(product);

            //when tracking stock quantity
            if (product.ManageInventoryMethodId == (int)ManageInventoryMethod.ManageStock)
            {
                if (productDelta.Dto.AllowOverSale)
                {
                    product.LowStockActivity = LowStockActivity.Nothing;
                }
                else
                {
                    product.LowStockActivity = LowStockActivity.DisableBuyButton;
                }
                product.DisplayStockQuantity = true;
                product.DisplayStockAvailability = true;
            }
            else
            {
                product.DisplayStockQuantity = false;
                product.DisplayStockAvailability = false;
            }

            _productService.InsertProduct(product);

            if (productDelta.Dto.CategoryId.HasValue && productDelta.Dto.CategoryId > 0)
            {
                product.ProductCategories.Add(new ProductCategory
                {
                    CategoryId = productDelta.Dto.CategoryId.Value
                });
            }

            UpdateProductPictures(product, productDelta.Dto.Images);

            UpdateProductTags(product, productDelta.Dto.Tags);

            UpdateProductManufacturers(product, productDelta.Dto.ManufacturerIds);

            UpdateAssociatedProducts(product, productDelta.Dto.AssociatedProductIds);

            //search engine name
            var seName = product.ValidateSeName(productDelta.Dto.SeName, product.Name, true);
            _urlRecordService.SaveSlug(product, seName, 0);

            UpdateAclRoles(product, productDelta.Dto.RoleIds);

            UpdateDiscountMappings(product, productDelta.Dto.DiscountIds);

            UpdateStoreMappings(product, productDelta.Dto.StoreIds);

            _productService.UpdateProduct(product);

            _customerActivityService.InsertActivity("AddNewProduct",
                _localizationService.GetResource("ActivityLog.AddNewProduct"), product.Name);

            // Preparing the result dto of the new product
            ProductDto productDto = _dtoHelper.PrepareProductDTO(product);

            var json = _jsonFieldsSerializer.Serialize(productDto, string.Empty);

            return new RawJsonActionResult(json);
        }

        /// <summary>
        /// [HienNguyen][09/08/2018][Cập nhật sản phẩm]
        /// </summary>
        /// <param name="productDelta"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("/api/products/{id:int}")]
        [ProducesResponseType(typeof(ProductDto), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ErrorsRootObject), 422)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        public IActionResult UpdateProduct([ModelBinder(typeof(JsonModelBinder<ProductDto>))] Delta<ProductDto> productDelta)
        {
            if (!Authorize(StandardPermissionProvider.ManageProducts))
            {
                return Error(HttpStatusCode.BadRequest, "", "Unauthorized products");
            }
            // Here we display the errors if the validation has failed at some point.
            if (!ModelState.IsValid)
            {
                return Error();
            }

            // We do not need to validate the product id, because this will happen in the model binder using the dto validator.
            int productId = int.Parse(productDelta.Dto.Id);

            Product product = _productService.GetProductById(productId);

            if (product == null || product.Deleted)
            {
                return Error(HttpStatusCode.NotFound, "product", "not found");
            }
            if (!_storeMappingService.Authorize(product))
            {
                return Error(HttpStatusCode.Unauthorized, "id", "Unauthorized specified product");
            }

            product.Name = productDelta.Dto.Name;
            product.ProductCost = productDelta.Dto.ProductCost ?? 0;
            product.Price = productDelta.Dto.Price ?? 0;
            product.VendorId = productDelta.Dto.VendorId ?? 0;
            product.Sku = productDelta.Dto.Sku;
            product.TaxCategoryId = productDelta.Dto.TaxCategoryId ?? 0;
            product.Published = productDelta.Dto.Published ?? false;
            product.PublishedPOS = productDelta.Dto.PublishedPOS ?? false;
            product.FullDescription = productDelta.Dto.FullDescription;
            product.MetaKeywords = productDelta.Dto.MetaKeywords;
            product.MetaTitle = productDelta.Dto.MetaTitle;
            product.MetaDescription = productDelta.Dto.MetaDescription;
            product.ProductTemplateId = productDelta.Dto.ProductTemplateId;
            product.FullDescription = productDelta.Dto.FullDescription;
            product.StockQuantity = productDelta.Dto.StockQuantity ?? 0;
            product.ManageInventoryMethodId = productDelta.Dto.ManageInventoryMethodId ?? 0;
            product.ProductTypeId = productDelta.Dto.ProductTypeId;
            product.MinStockQuantity = productDelta.Dto.MinStockQuantity ?? 0;
            product.NotifyAdminForQuantityBelow = productDelta.Dto.NotifyAdminForQuantityBelow ?? 0;
            product.IsSpecial = productDelta.Dto.IsSpecial;
            product.IsBestSale = productDelta.Dto.IsBestSale;

            //when tracking stock quantity
            if (product.ManageInventoryMethodId == (int)ManageInventoryMethod.ManageStock)
            {
                if (productDelta.Dto.AllowOverSale)
                {
                    product.LowStockActivity = LowStockActivity.Nothing;
                }
                else
                {
                    product.LowStockActivity = LowStockActivity.DisableBuyButton;
                }
                product.DisplayStockQuantity = true;
                product.DisplayStockAvailability = true;
            }
            else
            {
                product.DisplayStockQuantity = false;
                product.DisplayStockAvailability = false;
            }

            product.UpdatedOnUtc = DateTime.UtcNow;

            //clear category
            for (var i = 0; i < product.ProductCategories.Count; i++)
            {
                _categoryService.DeleteProductCategory(product.ProductCategories.ElementAt(i));
            }

            _productService.UpdateProduct(product);

            //add category
            if (productDelta.Dto.CategoryId.HasValue && productDelta.Dto.CategoryId > 0)
            {
                product.ProductCategories.Add(new ProductCategory
                {
                    CategoryId = productDelta.Dto.CategoryId.Value
                });
            }

            UpdateProductAttributes(product, productDelta);

            UpdateProductPictures(product, productDelta.Dto.Images);

            UpdateProductTags(product, productDelta.Dto.Tags);

            UpdateProductManufacturers(product, productDelta.Dto.ManufacturerIds);

            UpdateAssociatedProducts(product, productDelta.Dto.AssociatedProductIds);

            // Update the SeName if specified
            if (productDelta.Dto.SeName != null)
            {
                var seName = product.ValidateSeName(productDelta.Dto.SeName, product.Name, true);
                _urlRecordService.SaveSlug(product, seName, 0);
            }

            UpdateDiscountMappings(product, productDelta.Dto.DiscountIds);

            UpdateStoreMappings(product, productDelta.Dto.StoreIds);

            UpdateAclRoles(product, productDelta.Dto.RoleIds);

            _productService.UpdateProduct(product);

            _customerActivityService.InsertActivity("UpdateProduct",
               _localizationService.GetResource("ActivityLog.UpdateProduct"), product.Name);

            // Preparing the result dto of the new product
            ProductDto productDto = _dtoHelper.PrepareProductDTO(product);

            var json = _jsonFieldsSerializer.Serialize(productDto, string.Empty);

            return new RawJsonActionResult(json);
        }

        /// <summary>
        /// [Tuandang][01/12/2018][Hàm xóa nhiều sản phẩm]
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("/api/products/delete")]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult DeleteProductByIds(List<int> ids)
        {
            if (!Authorize(StandardPermissionProvider.ManageProducts))
            {
                return Error(HttpStatusCode.BadRequest, "", "Unauthorized products");
            }
            if (ids == null || ids.Count == 0)
            {
                return Error(HttpStatusCode.BadRequest, "productId", "invalid ids");
            }
            var products = _productService.GetProductByIds(ids).Where(p => !p.Deleted && _storeMappingService.Authorize(p)).ToList();

            _productService.DeleteProducts(products);

            //activity log
            _customerActivityService.InsertActivity("DeleteProducts", _localizationService.GetResource("ActivityLog.DeleteProducts"), products.Select(x => x.Id).ToList());

            return new RawJsonActionResult("{}");
        }

        /// <summary>
        /// [HienNguyen][07/08/2018][Hàm xóa sản phẩm]
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("/api/products/{id:int}")]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult DeleteProduct(int id)
        {
            if (!Authorize(StandardPermissionProvider.ManageProducts))
            {
                return Error(HttpStatusCode.BadRequest, "", "Unauthorized products");
            }
            if (id <= 0)
            {
                return Error(HttpStatusCode.BadRequest, "productId", "invalid id");
            }
            var product = _productService.GetProductById(id);
            if (product == null || product.Deleted)
            {
                return Error(HttpStatusCode.NotFound, "product", "not found");
            }
            if (!_storeMappingService.Authorize(product))
            {
                return Error(HttpStatusCode.Unauthorized, "id", "Unauthorized specified product");
            }

            _productService.DeleteProduct(id);

            //activity log
            _customerActivityService.InsertActivity("DeleteProduct", _localizationService.GetResource("ActivityLog.DeleteProduct"), product.Name);

            return new RawJsonActionResult("{}");
        }

        /// <summary>
        /// [HienNguyen][07/08/2018][Hàm cập nhật ngưng kinh doanh]
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("/api/un_publish_product/{id}")]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult UnPublishProduct(int id)
        {
            if (id <= 0)
            {
                return Error(HttpStatusCode.BadRequest, "productId", "invalid id");
            }

            Product product = _productService.Published(id, false);

            //activity log
            _customerActivityService.InsertActivity("UpdateProduct", _localizationService.GetResource("ActivityLog.UpdateProduct"), product.Name);

            return new RawJsonActionResult("{}");
        }

        /// <summary>
        /// [HienNguyen][07/08/2018][Hàm cập nhật kinh doanh]
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("/api/publish_product/{id}")]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult PublishProduct(int id)
        {
            if (id <= 0)
            {
                return Error(HttpStatusCode.BadRequest, "productId", "invalid id");
            }

            Product product = _productService.Published(id, true);

            //activity log
            _customerActivityService.InsertActivity("UpdateProduct", _localizationService.GetResource("ActivityLog.UpdateProduct"), product.Name);

            return new RawJsonActionResult("{}");
        }

        /// <summary>
        /// [HienNguyen][09/08/2018][Hàm cập nhật giá và tên sản phẩm]
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("/api/update_product_sale_price")]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult UpdateProductSalePrice([ModelBinder(typeof(JsonModelBinder<ProductDtoModel>))] Delta<ProductDtoModel> productDelta)
        {
            _productService.UpdateProduct(productDelta.Dto.ProductId,
                                            productDelta.Dto.ProductName,
                                            productDelta.Dto.SalePrice,
                                            productDelta.Dto.WholeSalePrice,
                                            productDelta.Dto.VipSalePrice);

            //activity log
            _customerActivityService.InsertActivity("UpdateProduct", _localizationService.GetResource("ActivityLog.UpdateProduct"), string.Empty);

            return new RawJsonActionResult("{}");
        }

        /// <summary>
        /// Receive a count of all products
        /// </summary>
        /// <response code="200">OK</response>
        /// <response code="401">Unauthorized</response>
        [HttpGet]
        [Route("/api/products/count")]
        [ProducesResponseType(typeof(ProductsCountRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult GetProductsCount(ProductsCountParametersModel parameters)
        {
            int allProductsCount = _productApiService.GetProductsCount(parameters.CreatedAtMin, parameters.CreatedAtMax, parameters.UpdatedAtMin,
                                                                       parameters.UpdatedAtMax, parameters.PublishedStatus, parameters.VendorName,
                                                                       parameters.CategoryId);

            var productsCountRootObject = new ProductsCountRootObject()
            {
                Count = allProductsCount
            };

            return Ok(productsCountRootObject);
        }


        /// <summary>
        /// [HienNguyen][07/08/2018][Hàm get hình ảnh theo sản phẩm]
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("/api/get_picture_by_product/{id}")]
        [ProducesResponseType(typeof(List<ProductPictureObjectDtoModel>), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult GetPictureByProduct(int id)
        {
            IList<ProductPicture> pictures = _productService.GetProductPicturesByProductId(id);

            IList<ProductPictureDtoModel> productsAsDtos = pictures.Select(picture =>
            {
                return _dtoHelper.PrepareProductPictureDTOModel(picture);
            }).ToList();

            ProductPictureObjectDtoModel result = new ProductPictureObjectDtoModel()
            {
                ProductPicture = new List<ProductPictureDtoModel>()
            };

            var json = _jsonFieldsSerializer.Serialize(result, string.Empty);

            return new RawJsonActionResult(json);
        }

        /// <summary>
        /// [HienNguyen][07/08/2018][Hàm tìm kiếm sản phẩm]
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        //[HttpGet]
        //[Route("/api/search_product")]
        //[ProducesResponseType(typeof(ProductsRootObjectDtoModel), (int)HttpStatusCode.OK)]
        //[ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        //[ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        //[GetRequestsErrorInterceptorActionFilter]
        //public IActionResult SearchProduct(ProductsParametersModel parameters)
        //{
        //    if (!Authorize(StandardPermissionProvider.ManageProducts))
        //    {
        //        return this.Error(HttpStatusCode.Unauthorized);
        //    }
        //    List<int> productStatusIds = null;
        //    List<int> categoryIds = null;

        //    if (parameters.CategoryId.HasValue)
        //    {
        //        categoryIds = new List<int>() { parameters.CategoryId.Value };
        //    }
        //    if (parameters.ProductStatusId.HasValue)
        //    {
        //        productStatusIds = new List<int>() { parameters.ProductStatusId.Value };
        //    }

        //    IPagedList<Product> products = _productService.SearchProducts(parameters.Keyword,
        //        productStatusIds,
        //        categoryIds,
        //        parameters.VendorId ?? 0,
        //        parameters.PageIndex,
        //        parameters.PageSize);

        //    IList<ProductDtoModel> productsAsDtos = products.Select(product =>
        //    {
        //        return _dtoHelper.PrepareProductDTOModel(product);
        //    }).ToList();

        //    var productsRootObject = new ProductsRootObjectDtoModel()
        //    {
        //        Products = productsAsDtos,
        //        TotalRow = products.TotalCount
        //    };

        //    var json = _jsonFieldsSerializer.Serialize(productsRootObject, parameters.Fields);

        //    return new RawJsonActionResult(json);
        //}

        #endregion

        #region Ultilities
        private void UpdateProductPictures(Product entityToUpdate, List<ImageMappingDto> setPictures)
        {
            // If no pictures are specified means we don't have to update anything
            if (setPictures == null)
                return;

            // delete unused product pictures
            var unusedProductPictures = entityToUpdate.ProductPictures.Where(x => setPictures.All(y => y.Id != x.Id)).ToList();
            foreach (var unusedProductPicture in unusedProductPictures)
            {
                var picture = _pictureService.GetPictureById(unusedProductPicture.PictureId);
                if (picture == null)
                    throw new ArgumentException("No picture found with the specified id");
                _pictureService.DeletePicture(picture);
            }

            foreach (var imageDto in setPictures)
            {
                if (imageDto.Id > 0)
                {
                    // update existing product picture
                    var productPictureToUpdate = entityToUpdate.ProductPictures.FirstOrDefault(x => x.Id == imageDto.Id);
                    if (productPictureToUpdate != null && imageDto.Position > 0)
                    {
                        productPictureToUpdate.DisplayOrder = imageDto.Position;
                        _productService.UpdateProductPicture(productPictureToUpdate);
                    }
                }
                else if (!string.IsNullOrEmpty(imageDto.DataUrl))
                {
                    var regex = _imageDataRegex.Match(imageDto.DataUrl);
                    var base64Data = regex.Groups["data"].Value;
                    var imageType = regex.Groups["type"].Value;
                    var binData = Convert.FromBase64String(base64Data);
                    var picture = _pictureService.InsertPicture(binData, imageType, string.Empty);
                    _productService.InsertProductPicture(new ProductPicture()
                    {
                        PictureId = picture.Id,
                        ProductId = entityToUpdate.Id,
                        DisplayOrder = imageDto.Position
                    });
                }
                else
                {
                    // add new product picture
                    Picture newPicture = _pictureService.InsertPicture(imageDto.Binary, imageDto.MimeType, string.Empty);
                    _productService.InsertProductPicture(new ProductPicture()
                    {
                        PictureId = newPicture.Id,
                        ProductId = entityToUpdate.Id,
                        DisplayOrder = imageDto.Position
                    });
                }
            }
        }

        private void UpdateProductAttributes(Product entityToUpdate, Delta<ProductDto> productDtoDelta)
        {
            // If no product attribute mappings are specified means we don't have to update anything
            if (productDtoDelta.Dto.ProductAttributeMappings == null)
                return;

            // delete unused product attribute mappings
            IEnumerable<int> toBeUpdatedIds = productDtoDelta.Dto.ProductAttributeMappings.Where(y => y.Id != 0).Select(x => x.Id);

            var unusedProductAttributeMappings = entityToUpdate.ProductAttributeMappings.Where(x => !toBeUpdatedIds.Contains(x.Id)).ToList();

            foreach (var unusedProductAttributeMapping in unusedProductAttributeMappings)
            {
                _productAttributeService.DeleteProductAttributeMapping(unusedProductAttributeMapping);
            }

            foreach (var productAttributeMappingDto in productDtoDelta.Dto.ProductAttributeMappings)
            {
                if (productAttributeMappingDto.Id > 0)
                {
                    // update existing product attribute mapping
                    var productAttributeMappingToUpdate = entityToUpdate.ProductAttributeMappings.FirstOrDefault(x => x.Id == productAttributeMappingDto.Id);
                    if (productAttributeMappingToUpdate != null)
                    {
                        productDtoDelta.Merge(productAttributeMappingDto, productAttributeMappingToUpdate, false);

                        _productAttributeService.UpdateProductAttributeMapping(productAttributeMappingToUpdate);

                        UpdateProductAttributeValues(productAttributeMappingDto, productDtoDelta);
                    }
                }
                else
                {
                    ProductAttributeMapping newProductAttributeMapping = new ProductAttributeMapping();
                    newProductAttributeMapping.ProductId = entityToUpdate.Id;

                    productDtoDelta.Merge(productAttributeMappingDto, newProductAttributeMapping);

                    // add new product attribute
                    _productAttributeService.InsertProductAttributeMapping(newProductAttributeMapping);
                }
            }
        }

        private void UpdateProductAttributeValues(ProductAttributeMappingDto productAttributeMappingDto, Delta<ProductDto> productDtoDelta)
        {
            // If no product attribute values are specified means we don't have to update anything
            if (productAttributeMappingDto.ProductAttributeValues == null)
                return;

            // delete unused product attribute values
            IEnumerable<int> toBeUpdatedIds = productAttributeMappingDto.ProductAttributeValues.Where(y => y.Id != 0).Select(x => x.Id);

            var unusedProductAttributeValues =
                _productAttributeService.GetProductAttributeValues(productAttributeMappingDto.Id).Where(x => !toBeUpdatedIds.Contains(x.Id)).ToList(); ;

            foreach (var unusedProductAttributeValue in unusedProductAttributeValues)
            {
                _productAttributeService.DeleteProductAttributeValue(unusedProductAttributeValue);
            }

            foreach (var productAttributeValueDto in productAttributeMappingDto.ProductAttributeValues)
            {
                if (productAttributeValueDto.Id > 0)
                {
                    // update existing product attribute mapping
                    var productAttributeValueToUpdate =
                        _productAttributeService.GetProductAttributeValueById(productAttributeValueDto.Id);
                    if (productAttributeValueToUpdate != null)
                    {
                        productDtoDelta.Merge(productAttributeValueDto, productAttributeValueToUpdate, false);

                        _productAttributeService.UpdateProductAttributeValue(productAttributeValueToUpdate);
                    }
                }
                else
                {
                    ProductAttributeValue newProductAttributeValue = new ProductAttributeValue();
                    productDtoDelta.Merge(productAttributeValueDto, newProductAttributeValue);

                    newProductAttributeValue.ProductAttributeMappingId = productAttributeMappingDto.Id;
                    // add new product attribute value
                    _productAttributeService.InsertProductAttributeValue(newProductAttributeValue);
                }
            }
        }

        private void UpdateProductTags(Product product, List<string> productTags)
        {
            if (productTags == null)
                return;

            if (product == null)
                throw new ArgumentNullException("product");

            //product tags
            var existingProductTags = product.ProductTags.ToList();
            var productTagsToRemove = new List<ProductTag>();
            foreach (var existingProductTag in existingProductTags)
            {
                bool found = false;
                foreach (string newProductTag in productTags)
                {
                    if (existingProductTag.Name.Equals(newProductTag, StringComparison.InvariantCultureIgnoreCase))
                    {
                        found = true;
                        break;
                    }
                }
                if (!found)
                {
                    productTagsToRemove.Add(existingProductTag);
                }
            }
            foreach (var productTag in productTagsToRemove)
            {
                product.ProductTags.Remove(productTag);
                _productService.UpdateProduct(product);
            }
            foreach (string productTagName in productTags)
            {
                ProductTag productTag;
                var productTag2 = _productTagService.GetProductTagByName(productTagName);
                if (productTag2 == null)
                {
                    //add new product tag
                    productTag = new ProductTag
                    {
                        Name = productTagName
                    };
                    _productTagService.InsertProductTag(productTag);
                }
                else
                {
                    productTag = productTag2;
                }
                if (!product.ProductTagExists(productTag.Id))
                {
                    product.ProductTags.Add(productTag);
                    _productService.UpdateProduct(product);
                }
            }
        }

        private void UpdateDiscountMappings(Product product, List<int> passedDiscountIds)
        {
            if (passedDiscountIds == null)
                return;

            var allDiscounts = _discountService.GetAllDiscounts(DiscountType.AssignedToSkus, showHidden: true);

            foreach (var discount in allDiscounts)
            {
                if (passedDiscountIds.Contains(discount.Id))
                {
                    //new discount
                    if (product.AppliedDiscounts.Count(d => d.Id == discount.Id) == 0)
                        product.AppliedDiscounts.Add(discount);
                }
                else
                {
                    //remove discount
                    if (product.AppliedDiscounts.Count(d => d.Id == discount.Id) > 0)
                        product.AppliedDiscounts.Remove(discount);
                }
            }

            _productService.UpdateProduct(product);
            _productService.UpdateHasDiscountsApplied(product);
        }

        private void UpdateProductManufacturers(Product product, List<int> passedManufacturerIds)
        {
            // If no manufacturers specified then there is nothing to map 
            if (passedManufacturerIds == null)
                return;

            var unusedProductManufacturers = product.ProductManufacturers.Where(x => !passedManufacturerIds.Contains(x.ManufacturerId)).ToList();

            // remove all manufacturers that are not passed
            foreach (var unusedProductManufacturer in unusedProductManufacturers)
            {
                _manufacturerService.DeleteProductManufacturer(unusedProductManufacturer);
            }

            foreach (var passedManufacturerId in passedManufacturerIds)
            {
                // not part of existing manufacturers so we will create a new one
                if (product.ProductManufacturers.All(x => x.ManufacturerId != passedManufacturerId))
                {
                    // if manufacturer does not exist we simply ignore it, otherwise add it to the product
                    var manufacturer = _manufacturerService.GetManufacturerById(passedManufacturerId);
                    if (manufacturer != null)
                    {
                        _manufacturerService.InsertProductManufacturer(new ProductManufacturer()
                        { ProductId = product.Id, ManufacturerId = manufacturer.Id });
                    }
                }
            }
        }

        private void UpdateAssociatedProducts(Product product, List<int> passedAssociatedProductIds)
        {
            // If no associated products specified then there is nothing to map 
            if (passedAssociatedProductIds == null)
                return;

            var noLongerAssociatedProducts =
                _productService.GetAssociatedProducts(product.Id, showHidden: true)
                    .Where(p => !passedAssociatedProductIds.Contains(p.Id));

            // update all products that are no longer associated with our product
            foreach (var noLongerAssocuatedProduct in noLongerAssociatedProducts)
            {
                noLongerAssocuatedProduct.ParentGroupedProductId = 0;
                _productService.UpdateProduct(noLongerAssocuatedProduct);
            }

            var newAssociatedProducts = _productService.GetProductsByIds(passedAssociatedProductIds.ToArray());
            foreach (var newAssociatedProduct in newAssociatedProducts)
            {
                newAssociatedProduct.ParentGroupedProductId = product.Id;
                _productService.UpdateProduct(newAssociatedProduct);
            }
        } 
        #endregion
    }
}