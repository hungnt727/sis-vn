﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using FluentValidation.Results;
using Nop.Core;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Shipping;
using Nop.Core.Infrastructure;
using Nop.Plugin.Api.Attributes;
using Nop.Plugin.Api.Constants;
using Nop.Plugin.Api.Delta;
using Nop.Plugin.Api.DTOs;
using Nop.Plugin.Api.DTOs.OrderItems;
using Nop.Plugin.Api.DTOs.Orders;
using Nop.Plugin.Api.Factories;
using Nop.Plugin.Api.Helpers;
using Nop.Plugin.Api.JSON.ActionResults;
using Nop.Plugin.Api.ModelBinders;
using Nop.Plugin.Api.Models.OrdersParameters;
using Nop.Plugin.Api.Services;
using Nop.Plugin.Api.Validators;
using Nop.Services.Catalog;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Services.Discounts;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Media;
using Nop.Services.Orders;
using Nop.Services.Payments;
using Nop.Services.Security;
using Nop.Services.Shipping;
using Nop.Services.Stores;
using Microsoft.AspNetCore.Mvc;

namespace Nop.Plugin.Api.Controllers
{
    using Microsoft.AspNetCore.Authentication.JwtBearer;
    using Newtonsoft.Json.Linq;
    using Nop.Core.Domain.Stores;
    using Nop.Core.Domain.Vendors;
    using Nop.Plugin.Api.DTOs.Errors;
    using Nop.Plugin.Api.DTOs.InputVouchers;
    using Nop.Plugin.Api.DTOs.Stores;
    using Nop.Plugin.Api.JSON.Serializers;
    using Nop.Plugin.Api.Models.InputVoucherParameters;
    using Nop.Plugin.Api.Models.OrderItemsParameters;
    using Nop.Services.Vendors;

    [ApiAuthorize(Policy = JwtBearerDefaults.AuthenticationScheme, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class InputVoucherController : BaseApiController
    {
        private readonly IInputVoucherApiService _inputVoucherApiService;
        private readonly IProductService _productService;
        private readonly IOrderProcessingService _orderProcessingService;
        private readonly IOrderService _orderService;
        private readonly IShoppingCartService _shoppingCartService;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly IShippingService _shippingService;
        private readonly IDTOHelper _dtoHelper;
        private readonly IProductAttributeConverter _productAttributeConverter;
        private readonly IStoreContext _storeContext;
        private readonly IStoreService _storeService;
        private readonly IVendorService _vendorService;
        
        private readonly IFactory<Order> _factory;

        // We resolve the order settings this way because of the tests.
        // The auto mocking does not support concreate types as dependencies. It supports only interfaces.
        private OrderSettings _orderSettings;

        private OrderSettings OrderSettings
        {
            get
            {
                if (_orderSettings == null)
                {
                    _orderSettings = EngineContext.Current.Resolve<OrderSettings>();
                }

                return _orderSettings;
            }
        }

        public InputVoucherController(IInputVoucherApiService inputVoucherApiService,
            IJsonFieldsSerializer jsonFieldsSerializer,
            IAclService aclService,
            ICustomerService customerService,
            IStoreMappingService storeMappingService,
            IStoreService storeService,
            IDiscountService discountService,
            ICustomerActivityService customerActivityService,
            ILocalizationService localizationService,
            IProductService productService,
            IFactory<Order> factory,
            IOrderProcessingService orderProcessingService,
            IOrderService orderService,
            IShoppingCartService shoppingCartService,
            IGenericAttributeService genericAttributeService,
            IStoreContext storeContext,
            IShippingService shippingService,
            IPictureService pictureService,
            IDTOHelper dtoHelper,
            IProductAttributeConverter productAttributeConverter,
            IVendorService vendorService
            )
        {
            _inputVoucherApiService = inputVoucherApiService;
            _factory = factory;
            _orderProcessingService = orderProcessingService;
            _orderService = orderService;
            _shoppingCartService = shoppingCartService;
            _genericAttributeService = genericAttributeService;
            _storeContext = storeContext;
            _shippingService = shippingService;
            _dtoHelper = dtoHelper;
            _productService = productService;
            _productAttributeConverter = productAttributeConverter;
            _storeService = storeService;
            _vendorService = vendorService;
        }

        /// <summary>
        /// [TuanAnh][28/09/2018][Lấy danh sách loại phiếu nhập].
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("/api/get_input_voucher_type")]
        [ProducesResponseType(typeof(DTOs.Discounts.DiscountTypeModel), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult GetInputVoucherType()
        {
            DTOs.InputVouchers.InputVoucherTypeModel inputVoucherTypeModel = new DTOs.InputVouchers.InputVoucherTypeModel();

            inputVoucherTypeModel.InputVoucherTypes = _inputVoucherApiService.GetListInputVoucherType();

            var json = _jsonFieldsSerializer.Serialize(inputVoucherTypeModel, string.Empty);
            return new RawJsonActionResult(json);
        }



        /// <summary>
        /// [TuanAnh][27/09/2018][Lấy danh sách phiếu nhập theo điêu kiện tìm kiếm].
        /// </summary>
        /// <param name="vendor_id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("/api/get_list_input_vouchers")]
        [ProducesResponseType(typeof(OrdersRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult GetListInputVouchers(InputVoucherSearchParametersModel parameters )
        {
            IPagedList<Order> inputVoucherByVendor = _inputVoucherApiService.GetListInputVouchers(
                parameters.ProductId,
                 parameters.VendorId, 
                 parameters.WareHouseId,
                 parameters.InputTypeId,
                 parameters.InputStatusId,
                 parameters.InputDateFrom,
                 parameters.InputDateTo,
                 parameters.PageIndex, 
                 parameters.PageSize
                );
            var ordersRootObject = new InputVoucherRootObject();
            ordersRootObject.InputVouchers = inputVoucherByVendor.Select(s => InputVoucherDto.CopyData(s)).ToList();

            var json = _jsonFieldsSerializer.Serialize(ordersRootObject, string.Empty);

            return new RawJsonActionResult(json);
        }


        /// <summary>
        /// [TuanAnh][08/09/2018][Load danh sách phiếu nhập theo nhà cung cấp].
        /// </summary>
        /// <param name="vendor_id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("/api/get_input_voucher_by_vendor/{vendor_id}")]
        [ProducesResponseType(typeof(OrdersRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult GetInputVouchersByVendorId(int vendor_id)
        {
            IPagedList<Order> inputVoucherByVendor = _inputVoucherApiService.GetInputVouchersByVendorId(vendor_id, 1, int.MaxValue -1 );
            var ordersRootObject = new InputVoucherRootObject();
            ordersRootObject.InputVouchers = inputVoucherByVendor.Select(s => InputVoucherDto.CopyData(s)).ToList();

            var json = _jsonFieldsSerializer.Serialize(ordersRootObject, string.Empty);

            return new RawJsonActionResult(json) ;
        }

        /// <summary>
        /// [TuanAnh][12/09/2018][Tạo mới phiếu nhập kho].
        /// </summary>
        /// <param name="inputVoucherDelta"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("/api/create_input_voucher")]
        [ProducesResponseType(typeof(InputVoucherRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ErrorsRootObject), 422)]
        public IActionResult CreateInputVoucher([ModelBinder(typeof(JsonModelBinder<InputVoucherDto>))] Delta<InputVoucherDto> inputVoucherDelta)
        {
            // Here we display the errors if the validation has failed at some point.
            if (!ModelState.IsValid)
            {
                return Error();
            }

            var inputVoucher = _orderService.GetOrderById(inputVoucherDelta.Dto.Id);
            if (inputVoucher == null)
            {
                return Error(HttpStatusCode.NotFound, "input voucher", "not found");
            }

            Vendor vendor = _vendorService.GetVendorById(inputVoucherDelta.Dto.VendorId);
            if (vendor == null)
            {
                return Error(HttpStatusCode.NotFound, "vendor", "not found");
            }

            Warehouse warehouse = _shippingService.GetWarehouseById(inputVoucherDelta.Dto.WareHouseId);
            if (warehouse == null)
            {
                return Error(HttpStatusCode.NotFound, "warehouse", "not found");
            }

            var objPaymentMethod = new SISVN_OrderPaymentBalance();
            objPaymentMethod.PaymentMethodId = inputVoucherDelta.Dto.PaymentMethodId;
            objPaymentMethod.PaymentAmount = inputVoucherDelta.Dto.PaymentAmount;

            if (inputVoucherDelta.Dto.InputVoucherItems != null)
            {
                var lstProductId = inputVoucherDelta.Dto.InputVoucherItems.Select(s => s.ProductId).ToList();
                var lstProduct = _productService.GetProductByIds(lstProductId);
                if (lstProduct == null || lstProduct.Count == 0)
                {
                    return Error(HttpStatusCode.NotFound, "products", "not found");
                }
                else
                {
                    if (lstProduct.Any(s => s.VendorId != inputVoucherDelta.Dto.VendorId))
                    {
                        return Error(HttpStatusCode.BadRequest, "vendor - product", "vendor and products asynchronously ");
                    }
                }

                //Sửa dụng lại hàm tạo order.
                Order newOrder = _orderService.CreateSaleOrder(
                    _dtoHelper.PrepareInputVoucher(inputVoucherDelta.Dto),
                    _dtoHelper.PrepareListInputVoucherItem(inputVoucherDelta.Dto.InputVoucherItems),
                     new List<SISVN_OrderPaymentBalance>() { objPaymentMethod },
                    _dtoHelper.PrepareListInputVoucherNote(inputVoucherDelta.Dto.InputNotes)
                    );

                _customerActivityService.InsertActivity("AddNewOrder",
                _localizationService.GetResource("ActivityLog.AddNewOrder"), newOrder.Id);

                InputVoucherRootObject inputVoucherRootObject = new InputVoucherRootObject();
                var _temp = InputVoucherDto.CopyData(newOrder);
                inputVoucherRootObject.InputVouchers.Add(_temp);
                var json = _jsonFieldsSerializer.Serialize(inputVoucherRootObject, string.Empty);
                return new RawJsonActionResult(json);

            }
            else
            {
                return Error(HttpStatusCode.NotFound, "InputVoucherItems", "not found");
            }
        }

        /// <summary>
        /// [TuanAnh][12/09/2018][Cập nhập phiếu nhập kho].
        /// </summary>
        /// <param name="inputVoucherDelta"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("/api/update_input_voucher")]
        [ProducesResponseType(typeof(InputVoucherRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ErrorsRootObject), 422)]
        public IActionResult UpdateInputVoucher([ModelBinder(typeof(JsonModelBinder<InputVoucherDto>))] Delta<InputVoucherDto> inputVoucherDelta)
        {
            // Here we display the errors if the validation has failed at some point.
            if (!ModelState.IsValid)
            {
                return Error();
            }

            var inputVoucher = _orderService.GetOrderById(inputVoucherDelta.Dto.Id);
            if (inputVoucher == null)
            {
                return Error(HttpStatusCode.NotFound, "input voucher", "not found");
            }

            Vendor vendor = _vendorService.GetVendorById(inputVoucherDelta.Dto.VendorId);
            if (vendor == null)
            {
                return Error(HttpStatusCode.NotFound, "vendor", "not found");
            }

            var objPaymentMethod = new SISVN_OrderPaymentBalance();
            objPaymentMethod.Id = inputVoucherDelta.Dto.PaymentMethodId;
            objPaymentMethod.PaymentAmount = inputVoucherDelta.Dto.PaymentAmount;

            if (inputVoucherDelta.Dto.InputVoucherItems != null)
            {
                var lstProductId = inputVoucherDelta.Dto.InputVoucherItems.Select(s => s.ProductId).ToList();
                var lstProduct = _productService.GetProductByIds(lstProductId);
                if (lstProduct == null || lstProduct.Count == 0)
                {
                    return Error(HttpStatusCode.NotFound, "products", "not found");
                }
                else
                {
                    if (lstProduct.Any(s => s.VendorId != inputVoucherDelta.Dto.VendorId))
                    {
                        return Error(HttpStatusCode.NotFound, "vendor - product", "vendor and products asynchronously ");
                    }
                }

                //Sửa dụng lại hàm tạo order.
                Order newOrder = _orderService.UpdateInputVouchcer(
                    _dtoHelper.PrepareInputVoucher(inputVoucherDelta.Dto),
                    _dtoHelper.PrepareListInputVoucherItem(inputVoucherDelta.Dto.InputVoucherItems),
                     new List<SISVN_OrderPaymentBalance>() { objPaymentMethod },
                    _dtoHelper.PrepareListInputVoucherNote(inputVoucherDelta.Dto.InputNotes)
                    );

                _customerActivityService.InsertActivity("AddNewOrder",
                _localizationService.GetResource("ActivityLog.AddNewOrder"), newOrder.Id);

                InputVoucherRootObject inputVoucherRootObject = new InputVoucherRootObject();
                var _temp = InputVoucherDto.CopyData(newOrder);
                inputVoucherRootObject.InputVouchers.Add(_temp);
                var json = _jsonFieldsSerializer.Serialize(inputVoucherRootObject, string.Empty);
                return new RawJsonActionResult(json);

            }
            else
            {
                return Error(HttpStatusCode.NotFound, "InputVoucherItems", "not found");
            }



        }

        /// <summary>
        /// [TuanAnh][18/09/2018][Cập nhập trạng thái phiếu nhập].
        /// </summary>
        /// <param name="parametersModel"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("/api/change_status_input_voucher/{id}")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ErrorsRootObject), 422)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult ChangeStatusInputVoucher(InputVoucherChangeStatusParametersModel parametersModel)
        {
            if (parametersModel == null )
            {
                return Error(HttpStatusCode.BadRequest, "parameters", "not found");
            }
            var inputVoucher = _orderService.GetOrderById(parametersModel.InputVoucherID);
            if (inputVoucher == null )
            {
                return Error(HttpStatusCode.BadRequest, "inputvoucher", "not found");
            }

            inputVoucher.OrderStatusId = parametersModel.InputStatusId;

            _orderService.UpdateOrder(inputVoucher);

            _customerActivityService.InsertActivity("changeStatusInputVoucher",
             _localizationService.GetResource("ActivityLog.changeStatusInputVoucher"), inputVoucher.Id, inputVoucher.OrderStatusId);
            return new RawJsonActionResult(true);
        }


        [HttpDelete]
        [Route("/api/delete_input_voucher/{id}")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ErrorsRootObject), 422)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult DeleteInputVoucher(int id)
        {
            if (id <= 0)
            {
                return Error(HttpStatusCode.BadRequest, "id", "invalid id");
            }
            var order = _orderService.GetOrderById(id);
            if (order.OrderStatus == Core.Domain.Orders.OrderStatus.Complete)
            {
                return Error(HttpStatusCode.BadRequest, "InputVoucher", "Inputvoucher has completed, can not delete");
            }
            _orderService.DeleteOrder(id);
            
            _customerActivityService.InsertActivity("deleteInputVoucher",
             _localizationService.GetResource("ActivityLog.deleteInputVoucher"), order.Id);
            return new RawJsonActionResult(true);
        }

    }
}