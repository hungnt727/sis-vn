﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Discounts;
using Nop.Core.Domain.Directory;
using Nop.Core.Infrastructure;
using Nop.Plugin.Api.Attributes;
using Nop.Plugin.Api.Constants;
using Nop.Plugin.Api.Delta;
using Nop.Plugin.Api.DTOs;
using Nop.Plugin.Api.DTOs.Discounts;
using Nop.Plugin.Api.Factories;
using Nop.Plugin.Api.Helpers;
using Nop.Plugin.Api.JSON.ActionResults;
using Nop.Plugin.Api.MappingExtensions;
using Nop.Plugin.Api.ModelBinders;
using Nop.Plugin.Api.Models.CustomersParameters;
using Nop.Plugin.Api.Services;
using Nop.Services.Common;
using Nop.Services.Discounts;
using Nop.Services.Directory;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Media;
using Nop.Services.Messages;
using Nop.Services.Security;
using Nop.Services.Stores;

namespace Nop.Plugin.Api.Controllers
{
    using Microsoft.AspNetCore.Authentication.JwtBearer;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Newtonsoft.Json;
    using Nop.Core;
    using Nop.Core.Domain.Discounts;
    using Nop.Plugin.Api.DTOs.Categories;
    using Nop.Plugin.Api.DTOs.Discounts;
    using Nop.Plugin.Api.DTOs.Errors;
    using Nop.Plugin.Api.DTOs.Products;
    using Nop.Plugin.Api.JSON.Serializers;
    using Nop.Plugin.Api.Models.DiscountsParameters;
    using Nop.Services.Catalog;
    using Nop.Services.Customers;
    public class DiscountsController : BaseApiController
    {
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly IEncryptionService _encryptionService;
        private readonly ICountryService _countryService;
        private readonly IMappingHelper _mappingHelper;
        private readonly INewsLetterSubscriptionService _newsLetterSubscriptionService;
        private readonly ILanguageService _languageService;
        private readonly IFactory<Discount> _factory;
        private readonly IDTOHelper _dtoHelper;
        private readonly ICurrencyService _currencyService;
        private readonly CurrencySettings _currencySettings;
        private readonly IProductService _productService;
        private readonly ICategoryService _categoryService;
        private readonly IDiscountApiService _discountApiService;


        public DiscountsController(
            IJsonFieldsSerializer jsonFieldsSerializer,
            IAclService aclService,
            ICustomerService customerService,
            IStoreMappingService storeMappingService,
            IStoreService storeService,
            IDiscountService discountService,
            ICustomerActivityService customerActivityService,
            ILocalizationService localizationService,
            IGenericAttributeService genericAttributeService,
            IEncryptionService encryptionService,
            IFactory<Discount> factory,
            ICountryService countryService,
            IMappingHelper mappingHelper,
            INewsLetterSubscriptionService newsLetterSubscriptionService,
            IPictureService pictureService,
            ILanguageService languageService,
            IDTOHelper dtoHelper,
            ICurrencyService currencyService,
            IProductService productService,
            ICategoryService categoryService,
            IDiscountApiService discountApiService
            ) 
        {
            _factory = factory;
            _countryService = countryService;
            _mappingHelper = mappingHelper;
            _newsLetterSubscriptionService = newsLetterSubscriptionService;
            _languageService = languageService;
            _encryptionService = encryptionService;
            _genericAttributeService = genericAttributeService;
            _dtoHelper = dtoHelper;
            _currencyService = currencyService;
            _productService = productService;
            _categoryService = categoryService;
            _discountApiService = discountApiService;


        }

        #region Custom Method

        #region /api/get_discount [Hàm tìm kiếm khuyễn mãi]
        /// <summary>
        /// [TuanAnh][Hàm tìm kiếm khuyễn mãi]
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("/api/get_discount")]
        [ProducesResponseType(typeof(DiscountsRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [GetRequestsErrorInterceptorActionFilter]
        public virtual IActionResult GetDiscount(DiscountParametersModel parameters)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Error();
                }

                Core.Domain.Discounts.DiscountType? discountType = null;
                if (parameters.SearchDiscountTypeId > 0)
                {
                    discountType = (Core.Domain.Discounts.DiscountType)parameters.SearchDiscountTypeId;
                }

                var discounts = _discountApiService.SearchDiscount(discountType, parameters.SearchDiscountCouponCode, parameters.SearchDiscountName, true, parameters.StartDateUtc, parameters.EndDateUtc, parameters.PageIndex, parameters.PageSize);

                IList<DiscountDtoModel> productsAsDtos = discounts.Select(s =>
                   DiscountDtoModel.CopyData(s)
                ).ToList();

                var discountsRootObject = new DiscountsRootObject()
                {
                    Discounts = productsAsDtos,
                    TotalRow = discounts.TotalCount
                };

                var json = _jsonFieldsSerializer.Serialize(discountsRootObject, string.Empty);

                return new RawJsonActionResult(json);
            }
            catch (Exception ex)
            {
                return Error(HttpStatusCode.BadRequest, "" , ex.Message );
            }
        }
        #endregion

        #region /api/get_discount_by_id/{discountId} [Hàm lấy thông tin khuyến mãi]
        /// <summary>
        /// [TuanAnh][09/08/2017][Hàm lấy thông tin khuyến mãi]
        /// </summary>
        /// <param name="discountId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("/api/get_discount_by_id/{discountId}")]
        [ProducesResponseType(typeof(DiscountDtoModel), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult GetDiscountById(int discountId)
        {
            Discount discounts = _discountService.GetDiscountById(discountId);

            if (discounts == null)
            {
                return Error(HttpStatusCode.NotFound, "discount", "not found");
            }

            DiscountDtoModel discountModel = DiscountDtoModel.CopyData(discounts);

            var json = JsonConvert.SerializeObject(discountModel);

            return new RawJsonActionResult(json);
        }
        #endregion

        #region /api/get_discount_type [Hàm lấy loại khuyến mãi]
        /// <summary>
        /// [TuanAnh][09/08/2017][Hàm lấy loại khuyến mãi]
        /// </summary>
        /// <param name="orderstatusids"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("/api/get_discount_type")]
        [ProducesResponseType(typeof(DTOs.Discounts.DiscountTypeModel), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult GetDiscountType()
        {
            DTOs.Discounts.DiscountTypeModel orderStatus = new DTOs.Discounts.DiscountTypeModel();

            orderStatus.DiscountType = Enum.GetValues(typeof(Core.Domain.Discounts.DiscountType))
                                .Cast<Core.Domain.Discounts.DiscountType>()
                                .Where(s => s == Core.Domain.Discounts.DiscountType.AssignedToOrderTotal || s == Core.Domain.Discounts.DiscountType.AssignedToSkus)
                                .Select(v => new DTOs.Discounts.DiscountType() {
                                    DiscountTypeId = (int)v,
                                    DiscountTypeName = CommonHelper.GetEnumDescription(v) })
                                .ToList();

            var json = _jsonFieldsSerializer.Serialize(orderStatus, string.Empty);
            return new RawJsonActionResult(json);
        }
        #endregion

        #region /api/get_discount_type [Hàm lấy loại khuyến mãi]
        /// <summary>
        /// [TuanAnh][30/08/2017][Lấy danh sách loại giới hạn khuyến mãi]
        /// </summary>
        /// <param name="orderstatusids"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("/api/get_discount_limitation_type")]
        [ProducesResponseType(typeof(DTOs.Discounts.DiscountLimitationTypeRootModel), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult GetDiscountLimitationType()
        {
            DTOs.Discounts.DiscountLimitationTypeRootModel orderStatus = new DTOs.Discounts.DiscountLimitationTypeRootModel();

            orderStatus.DiscountLimitationTypes = Enum.GetValues(typeof(Core.Domain.Discounts.DiscountLimitationType))
                                .Cast<Core.Domain.Discounts.DiscountLimitationType>()
                                .Select(v => new DTOs.Discounts.DiscountLimitationTypeModel()
                                {
                                    DiscountLimitationTypeId = (int)v,
                                    DiscountLimitationTypeName = CommonHelper.GetEnumDescription(v)
                                }).ToList();

            var json = _jsonFieldsSerializer.Serialize(orderStatus, string.Empty);
            return new RawJsonActionResult(json);
        }
        #endregion

        #region /api/create_discount [Hàm tạo mới khuyễn mãi]
        /// <summary>
        /// [TuanAnh][09/08/2017][Hàm tạo mới khuyễn mãi]
        /// </summary>
        /// <param name="discountDelta"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("/api/create_discount")]
        [ProducesResponseType(typeof(DiscountsRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), 422)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        public IActionResult CreateDiscount([ModelBinder(typeof(JsonModelBinder<DiscountDto>))] Delta<DiscountDto> discountDelta)
        {
            // Here we display the errors if the validation has failed at some point.
            if (!ModelState.IsValid)
            {
                return Error();
            }

            //If the validation has passed the discountDelta object won't be null for sure so we don't need to check for this.

            // Inserting the new discount
            Discount newDiscount = _factory.Initialize();
            discountDelta.Merge(newDiscount);

            _discountService.InsertDiscount(newDiscount);
            UpdatesProductAppliedDiscounts(newDiscount.Id, discountDelta.Dto.AppliedToProductIds);
            UpdateCategoriesAppliedDiscounts(newDiscount.Id, discountDelta.Dto.AppliedToCategoryIds);

            //DiscountDto newDiscountDto = newDiscount.ToDto();

            //activity log
            _customerActivityService.InsertActivity("CreateDiscount", _localizationService.GetResource("ActivityLog.CreateDiscount"), newDiscount.Id);

            var discountsRootObject = new DiscountsRootObject();

            DiscountDtoModel discountModel = DiscountDtoModel.CopyData(newDiscount);

            discountsRootObject.Discounts.Add(discountModel);

            var json = _jsonFieldsSerializer.Serialize(discountsRootObject, string.Empty);

            return new RawJsonActionResult(json);
        }
        #endregion

        #region /api/update_discount/ [Hàm cập nhập khuyễn mãi]
        /// <summary>
        /// [TuanAnh][09/08/2017][Hàm cập nhập khuyễn mãi]
        /// </summary>
        /// <param name="discountDelta"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("/api/update_discount/")]
        [ProducesResponseType(typeof(DiscountsRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), 422)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        public IActionResult UpdateDiscount([ModelBinder(typeof(JsonModelBinder<DiscountDto>))] Delta<DiscountDto> discountDelta)
        {
            // Here we display the errors if the validation has failed at some point.
            if (!ModelState.IsValid)
            {
                return Error();
            }

            var discount = _discountService.GetDiscountById(discountDelta.Dto.Id);
            if (discount == null)
            {
                return Error(HttpStatusCode.NotFound, "discount", "not found");
            }
            var prevDiscountType = discount.DiscountType;

            discountDelta.Merge(discount, true);
            _discountService.UpdateDiscount(discount);

            UpdatesProductAppliedDiscounts(discount.Id, discountDelta.Dto.AppliedToProductIds);
            UpdateCategoriesAppliedDiscounts(discount.Id, discountDelta.Dto.AppliedToCategoryIds);


            //DiscountDto newDiscountDto = discount.ToDto();

            //activity log
            _customerActivityService.InsertActivity("CreateDiscount", _localizationService.GetResource("ActivityLog.CreateDiscount"), discount.Id);

            var discountsRootObject = new DiscountsRootObject();

            DiscountDtoModel discountModel = DiscountDtoModel.CopyData(discount);

            discountsRootObject.Discounts.Add(discountModel);

            var json = _jsonFieldsSerializer.Serialize(discountsRootObject, string.Empty);

            return new RawJsonActionResult(json);
        }
        #endregion

        #region /api/discount/{id} [Hàm xóa khuyễn mãi]
        /// <summary>
        /// [TuanAnh][09/08/2017][Hàm xóa khuyễn mãi]
        /// </summary>
        /// <param name="discountDelta"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("/api/discount/{id}")]
        [GetRequestsErrorInterceptorActionFilter]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        public IActionResult Delete(int id)
        {
            if (!ModelState.IsValid)
            {
                return Error();
            }

            if (id <= 0)
            {
                return Error(HttpStatusCode.BadRequest, "id", "invalid id");
            }
            var discount = _discountService.GetDiscountById(id);
            if (discount == null)
            {
                return Error(HttpStatusCode.NotFound, "discount", "not found");
            }
            _discountService.DeleteDiscount(discount);
            _customerActivityService.InsertActivity("DeleteDiscount", _localizationService.GetResource("ActivityLog.DeleteDiscount"), discount.Id);
            return new RawJsonActionResult("{}");
        }

        #endregion

        #region Applied to products

        #region /api/discount_getproductlist/{discountId}" [Lấy danh sách sản phẩm áp dụng khuyến mãi]
        /// <summary>
        /// [12/08/2018][TuanAnh][Lấy danh sách sản phẩm theo khuyến mãi]
        /// </summary>
        /// <param name="discountId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("/api/discount_get_product_list/{discountId}")]
        [GetRequestsErrorInterceptorActionFilter]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        public IActionResult GetProductApplied(int discountId)
        {
            if (!ModelState.IsValid)
            {
                return Error();
            }

            var discount = _discountService.GetDiscountById(discountId);
            if (discount == null)
            {
                return Error(HttpStatusCode.NotFound, "discount", "not found");
            }
            var products = discount
                .AppliedToProducts
                .Where(x => !x.Deleted)
                .Select(s => new ProductDtoModel() { ProductId = s.Id, ProductName = s.Name })
                .ToList();

            var objRoot = new ProductsRootObjectDtoModel();
            objRoot.Products = products;
            var json = _jsonFieldsSerializer.Serialize(objRoot, string.Empty);
            return new RawJsonActionResult(json);

        }
        #endregion

        #region /api/discount_add_applied_product/ [Thêm sản phẩm áp dụng khuyến mãi]
        /// <summary>
        /// [12/08/2018][TuanAnh][Thêm sản phẩm áp dụng khuyến mãi]
        /// </summary>
        /// <param name="discountId"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("/api/discount_add_applied_product/")]
        [GetRequestsErrorInterceptorActionFilter]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        public IActionResult AddAppliedProduct([ModelBinder(typeof(JsonModelBinder<AddDiscountAppliedToProductsDto>))] Delta<AddDiscountAppliedToProductsDto>  modelDelta)
        {
            if (!ModelState.IsValid)
            {
                return Error();
            }

            if (modelDelta == null)
            {
                return Error(HttpStatusCode.BadRequest, "model", "is null");
            }

            if (modelDelta.Dto.AppliedToProductIds == null || modelDelta.Dto.AppliedToProductIds.Count == 0)
            {
                return Error(HttpStatusCode.BadRequest, "AppliedToProducts", "is null or empty");
            }

            UpdatesProductAppliedDiscounts(modelDelta.Dto.DiscountId, modelDelta.Dto.AppliedToProductIds, modelDelta.Dto.IsDeleteOldProductApplied);

            _customerActivityService.InsertActivity("AddAppliedProduct", "AddAppliedProduct");
            return new RawJsonActionResult("{}");
        }
        #endregion

        #region /api/discount_deleteappliedproduct [Xóa sản phẩm áp dụng khuyến mãi]
        /// <summary>
        /// [12/08/2018][TuanAnh][Xóa sản phẩm áp dụng khuyến mãi]
        /// </summary>
        /// <param name="discountId"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("/api/discount_delete_applied_product/")]
        [GetRequestsErrorInterceptorActionFilter]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        public IActionResult DeleteAppliedProduct([ModelBinder(typeof(JsonModelBinder<AddDiscountAppliedToProductsDto>))] Delta<AddDiscountAppliedToProductsDto> modelDelta)
        {
            if (modelDelta == null || modelDelta.Dto == null )
            {
                return Error(HttpStatusCode.BadRequest, "applied_to_products", "applied_to_products is null");
            }
            if (modelDelta.Dto.DiscountId <= 0)
            {
                return Error(HttpStatusCode.BadRequest, "DiscountId", "DiscountId is IsValid");
            }
            if (modelDelta.Dto.AppliedToProductIds == null || modelDelta.Dto.AppliedToProductIds.Count() < 0)
            {
                return Error(HttpStatusCode.BadRequest, "AppliedToProductIds", "is null or empty");
            }

            var discount = _discountService.GetDiscountById(modelDelta.Dto.DiscountId);
            if (discount == null)
            {
                return Error(HttpStatusCode.NotFound, "discount", "not found");
            }

            DeleteProductAppliedDiscounts(discount, modelDelta.Dto.AppliedToProductIds);

            _customerActivityService.InsertActivity("DeleteAppliedProduct", "DeleteAppliedProduct", modelDelta.Dto.DiscountId);
            return new RawJsonActionResult("{}");
        }
        #endregion

        public void UpdatesProductAppliedDiscounts(int discountId, IEnumerable<int> listProductId, bool isDeleteOldProductApplied = true)
        {

            if (listProductId != null)
            {
                var discount = _discountService.GetDiscountById(discountId);

                if (discount == null)
                {
                    throw new ArgumentException("No discount found with the specified id");
                }
                #region Delete list product old
                if (isDeleteOldProductApplied)
                {
                    var lstProductApplied = discount.AppliedToProducts.Where(s => !listProductId.Any(m => m == s.Id)).ToList();
                    foreach (var product in lstProductApplied)
                    {
                        product.AppliedDiscounts.Remove(discount);
                        _productService.UpdateProduct(product);
                        _productService.UpdateHasDiscountsApplied(product);
                    }
                }
                #endregion

                foreach (var _productId in listProductId)
                {
                    var product = _productService.GetProductById(_productId);
                    if (product != null)
                    {
                        if (product.AppliedDiscounts.Count(d => d.Id == discount.Id) == 0)
                        {
                            product.AppliedDiscounts.Add(discount);
                        }
                        _productService.UpdateProduct(product);
                        _productService.UpdateHasDiscountsApplied(product);
                    }
                }
            }

        }

        public void DeleteProductAppliedDiscounts(Discount discount, IEnumerable<int> listProductdto)
        {
            if (discount == null)
            {
                throw new ArgumentException("Discount is null");
            }
            if (listProductdto != null)
            {
                var lstProductApplied = discount.AppliedToProducts.Where(s => listProductdto.Any(m => m == s.Id)).ToList();
                foreach (var product in lstProductApplied)
                {
                    product.AppliedDiscounts.Remove(discount);
                    _productService.UpdateProduct(product);
                    _productService.UpdateHasDiscountsApplied(product);
                }
            }


        }

        #endregion

        #region Applied to categories

        #region /api/discount_getcategories/{discountId}
        /// <summary>
        /// [15/08/2018][TuanAnh][Lấy danh sách danh mục áp dụng khuyến mãi]
        /// </summary>
        /// <param name="discountId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("/api/discount_get_categories/{discountId}")]
        [GetRequestsErrorInterceptorActionFilter]
        [ProducesResponseType(typeof(TaxRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        public IActionResult GetListCategoriesByDiscountId(int discountId)
        {
            if (!ModelState.IsValid)
            {
                return Error();
            }

            var discount = _discountService.GetDiscountById(discountId);
            if (discount == null)
            {
                return Error(HttpStatusCode.NotFound, "discount", "not found");
            }

            var products = discount
                .AppliedToCategories
                .Where(x => !x.Deleted)
                .Select(s => new TaxDto { Id = s.Id, Name = s.Name })
                .ToList();

            var objRoot = new TaxRootObject();
            objRoot.Categories = products;
            var json = _jsonFieldsSerializer.Serialize(objRoot, string.Empty);

            return new RawJsonActionResult(json);

        }
        #endregion

        #region /api/discount_appliedtocategories/ [Thêm sản phẩm áp dụng khuyến mãi]
        /// <summary>
        /// [15/08/2018][TuanAnh][Thêm sản phẩm áp dụng khuyến mãi]
        /// </summary>
        /// <param name="discountId"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("/api/discount_applied_to_categories/")]
        [GetRequestsErrorInterceptorActionFilter]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        public IActionResult AddAppliedToCategories([ModelBinder(typeof(JsonModelBinder<AddDiscountAppliedToCategoriesDto>))] Delta<AddDiscountAppliedToCategoriesDto> modelDelta)
        {
            if (!ModelState.IsValid)
            {
                return Error();
            }

            if (modelDelta == null && modelDelta.Dto == null )
            {
                return Error(HttpStatusCode.BadRequest, "applied_to_categories", "is null");
            }

            if (modelDelta.Dto.AppliedToCategorieIds == null || modelDelta.Dto.AppliedToCategorieIds.Count == 0)
            {
                return Error(HttpStatusCode.BadRequest, "category_ids", "is null or empty");
            }

            UpdateCategoriesAppliedDiscounts(modelDelta.Dto.DiscountId, modelDelta.Dto.AppliedToCategorieIds, modelDelta.Dto.IsDeleteOldCategoryApplied);

            _customerActivityService.InsertActivity("AddAppliedToCategories", "AddAppliedToCategories");
            return new RawJsonActionResult("{}");
        }
        #endregion

        #region /api/discount_deleteappliedproduct/{discountId} [Xóa sản phẩm áp dụng khuyến mãi]
        /// <summary>
        /// [15/08/2018][TuanAnh] [Xóa sản phẩm áp dụng khuyến mãi]
        /// </summary>
        /// <param name="discountId"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("/api/discount_delete_applied_cateories/")]
        [GetRequestsErrorInterceptorActionFilter]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        public IActionResult DeleteAppliedsToCategories([ModelBinder(typeof(JsonModelBinder<AddDiscountAppliedToCategoriesDto>))] Delta<AddDiscountAppliedToCategoriesDto> modelDelta)
        {
            if (modelDelta == null)
            {
                return Error(HttpStatusCode.BadRequest, "parameters", "parameters is null");
            }
            if (modelDelta.Dto.DiscountId <= 0)
            {
                return Error(HttpStatusCode.BadRequest, "discount_id", "DiscountId is IsValid");
            }
            if (modelDelta.Dto.AppliedToCategorieIds == null || modelDelta.Dto.AppliedToCategorieIds.Count() < 0)
            {
                return Error(HttpStatusCode.BadRequest, "category_ids", "is null or empty");
            }


            var discount = _discountService.GetDiscountById(modelDelta.Dto.DiscountId);
            if (discount == null)
            {
                return Error(HttpStatusCode.NotFound, "discount", "not found");
            }

            DeleteProductAppliedDiscounts(discount, modelDelta.Dto.AppliedToCategorieIds);

            _customerActivityService.InsertActivity("DeleteAppliedProduct", "DeleteAppliedProduct", modelDelta.Dto.DiscountId);
            return new RawJsonActionResult("{}");
        }
        #endregion

        /// <summary>
        /// [15/08/2018][TuanAnh][Thêm sản phẩm áp dụng khuyến mãi]
        /// </summary>
        /// <param name="discountId"></param>
        /// <param name="listCategorieIds"></param>
        /// <param name="isDeleteOldCategoryApplied"></param>
        public void UpdateCategoriesAppliedDiscounts(int discountId, IEnumerable<int> listCategorieIds, bool isDeleteOldCategoryApplied = true)
        {
            if (listCategorieIds != null)
            {
                var discount = _discountService.GetDiscountById(discountId);
                if (discount == null)
                {
                    throw new ArgumentException("No discount found with the specified id");
                }
                #region Delete list category old
                if (isDeleteOldCategoryApplied)
                {
                    var lstCategoriesApplied = discount.AppliedToCategories.Where(s => !listCategorieIds.Any(m => m == s.Id)).ToList();
                    foreach (var category in lstCategoriesApplied)
                    {
                        category.AppliedDiscounts.Remove(discount);
                        _categoryService.UpdateCategory(category);
                    }
                }
                #endregion
                foreach (var categoryId in listCategorieIds)
                {
                    var category = _categoryService.GetCategoryById(categoryId);
                    if (category != null)
                    {
                        if (category.AppliedDiscounts.Count(d => d.Id == discount.Id) == 0)
                        {
                            category.AppliedDiscounts.Add(discount);
                        }
                        _categoryService.UpdateCategory(category);
                    }
                }
            }
        }

        /// <summary>
        /// [15/08/2018][TuanAnh] [Xóa sản phẩm áp dụng khuyến mãi]
        /// </summary>
        /// <param name="discountId"></param>
        /// <param name="listCategoryIds"></param>
        public void DeleteCategoryAppliedDiscounts(int discountId, IEnumerable<int> listCategoryIds)
        {

            if (listCategoryIds != null)
            {
                var discount = _discountService.GetDiscountById(discountId);
                if (discount == null)
                {
                    throw new ArgumentException("No discount found with the specified id");
                }
                var lstProductApplied = discount.AppliedToCategories.Where(s => listCategoryIds.Any(m => m == s.Id)).ToList();
                foreach (var category in lstProductApplied)
                {
                    category.AppliedDiscounts.Remove(discount);
                    _categoryService.UpdateCategory(category);
                }
            }
        }
    
        #endregion

        #endregion
    }
}