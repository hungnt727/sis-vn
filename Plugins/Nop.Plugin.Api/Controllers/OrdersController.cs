﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using FluentValidation.Results;
using Nop.Core;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Shipping;
using Nop.Core.Infrastructure;
using Nop.Plugin.Api.Attributes;
using Nop.Plugin.Api.Constants;
using Nop.Plugin.Api.Delta;
using Nop.Plugin.Api.DTOs;
using Nop.Plugin.Api.DTOs.OrderItems;
using Nop.Plugin.Api.DTOs.Orders;
using Nop.Plugin.Api.Factories;
using Nop.Plugin.Api.Helpers;
using Nop.Plugin.Api.JSON.ActionResults;
using Nop.Plugin.Api.ModelBinders;
using Nop.Plugin.Api.Models.OrdersParameters;
using Nop.Plugin.Api.Services;
using Nop.Plugin.Api.Validators;
using Nop.Services.Catalog;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Services.Discounts;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Media;
using Nop.Services.Orders;
using Nop.Services.Payments;
using Nop.Services.Security;
using Nop.Services.Shipping;
using Nop.Services.Stores;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Nop.Core.Domain.Stores;
using Nop.Plugin.Api.Converters;
using Nop.Plugin.Api.DTOs.Errors;
using Nop.Plugin.Api.JSON.Serializers;
using Nop.Plugin.Api.Models.Orders;
using Nop.Plugin.Api.Models;
using Nop.Plugin.Api;
using Nop.Services.Helpers;
using Nop.Core.Domain.Payments;
using Nop.Services.Events;
using Nop.Plugin.Api.Models.Methods;

namespace Nop.Plugin.Api.Controllers
{
    [ApiAuthorize(Policy = JwtBearerDefaults.AuthenticationScheme, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class OrdersController : BaseApiController
    {
        #region Fields
        private readonly IPaymentService _paymentService;
        private readonly IShipmentService _shipmentService;
        private readonly IMappingHelper _mappingHelper;
        private readonly IEventPublisher _eventPublisher;
        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly ICustomNumberFormatter _customNumberFormatter;
        private readonly IPriceFormatter _priceFormatter;
        private readonly IModelConverter _modelConverter;
        private readonly IOrderApiService _orderApiService;
        private readonly IProductService _productService;
        private readonly IOrderProcessingService _orderProcessingService;
        private readonly IOrderService _orderService;
        private readonly IShoppingCartService _shoppingCartService;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly IShippingService _shippingService;
        private readonly IDTOHelper _dtoHelper;
        private readonly IProductAttributeConverter _productAttributeConverter;
        private readonly IStoreContext _storeContext;
        private readonly IFactory<Order> _factory;
        #endregion

        #region Ctor
        public OrdersController(
            IPaymentService paymentService,
            IShipmentService shipmentService,
            IMappingHelper mappingHelper,
            IEventPublisher eventPublisher,
            IDateTimeHelper dateTimeHelper,
            ICustomNumberFormatter customNumberFormatter,
            IPriceFormatter priceFormatter,
            IModelConverter modelConverter,
            IOrderApiService orderApiService,
            IJsonFieldsSerializer jsonFieldsSerializer,
            IAclService aclService,
            ICustomerService customerService,
            IStoreMappingService storeMappingService,
            IStoreService storeService,
            IDiscountService discountService,
            ICustomerActivityService customerActivityService,
            ILocalizationService localizationService,
            IProductService productService,
            IFactory<Order> factory,
            IOrderProcessingService orderProcessingService,
            IOrderService orderService,
            IShoppingCartService shoppingCartService,
            IGenericAttributeService genericAttributeService,
            IStoreContext storeContext,
            IShippingService shippingService,
            IPictureService pictureService,
            IDTOHelper dtoHelper,
            IProductAttributeConverter productAttributeConverter)
        {
            _paymentService = paymentService;
            _shipmentService = shipmentService;
            _mappingHelper = mappingHelper;
            _eventPublisher = eventPublisher;
            _dateTimeHelper = dateTimeHelper;
            _customNumberFormatter = customNumberFormatter;
            _priceFormatter = priceFormatter;
            _modelConverter = modelConverter;
            _orderApiService = orderApiService;
            _factory = factory;
            _orderProcessingService = orderProcessingService;
            _orderService = orderService;
            _shoppingCartService = shoppingCartService;
            _genericAttributeService = genericAttributeService;
            _storeContext = storeContext;
            _shippingService = shippingService;
            _dtoHelper = dtoHelper;
            _productService = productService;
            _productAttributeConverter = productAttributeConverter;
        }
        #endregion

        #region Apis

        #region Custom Method

        [HttpGet]
        [Route("/api/search_transporter")]
        [ProducesResponseType(typeof(SISVN_TransporterRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult SearchTransporter(string keyword)
        {
            IList<SISVN_Transporter> listTransporter = _orderService.SearchSISVN_Transporter(keyword);

            IList<SISVN_TransporterDto> listTransporterModel = listTransporter.Select(m =>
            {
                return new SISVN_TransporterDto()
                {
                    CreatedOnUtc = m.CreatedOnUtc,
                    Deleted = m.Deleted,
                    DisplayOrder = m.DisplayOrder,
                    Id = m.Id,
                    Name = m.Name,
                    UpdatedOnUtc = m.UpdatedOnUtc
                };

            }).ToList();

            var orderTransporterRootObject = new SISVN_TransporterRootObject()
            {
                SISVN_Transporters = listTransporterModel
            };

            var json = _jsonFieldsSerializer.Serialize(orderTransporterRootObject, string.Empty);

            return new RawJsonActionResult(json);
        }

        [HttpPost]
        [Route("/api/create_transporter")]
        [ProducesResponseType(typeof(SISVN_TransporterRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ErrorsRootObject), 422)]
        public IActionResult CreateTransporter([ModelBinder(typeof(JsonModelBinder<SISVN_TransporterDto>))] Delta<SISVN_TransporterDto> transporterDelta)
        {
            _orderService.CreateSISVN_Transporter(new SISVN_Transporter()
            {
                CreatedOnUtc = DateTime.Now,
                Deleted = false,
                DisplayOrder = transporterDelta.Dto.DisplayOrder,
                Name = transporterDelta.Dto.Name,
                UpdatedOnUtc = DateTime.Now,
            });

            var orderTransportersRootObject = new SISVN_TransporterRootObject()
            {
                SISVN_Transporters = new List<SISVN_TransporterDto>() { transporterDelta.Dto }
            };

            var json = _jsonFieldsSerializer.Serialize(orderTransportersRootObject, string.Empty);

            return new RawJsonActionResult(json);
        }

        [HttpPost]
        [Route("/api/update_transporter")]
        [ProducesResponseType(typeof(SISVN_TransporterRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ErrorsRootObject), 422)]
        public IActionResult UpdateTransporter([ModelBinder(typeof(JsonModelBinder<SISVN_TransporterDto>))] Delta<SISVN_TransporterDto> transporterDelta)
        {
            _orderService.UpdateSISVN_Transporter(new SISVN_Transporter()
            {
                Deleted = false,
                DisplayOrder = transporterDelta.Dto.DisplayOrder,
                Name = transporterDelta.Dto.Name,
                UpdatedOnUtc = DateTime.Now,
            });

            var orderTransportersRootObject = new SISVN_TransporterRootObject()
            {
                SISVN_Transporters = new List<SISVN_TransporterDto>() { transporterDelta.Dto }
            };

            var json = _jsonFieldsSerializer.Serialize(orderTransportersRootObject, string.Empty);

            return new RawJsonActionResult(json);
        }

        [HttpDelete]
        [Route("/api/delete_transporter/{id}")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ErrorsRootObject), 422)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult DeleteTransporter(int id)
        {
            if (id <= 0)
            {
                return Error(HttpStatusCode.BadRequest, "id", "invalid id");
            }

            _orderService.DeleteSISVN_Transporter(id);
            return new RawJsonActionResult(true);
        }

        #region Order Notes
        /// <summary>
        /// [TuanDang][19/12/2018][Tìm kiếm ghi chú]
        /// [HienNguyen][Tìm kiếm ghi chú]
        /// </summary>
        /// <param name="keyword"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("/api/orders/{orderId}/notes")]
        [ProducesResponseType(typeof(OrderNoteListModel), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult SearchOrderNote(int orderId, int pageNumber = 1, int pageSize = int.MaxValue)
        {
            if (!Authorize(StandardPermissionProvider.ManageOrders))
            {
                return Error(HttpStatusCode.BadRequest, "", "Unauthorized orders");
            }
            var order = _orderService.GetOrderById(orderId);

            if (order == null || order.Deleted)
            {
                return Error(HttpStatusCode.NotFound, "order", "not found");
            }
            if (pageNumber < 1) pageNumber = 1;
            if (pageSize <= 0) pageSize = int.MaxValue;
            var notes = order.OrderNotes.OrderByDescending(n => n.CreatedOnUtc)
                .Select(n => new OrderNoteDto
                {
                    CreatedOn = n.CreatedOnUtc.Format(_dateTimeHelper, "dd/MM/yyyy hh:mm:ss"),
                    Note = n.Note,
                    Id = n.Id
                })
                .ToList();
            var list = new PagedList<OrderNoteDto>(notes, pageNumber - 1, pageSize);

            var result = new OrderNoteListModel()
            {
                Notes = list,
                PageNumber = list.PageIndex + 1,
                PageSize = list.PageSize,
                TotalPage = list.TotalPages,
                TotalRow = list.TotalCount
            };

            var json = _jsonFieldsSerializer.Serialize(result, string.Empty);

            return new RawJsonActionResult(json);
        }

        /// <summary>
        /// [TuanDang][19/12/2018]
        /// [HienNguyen][Tạo mới ghi chú]
        /// </summary>
        /// <param name="orderDelta"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("/api/orders/{orderId}/notes")]
        [ProducesResponseType(typeof(ResultObjectModel<>), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ErrorsRootObject), 422)]
        public IActionResult CreateOrderNote(int orderId, [ModelBinder(typeof(JsonModelBinder<OrderNoteDto>))] Delta<OrderNoteDto> orderNoteDelta)
        {
            if (!Authorize(StandardPermissionProvider.ManageOrders))
            {
                return Error(HttpStatusCode.BadRequest, "", "Unauthorized orders");
            }
            var order = _orderService.GetOrderById(orderId);

            if (order == null || order.Deleted)
            {
                return Error(HttpStatusCode.NotFound, "order", "not found");
            }
            // Here we display the errors if the validation has failed at some point.
            if (!ModelState.IsValid)
            {
                return Error();
            }
            var note = new OrderNote
            {
                CreatedOnUtc = DateTime.UtcNow,
                Note = orderNoteDelta.Dto.Note,
                DisplayToCustomer = false
            };
            order.OrderNotes.Add(note);
            _orderService.UpdateOrder(order);

            var result = new ResultObjectModel<int>()
            {
                Data = note.Id,
                Message = _localizationService.GetResource("Order.Notes.AddSuccess"),
                Success = true
            };

            var json = _jsonFieldsSerializer.Serialize(result, string.Empty);

            return new RawJsonActionResult(json);
        }

        /// <summary>
        /// [TuanDang][19/12/2018]
        /// </summary>
        /// <returns></returns>
        [HttpDelete]
        [Route("/api/orders/{orderId}/notes/{noteId}")]
        [ProducesResponseType(typeof(ResultObjectModel<>), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ErrorsRootObject), 422)]
        public IActionResult DeleteOrderNote(int orderId, int noteId)
        {
            if (!Authorize(StandardPermissionProvider.ManageOrders))
            {
                return Error(HttpStatusCode.BadRequest, "", "Unauthorized orders");
            }
            var order = _orderService.GetOrderById(orderId);

            if (order == null || order.Deleted)
            {
                return Error(HttpStatusCode.NotFound, "order", "not found");
            }
            var orderNote = order.OrderNotes.Where(n => n.Id == noteId).FirstOrDefault();

            if (orderNote == null)
            {
                return Error(HttpStatusCode.NotFound, "orderNote", "not found");
            }
            _orderService.DeleteOrderNote(orderNote);

            var result = new ResultObjectModel<int>()
            {
                Message = _localizationService.GetResource("Order.Notes.DeleteSuccess"),
                Success = true
            };

            var json = _jsonFieldsSerializer.Serialize(result, string.Empty);

            return new RawJsonActionResult(json);
        }
        #endregion

        #region Order Payments
        /// <summary>
        /// [TuanDang][19/12/2018]
        /// </summary>
        /// <param name="keyword"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("/api/orders/{orderId}/payments")]
        [ProducesResponseType(typeof(OrderPaymentListModel), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult SearchOrderPayment(int orderId, int pageNumber = 1, int pageSize = int.MaxValue)
        {
            if (!Authorize(StandardPermissionProvider.ManageOrders))
            {
                return Error(HttpStatusCode.BadRequest, "", "Unauthorized orders");
            }
            var order = _orderService.GetOrderById(orderId);

            if (order == null || order.Deleted)
            {
                return Error(HttpStatusCode.NotFound, "order", "not found");
            }
            if (pageNumber < 1) pageNumber = 1;
            if (pageSize <= 0) pageSize = int.MaxValue;
            var payments = order.SISVN_OrderPaymentBalances.OrderByDescending(p => p.PaymentDateUtc)
                .Select(p =>
                {
                    var item = new OrderPaymentHistoryModel
                    {
                        Id = p.Id,
                        Amount = p.PaymentAmount.ToModel(_priceFormatter),
                        DatePaid = p.PaymentDateUtc.Format(_dateTimeHelper, "dd/MM/yyyy hh:mm:ss"),
                        Payment = new PaymentModel
                        {
                            SystemName = p.PaymentMethodSystemName
                        }
                    };
                    var pm = _paymentService.LoadPaymentMethodBySystemName(p.PaymentMethodSystemName);
                    if (pm != null)
                    {
                        item.Payment.Name = pm.PluginDescriptor.FriendlyName;
                    }
                    return item;
                }).ToList();
            var list = new PagedList<OrderPaymentHistoryModel>(payments, pageNumber - 1, pageSize);

            var result = new OrderPaymentListModel()
            {
                Payments = list,
                PageNumber = list.PageIndex + 1,
                PageSize = list.PageSize,
                TotalPage = list.TotalPages,
                TotalRow = list.TotalCount
            };

            var json = _jsonFieldsSerializer.Serialize(result, string.Empty);

            return new RawJsonActionResult(json);
        }

        /// <summary>
        /// [TuanDang][19/12/2018]
        /// </summary>
        /// <param name="orderDelta"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("/api/orders/{orderId}/payments")]
        [ProducesResponseType(typeof(ResultObjectModel<>), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ErrorsRootObject), 422)]
        public IActionResult CreateOrderPayment(int orderId, [ModelBinder(typeof(JsonModelBinder<OrderPaymentDto>))] Delta<OrderPaymentDto> paymentDelta)
        {
            if (!Authorize(StandardPermissionProvider.ManageOrders))
            {
                return Error(HttpStatusCode.BadRequest, "", "Unauthorized orders");
            }
            var order = _orderService.GetOrderById(orderId);

            if (order == null || order.Deleted)
            {
                return Error(HttpStatusCode.NotFound, "order", "not found");
            }
            // Here we display the errors if the validation has failed at some point.
            if (!ModelState.IsValid)
            {
                return Error();
            }
            var payment = new SISVN_OrderPaymentBalance
            {
                OrderId = orderId,
                PaymentAmount = RoundingHelper.RoundPrice(paymentDelta.Dto.PaymentAmount),
                PaymentDateUtc = DateTime.UtcNow,
                PaymentMethodSystemName = paymentDelta.Dto.PaymentSystemName
            };
            order.SISVN_OrderPaymentBalances.Add(payment);
            _orderService.UpdateOrder(order);


            order.OrderPaid = RoundingHelper.RoundPrice(order.SISVN_OrderPaymentBalances.Sum(p => p.PaymentAmount));
            order.OrderDebt = RoundingHelper.RoundPrice(order.OrderTotal - order.OrderPaid.Value);
            order.PaidDateUtc = DateTime.UtcNow;

            if (order.OrderPaid >= order.OrderTotal)
            {
                order.PaymentStatus = PaymentStatus.Paid;
            }
            else if (order.OrderPaid > 0)
            {
                order.PaymentStatus = PaymentStatus.PartiallyPaid;
            }
            else
            {
                order.PaymentStatus = PaymentStatus.Pending;
            }
            _orderService.UpdateOrder(order);
            _orderProcessingService.CheckOrderStatus(order);

            var result = new ResultObjectModel<int>()
            {
                Data = payment.Id,
                Message = _localizationService.GetResource("Order.Payments.AddSuccess"),
                Success = true
            };

            var json = _jsonFieldsSerializer.Serialize(result, string.Empty);

            return new RawJsonActionResult(json);
        }

        /// <summary>
        /// [TuanDang][19/12/2018]
        /// </summary>
        /// <returns></returns>
        [HttpDelete]
        [Route("/api/orders/{orderId}/payments/{paymentId}")]
        [ProducesResponseType(typeof(ResultObjectModel<>), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ErrorsRootObject), 422)]
        public IActionResult DeleteOrderPayments(int orderId, int paymentId)
        {
            if (!Authorize(StandardPermissionProvider.ManageOrders))
            {
                return Error(HttpStatusCode.BadRequest, "", "Unauthorized orders");
            }
            var order = _orderService.GetOrderById(orderId);

            if (order == null || order.Deleted)
            {
                return Error(HttpStatusCode.NotFound, "order", "not found");
            }
            var payment = order.SISVN_OrderPaymentBalances.Where(n => n.Id == paymentId).FirstOrDefault();

            if (payment == null)
            {
                return Error(HttpStatusCode.NotFound, "payment", "not found");
            }
            _orderService.DeleteOrderPaymentItem(payment);


            order.OrderPaid = RoundingHelper.RoundPrice(order.SISVN_OrderPaymentBalances.Sum(p => p.PaymentAmount));
            order.OrderDebt = RoundingHelper.RoundPrice(order.OrderTotal - order.OrderPaid.Value);
            order.PaidDateUtc = DateTime.UtcNow;

            if (order.OrderPaid >= order.OrderTotal)
            {
                order.PaymentStatus = PaymentStatus.Paid;
            }
            else if (order.OrderPaid > 0)
            {
                order.PaymentStatus = PaymentStatus.PartiallyPaid;
            }
            else
            {
                order.PaymentStatus = PaymentStatus.Pending;
            }
            _orderService.UpdateOrder(order);
            _orderProcessingService.CheckOrderStatus(order);

            var result = new ResultObjectModel<int>()
            {
                Message = _localizationService.GetResource("Order.Payments.DeleteSuccess"),
                Success = true
            };

            var json = _jsonFieldsSerializer.Serialize(result, string.Empty);

            return new RawJsonActionResult(json);
        }
        #endregion

        #region Order Shipping
        /// <summary>
        /// [TuanDang][19/12/2018][Tìm kiếm ghi chú]
        /// [HienNguyen][Tìm kiếm ghi chú]
        /// </summary>
        /// <param name="keyword"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("/api/orders/{orderId}/shippings")]
        [ProducesResponseType(typeof(OrderNoteListModel), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult SearchOrderShippings(int orderId, int pageNumber = 1, int pageSize = int.MaxValue)
        {
            if (!Authorize(StandardPermissionProvider.ManageOrders))
            {
                return Error(HttpStatusCode.BadRequest, "", "Unauthorized orders");
            }
            var order = _orderService.GetOrderById(orderId);

            if (order == null || order.Deleted)
            {
                return Error(HttpStatusCode.NotFound, "order", "not found");
            }
            if (pageNumber < 1) pageNumber = 1;
            if (pageSize <= 0) pageSize = int.MaxValue;
            var shipments = order.Shipments.OrderByDescending(n => n.CreatedOnUtc)
                .Select(n => new OrderShipmentModel
                {
                    Id = n.Id,
                    Weight = n.TotalWeight,
                    TrackingNumber = n.TrackingNumber,
                    DateDeliveried = n.DeliveryDateUtc?.Format(_dateTimeHelper, "dd/MM/yyyy hh:mm:ss"),
                    DateShipped = n.ShippedDateUtc?.Format(_dateTimeHelper, "dd/MM/yyyy hh:mm:ss")
                })
                .ToList();
            var list = new PagedList<OrderShipmentModel>(shipments, pageNumber - 1, pageSize);

            var result = new OrderShipmentListModel()
            {
                Shipments = list,
                PageNumber = list.PageIndex + 1,
                PageSize = list.PageSize,
                TotalPage = list.TotalPages,
                TotalRow = list.TotalCount
            };

            var json = _jsonFieldsSerializer.Serialize(result, string.Empty);

            return new RawJsonActionResult(json);
        }

        /// <summary>
        /// [TuanDang][19/12/2018]
        /// </summary>
        /// <param name="orderDelta"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("/api/orders/{orderId}/shippings")]
        [ProducesResponseType(typeof(ResultObjectModel<>), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ErrorsRootObject), 422)]
        public IActionResult CreateOrderShipment(int orderId, [ModelBinder(typeof(JsonModelBinder<OrderShipmentDto>))] Delta<OrderShipmentDto> shipmentDelta)
        {
            if (!Authorize(StandardPermissionProvider.ManageOrders))
            {
                return Error(HttpStatusCode.BadRequest, "", "Unauthorized orders");
            }
            var order = _orderService.GetOrderById(orderId);

            if (order == null || order.Deleted)
            {
                return Error(HttpStatusCode.NotFound, "order", "not found");
            }
            // Here we display the errors if the validation has failed at some point.
            if (!ModelState.IsValid)
            {
                return Error();
            }
            var shipment = new Shipment
            {
                DeliveryDateUtc = shipmentDelta.Dto.DateDeliveried,
                ShippedDateUtc = shipmentDelta.Dto.DateShipped,
                TotalWeight = shipmentDelta.Dto.Weight,
                TrackingNumber = shipmentDelta.Dto.TrackingNumber,
                CreatedOnUtc = DateTime.UtcNow
            };
            order.Shipments.Add(shipment);
            _orderService.UpdateOrder(order);

            foreach (var item in shipmentDelta.Dto.Items)
            {
                foreach (var orderItem in order.OrderItems)
                {
                    if (orderItem.ProductId == item.ProductId)
                    {
                        shipment.ShipmentItems.Add(new ShipmentItem
                        {
                            OrderItemId = orderItem.Id,
                            Quantity = item.Quantity,
                            WarehouseId = item.WarehouseId
                        });
                    }
                }
            }
            _orderService.UpdateOrder(order);
            _orderProcessingService.CheckOrderStatus(order);

            var result = new ResultObjectModel<int>()
            {
                Data = shipment.Id,
                Message = _localizationService.GetResource("Order.Shipments.AddSuccess"),
                Success = true
            };

            var json = _jsonFieldsSerializer.Serialize(result, string.Empty);

            return new RawJsonActionResult(json);
        }

        /// <summary>
        /// [TuanDang][19/12/2018]
        /// </summary>
        /// <returns></returns>
        [HttpDelete]
        [Route("/api/orders/{orderId}/shippings/{shipmentId}")]
        [ProducesResponseType(typeof(ResultObjectModel<>), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ErrorsRootObject), 422)]
        public IActionResult DeleteOrderShipment(int orderId, int shipmentId)
        {
            if (!Authorize(StandardPermissionProvider.ManageOrders))
            {
                return Error(HttpStatusCode.BadRequest, "", "Unauthorized orders");
            }
            var order = _orderService.GetOrderById(orderId);

            if (order == null || order.Deleted)
            {
                return Error(HttpStatusCode.NotFound, "order", "not found");
            }
            var shipment = order.Shipments.Where(n => n.Id == shipmentId).FirstOrDefault();

            if (shipment == null)
            {
                return Error(HttpStatusCode.NotFound, "shipment", "not found");
            }
            _shipmentService.DeleteShipment(shipment);
            _orderProcessingService.CheckOrderStatus(order);

            var result = new ResultObjectModel<int>()
            {
                Message = _localizationService.GetResource("Order.Shipments.DeleteSuccess"),
                Success = true
            };

            var json = _jsonFieldsSerializer.Serialize(result, string.Empty);

            return new RawJsonActionResult(json);
        }
        #endregion

        /// <summary>
        /// [TuanDang][15/12/2018][Hàm tìm kiếm đơn hàng]
        /// [HienNguyen][20/07/2018][Hàm tìm kiếm đơn hàng]
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("/api/orders")]
        [ProducesResponseType(typeof(OrderListModel), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult GetOrders(OrdersParametersModel parameters)
        {
            if (!Authorize(StandardPermissionProvider.ManageOrders))
            {
                return Error(HttpStatusCode.BadRequest, "", "Unauthorized orders");
            }

            if (parameters.PageNumber <= 0)
            {
                return Error(HttpStatusCode.BadRequest, "page", "Invalid page parameter");
            }

            if (parameters.PageSize < Configurations.MinLimit || parameters.PageSize > Configurations.MaxLimit)
            {
                return Error(HttpStatusCode.BadRequest, "page", "Invalid limit parameter");
            }

            //*TODO* get current storeid
            //var storeId = _storeContext.CurrentStore.Id;

            IPagedList<Order> orders = _orderService.SearchOrders(parameters.Keyword,
                                                                parameters.StoreId,
                                                                0, //vendor
                                                                parameters.CustomerId ?? 0,
                                                                0, 0, 0, 0, null,
                                                                parameters.FromDate,
                                                                parameters.ToDate,
                                                                parameters.OrderStatusIds,
                                                                parameters.OrderPaymentIds,
                                                                parameters.OrderShipmentIds,
                                                                null, null, null,
                                                                parameters.PageNumber - 1,
                                                                parameters.PageSize,
                                                                parameters.OrderType);

            var model = _modelConverter.ToModel(orders);

            var json = _jsonFieldsSerializer.Serialize(model, parameters.Fields);

            return new RawJsonActionResult(json);
        }

        /// <summary>
        /// [HienNguyen][20/07/2018][Hàm xuất excel dữ liệu đơn hàng]
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("/api/exportexcelorders")]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult ExportExcelOrders(OrdersParametersModel parameters)
        {
            var storeId = _storeContext.CurrentStore.Id;

            IPagedList<Order> orders = _orderService.SearchOrdersCustom(parameters.Keyword,
                                                                storeId,
                                                                parameters.OrderStatusIds,
                                                                parameters.OrderTypeIds,
                                                                parameters.FromDate,
                                                                parameters.ToDate,
                                                                parameters.PageIndex,
                                                                parameters.PageSize);

            IList<Store> allStores = _storeService.GetAllStores();

            IList<OrderDto> listOrderModel = orders.Select(m => _dtoHelper.PrepareOrderDTO(m)).ToList();

            var ordersRootObject = new OrdersRootObject()
            {
                Orders = listOrderModel,
                TotalRow = orders.TotalCount,
                TotalCountOrder = _orderService.GetTotalCountOrder(),
                TotalAmount = 0,
                TotalDebtAmount = 0,
            };

            var json = _jsonFieldsSerializer.Serialize(ordersRootObject, parameters.Fields);

            return new RawJsonActionResult(json);
        }

        /// <summary>
        /// [HienNguyen][20/07/2018][Hàm lấy dữ liệu trạng thái đơn hàng]
        /// </summary>
        /// <param name="orderstatusids"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("/api/get_order_status")]
        [ProducesResponseType(typeof(DTOs.Orders.OrderStatusModel), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult GetOrderStatus(int[] orderStatusIds = null)
        {
            DTOs.Orders.OrderStatusModel orderStatus = new DTOs.Orders.OrderStatusModel();

            orderStatus.OrderStatus = Enum.GetValues(typeof(Core.Domain.Orders.OrderStatus))
                                .Cast<Core.Domain.Orders.OrderStatus>()
                                .Select(v => new DTOs.Orders.OrderStatus() { OrderStatusId = (int)v, OrderStatusName = CommonHelper.GetEnumDescription(v) })
                                .ToList();

            if (orderStatusIds != null && orderStatusIds.Any())
            {
                orderStatus.OrderStatus = orderStatus.OrderStatus.Where(m => orderStatusIds.Contains(m.OrderStatusId)).ToList();
            }

            var json = _jsonFieldsSerializer.Serialize(orderStatus, string.Empty);
            return new RawJsonActionResult(orderStatus);
        }

        /// <summary>
        /// [HienNguyen][20/07/2018][Hàm lấy dữ liệu loại đơn hàng]
        /// </summary>
        /// <param name="orderstatusids"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("/api/get_order_type")]
        [ProducesResponseType(typeof(DTOs.Orders.OrderTypeModel), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult GetOrderType(int[] orderTypeIds = null)
        {
            DTOs.Orders.OrderTypeModel orderType = new DTOs.Orders.OrderTypeModel();

            orderType.OrderType = Enum.GetValues(typeof(Core.Domain.Orders.OrderType))
                                .Cast<Core.Domain.Orders.OrderType>()
                                .Where(s => !s.ToString().StartsWith(Constant.KeyWorkInventory)) //=> Loại bỏ những type dùng cho module kho.
                                .Select(v => new DTOs.Orders.OrderType() { OrderTypeId = (int)v, OrderTypeName = CommonHelper.GetEnumDescription(v) })
                                .ToList();

            if (orderTypeIds != null && orderTypeIds.Any())
            {
                orderType.OrderType = orderType.OrderType.Where(m => orderTypeIds.Contains(m.OrderTypeId)).ToList();
            }

            var json = _jsonFieldsSerializer.Serialize(orderType, string.Empty);
            return new RawJsonActionResult(orderType);
        }

        /// <summary>
        /// [HienNguyen][20/07/2018][Hàm lấy dữ liệu hình thức thanh toán]
        /// </summary>
        /// <param name="orderstatusids"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("/api/get_payment_method")]
        [ProducesResponseType(typeof(DTOs.Orders.OrderTypeModel), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult GetPaymentMethod(int[] paymentMethodIds = null)
        {
            DTOs.Orders.PaymentMethodModel paymentMethod = new DTOs.Orders.PaymentMethodModel();

            paymentMethod.PaymentMethod = Enum.GetValues(typeof(Core.Domain.Orders.PaymentMethod))
                                .Cast<Core.Domain.Orders.PaymentMethod>()
                                .Select(v => new DTOs.Orders.PaymentMethod() { PaymentMethodId = (int)v, PaymentMethodName = CommonHelper.GetEnumDescription(v) })
                                .ToList();

            if (paymentMethodIds != null && paymentMethodIds.Any())
            {
                paymentMethod.PaymentMethod = paymentMethod.PaymentMethod.Where(m => paymentMethodIds.Contains(m.PaymentMethodId)).ToList();
            }

            var json = _jsonFieldsSerializer.Serialize(paymentMethod, string.Empty);
            return new RawJsonActionResult(paymentMethod);
        }

        /// <summary>
        /// [Tuandang][16/12/2018][Huy đơn hàng]
        /// [HienNguyen][20/07/2018][Hủy đơn hàng]
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("/api/orders/{id}/cancel")]
        [ProducesResponseType(typeof(ResultObjectModel<>), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult CancelOrder(int id)
        {
            if (!Authorize(StandardPermissionProvider.ManageOrders))
            {
                return Error(HttpStatusCode.BadRequest, "", "Unauthorized orders");
            }
            if (id <= 0)
            {
                return Error(HttpStatusCode.BadRequest, "id", "invalid id");
            }

            var order = _orderService.GetOrderById(id);
            if (order == null)
            {
                return Error(HttpStatusCode.BadRequest, "order", "not found");
            }
            if (_orderProcessingService.CanCancelOrder(order))
            {
                _orderProcessingService.CancelOrder(order, false);
            }
            var result = new ResultObjectModel<int>
            {
                Message = _localizationService.GetResource("Orders.Cancel.Success"),
                Success = true,
                Data = id
            };
            return new RawJsonActionResult(_jsonFieldsSerializer.Serialize(result, string.Empty));
        }

        /// <summary>
        /// [TuanDang][16/12/2018][Xóa đơn hàng]
        /// [HienNguyen][20/07/2018][Xóa đơn hàng]
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("/api/orders/{id}")]
        [ProducesResponseType(typeof(ResultObjectModel<>), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ErrorsRootObject), 422)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult DeleteOrder(int id)
        {
            if (!Authorize(StandardPermissionProvider.ManageOrders))
            {
                return Error(HttpStatusCode.BadRequest, "", "Unauthorized orders");
            }
            if (id <= 0)
            {
                return Error(HttpStatusCode.BadRequest, "id", "invalid id");
            }

            var order = _orderService.GetOrderById(id);
            if (order == null)
            {
                return Error(HttpStatusCode.BadRequest, "order", "not found");
            }

            _orderProcessingService.DeleteOrder(order);
            var result = new ResultObjectModel<int>
            {
                Message = _localizationService.GetResource("Orders.Delete.Success"),
                Success = true,
                Data = id
            };
            return new RawJsonActionResult(_jsonFieldsSerializer.Serialize(result, string.Empty));
        }

        /// <summary>
        /// [TuanDang][16/12/2018][Khôi phục đơn hàng]
        /// [HienNguyen][20/07/2018][Khôi phục đơn hàng]
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("/api/orders/{id}/active")]
        [ProducesResponseType(typeof(ResultObjectModel<>), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult ActiveOrder(int id)
        {
            if (id <= 0)
            {
                return Error(HttpStatusCode.BadRequest, "id", "invalid id");
            }

            var order = _orderService.GetOrderById(id);
            if (order == null)
            {
                return Error(HttpStatusCode.BadRequest, "order", "not found");
            }
            order.OrderStatus = Core.Domain.Orders.OrderStatus.Pending;
            _orderService.UpdateOrder(order);

            _orderProcessingService.CheckOrderStatus(order);

            var result = new ResultObjectModel<int>
            {
                Message = _localizationService.GetResource("Orders.Active.Success"),
                Success = true,
                Data = id
            };
            return new RawJsonActionResult(_jsonFieldsSerializer.Serialize(result, string.Empty));
        }

        /// <summary>
        /// [HienNguyen][03/08/2018][Hàm tạo đơn hàng bán hàng]
        /// </summary>
        /// <param name="SaleorderDelta"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("/api/create_sale_order")]
        [ProducesResponseType(typeof(OrdersRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ErrorsRootObject), 422)]
        public IActionResult CreateSaleOrder([ModelBinder(typeof(JsonModelBinder<SaleOrderDtoModel>))] Delta<SaleOrderDtoModel> saleOrderDelta)
        {
            // Here we display the errors if the validation has failed at some point.
            if (!ModelState.IsValid)
            {
                return Error();
            }

            //Check validation model
            if (!ValidateSaleOrder(saleOrderDelta.Dto))
            {
                return Error(HttpStatusCode.BadRequest);
            }

            Order newOrder = _orderService.CreateSaleOrder(
                _dtoHelper.PrepareSaleOrderModelEntity(saleOrderDelta.Dto),
                _dtoHelper.PrepareOrderItemEntity(saleOrderDelta.Dto.OrderItems),
                new List<SISVN_OrderPaymentBalance>() { _dtoHelper.PrepareOrderPaymentBalanceEntity(saleOrderDelta.Dto.PaymentMethod) },
                _dtoHelper.PrepareOrderNoteEntity(saleOrderDelta.Dto.OrderNotes));

            _customerActivityService.InsertActivity("AddNewSaleOrder",
                 _localizationService.GetResource("ActivityLog.AddNewSaleOrder"), newOrder.Id);

            OrdersRootObject ordersRootObject = new OrdersRootObject();
            ordersRootObject.Orders.Add(_dtoHelper.PrepareOrderDTO(newOrder));

            var json = _jsonFieldsSerializer.Serialize(ordersRootObject, string.Empty);

            return new RawJsonActionResult(json);
        }

        /// <summary>
        /// [TuanDang][17/12/2018][Hàm tạo mới đơn hàng]
        /// [HienNguyen][28/07/2018][Hàm tạo mới đơn hàng]
        /// </summary>
        /// <param name="orderDelta"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("/api/orders")]
        [ProducesResponseType(typeof(ResultObjectModel<>), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ErrorsRootObject), 422)]
        public IActionResult CreateOrder([ModelBinder(typeof(JsonModelBinder<OrderDto>))] Delta<OrderDto> orderDelta)
        {
            if (!Authorize(StandardPermissionProvider.ManageOrders))
            {
                return Error(HttpStatusCode.BadRequest, "", "Unauthorized orders");
            }
            // Here we display the errors if the validation has failed at some point.
            if (!ModelState.IsValid)
            {
                return Error();
            }

            // We doesn't have to check for value because this is done by the order validator.
            Customer customer = _customerService.GetCustomerById(orderDelta.Dto.CustomerId ?? 0);

            if (customer == null)
            {
                return Error(HttpStatusCode.NotFound, "customer", "not found");
            }

            //address
            if (customer.BillingAddress == null || customer.ShippingAddress == null)
            {
                var firstAddress = customer.Addresses.FirstOrDefault();
                if (firstAddress != null)
                {
                    if (customer.BillingAddress == null)
                    {
                        customer.BillingAddress = firstAddress;
                    }
                    if (customer.ShippingAddress == null)
                    {
                        customer.ShippingAddress = firstAddress;
                    }
                    _customerService.UpdateCustomer(customer);
                }
            }

            bool shippingRequired = false;

            if (orderDelta.Dto.OrderItemDtos != null)
            {
                bool shouldReturnError = ValidateEachOrderItem(orderDelta.Dto.OrderItemDtos);

                if (shouldReturnError)
                {
                    return Error(HttpStatusCode.BadRequest);
                }

                //shouldReturnError = AddOrderItemsToCart(orderDelta.Dto.OrderItemDtos, customer, orderDelta.Dto.StoreId ?? _storeContext.CurrentStore.Id);

                //if (shouldReturnError)
                //{
                //    return Error(HttpStatusCode.BadRequest);
                //}

                shippingRequired = IsShippingAddressRequired(orderDelta.Dto.OrderItemDtos);
            }

            if (shippingRequired)
            {
                bool isValid = true;

                isValid &= SetShippingOption(orderDelta.Dto.Shipping?.SystemName,
                                            orderDelta.Dto.Shipping?.SystemName,
                                            orderDelta.Dto.StoreId ?? _storeContext.CurrentStore.Id,
                                            customer,
                                            BuildShoppingCartItemsFromOrderItemDtos(orderDelta.Dto.OrderItemDtos.ToList(),
                                                                                    customer.Id,
                                                                                    orderDelta.Dto.StoreId ?? _storeContext.CurrentStore.Id));
                AddressDto addressDto = null;
                if (customer.ShippingAddress != null)
                {
                    addressDto = new AddressDto();
                    _mappingHelper.Merge(customer.ShippingAddress, addressDto);
                }
                isValid &= ValidateAddress(addressDto, "customer shipping_address");

                if (!isValid)
                {
                    return Error(HttpStatusCode.BadRequest);
                }
            }

            if (!OrderSettings.DisableBillingAddressCheckoutStep)
            {
                AddressDto addressDto = null;
                if (customer.BillingAddress != null)
                {
                    addressDto = new AddressDto();
                    _mappingHelper.Merge(customer.BillingAddress, addressDto);
                }
                bool isValid = ValidateAddress(orderDelta.Dto.BillingAddress, "customer billing_address");

                if (!isValid)
                {
                    return Error(HttpStatusCode.BadRequest);
                }
            }

            //Order newOrder = _factory.Initialize();
            //orderDelta.Merge(newOrder);
            var newOrder = new Order
            {
                OrderGuid = Guid.NewGuid(),
                Deleted = false,
                CreatedOnUtc = orderDelta.Dto.CreatedOnUtc.HasValue ? _dateTimeHelper.ConvertToUtcTime(orderDelta.Dto.CreatedOnUtc.Value) : DateTime.UtcNow,
                CustomerId = customer.Id,
                SellerId = orderDelta.Dto.SellerId ?? 0,
                StoreId = orderDelta.Dto.StoreId ?? 0,
                OrderStatus = Core.Domain.Orders.OrderStatus.Pending,
                ShippingStatus = ShippingStatus.NotYetShipped,
                PaymentStatus = PaymentStatus.Pending,
                OrderTax = RoundingHelper.RoundPrice(orderDelta.Dto.OrderTax ?? 0)
            };
            newOrder.OrderSubTotalDiscountInclTax = newOrder.OrderSubTotalDiscountExclTax
                = newOrder.OrderSubtotalExclTax
                = newOrder.OrderSubtotalInclTax
                = RoundingHelper.RoundPrice(orderDelta.Dto.OrderSubTotalDiscountInclTax ?? 0);

            newOrder.OrderShippingInclTax = newOrder.OrderShippingExclTax = RoundingHelper.RoundPrice(orderDelta.Dto.OrderShippingInclTax ?? 0);

            //items
            var subtotalTemp = decimal.Zero;
            foreach (var p in orderDelta.Dto.OrderItemDtos)
            {
                var orderItem = new OrderItem
                {
                    ProductId = p.ProductId ?? 0,
                    PriceInclTax = RoundingHelper.RoundPrice(p.PriceInclTax ?? 0),
                    PriceExclTax = RoundingHelper.RoundPrice(p.PriceInclTax ?? 0),
                    Quantity = p.Quantity ?? 0
                };
                orderItem.UnitPriceInclTax = orderItem.UnitPriceExclTax = RoundingHelper.RoundPrice(p.UnitPriceInclTax ?? 0);
                orderItem.DiscountAmountExclTax = orderItem.DiscountAmountInclTax = RoundingHelper.RoundPrice(p.DiscountAmountInclTax ?? 0);

                newOrder.OrderItems.Add(orderItem);
                if (orderItem.PriceInclTax == decimal.Zero && orderItem.UnitPriceInclTax != decimal.Zero)
                {
                    orderItem.PriceInclTax = orderItem.PriceExclTax = RoundingHelper.RoundPrice(orderItem.UnitPriceInclTax * orderItem.Quantity - orderItem.DiscountAmountInclTax);
                }
                subtotalTemp += orderItem.PriceInclTax;
            }
            if (!orderDelta.Dto.OrderSubTotalDiscountInclTax.HasValue)
            {
                newOrder.OrderSubTotalDiscountInclTax = newOrder.OrderSubTotalDiscountExclTax
                = newOrder.OrderSubtotalExclTax
                = newOrder.OrderSubtotalInclTax
                = RoundingHelper.RoundPrice(subtotalTemp);
            }

            //total
            var orderTotalTemp = newOrder.OrderSubTotalDiscountInclTax + newOrder.OrderShippingInclTax + newOrder.OrderTax;
            var discountTemp = decimal.Zero;
            if (orderDelta.Dto.OrderDiscount2 != null && orderDelta.Dto.OrderDiscount2.Value > 0)
            {
                if (orderDelta.Dto.OrderDiscount2.IsPercent)
                {
                    var discountAmount = (orderTotalTemp * orderDelta.Dto.OrderDiscount2.Value) / 100;
                    discountTemp = discountAmount;
                }
                else
                {
                    discountTemp = orderDelta.Dto.OrderDiscount2.Value;
                }
            }
            orderTotalTemp -= discountTemp;

            newOrder.OrderTotal = orderTotalTemp > 0 ? RoundingHelper.RoundPrice(orderTotalTemp) : 0;
            newOrder.OrderDiscount = RoundingHelper.RoundPrice(discountTemp);
            //notes
            foreach (var note in orderDelta.Dto.Notes)
            {
                newOrder.OrderNotes.Add(new OrderNote
                {
                    CreatedOnUtc = DateTime.UtcNow,
                    DisplayToCustomer = false,
                    Note = note
                });
            }

            //payments
            decimal totalPaymentAmount = decimal.Zero;
            foreach (var payment in orderDelta.Dto.PaymentHistory)
            {
                newOrder.SISVN_OrderPaymentBalances.Add(new SISVN_OrderPaymentBalance
                {
                    PaymentAmount = RoundingHelper.RoundPrice(payment.PaymentAmount),
                    PaymentMethodSystemName = payment.PaymentSystemName,
                    PaymentDateUtc = _dateTimeHelper.ConvertToUtcTime(payment.PaymentDate ?? DateTime.Now)
                });
                totalPaymentAmount += payment.PaymentAmount;
            }
            if (totalPaymentAmount >= newOrder.OrderTotal)
            {
                newOrder.PaymentStatus = PaymentStatus.Paid;
            }
            else if (totalPaymentAmount > 0)
            {
                newOrder.PaymentStatus = PaymentStatus.PartiallyPaid;
            }
            else
            {
                newOrder.PaymentStatus = PaymentStatus.Pending;
            }
            if (totalPaymentAmount > 0)
            {
                newOrder.OrderPaid = RoundingHelper.RoundPrice(totalPaymentAmount);
                newOrder.OrderDebt = RoundingHelper.RoundPrice(newOrder.OrderTotal - totalPaymentAmount);
                newOrder.PaidDateUtc = DateTime.UtcNow;
            }

            //address
            //customer.BillingAddress = newOrder.BillingAddress;
            //customer.ShippingAddress = newOrder.ShippingAddress;
            newOrder.BillingAddressId = customer.BillingAddress.Id;
            newOrder.ShippingAddressId = customer.ShippingAddress.Id;

            //order code
            if (!string.IsNullOrEmpty(orderDelta.Dto.CustomerOrderNumber))
            {
                newOrder.CustomOrderNumber = orderDelta.Dto.CustomerOrderNumber;
            }
            else
            {
                newOrder.CustomOrderNumber = _customNumberFormatter.GenerateOrderCustomNumber(newOrder);
            }
            _orderService.InsertOrder(newOrder);

            //shipping
            if (orderDelta.Dto.Shipping != null)
            {
                newOrder.ShippingMethod = newOrder.ShippingRateComputationMethodSystemName = orderDelta.Dto.Shipping.SystemName;

                foreach (var sm in orderDelta.Dto.Shipments)
                {
                    var shipment = new Shipment
                    {
                        CreatedOnUtc = DateTime.UtcNow,
                        DeliveryDateUtc = sm.DateDeliveried.HasValue ? _dateTimeHelper.ConvertToUtcTime(sm.DateDeliveried.Value) : (DateTime?)null,
                        ShippedDateUtc = sm.DateShipped.HasValue ? _dateTimeHelper.ConvertToUtcTime(sm.DateShipped.Value) : (DateTime?)null,
                        TotalWeight = sm.Weight,
                        TrackingNumber = sm.TrackingNumber
                    };
                    foreach (var item in sm.Items)
                    {
                        foreach (var orderItem in newOrder.OrderItems)
                        {
                            if (orderItem.ProductId == item.ProductId)
                            {
                                shipment.ShipmentItems.Add(new ShipmentItem
                                {
                                    OrderItemId = orderItem.Id,
                                    Quantity = item.Quantity,
                                    WarehouseId = item.WarehouseId
                                });
                            }
                        }
                    }
                    newOrder.Shipments.Add(shipment);
                }
            }
            if (newOrder.HasItemsToAddToShipment())
            {
                if (newOrder.HasItemsToShip())
                {
                    if (newOrder.HasItemsToDeliver())
                    {
                        newOrder.ShippingStatus = ShippingStatus.PartiallyShipped;
                    }
                    else
                    {
                        newOrder.ShippingStatus = ShippingStatus.NotYetShipped;
                    }
                }
                else
                {
                    if (newOrder.HasItemsToDeliver())
                    {
                        newOrder.ShippingStatus = ShippingStatus.Shipped;
                    }
                    else
                    {
                        newOrder.ShippingStatus = ShippingStatus.Delivered;
                    }
                }
            }
            else
            {
                newOrder.ShippingStatus = ShippingStatus.ShippingNotRequired;
            }
            _orderProcessingService.CheckOrderStatus(newOrder);
            _orderService.UpdateOrder(newOrder);

            //raise event       
            _eventPublisher.Publish(new OrderPlacedEvent(newOrder));

            //*TODO*
            //if (newOrder.PaymentStatus == PaymentStatus.Paid)
            //    ProcessOrderPaid(newOrder);
            ////notifications
            //_orderProcessingService.SendNotificationsAndSaveNotes(newOrder);

            _customerActivityService.InsertActivity("AddNewOrder",
                 _localizationService.GetResource("ActivityLog.AddNewOrder"), newOrder.Id);

            var result = new ResultObjectModel<int>
            {
                Message = _localizationService.GetResource("Orders.Add.Success"),
                Success = true,
                Data = newOrder.Id
            };

            var json = _jsonFieldsSerializer.Serialize(result, string.Empty);

            return new RawJsonActionResult(json);
        }

        /// <summary>
        /// [TuanDang][19/12/2018][Hàm cập nhật đơn hàng]
        /// [HienNguyen][28/07/2018][Hàm cập nhật đơn hàng]
        /// </summary>
        /// <param name="orderDelta"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("/api/orders/{id}")]
        [ProducesResponseType(typeof(ResultObjectModel<>), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ErrorsRootObject), 422)]
        public IActionResult UpdateOrder(int id, [ModelBinder(typeof(JsonModelBinder<OrderDto>))] Delta<OrderDto> orderDelta)
        {
            if (!Authorize(StandardPermissionProvider.ManageOrders))
            {
                return Error(HttpStatusCode.BadRequest, "", "Unauthorized orders");
            }
            var order = _orderService.GetOrderById(id);
            var oldCustomerId = order.CustomerId;

            if (order == null || order.Deleted)
            {
                return Error(HttpStatusCode.NotFound, "order", "not found");
            }
            // Here we display the errors if the validation has failed at some point.
            if (!ModelState.IsValid)
            {
                return Error();
            }

            Customer customer = null;
            if (orderDelta.Dto.CustomerId.HasValue)
            {
                customer = _customerService.GetCustomerById(orderDelta.Dto.CustomerId.Value);

                if (customer == null)
                {
                    return Error(HttpStatusCode.NotFound, "customer", "not found");
                }
                else
                {
                    order.CustomerId = customer.Id;
                }
            }
            else
            {
                customer = order.Customer;
            }

            //address
            if (customer.Id != oldCustomerId)
            {
                if (customer.BillingAddress == null || customer.ShippingAddress == null)
                {
                    var firstAddress = customer.Addresses.FirstOrDefault();
                    if (firstAddress != null)
                    {
                        if (customer.BillingAddress == null)
                        {
                            customer.BillingAddress = firstAddress;
                        }
                        if (customer.ShippingAddress == null)
                        {
                            customer.ShippingAddress = firstAddress;
                        }
                        _customerService.UpdateCustomer(customer);
                    }
                }
            }

            bool shippingRequired = false;

            if (orderDelta.Dto.OrderItemDtos != null)
            {
                bool shouldReturnError = ValidateEachOrderItem(orderDelta.Dto.OrderItemDtos);

                if (shouldReturnError)
                {
                    return Error(HttpStatusCode.BadRequest);
                }

                //shouldReturnError = AddOrderItemsToCart(orderDelta.Dto.OrderItemDtos, customer, orderDelta.Dto.StoreId ?? _storeContext.CurrentStore.Id);

                //if (shouldReturnError)
                //{
                //    return Error(HttpStatusCode.BadRequest);
                //}

                shippingRequired = IsShippingAddressRequired(orderDelta.Dto.OrderItemDtos);
            }

            if (shippingRequired && customer != null)
            {
                bool isValid = true;

                isValid &= SetShippingOption(order.ShippingRateComputationMethodSystemName,
                                            order.ShippingMethod,
                                            orderDelta.Dto.StoreId ?? _storeContext.CurrentStore.Id,
                                            customer,
                                            BuildShoppingCartItemsFromOrderItemDtos(orderDelta.Dto.OrderItemDtos.ToList(),
                                                                                    customer.Id,
                                                                                    orderDelta.Dto.StoreId ?? _storeContext.CurrentStore.Id));
                if (customer.Id != oldCustomerId)
                {
                    AddressDto addressDto = null;
                    if (customer.ShippingAddress != null)
                    {
                        addressDto = new AddressDto();
                        _mappingHelper.Merge(customer.ShippingAddress, addressDto);
                    }
                    isValid &= ValidateAddress(addressDto, "customer shipping_address");
                }
                if (!isValid)
                {
                    return Error(HttpStatusCode.BadRequest);
                }
            }

            if (!OrderSettings.DisableBillingAddressCheckoutStep)
            {
                if (customer.Id != oldCustomerId)
                {
                    AddressDto addressDto = null;
                    if (customer.BillingAddress != null)
                    {
                        addressDto = new AddressDto();
                        _mappingHelper.Merge(customer.BillingAddress, addressDto);
                    }
                    bool isValid = ValidateAddress(orderDelta.Dto.BillingAddress, "customer billing_address");

                    if (!isValid)
                    {
                        return Error(HttpStatusCode.BadRequest);
                    }
                }
            }

            if (orderDelta.Dto.SellerId.HasValue)
            {
                order.SellerId = orderDelta.Dto.SellerId.Value;
            }

            if (orderDelta.Dto.StoreId.HasValue)
            {
                order.StoreId = orderDelta.Dto.StoreId.Value;
            }

            if (orderDelta.Dto.CreatedOnUtc.HasValue)
            {
                order.CreatedOnUtc = _dateTimeHelper.ConvertToUtcTime(orderDelta.Dto.CreatedOnUtc.Value);
            }

            if (orderDelta.Dto.OrderTax.HasValue)
            {
                order.OrderTax = RoundingHelper.RoundPrice(orderDelta.Dto.OrderTax.Value);
            }
            if (orderDelta.Dto.OrderSubTotalDiscountInclTax.HasValue)
            {
                order.OrderSubTotalDiscountInclTax = order.OrderSubTotalDiscountExclTax
                    = order.OrderSubtotalExclTax
                    = order.OrderSubtotalInclTax
                    = RoundingHelper.RoundPrice(orderDelta.Dto.OrderSubTotalDiscountInclTax ?? 0);
            }
            if (orderDelta.Dto.OrderShippingInclTax.HasValue)
            {
                order.OrderShippingInclTax = order.OrderShippingExclTax = RoundingHelper.RoundPrice(orderDelta.Dto.OrderShippingInclTax ?? 0);
            }
            //items
            if (orderDelta.Dto.OrderItemDtos.Count > 0)
            {
                var deletedItems = order.OrderItems.Where(item => !orderDelta.Dto.OrderItemDtos.Any(a => a.Id == 0 || a.Id == item.Id)).ToList();
                for (var i = 0; i < deletedItems.Count; i++)
                {
                    _orderService.DeleteOrderItem(deletedItems[i]);
                }
                
                foreach (var p in orderDelta.Dto.OrderItemDtos)
                {
                    OrderItem orderItem = null;
                    if (p.Id > 0)
                    {
                        orderItem = order.OrderItems.Where(o => o.Id == p.Id).FirstOrDefault();
                        if (orderItem != null)
                        {
                            if (p.PriceInclTax.HasValue)
                            {
                                orderItem.PriceInclTax = orderItem.PriceExclTax = RoundingHelper.RoundPrice(p.PriceInclTax.Value);
                            }
                            if (p.Quantity.HasValue)
                            {
                                orderItem.Quantity = p.Quantity.Value;
                            }
                            if (p.ProductId.HasValue)
                            {
                                orderItem.ProductId = p.ProductId.Value;
                            }
                            if (p.UnitPriceInclTax.HasValue)
                            {
                                orderItem.UnitPriceInclTax = orderItem.UnitPriceExclTax = RoundingHelper.RoundPrice(p.UnitPriceInclTax ?? 0);
                            }
                            if (p.DiscountAmountInclTax.HasValue)
                            {
                                orderItem.DiscountAmountExclTax = orderItem.DiscountAmountInclTax = RoundingHelper.RoundPrice(p.DiscountAmountInclTax ?? 0);
                            }
                        }
                    }
                    else
                    {
                        orderItem = new OrderItem
                        {
                            ProductId = p.ProductId ?? 0,
                            PriceInclTax = RoundingHelper.RoundPrice(p.PriceInclTax ?? 0),
                            PriceExclTax = RoundingHelper.RoundPrice(p.PriceInclTax ?? 0),
                            Quantity = p.Quantity ?? 0
                        };
                        orderItem.UnitPriceInclTax = orderItem.UnitPriceExclTax = p.UnitPriceInclTax ?? 0;
                        orderItem.DiscountAmountExclTax = orderItem.DiscountAmountInclTax = RoundingHelper.RoundPrice(p.DiscountAmountInclTax ?? 0);

                        order.OrderItems.Add(orderItem);
                    }
                    if (orderItem != null && orderItem.PriceInclTax == decimal.Zero && orderItem.UnitPriceInclTax != decimal.Zero)
                    {
                        orderItem.PriceInclTax = orderItem.PriceExclTax = RoundingHelper.RoundPrice(orderItem.UnitPriceInclTax * orderItem.Quantity - orderItem.DiscountAmountInclTax);
                    }
                }
                if (!orderDelta.Dto.OrderSubTotalDiscountInclTax.HasValue)
                {
                    var total = order.OrderItems.Sum(o => o.PriceInclTax);

                    order.OrderSubTotalDiscountInclTax = order.OrderSubTotalDiscountExclTax
                        = order.OrderSubtotalExclTax
                        = order.OrderSubtotalInclTax
                        = RoundingHelper.RoundPrice(total);
                }
            }
            //total
            if (orderDelta.Dto.OrderTotal.HasValue)
            {
                order.OrderTotal = RoundingHelper.RoundPrice(orderDelta.Dto.OrderTotal.Value);
            } else if(orderDelta.Dto.OrderItemDtos.Count > 0)
            {

                order.OrderTotal = order.OrderSubTotalDiscountInclTax + order.OrderShippingInclTax + order.OrderTax;
            }
            if (orderDelta.Dto.OrderDiscount2 != null && orderDelta.Dto.OrderDiscount2.Value > 0)
            {
                var discountTemp = decimal.Zero;
                if (orderDelta.Dto.OrderDiscount2.IsPercent)
                {
                    var discountAmount = (order.OrderTotal * orderDelta.Dto.OrderDiscount2.Value) / 100;
                    discountTemp = discountAmount;
                }
                else
                {
                    discountTemp = orderDelta.Dto.OrderDiscount2.Value;
                }
                var value = order.OrderTotal - discountTemp;
                order.OrderTotal = value > 0 ? RoundingHelper.RoundPrice(value) : 0;
                order.OrderDiscount = RoundingHelper.RoundPrice(discountTemp);
            }

            //notes
            foreach (var note in orderDelta.Dto.Notes)
            {
                order.OrderNotes.Add(new OrderNote
                {
                    CreatedOnUtc = DateTime.UtcNow,
                    DisplayToCustomer = false,
                    Note = note
                });
            }

            //payments
            if (orderDelta.Dto.PaymentHistory.Count > 0)
            {
                decimal totalPaymentAmount = decimal.Zero;
                var deletedItems = order.SISVN_OrderPaymentBalances.Where(item => !orderDelta.Dto.PaymentHistory.Any(a => a.Id == 0 || a.Id == item.Id)).ToList();
                for (var i = 0; i < deletedItems.Count; i++)
                {
                    totalPaymentAmount -= deletedItems[i].PaymentAmount;
                    _orderService.DeleteOrderPaymentItem(deletedItems[i]);
                }
                foreach (var payment in orderDelta.Dto.PaymentHistory)
                {
                    if (payment.Id > 0)
                    {
                        var paymentRecord = order.SISVN_OrderPaymentBalances.Where(r => r.Id == payment.Id).FirstOrDefault();
                        if (paymentRecord != null)
                        {
                            paymentRecord.PaymentAmount = RoundingHelper.RoundPrice(payment.PaymentAmount);
                            paymentRecord.PaymentMethodSystemName = payment.PaymentSystemName;
                            paymentRecord.PaymentDateUtc = _dateTimeHelper.ConvertToUtcTime(payment.PaymentDate ?? DateTime.Now);
                        }
                    }
                    else
                    {
                        order.SISVN_OrderPaymentBalances.Add(new SISVN_OrderPaymentBalance
                        {
                            PaymentAmount = RoundingHelper.RoundPrice(payment.PaymentAmount),
                            PaymentMethodSystemName = payment.PaymentSystemName,
                            PaymentDateUtc = _dateTimeHelper.ConvertToUtcTime(payment.PaymentDate ?? DateTime.Now)
                        });
                    }
                    totalPaymentAmount += payment.PaymentAmount;
                }
                //*TODO* delete payment, subtract totalpayment
                if (orderDelta.Dto.PaymentHistory.Count > 0)
                {
                    order.OrderPaid = RoundingHelper.RoundPrice(totalPaymentAmount);
                    order.OrderDebt = RoundingHelper.RoundPrice(order.OrderTotal - totalPaymentAmount);
                    order.PaidDateUtc = DateTime.UtcNow;

                    if (totalPaymentAmount >= order.OrderTotal)
                    {
                        order.PaymentStatus = PaymentStatus.Paid;
                    }
                    else if (totalPaymentAmount > 0)
                    {
                        order.PaymentStatus = PaymentStatus.PartiallyPaid;
                    }
                    else
                    {
                        order.PaymentStatus = PaymentStatus.Pending;
                    }
                }
            }


            //address
            //customer.BillingAddress = newOrder.BillingAddress;
            //customer.ShippingAddress = newOrder.ShippingAddress;
            if (customer.Id != oldCustomerId)
            {
                order.BillingAddressId = customer.BillingAddress.Id;
                order.ShippingAddressId = customer.ShippingAddress.Id;
            }

            //order code
            if (!string.IsNullOrEmpty(orderDelta.Dto.CustomerOrderNumber))
            {
                order.CustomOrderNumber = orderDelta.Dto.CustomerOrderNumber;
            }

            //shipping
            if (orderDelta.Dto.Shipping != null)
            {
                order.ShippingRateComputationMethodSystemName =
                 order.ShippingMethod = orderDelta.Dto.Shipping.SystemName;
                if (orderDelta.Dto.Shipments.Count > 0)
                {
                    var deletedItems = order.Shipments.Where(item => !orderDelta.Dto.Shipments.Any(a => a.Id == 0 || a.Id == item.Id)).ToList();
                    for (var i = 0; i < deletedItems.Count; i++)
                    {
                        _shipmentService.DeleteShipment(deletedItems[i]);
                    }

                    foreach (var sm in orderDelta.Dto.Shipments)
                    {
                        Shipment shipment = null;
                        if (sm.Id > 0)
                        {
                            shipment = order.Shipments.Where(s => s.Id == sm.Id).FirstOrDefault();
                            if (shipment != null)
                            {
                                if (sm.DateDeliveried.HasValue)
                                {
                                    shipment.DeliveryDateUtc = _dateTimeHelper.ConvertToUtcTime(sm.DateDeliveried.Value);
                                }
                                if (sm.DateShipped.HasValue)
                                {
                                    shipment.ShippedDateUtc = _dateTimeHelper.ConvertToUtcTime(sm.DateShipped.Value);
                                }
                                if (sm.Weight.HasValue)
                                {
                                    shipment.TotalWeight = sm.Weight;
                                }
                                if (!string.IsNullOrEmpty(sm.TrackingNumber))
                                {
                                    shipment.TrackingNumber = sm.TrackingNumber;
                                }
                                foreach (var item in sm.Items)
                                {
                                    foreach (var orderItem in order.OrderItems)
                                    {
                                        if (orderItem.ProductId == item.ProductId)
                                        {
                                            shipment.ShipmentItems.Add(new ShipmentItem
                                            {
                                                OrderItemId = orderItem.Id,
                                                Quantity = item.Quantity,
                                                WarehouseId = item.WarehouseId
                                            });
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            shipment = new Shipment
                            {
                                CreatedOnUtc = DateTime.UtcNow,
                                DeliveryDateUtc = sm.DateDeliveried,
                                ShippedDateUtc = sm.DateShipped,
                                TotalWeight = sm.Weight,
                                TrackingNumber = sm.TrackingNumber
                            };
                            order.Shipments.Add(shipment);
                        }
                        if (shipment != null)
                        {
                            foreach (var item in sm.Items)
                            {
                                foreach (var orderItem in order.OrderItems)
                                {
                                    if (orderItem.ProductId == item.ProductId)
                                    {
                                        shipment.ShipmentItems.Add(new ShipmentItem
                                        {
                                            OrderItemId = orderItem.Id,
                                            Quantity = item.Quantity,
                                            WarehouseId = item.WarehouseId
                                        });
                                    }
                                }
                            }
                        }
                    }

                    if (order.HasItemsToAddToShipment())
                    {
                        if (order.HasItemsToShip())
                        {
                            if (order.HasItemsToDeliver())
                            {
                                order.ShippingStatus = ShippingStatus.PartiallyShipped;
                            }
                            else
                            {
                                order.ShippingStatus = ShippingStatus.NotYetShipped;
                            }
                        }
                        else
                        {
                            if (order.HasItemsToDeliver())
                            {
                                order.ShippingStatus = ShippingStatus.Shipped;
                            }
                            else
                            {
                                order.ShippingStatus = ShippingStatus.Delivered;
                            }
                        }
                    }
                }
                else
                {
                    order.ShippingStatus = ShippingStatus.ShippingNotRequired;
                }
            }
            _orderService.UpdateOrder(order);

            if (orderDelta.Dto.UpdatedPaymentStatus != order.PaymentStatus)
            {
                order.PaymentStatus = orderDelta.Dto.UpdatedPaymentStatus;
            }
            if (orderDelta.Dto.UpdatedShippingStatus != order.ShippingStatus)
            {
                order.ShippingStatus = orderDelta.Dto.UpdatedShippingStatus;
            }
            if (orderDelta.Dto.UpdatedOrderStatus != order.OrderStatus)
            {
                order.OrderStatus = orderDelta.Dto.UpdatedOrderStatus;
            }
            else
            {
                _orderProcessingService.CheckOrderStatus(order);
            }

            //*TODO*
            //if (newOrder.PaymentStatus == PaymentStatus.Paid)
            //    ProcessOrderPaid(newOrder);
            ////notifications
            //_orderProcessingService.SendNotificationsAndSaveNotes(newOrder);

            _customerActivityService.InsertActivity("UpdateOrder",
                 _localizationService.GetResource("ActivityLog.UpdateOrder"), order.Id);

            var result = new ResultObjectModel<int>
            {
                Message = _localizationService.GetResource("Orders.Update.Success"),
                Success = true,
                Data = order.Id
            };

            var json = _jsonFieldsSerializer.Serialize(result, string.Empty);

            return new RawJsonActionResult(json);
        }
        #endregion

        #region Methods
        [HttpGet]
        [Route("/api/payments")]
        [ProducesResponseType(typeof(MethodListModel), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult PaymentMethods()
        {
            var paymentMethods = _paymentService.LoadAllPaymentMethods().Select(m =>
            {
                var item = new MethodModel
                {
                    Name = m.PluginDescriptor.FriendlyName,
                    SystemName = m.PluginDescriptor.SystemName
                };
                return item;
            }).ToList();
            var result = new MethodListModel()
            {
                Methods = paymentMethods
            };

            var json = _jsonFieldsSerializer.Serialize(result, string.Empty);

            return new RawJsonActionResult(json);
        }
        [HttpGet]
        [Route("/api/shippings")]
        [ProducesResponseType(typeof(MethodListModel), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult ShippingMethods()
        {
            var paymentMethods = _shippingService.LoadAllShippingRateComputationMethods().Select(m =>
            {
                var item = new MethodModel
                {
                    Name = m.PluginDescriptor.FriendlyName,
                    SystemName = m.PluginDescriptor.SystemName
                };
                return item;
            }).ToList();
            var result = new MethodListModel()
            {
                Methods = paymentMethods
            };

            var json = _jsonFieldsSerializer.Serialize(result, string.Empty);

            return new RawJsonActionResult(json);
        }
        #endregion

        /// <summary>
        /// Receive a count of all Orders
        /// </summary>
        /// <response code="200">OK</response>
        /// <response code="401">Unauthorized</response>
        [HttpGet]
        [Route("/api/orders/count")]
        [ProducesResponseType(typeof(OrdersCountRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult GetOrdersCount(OrdersCountParametersModel parameters)
        {
            var storeId = _storeContext.CurrentStore.Id;

            int ordersCount = _orderApiService.GetOrdersCount(null, null, parameters.FromDate, parameters.ToDate, parameters.Status,
                                                              parameters.PaymentStatus, parameters.ShippingStatus, parameters.CustomerId, storeId);

            var ordersCountRootObject = new OrdersCountRootObject()
            {
                Count = ordersCount
            };

            return Ok(ordersCountRootObject);
        }

        /// [TuanDang][16/12/2018]
        /// <summary>
        /// Retrieve order by spcified id
        /// </summary>
        ///   /// <param name="id">Id of the order</param>
        /// <param name="fields">Fields from the order you want your json to contain</param>
        /// <response code="200">OK</response>
        /// <response code="404">Not Found</response>
        /// <response code="401">Unauthorized</response>
        [HttpGet]
        [Route("/api/orders/{id}")]
        [ProducesResponseType(typeof(OrderModel), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult GetOrderById(int id, string fields = "")
        {
            if (!Authorize(StandardPermissionProvider.ManageOrders))
            {
                return Error(HttpStatusCode.BadRequest, "", "Unauthorized orders");
            }
            if (id <= 0)
            {
                return Error(HttpStatusCode.BadRequest, "id", "invalid id");
            }

            Order order = _orderApiService.GetOrderById(id);

            if (order == null)
            {
                return Error(HttpStatusCode.NotFound, "order", "not found");
            }
            //var ordersRootObject = new OrdersRootObject();
            //OrderDto orderDto = _dtoHelper.PrepareOrderDTO(order);
            ////ordersRootObject.Orders.Add(orderDto);
            ///
            var model = _modelConverter.ToModel(order);

            var json = _jsonFieldsSerializer.Serialize(model, fields);

            return new RawJsonActionResult(json);
        }

        /// [TuanDang][16/12/2018]
        /// <summary>
        /// Retrieve order by spcified id
        /// </summary>
        ///   /// <param name="id">Id of the order</param>
        /// <param name="fields">Fields from the order you want your json to contain</param>
        /// <response code="200">OK</response>
        /// <response code="404">Not Found</response>
        /// <response code="401">Unauthorized</response>
        [HttpGet]
        [Route("/api/orders/{id}/summary")]
        [ProducesResponseType(typeof(OrderSummaryModel), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult GetOrderSummaryById(int id, string fields = "")
        {
            if (!Authorize(StandardPermissionProvider.ManageOrders))
            {
                return Error(HttpStatusCode.BadRequest, "", "Unauthorized orders");
            }
            if (id <= 0)
            {
                return Error(HttpStatusCode.BadRequest, "id", "invalid id");
            }

            Order order = _orderApiService.GetOrderById(id);

            if (order == null)
            {
                return Error(HttpStatusCode.NotFound, "order", "not found");
            }

            var model = _modelConverter.ToSummaryModel(order);

            var json = _jsonFieldsSerializer.Serialize(model, fields);

            return new RawJsonActionResult(json);
        }

        /// [TuanDang][16/12/2018]
        /// <summary>
        /// Retrieve order by spcified id
        /// </summary>
        ///   /// <param name="id">Id of the order</param>
        /// <param name="fields">Fields from the order you want your json to contain</param>
        /// <response code="200">OK</response>
        /// <response code="404">Not Found</response>
        /// <response code="401">Unauthorized</response>
        [HttpGet]
        [Route("/api/orders/{id}/copy")]
        [ProducesResponseType(typeof(OrderCopyModel), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult CopyOrder(int id, string fields = "")
        {
            if (!Authorize(StandardPermissionProvider.ManageOrders))
            {
                return Error(HttpStatusCode.BadRequest, "", "Unauthorized orders");
            }
            if (id <= 0)
            {
                return Error(HttpStatusCode.BadRequest, "id", "invalid id");
            }

            Order order = _orderApiService.GetOrderById(id);

            if (order == null)
            {
                return Error(HttpStatusCode.NotFound, "order", "not found");
            }

            var model = _modelConverter.Copy(order);

            var json = _jsonFieldsSerializer.Serialize(model, fields);

            return new RawJsonActionResult(json);
        }

        /// <summary>
        /// Retrieve all orders for customer
        /// </summary>
        /// <param name="customer_id">Id of the customer whoes orders you want to get</param>
        /// <response code="200">OK</response>
        /// <response code="401">Unauthorized</response>
        [HttpGet]
        [Route("/api/orders/customer/{customer_id}")]
        [ProducesResponseType(typeof(OrdersRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult GetOrdersByCustomerId(int customer_id)
        {
            IList<OrderDto> ordersForCustomer = _orderApiService.GetOrdersByCustomerId(customer_id).Select(x => _dtoHelper.PrepareOrderDTO(x)).ToList();

            var ordersRootObject = new OrdersRootObject()
            {
                // Orders = ordersForCustomer
            };

            return Ok(ordersRootObject);
        }

        //[HttpDelete]
        //[Route("/api/orders/{id}")]
        //[ProducesResponseType(typeof(void), (int)HttpStatusCode.OK)]
        //[ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        //[ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        //[ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        //[ProducesResponseType(typeof(ErrorsRootObject), 422)]
        //[GetRequestsErrorInterceptorActionFilter]
        //public IActionResult DeleteOrder(int id)
        //{
        //    if (id <= 0)
        //    {
        //        return Error(HttpStatusCode.BadRequest, "id", "invalid id");
        //    }

        //    Order orderToDelete = _orderApiService.GetOrderById(id);

        //    if (orderToDelete == null)
        //    {
        //        return Error(HttpStatusCode.NotFound, "order", "not found");
        //    }

        //    _orderProcessingService.DeleteOrder(orderToDelete);

        //    //activity log
        //    _customerActivityService.InsertActivity("DeleteOrder", _localizationService.GetResource("ActivityLog.DeleteOrder"), orderToDelete.Id);

        //    return new RawJsonActionResult("{}");
        //} 
        #endregion

        #region Ultilities

        // We resolve the order settings this way because of the tests.
        // The auto mocking does not support concreate types as dependencies. It supports only interfaces.
        private OrderSettings _orderSettings;

        private OrderSettings OrderSettings
        {
            get
            {
                if (_orderSettings == null)
                {
                    _orderSettings = EngineContext.Current.Resolve<OrderSettings>();
                }

                return _orderSettings;
            }
        }
        private bool SetShippingOption(string shippingRateComputationMethodSystemName, string shippingOptionName, int storeId, Customer customer, List<ShoppingCartItem> shoppingCartItems)
        {
            bool isValid = true;

            if (string.IsNullOrEmpty(shippingRateComputationMethodSystemName))
            {
                isValid = false;

                ModelState.AddModelError("shipping system_name",
                    "Please provide shipping system_name");
            }
            else if (string.IsNullOrEmpty(shippingOptionName))
            {
                isValid = false;

                ModelState.AddModelError("shipping_option_name", "Please provide shipping_option_name");
            }
            else
            {
                GetShippingOptionResponse shippingOptionResponse = _shippingService.GetShippingOptions(shoppingCartItems, customer.ShippingAddress, customer,
                        shippingRateComputationMethodSystemName, storeId);

                var shippingOptions = new List<ShippingOption>();

                if (shippingOptionResponse.Success)
                {
                    shippingOptions = shippingOptionResponse.ShippingOptions.ToList();

                    ShippingOption shippingOption = shippingOptions
                        .Find(so => !string.IsNullOrEmpty(so.Name) && so.Name.Equals(shippingOptionName, StringComparison.InvariantCultureIgnoreCase));

                    _genericAttributeService.SaveAttribute(customer,
                        SystemCustomerAttributeNames.SelectedShippingOption,
                        shippingOption, storeId);
                }
                else
                {
                    isValid = false;

                    foreach (var errorMessage in shippingOptionResponse.Errors)
                    {
                        ModelState.AddModelError("shipping_option", errorMessage);
                    }
                }
            }

            return isValid;
        }

        private List<ShoppingCartItem> BuildShoppingCartItemsFromOrderItems(List<OrderItem> orderItems, int customerId, int storeId)
        {
            var shoppingCartItems = new List<ShoppingCartItem>();

            foreach (var orderItem in orderItems)
            {
                shoppingCartItems.Add(new ShoppingCartItem()
                {
                    ProductId = orderItem.ProductId,
                    CustomerId = customerId,
                    Quantity = orderItem.Quantity,
                    RentalStartDateUtc = orderItem.RentalStartDateUtc,
                    RentalEndDateUtc = orderItem.RentalEndDateUtc,
                    StoreId = storeId,
                    Product = orderItem.Product,
                    ShoppingCartType = ShoppingCartType.ShoppingCart
                });
            }

            return shoppingCartItems;
        }

        private List<ShoppingCartItem> BuildShoppingCartItemsFromOrderItemDtos(List<OrderItemDto> orderItemDtos, int customerId, int storeId)
        {
            var shoppingCartItems = new List<ShoppingCartItem>();

            foreach (var orderItem in orderItemDtos)
            {
                shoppingCartItems.Add(new ShoppingCartItem()
                {
                    ProductId = orderItem.ProductId.Value, // required field
                    CustomerId = customerId,
                    Quantity = orderItem.Quantity ?? 1,
                    RentalStartDateUtc = orderItem.RentalStartDateUtc,
                    RentalEndDateUtc = orderItem.RentalEndDateUtc,
                    StoreId = storeId,
                    Product = _productService.GetProductById(orderItem.ProductId.Value),
                    ShoppingCartType = ShoppingCartType.ShoppingCart
                });
            }

            return shoppingCartItems;
        }

        private PlaceOrderResult PlaceOrder(Order newOrder, Customer customer)
        {
            var processPaymentRequest = new ProcessPaymentRequest();

            processPaymentRequest.StoreId = newOrder.StoreId;
            processPaymentRequest.CustomerId = customer.Id;
            processPaymentRequest.PaymentMethodSystemName = newOrder.PaymentMethodSystemName;

            PlaceOrderResult placeOrderResult = _orderProcessingService.PlaceOrder(processPaymentRequest);

            return placeOrderResult;
        }

        private bool ValidateEachOrderItem(ICollection<OrderItemDto> orderItems)
        {
            bool shouldReturnError = false;

            foreach (var orderItem in orderItems)
            {
                var orderItemDtoValidator = new OrderItemDtoValidator("post", null);
                ValidationResult validation = orderItemDtoValidator.Validate(orderItem);

                if (validation.IsValid)
                {
                    Product product = _productService.GetProductById(orderItem.ProductId.Value);

                    if (product == null)
                    {
                        ModelState.AddModelError("order_item.product", string.Format("Product not found for products.product_id = {0}", orderItem.ProductId));
                        shouldReturnError = true;
                    }
                }
                else
                {
                    foreach (var error in validation.Errors)
                    {
                        ModelState.AddModelError("product", error.ErrorMessage);
                    }

                    shouldReturnError = true;
                }
            }

            return shouldReturnError;
        }

        private bool IsShippingAddressRequired(ICollection<OrderItemDto> orderItems)
        {
            bool shippingAddressRequired = false;

            foreach (var orderItem in orderItems)
            {
                Product product = _productService.GetProductById(orderItem.ProductId.Value);

                shippingAddressRequired |= product.IsShipEnabled;
            }

            return shippingAddressRequired;
        }

        private bool AddOrderItemsToCart(ICollection<OrderItemDto> orderItems, Customer customer, int storeId)
        {
            bool shouldReturnError = false;

            foreach (var orderItem in orderItems)
            {
                Product product = _productService.GetProductById(orderItem.ProductId.Value);

                if (!product.IsRental)
                {
                    orderItem.RentalStartDateUtc = null;
                    orderItem.RentalEndDateUtc = null;
                }

                string attributesXml = _productAttributeConverter.ConvertToXml(orderItem.Attributes, product.Id);

                IList<string> errors = _shoppingCartService.AddToCart(customer, product,
                    ShoppingCartType.ShoppingCart, storeId, attributesXml,
                    0M, orderItem.RentalStartDateUtc, orderItem.RentalEndDateUtc,
                    orderItem.Quantity ?? 1);

                if (errors.Count > 0)
                {
                    foreach (var error in errors)
                    {
                        ModelState.AddModelError("order", error);
                    }

                    shouldReturnError = true;
                }
            }

            return shouldReturnError;
        }

        private bool ValidateAddress(AddressDto address, string addressKind)
        {
            bool addressValid = true;

            if (address == null)
            {
                ModelState.AddModelError(addressKind, string.Format("{0} address required", addressKind));
                addressValid = false;
            }
            else
            {
                var addressValidator = new AddressDtoValidator();
                ValidationResult validationResult = addressValidator.Validate(address);

                foreach (var validationFailure in validationResult.Errors)
                {
                    ModelState.AddModelError(addressKind, validationFailure.ErrorMessage);
                }

                addressValid = validationResult.IsValid;
            }

            return addressValid;
        }

        /// <summary>
        /// [HienNguyen][03/08/2018][Hàm validation dữ liệu đơn hàng bán hàng]
        /// </summary>
        /// <param name="saleOrder"></param>
        /// <returns></returns>
        private bool ValidateSaleOrder(SaleOrderDtoModel saleOrder)
        {
            #region Store
            if (saleOrder.StoreId == 0 || _storeService.GetStoreById(saleOrder.StoreId) == null)
            {
                ModelState.AddModelError("sale_order", string.Format("Mã siêu thị {0} không tồn tại trong hệ thống !", saleOrder.StoreId));
                return false;
            }
            #endregion

            #region Customer
            if (saleOrder.CustomerId == 0
                    || _customerService.GetCustomerById(saleOrder.CustomerId) == null)
            {
                ModelState.AddModelError("sale_order", string.Format("Mã khách hàng {0} không tồn tại trong hệ thống !", saleOrder.CustomerId));
                return false;
            }
            if (saleOrder.CustomerCreateId == 0
                || _customerService.GetCustomerById(saleOrder.CustomerCreateId) == null)
            {
                ModelState.AddModelError("sale_order", string.Format("Mã nhân viên {0} không tồn tại trong hệ thống !", saleOrder.CustomerCreateId));
            }
            #endregion

            #region Payment method
            if (saleOrder.PaymentMethod == null)
            {
                ModelState.AddModelError("sale_order", string.Format("Mã khách hàng {0} không tồn tại trong hệ thống !", saleOrder.CustomerId));
                return false;
            }
            #endregion

            #region Payment Status

            //Hàm tạo đơn hàng bán chỉ có 2 trạng thái là lưu tạm và hoàn thành
            if (saleOrder.OrderStatusId != (int)Core.Domain.Orders.OrderStatus.Temporary ||
                saleOrder.OrderStatusId != (int)Core.Domain.Orders.OrderStatus.Complete)
            {
                ModelState.AddModelError("sale_order.order_status_id", "Trạng thái đơn hàng không hợp lệ !");
                return false;
            }

            #endregion

            #region Order items

            if (saleOrder.OrderItems == null || saleOrder.OrderItems.Count == 0)
            {
                ModelState.AddModelError("sale_order.order_items", "Vui lòng chọn ít nhất 1 sản phẩm !");
                return false;
            }

            foreach (OrderItemDtoModel orderItem in saleOrder.OrderItems)
            {
                if (orderItem.Quantity == 0)
                {
                    ModelState.AddModelError("sale_order.order_items.product_id", string.Format("Số lượng sản phẩm {0} phải lớn hơn 0 !", orderItem.ProductName));
                    return false;
                }
            }

            #endregion

            return true;
        }
        #endregion
    }
}