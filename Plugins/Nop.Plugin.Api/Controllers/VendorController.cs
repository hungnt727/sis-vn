﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Discounts;
using Nop.Core.Domain.Media;
using Nop.Plugin.Api.Attributes;
using Nop.Plugin.Api.Constants;
using Nop.Plugin.Api.Delta;
using Nop.Plugin.Api.DTOs.Images;
using Nop.Plugin.Api.DTOs.Products;
using Nop.Plugin.Api.Factories;
using Nop.Plugin.Api.JSON.ActionResults;
using Nop.Plugin.Api.ModelBinders;
using Nop.Plugin.Api.Models.ProductsParameters;
using Nop.Plugin.Api.Services;
using Nop.Services.Catalog;
using Nop.Services.Customers;
using Nop.Services.Discounts;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Media;
using Nop.Services.Security;
using Nop.Services.Seo;
using Nop.Services.Stores;
using Nop.Plugin.Api.Helpers;

namespace Nop.Plugin.Api.Controllers
{
    using Microsoft.AspNetCore.Authentication.JwtBearer;
    using Microsoft.AspNetCore.Mvc;
    using Nop.Core;
    using Nop.Core.Domain.Vendors;
    using Nop.Plugin.Api.DTOs.Categories;
    using Nop.Plugin.Api.DTOs.Errors;
    using Nop.Plugin.Api.DTOs.Vendor;
    using Nop.Plugin.Api.JSON.Serializers;
    using Nop.Services.Vendors;

    [ApiAuthorize(Policy = JwtBearerDefaults.AuthenticationScheme, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class VendorController : BaseApiController
    {
        private readonly IProductApiService _productApiService;
        private readonly IProductService _productService;
        private readonly IUrlRecordService _urlRecordService;
        private readonly IPictureService _pictureService;
        private readonly IManufacturerService _manufacturerService;
        private readonly IFactory<Product> _factory;
        private readonly IProductTagService _productTagService;
        private readonly IProductAttributeService _productAttributeService;
        private readonly IVendorService _vendorService;
        private readonly IDTOHelper _dtoHelper;

        public VendorController(IProductApiService productApiService,
                                  IJsonFieldsSerializer jsonFieldsSerializer,
                                  IProductService productService,
                                  IUrlRecordService urlRecordService,
                                  ICustomerActivityService customerActivityService,
                                  ILocalizationService localizationService,
                                  IFactory<Product> factory,
                                  IAclService aclService,
                                  IStoreMappingService storeMappingService,
                                  IStoreService storeService,
                                  ICustomerService customerService,
                                  IDiscountService discountService,
                                  IPictureService pictureService,
                                  IManufacturerService manufacturerService,
                                  IProductTagService productTagService,
                                  IProductAttributeService productAttributeService,
                                  IVendorService vendorService,
                                  IDTOHelper dtoHelper)
        {
            _productApiService = productApiService;
            _factory = factory;
            _manufacturerService = manufacturerService;
            _productTagService = productTagService;
            _urlRecordService = urlRecordService;
            _productService = productService;
            _productAttributeService = productAttributeService;
            _dtoHelper = dtoHelper;
            _pictureService = pictureService;
            _vendorService = vendorService;
        }

        #region Custom Method

        /// <summary>
        /// [HienNguyen][07/08/2018][Hàm tìm kiếm NCC]
        /// </summary>
        /// <param name="keyword"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("/api/vendors")]
        [ProducesResponseType(typeof(VendorRootObjectModel), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult SearchVendor(VendorParametersModel parameters)
        {
            if (!Authorize(StandardPermissionProvider.ManageVendors))
            {
                return Error(HttpStatusCode.BadRequest, "", "Unauthorized vendors");
            }
            var listVendor = _vendorService.SearchVendor(parameters.Keyword);

            IList<VendorDtoModel> vendorAsDtos = listVendor.Select(vendor =>
            {
                return _dtoHelper.PrepareVendorDTOModel(vendor);

            }).ToList();

            var vendorRootObjectModel = new VendorRootObjectModel()
            {
                Vendors = vendorAsDtos
            };

            var json = _jsonFieldsSerializer.Serialize(vendorRootObjectModel, string.Empty);

            return new RawJsonActionResult(json);
        }

        /// <summary>
        /// [HienNguyen][08/08/2018][Cập nhật NCC]
        /// </summary>
        /// <param name="vendorDelta"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("/api/vendors")]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult CreateVendor([ModelBinder(typeof(JsonModelBinder<VendorDtoModel>))] Delta<VendorDtoModel> vendorDelta)
        {
            if (!Authorize(StandardPermissionProvider.ManageVendors))
            {
                return Error(HttpStatusCode.BadRequest, "", "Unauthorized vendors");
            }
            _vendorService.InsertVendor(new Core.Domain.Vendors.Vendor()
            {
                //Id= vendorDelta.Dto.Id,
                Name = vendorDelta.Dto.Name,
                Email = vendorDelta.Dto.Email,
                Active = true,
            });

            //activity log
            _customerActivityService.InsertActivity("AddNewVendor", _localizationService.GetResource("ActivityLog.AddNewVendor"), 0);

            return new RawJsonActionResult("{}");
        }

        /// <summary>
        /// [HienNguyen][08/08/2018][Cập nhật NCC]
        /// </summary>
        /// <param name="vendorDelta"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("/api/vendors/{id:int}")]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult UpdateVendor([ModelBinder(typeof(JsonModelBinder<VendorDtoModel>))] Delta<VendorDtoModel> vendorDelta)
        {
            if (!Authorize(StandardPermissionProvider.ManageVendors))
            {
                return Error(HttpStatusCode.BadRequest, "", "Unauthorized vendors");
            }
            var vendor = _vendorService.GetVendorById(vendorDelta.Dto.Id);
            if(vendor == null || vendor.Deleted)
            {
                return Error(HttpStatusCode.NotFound, "vendor", "vendor not found");
            }
            vendor.Name = vendorDelta.Dto.Name;
            vendor.Email = vendorDelta.Dto.Email;

            _vendorService.UpdateVendor(vendor);

            _customerActivityService.InsertActivity("EditVendor", _localizationService.GetResource("ActivityLog.EditVendor"), vendor.Id);

            return new RawJsonActionResult("{}");
        }

        [HttpDelete]
        [Route("/api/vendors/{id:int}")]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult DeleteVendor(int id)
        {
            if (!Authorize(StandardPermissionProvider.ManageVendors))
            {
                return Error(HttpStatusCode.BadRequest, "", "Unauthorized vendors");
            }
            if (id <= 0)
            {
                return Error(HttpStatusCode.BadRequest, "id", "invalid id");
            }

            Vendor vendorToDelete = _vendorService.GetVendorById(id);
            
            if (vendorToDelete == null || vendorToDelete.Deleted)
            {
                return Error(HttpStatusCode.NotFound, "vendor", "vendor not found");
            }

            _vendorService.DeleteVendor(vendorToDelete);

            //activity log
            _customerActivityService.InsertActivity("DeleteVendor", _localizationService.GetResource("ActivityLog.DeleteVendor"), vendorToDelete.Name);

            return new RawJsonActionResult("{}");
        }
        #endregion
    }
}
