﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Directory;
using Nop.Core.Infrastructure;
using Nop.Plugin.Api.Attributes;
using Nop.Plugin.Api.Constants;
using Nop.Plugin.Api.Delta;
using Nop.Plugin.Api.DTOs;
using Nop.Plugin.Api.DTOs.Customers;
using Nop.Plugin.Api.Factories;
using Nop.Plugin.Api.Helpers;
using Nop.Plugin.Api.JSON.ActionResults;
using Nop.Plugin.Api.MappingExtensions;
using Nop.Plugin.Api.ModelBinders;
using Nop.Plugin.Api.Models.CustomersParameters;
using Nop.Plugin.Api.Services;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Services.Directory;
using Nop.Services.Discounts;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Media;
using Nop.Services.Messages;
using Nop.Services.Security;
using Nop.Services.Stores;

namespace Nop.Plugin.Api.Controllers
{
    using Microsoft.AspNetCore.Authentication.JwtBearer;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Nop.Core;
    using Nop.Plugin.Api.DTOs.Errors;
    using Nop.Plugin.Api.JSON.Serializers;
    using Nop.Plugin.Api.Models;

    //[ApiAuthorize(Policy = JwtBearerDefaults.AuthenticationScheme, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class CustomersController : BaseApiController
    {
        private readonly ICustomerApiService _customerApiService;
        private readonly ICustomerRolesHelper _customerRolesHelper;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly IEncryptionService _encryptionService;
        private readonly ICountryService _countryService;
        private readonly IMappingHelper _mappingHelper;
        private readonly INewsLetterSubscriptionService _newsLetterSubscriptionService;
        private readonly ILanguageService _languageService;
        private readonly IFactory<Customer> _factory;
        private readonly IDTOHelper _dtoHelper;
        private readonly ICustomerRegistrationService _customerRegistrationService;

        // We resolve the customer settings this way because of the tests.
        // The auto mocking does not support concreate types as dependencies. It supports only interfaces.
        private readonly CustomerSettings _customerSettings;

        public CustomersController(
            CustomerSettings customerSettings,
            ICustomerApiService customerApiService,
            IJsonFieldsSerializer jsonFieldsSerializer,
            IAclService aclService,
            ICustomerService customerService,
            IStoreMappingService storeMappingService,
            IStoreService storeService,
            IDiscountService discountService,
            ICustomerActivityService customerActivityService,
            ILocalizationService localizationService,
            ICustomerRolesHelper customerRolesHelper,
            IGenericAttributeService genericAttributeService,
            IEncryptionService encryptionService,
            IFactory<Customer> factory,
            ICountryService countryService,
            IMappingHelper mappingHelper,
            INewsLetterSubscriptionService newsLetterSubscriptionService,
            IPictureService pictureService, ILanguageService languageService, IDTOHelper dtoHelper)
        {
            _customerSettings = customerSettings;
            _customerApiService = customerApiService;
            _factory = factory;
            _countryService = countryService;
            _mappingHelper = mappingHelper;
            _newsLetterSubscriptionService = newsLetterSubscriptionService;
            _languageService = languageService;
            _encryptionService = encryptionService;
            _genericAttributeService = genericAttributeService;
            _customerRolesHelper = customerRolesHelper;
            _dtoHelper = dtoHelper;
        }

        #region Custom Method

        #region GetCustomerById [Hàm lấy khách hàng theo mã khách hàng]
        /// <summary>
        /// [HienNguyen][31/07/2018][Hàm lấy khách hàng theo mã khách hàng]
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("/api/get_customer_by_id/{customerId}")]
        [ProducesResponseType(typeof(CustomersRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult GetCustomerById(int customerId)
        {
            Customer customers = _customerService.GetCustomerById(customerId);

            CustomerDtoModel customerModel = CustomerDtoModel.CopyData(customers);

            var customersRootObject = new CustomersRootObject()
            {
                Customers = new List<CustomerDtoModel>() { customerModel },
                TotalRow = 1
            };

            var json = _jsonFieldsSerializer.Serialize(customersRootObject, string.Empty);

            return new RawJsonActionResult(json);
        }
        #endregion

        #region GetCustomerSystem [Hàm tìm kiếm user là nhân viên]
        /// <summary>
        /// [HienNguyen][25/07/2018][Hàm tìm kiếm user là nhân viên]
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("/api/get_customer_system")]
        [ProducesResponseType(typeof(CustomersRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult GetCustomerSystem(CustomersParametersModel parameters)
        {
            var customers = _customerService.SearchCustomers(parameters.Keyword, new List<int>() { 2 }, parameters.PageIndex, parameters.PageSize);

            IList<CustomerDtoModel> productsAsDtos = customers.Select(customer =>
                 CustomerDtoModel.CopyData(customer)
            ).ToList();

            var customersRootObject = new CustomersRootObject()
            {
                Customers = productsAsDtos,
                TotalRow = customers.TotalCount
            };

            var json = _jsonFieldsSerializer.Serialize(customersRootObject, parameters.Fields);

            return new RawJsonActionResult(json);
        }
        #endregion

        #region GetCustomerOnline [Hàm tìm kiếm user là khách hàng]
        /// <summary>
        /// [HienNguyen][25/07/2018][Hàm tìm kiếm user là khách hàng]
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("/api/get_customer_online")]
        [ProducesResponseType(typeof(CustomersRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult GetCustomerOnline(CustomersParametersModel parameters)
        {
            var customers = _customerService.SearchCustomers(parameters.Keyword, new List<int>() { 4 }, parameters.PageIndex, parameters.PageSize);

            IList<CustomerDtoModel> productsAsDtos = customers.Select(customer =>
                CustomerDtoModel.CopyData(customer)
            ).ToList();

            var customersRootObject = new CustomersRootObject()
            {
                Customers = productsAsDtos,
                TotalRow = customers.TotalCount
            };

            var json = _jsonFieldsSerializer.Serialize(customersRootObject, parameters.Fields);

            return new RawJsonActionResult(json);
        }
        #endregion

        #region CreateCustomer [Hàm tạo mới khách hàng]
        /// <summary>
        /// [TuanDang][13-12-2018][Hàm tạo mới khách hàng]
        /// [HienNguyen][28/07/2018][Hàm tạo mới khách hàng]
        /// </summary>
        /// <param name="customerModel"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("/api/customers")]
        [ProducesResponseType(typeof(ResultObjectModel<>), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), 422)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        public IActionResult CreateCustomer([ModelBinder(typeof(JsonModelBinder<CustomerDto>))] Delta<CustomerDto> customerDelta)
        {
            // Here we display the errors if the validation has failed at some point.
            if (!ModelState.IsValid)
            {
                return Error();
            }

            //If the validation has passed the customerDelta object won't be null for sure so we don't need to check for this.

            // Inserting the new customer
            var newCustomer = new Customer
            {
                CustomerGuid = Guid.NewGuid(),
                Email = customerDelta.Dto.Email,
                Username = customerDelta.Dto.Username,
                //VendorId = customerDelta.Dto.VendorId,
                AdminComment = customerDelta.Dto.AdminComment,
                IsTaxExempt = false,
                Active = true,
                CreatedOnUtc = DateTime.UtcNow,
                LastActivityDateUtc = DateTime.UtcNow,
                //*TODO* store id
                ///RegisteredInStoreId = _storeContext.CurrentStore.Id
            };

            //validate customer roles
            var allCustomerRoles = _customerService.GetAllCustomerRoles(true);
            var newCustomerRoles = new List<CustomerRole>();
            foreach (var customerRole in allCustomerRoles)
                if (customerDelta.Dto.RoleIds.Contains(customerRole.Id))
                    newCustomerRoles.Add(customerRole);


            //customer roles
            foreach (var customerRole in newCustomerRoles)
            {
                ////ensure that the current customer cannot add to "Administrators" system role if he's not an admin himself
                //if (customerRole.SystemName == SystemCustomerRoleNames.Administrators &&
                //    !_workContext.CurrentCustomer.IsAdmin())
                //    continue;

                newCustomer.CustomerRoles.Add(customerRole);
            }

            _customerService.InsertCustomer(newCustomer);

            //InsertFirstAndLastNameGenericAttributes(customerDelta.Dto.FirstName, customerDelta.Dto.LastName, newCustomer);
            if (!string.IsNullOrEmpty(customerDelta.Dto.FullName))
            {
                _genericAttributeService.SaveAttribute(newCustomer, SystemCustomerAttributeNames.FirstName, customerDelta.Dto.FullName);
            }

            //*TODO* set address customer
            newCustomer.BillingAddress = new Address
            {
                FirstName = customerDelta.Dto.FullName,
                Email = customerDelta.Dto.Email,
                Address1 = customerDelta.Dto.Address,
                PhoneNumber = customerDelta.Dto.Phone,
                CreatedOnUtc = DateTime.Now
            };
            //*TODO* set address customer
            newCustomer.ShippingAddress = new Address
            {
                FirstName = customerDelta.Dto.FullName,
                Address1 = customerDelta.Dto.Address,
                PhoneNumber = customerDelta.Dto.Phone,
                CreatedOnUtc = DateTime.Now
            };

            if (!string.IsNullOrWhiteSpace(customerDelta.Dto.Password))
            {
                var changePasswordRequest = new ChangePasswordRequest(customerDelta.Dto.Email, false, _customerSettings.DefaultPasswordFormat, customerDelta.Dto.Password);
                string message = string.Empty;
                CreateOrUpdate(changePasswordRequest, ref message);
            }

            if (customerDelta.Dto.CustomerTypeId != null)
            {
                _genericAttributeService.SaveAttribute(newCustomer, SystemCustomerAttributeNames.CustomerTypeId, customerDelta.Dto.CustomerTypeId);
            }


            //form fields
            if (_customerSettings.GenderEnabled)
                _genericAttributeService.SaveAttribute(newCustomer, SystemCustomerAttributeNames.Gender, customerDelta.Dto.Gender);
            //_genericAttributeService.SaveAttribute(newCustomer, SystemCustomerAttributeNames.FirstName, customerDelta.Dto.FirstName);
            _genericAttributeService.SaveAttribute(newCustomer, SystemCustomerAttributeNames.LastName, customerDelta.Dto.LastName);
            if (_customerSettings.DateOfBirthEnabled && customerDelta.Dto.DateOfBirth.HasValue)
                _genericAttributeService.SaveAttribute(newCustomer, SystemCustomerAttributeNames.DateOfBirth, customerDelta.Dto.DateOfBirth.Value);
            //*TODO*
            //if (_customerSettings.StreetAddressEnabled)
            //    _genericAttributeService.SaveAttribute(newCustomer, SystemCustomerAttributeNames.StreetAddress, customerDelta.Dto.Address);
            if (_customerSettings.PhoneEnabled)
                _genericAttributeService.SaveAttribute(newCustomer, SystemCustomerAttributeNames.Phone, customerDelta.Dto.Phone);

            _customerService.UpdateCustomer(newCustomer);

            // Preparing the result dto of the new customer
            // We do not prepare the shopping cart items because we have a separate endpoint for them.
            CustomerDto newCustomerDto = newCustomer.ToDto();

            // This is needed because the entity framework won't populate the navigation properties automatically
            // and the country will be left null. So we do it by hand here.
            PopulateAddressCountryNames(newCustomerDto);

            // Set the fist and last name separately because they are not part of the customer entity, but are saved in the generic attributes.
            newCustomerDto.FirstName = customerDelta.Dto.FirstName;
            newCustomerDto.LastName = customerDelta.Dto.LastName;

            //activity log
            _customerActivityService.InsertActivity("AddNewCustomer", _localizationService.GetResource("ActivityLog.AddNewCustomer"), newCustomer.Id);

            //var customersRootObject = new CustomersRootObject();

            //CustomerDtoModel customerModel = CustomerDtoModel.CopyData(newCustomer);

            //customersRootObject.Customers.Add(customerModel);

            var json = _jsonFieldsSerializer.Serialize(new ResultObjectModel<int>
            {
                Success = true,
                Message = _localizationService.GetResource("Customers.Add.Success"),
                Data = newCustomer.Id
            }, string.Empty);

            return new RawJsonActionResult(json);
        }
        #endregion

        #region [Hàm lấy loại khách hàng]
        /// <summary>
        /// [HienNguyen][28/07/2018][Hàm lấy loại khách hàng]
        /// </summary>
        /// <param name="orderstatusids"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("/api/get_customer_type")]
        [ProducesResponseType(typeof(DTOs.Customers.CustomerTypeModel), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult GetCustomerType(int[] orderStatusIds = null)
        {
            DTOs.Customers.CustomerTypeModel orderStatus = new DTOs.Customers.CustomerTypeModel();

            orderStatus.CustomerType = Enum.GetValues(typeof(Core.Domain.Customers.CustomerType))
                                .Cast<Core.Domain.Orders.OrderStatus>()
                                .Select(v => new DTOs.Customers.CustomerType() { CustomerTypeId = (int)v, CustomerTypeName = CommonHelper.GetEnumDescription(v) })
                                .ToList();

            if (orderStatusIds != null && orderStatusIds.Any())
            {
                orderStatus.CustomerType = orderStatus.CustomerType.Where(m => orderStatusIds.Contains(m.CustomerTypeId)).ToList();
            }

            var json = _jsonFieldsSerializer.Serialize(orderStatus, string.Empty);
            return new RawJsonActionResult(json);
        }
        #endregion

        #region UpdateCustomer [Hàm cập nhập password]
        /// <summary>
        /// Hàm cập nhập password 
        /// </summary>
        /// <param name="customerDelta"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("/api/customers_changepassword/{id}")]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), 422)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        public IActionResult ChangePassword([ModelBinder(typeof(JsonModelBinder<CustomerDto>))] Delta<CustomerDto> customerDelta)
        {
            // Here we display the errors if the validation has failed at some point.
            if (!ModelState.IsValid)
            {
                return Error();
            }
            // check cumtomer exist
            Customer currentCustomer = _customerApiService.GetCustomerEntityById(customerDelta.Dto.Id);
            if (currentCustomer == null)
            {
                return Error(HttpStatusCode.NotFound, "customer", "not found");
            }

            var changePasswordRequest = new ChangePasswordRequest(currentCustomer.Email, true, _customerSettings.DefaultPasswordFormat, customerDelta.Dto.Password, customerDelta.Dto.OldPassword);
            string message = string.Empty;
            if (!CreateOrUpdate(changePasswordRequest, ref message))
            {
                return Error(HttpStatusCode.BadRequest, "Password", message);
            }

            //activity log
            _customerActivityService.InsertActivity("ChangePassword", _localizationService.GetResource("ActivityLog.ChangePassword"), currentCustomer.Id);

            return new RawJsonActionResult("{}");
        }
        #endregion

        #endregion

        /// [TuanDang][13-12-2018][Tìm kiếm khách hàng]
        /// <summary>
        /// </summary>
        /// <response code="200">OK</response>
        /// <response code="400">Bad request</response>
        /// <response code="401">Unauthorized</response>
        [HttpGet]
        [Route("/api/customers")]
        [ProducesResponseType(typeof(CustomersRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult GetCustomers(CustomersParametersModel parameters)
        {
            if (parameters.PageSize < Configurations.MinLimit || parameters.PageSize > Configurations.MaxLimit)
            {
                return Error(HttpStatusCode.BadRequest, "limit", "Invalid limit parameter");
            }

            if (parameters.PageNumber <= 0)
            {
                return Error(HttpStatusCode.BadRequest, "page", "Invalid request parameters");
            }

            var customers = _customerService.GetAllCustomers(customerRoleIds: parameters.Roles?.ToArray(), pageIndex: parameters.PageNumber - 1, pageSize: parameters.PageSize, keywords: parameters.Keyword,  customerType: parameters.CustomerType);

            var data = customers.Select(c => _dtoHelper.PrepareCustomerDTOModel(c)).ToList();
            var customersRootObject = new CustomersRootObject()
            {
                Customers = data,
                PageNumber = customers.PageIndex + 1,
                PageSize = customers.PageSize,
                TotalPage = customers.TotalPages,
                TotalRow = customers.TotalCount
            };

            var json = _jsonFieldsSerializer.Serialize(customersRootObject, parameters.Fields);

            return new RawJsonActionResult(json);
        }

        /// [TuanDang][13-12-2018][Chi tiet khach hangf]
        /// <summary>
        /// Retrieve customer by spcified id
        /// </summary>
        /// <param name="id">Id of the customer</param>
        /// <param name="fields">Fields from the customer you want your json to contain</param>
        /// <response code="200">OK</response>
        /// <response code="404">Not Found</response>
        /// <response code="401">Unauthorized</response>
        [HttpGet]
        [Route("/api/customers/{id:int}")]
        [ProducesResponseType(typeof(CustomersRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult GetCustomerById(int id, string fields = "")
        {
            if (id <= 0)
            {
                return Error(HttpStatusCode.BadRequest, "id", "invalid id");
            }

            var customer = _customerService.GetCustomerById(id);

            if (customer == null)
            {
                return Error(HttpStatusCode.NotFound, "customer", "not found");
            }
            var model = _dtoHelper.PrepareCustomerDTOModel(customer);

            var json = _jsonFieldsSerializer.Serialize(model, fields);

            return new RawJsonActionResult(json);
        }


        /// <summary>
        /// Get a count of all customers
        /// </summary>
        /// <response code="200">OK</response>
        /// <response code="401">Unauthorized</response>
        [HttpGet]
        [Route("/api/customers/count")]
        [ProducesResponseType(typeof(CustomersCountRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        public IActionResult GetCustomersCount()
        {
            var allCustomersCount = _customerApiService.GetCustomersCount();

            var customersCountRootObject = new CustomersCountRootObject()
            {
                Count = allCustomersCount
            };

            return Ok(customersCountRootObject);
        }

        #region Search for customers matching supplied query
        /// <summary>
        /// Search for customers matching supplied query
        /// </summary>
        /// <response code="200">OK</response>
        /// <response code="400">Bad Request</response>
        /// <response code="401">Unauthorized</response>
        [HttpGet]
        [Route("/api/customers/search")]
        [ProducesResponseType(typeof(CustomersRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        public IActionResult Search(CustomersSearchParametersModel parameters)
        {
            if (parameters.Limit <= Configurations.MinLimit || parameters.Limit > Configurations.MaxLimit)
            {
                return Error(HttpStatusCode.BadRequest, "limit", "Invalid limit parameter");
            }

            if (parameters.Page <= 0)
            {
                return Error(HttpStatusCode.BadRequest, "page", "Invalid page parameter");
            }

            IList<CustomerDto> customersDto = _customerApiService.Search(parameters.Query, parameters.Order, parameters.Page, parameters.Limit);

            var customersRootObject = new CustomersRootObject()
            {
                Customers = customersDto.Where(s => s != null).Select(s => CustomerDtoModel.CopyDataFromDto(s)).ToList()
            };

            var json = _jsonFieldsSerializer.Serialize(customersRootObject, parameters.Fields);

            return new RawJsonActionResult(json);
        }
        #endregion

        #region UpdateCustomer [Hàm cập nhập thông tin khách hàng]
        [HttpPut]
        [Route("/api/customers/{id}")]
        [ProducesResponseType(typeof(CustomersRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), 422)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        public IActionResult UpdateCustomer([ModelBinder(typeof(JsonModelBinder<CustomerDto>))] Delta<CustomerDto> customerDelta)
        {
            // Here we display the errors if the validation has failed at some point.
            if (!ModelState.IsValid)
            {
                return Error();
            }

            //If the validation has passed the customerDelta object won't be null for sure so we don't need to check for this.

            // Updateting the customer
            Customer currentCustomer = _customerApiService.GetCustomerEntityById(customerDelta.Dto.Id);

            if (currentCustomer == null)
            {
                return Error(HttpStatusCode.NotFound, "customer", "not found");
            }

            customerDelta.Merge(currentCustomer);

            if (customerDelta.Dto.RoleIds.Count > 0)
            {
                // Remove all roles
                while (currentCustomer.CustomerRoles.Count > 0)
                {
                    currentCustomer.CustomerRoles.Remove(currentCustomer.CustomerRoles.First());
                }

                AddValidRoles(customerDelta, currentCustomer);
            }

            if (customerDelta.Dto.CustomerAddresses.Count > 0)
            {
                var currentCustomerAddresses = currentCustomer.Addresses.ToDictionary(address => address.Id, address => address);

                foreach (var passedAddress in customerDelta.Dto.CustomerAddresses)
                {
                    int passedAddressId = int.Parse(passedAddress.Id);
                    Address addressEntity = passedAddress.ToEntity();

                    if (currentCustomerAddresses.ContainsKey(passedAddressId))
                    {
                        _mappingHelper.Merge(passedAddress, currentCustomerAddresses[passedAddressId]);
                    }
                    else
                    {
                        currentCustomer.Addresses.Add(addressEntity);
                    }
                }
            }

            _customerService.UpdateCustomer(currentCustomer);

            InsertFirstAndLastNameGenericAttributes(customerDelta.Dto.FirstName, customerDelta.Dto.LastName, currentCustomer);


            int languageId = 0;

            if (!string.IsNullOrEmpty(customerDelta.Dto.LanguageId) && int.TryParse(customerDelta.Dto.LanguageId, out languageId)
                && _languageService.GetLanguageById(languageId) != null)
            {
                _genericAttributeService.SaveAttribute(currentCustomer, SystemCustomerAttributeNames.LanguageId, languageId);
            }



            // TODO: Localization

            // Preparing the result dto of the new customer
            // We do not prepare the shopping cart items because we have a separate endpoint for them.
            CustomerDto updatedCustomer = currentCustomer.ToDto();

            // This is needed because the entity framework won't populate the navigation properties automatically
            // and the country name will be left empty because the mapping depends on the navigation property
            // so we do it by hand here.
            PopulateAddressCountryNames(updatedCustomer);

            // Set the fist and last name separately because they are not part of the customer entity, but are saved in the generic attributes.
            var firstNameGenericAttribute = _genericAttributeService.GetAttributesForEntity(currentCustomer.Id, typeof(Customer).Name)
                .FirstOrDefault(x => x.Key == "FirstName");

            if (firstNameGenericAttribute != null)
            {
                updatedCustomer.FirstName = firstNameGenericAttribute.Value;
            }

            var lastNameGenericAttribute = _genericAttributeService.GetAttributesForEntity(currentCustomer.Id, typeof(Customer).Name)
                .FirstOrDefault(x => x.Key == "LastName");

            if (lastNameGenericAttribute != null)
            {
                updatedCustomer.LastName = lastNameGenericAttribute.Value;
            }

            var languageIdGenericAttribute = _genericAttributeService.GetAttributesForEntity(currentCustomer.Id, typeof(Customer).Name)
                .FirstOrDefault(x => x.Key == "LanguageId");

            if (languageIdGenericAttribute != null)
            {
                updatedCustomer.LanguageId = languageIdGenericAttribute.Value;
            }

            //activity log
            _customerActivityService.InsertActivity("UpdateCustomer", _localizationService.GetResource("ActivityLog.UpdateCustomer"), currentCustomer.Id);

            var customersRootObject = new CustomersRootObject();

            //customersRootObject.Customers.Add(updatedCustomer);

            var json = _jsonFieldsSerializer.Serialize(customersRootObject, string.Empty);

            return new RawJsonActionResult(json);
        }
        #endregion


        [HttpDelete]
        [Route("/api/customers/{id:int}")]
        [GetRequestsErrorInterceptorActionFilter]
        [ProducesResponseType(typeof(ResultObjectModel<>), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        public IActionResult DeleteCustomer(int id)
        {
            if (id <= 0)
            {
                return Error(HttpStatusCode.BadRequest, "id", "invalid id");
            }

            Customer customer = _customerApiService.GetCustomerEntityById(id);

            if (customer == null)
            {
                return Error(HttpStatusCode.BadRequest, "customer", "not found");
            }

            _customerService.DeleteCustomer(customer);

            //remove newsletter subscription (if exists)
            foreach (var store in _storeService.GetAllStores())
            {
                var subscription = _newsLetterSubscriptionService.GetNewsLetterSubscriptionByEmailAndStoreId(customer.Email, store.Id);
                if (subscription != null)
                    _newsLetterSubscriptionService.DeleteNewsLetterSubscription(subscription);
            }

            //activity log
            _customerActivityService.InsertActivity("DeleteCustomer", _localizationService.GetResource("ActivityLog.DeleteCustomer"), customer.Id);
                       
            var json = _jsonFieldsSerializer.Serialize(new ResultObjectModel<int>
            {
                Success = true,
                Message = _localizationService.GetResource("Customers.Delete.Success")
            }, string.Empty);

            return new RawJsonActionResult(json);
        }

        private void InsertFirstAndLastNameGenericAttributes(string firstName, string lastName, Customer newCustomer)
        {
            // we assume that if the first name is not sent then it will be null and in this case we don't want to update it
            if (firstName != null)
            {
                _genericAttributeService.SaveAttribute(newCustomer, SystemCustomerAttributeNames.FirstName, firstName);
            }

            if (lastName != null)
            {
                _genericAttributeService.SaveAttribute(newCustomer, SystemCustomerAttributeNames.LastName, lastName);
            }
        }

        private void AddValidRoles(Delta<CustomerDto> customerDelta, Customer currentCustomer)
        {
            IList<CustomerRole> validCustomerRoles =
                _customerRolesHelper.GetValidCustomerRoles(customerDelta.Dto.RoleIds).ToList();

            // Add all newly passed roles
            foreach (var role in validCustomerRoles)
            {
                currentCustomer.CustomerRoles.Add(role);
            }
        }

        private void PopulateAddressCountryNames(CustomerDto newCustomerDto)
        {
            foreach (var address in newCustomerDto.CustomerAddresses)
            {
                SetCountryName(address);
            }

            if (newCustomerDto.BillingAddress != null)
            {
                SetCountryName(newCustomerDto.BillingAddress);
            }

            if (newCustomerDto.ShippingAddress != null)
            {
                SetCountryName(newCustomerDto.ShippingAddress);
            }
        }

        private void SetCountryName(AddressDto address)
        {
            if (string.IsNullOrEmpty(address.CountryName) && address.CountryId.HasValue)
            {
                Country country = _countryService.GetCountryById(address.CountryId.Value);
                address.CountryName = country.Name;
            }
        }

        private bool CreateOrUpdate(ChangePasswordRequest request, ref string message)
        {
            if (request != null)
            {
                var changePassResult = _customerRegistrationService.ChangePassword(request);
                if (!changePassResult.Success)
                {
                    return false;
                }
            }
            return true;
        }
    }
}