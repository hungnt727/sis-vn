﻿using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Core.Domain.Customers;
using Nop.Plugin.Api.Services;
using Nop.Services.Catalog;
using Nop.Services.Seo;
using Nop.Core.Domain.Catalog;
using Nop.Plugin.Api.Factories;
using Nop.Plugin.Api.Helpers;
using Nop.Plugin.Api.JSON.Serializers;
using Nop.Services.Logging;
using Nop.Services.Localization;
using Nop.Services.Media;
using Nop.Services.Stores;
using Nop.Services.Discounts;
using Nop.Services.Security;
using Nop.Services.Customers;
using System.Net.Http;
using Microsoft.AspNetCore.Http;
using Nop.Plugin.Api.Attributes;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Nop.Plugin.Api.Models;

namespace Nop.Plugin.Api.Controllers
{
    [Route("/api/auth")]
    public class AuthenticationController : BaseApiController
    {
        private readonly ICategoryApiService _categoryApiService;
        private readonly CustomerSettings _customerSettings;
        private readonly ICategoryService _categoryService;
        private readonly IUrlRecordService _urlRecordService;
        private readonly IFactory<Category> _factory;
        private readonly IDTOHelper _dtoHelper;
        private readonly IWorkContext _workContext;
        private readonly ICustomerRegistrationService _customerRegistrationService;

        public AuthenticationController(
            CustomerSettings customerSettings,
            IClientService _clientService,
            IHttpContextAccessor _httpContextAccessor,
            IWorkContext workContext,
            ICategoryApiService categoryApiService,
            IJsonFieldsSerializer jsonFieldsSerializer,
            ICategoryService categoryService,
            IUrlRecordService urlRecordService,
            ICustomerActivityService customerActivityService,
            ILocalizationService localizationService,
            IPictureService pictureService,
            IStoreMappingService storeMappingService,
            IStoreService storeService,
            IDiscountService discountService,
            IAclService aclService,
            ICustomerService customerService,
            IFactory<Category> factory,
            IDTOHelper dtoHelper,
            ICustomerRegistrationService customerRegistrationService
            )
        {
            _customerSettings = customerSettings;
            _customerRegistrationService = customerRegistrationService;
            _workContext = workContext;
            _categoryApiService = categoryApiService;
            _categoryService = categoryService;
            _urlRecordService = urlRecordService;
            _factory = factory;
            _dtoHelper = dtoHelper;
        }

        [Route("callback")]
        [HttpGet]
        public IActionResult Callback(string code, string scope)
        {
            if (string.IsNullOrEmpty(code))
            {
                return BadRequest("code is empty");
            }
            return Json(new { code, scope });
        }

        [Route("login")]
        [HttpPost]
        public IActionResult Login(string username, string password)
        {
            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
            {
                return Json(new ResultObjectModel<object>
                {
                    Success = false,
                    Message = _localizationService.GetResource("SIS.Login.UsernameOrPasswordIsRequired")
                });
            }
            var result = _customerRegistrationService.ValidateCustomer(username, password);
            if (result != CustomerLoginResults.Successful)
            {
                return Json(new ResultObjectModel<object>
                {
                    Success = false,
                    Message = GetLoginErrorMessage(result)
                });
            }
            var customer = _customerSettings.UsernamesEnabled ?
                _customerService.GetCustomerByUsername(username) :
                _customerService.GetCustomerByEmail(username);
            var clientCustomer = _clientService.GetAllClientCustomers()
                .FirstOrDefault(cc => cc.CustomerId == customer.Id);
            if (clientCustomer == null)
            {
                return Json(new ResultObjectModel<object>
                {
                    Success = false,
                    Message = _localizationService.GetResource("SIS.Login.CustomerDoesnotHaveClient")
                });
            }

            var clientModel = _clientService.FindClientByIdAsync(clientCustomer.ClientId);

            return Json(new ResultObjectModel<ClientApiModel>
            {
                Success = true,
                Message = null,
                Data = clientModel
            });
        }

        private string GetLoginErrorMessage(CustomerLoginResults result)
        {
            var errorMessage = "";
            switch (result)
            {
                case CustomerLoginResults.CustomerNotExist:
                    errorMessage = _localizationService.GetResource("SIS.Login.CustomerNotExist");
                    break;
                case CustomerLoginResults.WrongPassword:
                    errorMessage = _localizationService.GetResource("SIS.Login.WrongPassword");
                    break;
                case CustomerLoginResults.NotRegistered:
                    errorMessage = _localizationService.GetResource("SIS.Login.NotRegistered");
                    break;
                case CustomerLoginResults.NotActive:
                    errorMessage = _localizationService.GetResource("SIS.Login.NotActive");
                    break;
                case CustomerLoginResults.LockedOut:
                    errorMessage = _localizationService.GetResource("SIS.Login.LockedOut");
                    break;
                case CustomerLoginResults.Deleted:
                    errorMessage = _localizationService.GetResource("SIS.Login.Deleted");
                    break;
            }
            return errorMessage;
        }
    }
}
