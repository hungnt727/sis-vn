﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Discounts;
using Nop.Core.Domain.Media;
using Nop.Plugin.Api.Attributes;
using Nop.Plugin.Api.Constants;
using Nop.Plugin.Api.DTOs.Categories;
using Nop.Plugin.Api.Models.CategoriesParameters;
using Nop.Plugin.Api.Services;
using Nop.Services.Catalog;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Seo;
using Nop.Plugin.Api.Delta;
using Nop.Plugin.Api.DTOs.Images;
using Nop.Plugin.Api.Factories;
using Nop.Plugin.Api.JSON.ActionResults;
using Nop.Plugin.Api.ModelBinders;
using Nop.Services.Customers;
using Nop.Services.Discounts;
using Nop.Services.Media;
using Nop.Services.Security;
using Nop.Services.Stores;
using Nop.Plugin.Api.Helpers;
using Microsoft.AspNetCore.Mvc;

namespace Nop.Plugin.Api.ControllersMO
{
    using Microsoft.AspNetCore.Authentication.JwtBearer;
    using Microsoft.AspNetCore.Authorization;
    using Nop.Core;
    using Nop.Plugin.Api.Controllers;
    using Nop.Plugin.Api.DTOs.Errors;
    using Nop.Plugin.Api.JSON.Serializers;

    [ApiAuthorize(Policy = JwtBearerDefaults.AuthenticationScheme, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class CategoriesController : BaseApiController
    {
        private readonly ICategoryApiService _categoryApiService;
        private readonly ICategoryService _categoryService;
        private readonly IUrlRecordService _urlRecordService;
        private readonly IFactory<Category> _factory;
        private readonly IDTOHelper _dtoHelper;
        private readonly IWorkContext _workContext;

        public CategoriesController(
            IWorkContext workContext,
            ICategoryApiService categoryApiService,
            IJsonFieldsSerializer jsonFieldsSerializer,
            ICategoryService categoryService,
            IUrlRecordService urlRecordService,
            ICustomerActivityService customerActivityService,
            ILocalizationService localizationService,
            IPictureService pictureService,
            IStoreMappingService storeMappingService,
            IStoreService storeService,
            IDiscountService discountService,
            IAclService aclService,
            ICustomerService customerService,
            IFactory<Category> factory,
            IDTOHelper dtoHelper)
        {
            _workContext = workContext;
            _categoryApiService = categoryApiService;
            _categoryService = categoryService;
            _urlRecordService = urlRecordService;
            _factory = factory;
            _dtoHelper = dtoHelper;
        }

        #region Custom Method


        //[HttpGet]
        //[Route("/api/search_category")]
        //[ProducesResponseType(typeof(CategoriesRootObjectModel), (int)HttpStatusCode.OK)]
        //[ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        //[GetRequestsErrorInterceptorActionFilter]
        //IActionResult SearchCategory(CategoriesParametersModel parameters)
        //{
        //    if (parameters.Limit < Configurations.MinLimit || parameters.Limit > Configurations.MaxLimit)
        //    {
        //        return Error(HttpStatusCode.BadRequest, "limit", "Invalid limit parameter");
        //    }

        //    if (parameters.Page < Configurations.DefaultPageValue)
        //    {
        //        return Error(HttpStatusCode.BadRequest, "page", "Invalid page parameter");
        //    }

        //    var allCategories = _categoryApiService.GetCategories(parameters.Keyword, parameters.Ids, parameters.CreatedAtMin, parameters.CreatedAtMax,
        //                                                                     parameters.UpdatedAtMin, parameters.UpdatedAtMax,
        //                                                                     parameters.Limit, parameters.Page, parameters.SinceId,
        //                                                                     parameters.ProductId, parameters.PublishedStatus)
        //                                           .Where(c => _storeMappingService.Authorize(c));

        //    IList<CategoryDtoModel> categoriesAsDtos = allCategories.Select(category =>
        //    {
        //        return _dtoHelper.PrepareCategoryDTOModel(category);

        //    }).ToList();

        //    var categoriesRootObject = new CategoriesRootObjectModel()
        //    {
        //        Categories = categoriesAsDtos
        //    };

        //    var json = _jsonFieldsSerializer.Serialize(categoriesRootObject, string.Empty);

        //    return new RawJsonActionResult(json);
        //}

        /// <summary>
        /// [HienNguyen][07/08/2018][Hàm tìm kiếm loại hàng hóa]
        /// </summary>
        /// <param name="keyword"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("/api/categories")]
        [ProducesResponseType(typeof(CategoriesRootObjectModel), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult GetCategories(CategoriesParametersModel parameters)
        {
            if (!Authorize(StandardPermissionProvider.ManageCategories))
            {
                return Error(HttpStatusCode.BadRequest, "", "Unauthorized category");
            }
            if (parameters.Page <= 0) parameters.Page = 1;
            if (parameters.Limit <= 0) parameters.Limit = int.MaxValue;
            //if (parameters.Limit < Configurations.MinLimit || parameters.Limit > Configurations.MaxLimit)
            //{
            //    return Error(HttpStatusCode.BadRequest, "limit", "Invalid limit parameter");
            //}

            //if (parameters.Page < Configurations.DefaultPageValue)
            //{
            //    return Error(HttpStatusCode.BadRequest, "page", "Invalid page parameter");
            //}

            var allCategories = _categoryApiService.GetCategories(parameters.Keyword, parameters.Ids, parameters.CreatedAtMin, parameters.CreatedAtMax,
                                                                             parameters.UpdatedAtMin, parameters.UpdatedAtMax,
                                                                             parameters.Limit, parameters.Page, parameters.SinceId,
                                                                             parameters.ProductId, parameters.PublishedStatus)
                                                   .Where(c => _storeMappingService.Authorize(c));

            IList<CategoryDtoModel> categoriesAsDtos = allCategories.Select(category =>
            {
                return _dtoHelper.PrepareCategoryDTOModel(category);

            }).ToList();

            var categoriesRootObject = new CategoriesRootObjectModel()
            {
                Categories = categoriesAsDtos
            };

            var json = _jsonFieldsSerializer.Serialize(categoriesRootObject, parameters.Fields);

            return new RawJsonActionResult(json);
        }

        /// <summary>
        /// [HienNguyen][08/08/2018][Cập nhật nhóm hàng hóa]
        /// </summary>
        /// <param name="CategoryDelta"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("/api/categories")]
        [ProducesResponseType(typeof(CategoryDtoModel), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), 422)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        public IActionResult CreateCategory([ModelBinder(typeof(JsonModelBinder<CategoryDto>))] Delta<CategoryDto> categoryDelta)
        {
            if (!Authorize(StandardPermissionProvider.ManageCategories))
            {
                return Error(HttpStatusCode.BadRequest, "", "Unauthorized category");
            }
            // Here we display the errors if the validation has failed at some point.
            if (!ModelState.IsValid)
            {
                return Error();
            }

            //If the validation has passed the categoryDelta object won't be null for sure so we don't need to check for this.

            Picture insertedPicture = null;

            // We need to insert the picture before the category so we can obtain the picture id and map it to the category.
            if (categoryDelta.Dto.Image != null && categoryDelta.Dto.Image.Binary != null)
            {
                insertedPicture = _pictureService.InsertPicture(categoryDelta.Dto.Image.Binary, categoryDelta.Dto.Image.MimeType, string.Empty);
            }

            // Inserting the new category
            Category category = _factory.Initialize();
            categoryDelta.Merge(category);

            if (insertedPicture != null)
            {
                category.PictureId = insertedPicture.Id;
            }

            _categoryService.InsertCategory(category);


            UpdateAclRoles(category, categoryDelta.Dto.RoleIds);

            UpdateDiscounts(category, categoryDelta.Dto.DiscountIds);

            UpdateStoreMappings(category, categoryDelta.Dto.StoreIds);

            //search engine name
            if (categoryDelta.Dto.SeName != null)
            {
                var seName = category.ValidateSeName(categoryDelta.Dto.SeName, category.Name, true);
                _urlRecordService.SaveSlug(category, seName, 0);
            }

            _customerActivityService.InsertActivity("AddNewCategory",
                _localizationService.GetResource("ActivityLog.AddNewCategory"), category.Name);

            // Preparing the result dto of the new category
            CategoryDto newCategoryDto = _dtoHelper.PrepareCategoryDTO(category);

            var json = _jsonFieldsSerializer.Serialize(newCategoryDto, string.Empty);

            return new RawJsonActionResult(json);
        }


        /// <summary>
        /// [HienNguyen][08/08/2018][Cập nhật nhóm hàng hóa]
        /// </summary>
        /// <param name="CategoryDelta"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("/api/categories/{id}")]
        [ProducesResponseType(typeof(CategoryDtoModel), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), 422)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        public IActionResult UpdateCategory(
            [ModelBinder(typeof(JsonModelBinder<CategoryDto>))] Delta<CategoryDto> categoryDelta)
        {
            if (!Authorize(StandardPermissionProvider.ManageCategories))
            {
                return Error(HttpStatusCode.BadRequest, "", "Unauthorized category");
            }
            // Here we display the errors if the validation has failed at some point.
            if (!ModelState.IsValid)
            {
                return Error();
            }

            // We do not need to validate the category id, because this will happen in the model binder using the dto validator.
            int updateCategoryId = int.Parse(categoryDelta.Dto.Id);

            Category category = _categoryApiService.GetCategoryById(updateCategoryId);

            if (category == null || category.Deleted)
            {
                return Error(HttpStatusCode.NotFound, "category", "category not found");
            }
            if (!_storeMappingService.Authorize(category))
            {
                return Error(HttpStatusCode.BadRequest, "", "Unauthorized specified category");
            }

            categoryDelta.Merge(category);

            category.UpdatedOnUtc = DateTime.UtcNow;

            _categoryService.UpdateCategory(category);

            UpdatePicture(category, categoryDelta.Dto.Image);

            UpdateAclRoles(category, categoryDelta.Dto.RoleIds);

            UpdateDiscounts(category, categoryDelta.Dto.DiscountIds);

            UpdateStoreMappings(category, categoryDelta.Dto.StoreIds);

            //search engine name
            if (categoryDelta.Dto.SeName != null)
            {
                var seName = category.ValidateSeName(categoryDelta.Dto.SeName, category.Name, true);
                _urlRecordService.SaveSlug(category, seName, 0);
            }

            _categoryService.UpdateCategory(category);

            _customerActivityService.InsertActivity("UpdateCategory",
                _localizationService.GetResource("ActivityLog.UpdateCategory"), category.Name);

            CategoryDto categoryDto = _dtoHelper.PrepareCategoryDTO(category);

            var json = _jsonFieldsSerializer.Serialize(categoryDto, string.Empty);

            return new RawJsonActionResult(json);
        }

        [HttpDelete]
        [Route("/api/categories/{id}")]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult DeleteCategory(int id)
        {
            if (!Authorize(StandardPermissionProvider.ManageCategories))
            {
                return Error(HttpStatusCode.BadRequest, "", "Unauthorized category");
            }
            if (id <= 0)
            {
                return Error(HttpStatusCode.BadRequest, "id", "invalid id");
            }

            Category category = _categoryApiService.GetCategoryById(id);
            
            if (category == null || category.Deleted)
            {
                return Error(HttpStatusCode.NotFound, "category", "category not found");
            }
            if (!_storeMappingService.Authorize(category))
            {
                return Error(HttpStatusCode.BadRequest, "", "Unauthorized specified category");
            }

            _categoryService.DeleteCategory(category);

            //activity log
            _customerActivityService.InsertActivity("DeleteCategory", _localizationService.GetResource("ActivityLog.DeleteCategory"), category.Name);

            return new RawJsonActionResult("{}");
        }
        #endregion

        /// <summary>
        /// Receive a count of all Categories
        /// </summary>
        /// <response code="200">OK</response>
        /// <response code="401">Unauthorized</response>
        [HttpGet]
        [Route("/api/categories/count")]
        [ProducesResponseType(typeof(CategoriesCountRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult GetCategoriesCount(CategoriesCountParametersModel parameters)
        {
            if (!Authorize(StandardPermissionProvider.ManageCategories))
            {
                return Error(HttpStatusCode.BadRequest, "", "Unauthorized category");
            }
            var allCategoriesCount = _categoryApiService.GetCategoriesCount(parameters.CreatedAtMin, parameters.CreatedAtMax,
                                                                            parameters.UpdatedAtMin, parameters.UpdatedAtMax,
                                                                            parameters.PublishedStatus, parameters.ProductId);

            var categoriesCountRootObject = new CategoriesCountRootObject()
            {
                Count = allCategoriesCount
            };

            return Ok(categoriesCountRootObject);
        }

        /// <summary>
        /// Retrieve category by spcified id
        /// </summary>
        /// <param name="id">Id of the category</param>
        /// <param name="fields">Fields from the category you want your json to contain</param>
        /// <response code="200">OK</response>
        /// <response code="404">Not Found</response>
        /// <response code="401">Unauthorized</response>
        [HttpGet]
        [Route("/api/categories/{id}")]
        [ProducesResponseType(typeof(CategoryDtoModel), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult GetCategoryById(int id, string fields = "")
        {
            if (!Authorize(StandardPermissionProvider.ManageCategories))
            {
                return Error(HttpStatusCode.BadRequest, "", "Unauthorized category");
            }
            if (id <= 0)
            {
                return Error(HttpStatusCode.BadRequest, "id", "invalid id");
            }

            Category category = _categoryApiService.GetCategoryById(id);

            if (category == null || category.Deleted)
            {
                return Error(HttpStatusCode.NotFound, "category", "category not found");
            }
            if (!_storeMappingService.Authorize(category))
            {
                return Error(HttpStatusCode.BadRequest, "", "Unauthorized specified category");
            }

            CategoryDto categoryDto = _dtoHelper.PrepareCategoryDTO(category);

            var json = _jsonFieldsSerializer.Serialize(categoryDto, fields);

            return new RawJsonActionResult(json);
        }

        private void UpdatePicture(Category categoryEntityToUpdate, ImageDto imageDto)
        {
            // no image specified then do nothing
            if (imageDto == null)
                return;

            Picture updatedPicture = null;
            Picture currentCategoryPicture = _pictureService.GetPictureById(categoryEntityToUpdate.PictureId);

            // when there is a picture set for the category
            if (currentCategoryPicture != null)
            {
                _pictureService.DeletePicture(currentCategoryPicture);

                // When the image attachment is null or empty.
                if (imageDto.Binary == null)
                {
                    categoryEntityToUpdate.PictureId = 0;
                }
                else
                {
                    updatedPicture = _pictureService.InsertPicture(imageDto.Binary, imageDto.MimeType, string.Empty);
                    categoryEntityToUpdate.PictureId = updatedPicture.Id;
                }
            }
            // when there isn't a picture set for the category
            else
            {
                if (imageDto.Binary != null)
                {
                    updatedPicture = _pictureService.InsertPicture(imageDto.Binary, imageDto.MimeType, string.Empty);
                    categoryEntityToUpdate.PictureId = updatedPicture.Id;
                }
            }
        }

        private void UpdateDiscounts(Category category, List<int> passedDiscountIds)
        {
            if (passedDiscountIds == null)
                return;

            var allDiscounts = _discountService.GetAllDiscounts(DiscountType.AssignedToCategories, showHidden: true);
            foreach (var discount in allDiscounts)
            {
                if (passedDiscountIds.Contains(discount.Id))
                {
                    //new discount
                    if (category.AppliedDiscounts.Count(d => d.Id == discount.Id) == 0)
                        category.AppliedDiscounts.Add(discount);
                }
                else
                {
                    //remove discount
                    if (category.AppliedDiscounts.Count(d => d.Id == discount.Id) > 0)
                        category.AppliedDiscounts.Remove(discount);
                }
            }
            _categoryService.UpdateCategory(category);
        }
    }
}