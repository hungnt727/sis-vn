﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using Nop.Core.Domain.Catalog;
using Nop.Plugin.Api.Attributes;
using Nop.Plugin.Api.Factories;
using Nop.Plugin.Api.JSON.ActionResults;
using Nop.Plugin.Api.Services;
using Nop.Services.Catalog;
using Nop.Services.Customers;
using Nop.Services.Discounts;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Media;
using Nop.Services.Security;
using Nop.Services.Seo;
using Nop.Services.Stores;
using Nop.Plugin.Api.Helpers;

namespace Nop.Plugin.Api.Controllers
{
    using Microsoft.AspNetCore.Authentication.JwtBearer;
    using Microsoft.AspNetCore.Mvc;
    using Nop.Plugin.Api.Delta;
    using Nop.Plugin.Api.DTOs.Categories;
    using Nop.Plugin.Api.DTOs.Errors;
    using Nop.Plugin.Api.DTOs.ProductTemplate;
    using Nop.Plugin.Api.JSON.Serializers;
    using Nop.Plugin.Api.ModelBinders;

    [ApiAuthorize(Policy = JwtBearerDefaults.AuthenticationScheme, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class ProductTemplateController : BaseApiController
    {
        private readonly IProductApiService _productApiService;
        private readonly IProductService _productService;
        private readonly IUrlRecordService _urlRecordService;
        private readonly IPictureService _pictureService;
        private readonly IManufacturerService _manufacturerService;
        private readonly IFactory<Product> _factory;
        private readonly IProductTagService _productTagService;
        private readonly IProductAttributeService _productAttributeService;
        private readonly IProductTemplateService _productTemplateService;
        private readonly IDTOHelper _dtoHelper;

        public ProductTemplateController(IProductApiService productApiService,
                                  IJsonFieldsSerializer jsonFieldsSerializer,
                                  IProductService productService,
                                  IUrlRecordService urlRecordService,
                                  ICustomerActivityService customerActivityService,
                                  ILocalizationService localizationService,
                                  IFactory<Product> factory,
                                  IAclService aclService,
                                  IStoreMappingService storeMappingService,
                                  IStoreService storeService,
                                  ICustomerService customerService,
                                  IDiscountService discountService,
                                  IPictureService pictureService,
                                  IManufacturerService manufacturerService,
                                  IProductTagService productTagService,
                                  IProductAttributeService productAttributeService,
                                  IProductTemplateService productTemplateService,
                                  IDTOHelper dtoHelper)
        {
            _productApiService = productApiService;
            _factory = factory;
            _manufacturerService = manufacturerService;
            _productTagService = productTagService;
            _urlRecordService = urlRecordService;
            _productService = productService;
            _productAttributeService = productAttributeService;
            _dtoHelper = dtoHelper;
            _pictureService = pictureService;
            _productTemplateService = productTemplateService;
        }

        #region Custom Method

        /// <summary>
        /// [HienNguyen][07/08/2018][Hàm tìm kiếm loại hàng hóa]
        /// </summary>
        /// <param name="keyword"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("/api/product-templates")]
        [ProducesResponseType(typeof(ProductTemplateRootObjectModel), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult SearchProductTemplate(string keyword)
        {
            var listProductTemplate = _productTemplateService.GetAllProductTemplates();

            if (!string.IsNullOrEmpty(keyword))
            {
                listProductTemplate = listProductTemplate.Where(m => m.Name.ToLower().Contains(keyword.ToLower())).ToList();
            }

            IList<ProductTemplateDtoModel> productTemplateAsDtos = listProductTemplate.Select(productTemplate =>
            {
                return _dtoHelper.PrepareProductTemplateDTOModel(productTemplate);

            }).ToList();

            var productTemplateRootObject = new ProductTemplateRootObjectModel()
            {
                ProductTemplates = productTemplateAsDtos
            };

            var json = _jsonFieldsSerializer.Serialize(productTemplateRootObject, string.Empty);

            return new RawJsonActionResult(json);
        }

        /// <summary>
        /// [HienNguyen][15/08/2018][Cập nhật bộ sưu tập]
        /// </summary>
        /// <param name="productTemplateDelta"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("/api/product-templates")]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult CreateProductTemplate([ModelBinder(typeof(JsonModelBinder<ProductTemplateDtoModel>))] Delta<ProductTemplateDtoModel> productTemplateDelta)
        {
            _productTemplateService.InsertProductTemplate(new ProductTemplate()
            {
                DisplayOrder = productTemplateDelta.Dto.DisplayOrder,
                IgnoredProductTypes = productTemplateDelta.Dto.IgnoredProductTypes,
                Name = productTemplateDelta.Dto.Name,
                ViewPath = productTemplateDelta.Dto.ViewPath
            });

            //activity log
            _customerActivityService.InsertActivity("AddNewProductTemplate", _localizationService.GetResource("ActivityLog.AddNewProductTemplate"), 0);

            return new RawJsonActionResult("{}");
        }

        /// <summary>
        /// [HienNguyen][15/08/2018][Cập nhật bộ sưu tập]
        /// </summary>
        /// <param name="productTemplateDelta"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("/api/product-templates/{id}")]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult UpdateProductTemplate([ModelBinder(typeof(JsonModelBinder<ProductTemplateDtoModel>))] Delta<ProductTemplateDtoModel> productTemplateDelta)
        {
            ProductTemplate ProductTemplate = _productTemplateService.GetProductTemplateById(productTemplateDelta.Dto.Id);

            ProductTemplate.DisplayOrder = productTemplateDelta.Dto.DisplayOrder;
            ProductTemplate.IgnoredProductTypes = productTemplateDelta.Dto.IgnoredProductTypes;
            ProductTemplate.Name = productTemplateDelta.Dto.Name;
            ProductTemplate.ViewPath = productTemplateDelta.Dto.ViewPath;

            _productTemplateService.UpdateProductTemplate(ProductTemplate);

            //activity log
            _customerActivityService.InsertActivity("EditProductTemplate", _localizationService.GetResource("ActivityLog.EditProductTemplate"), 0);

            return new RawJsonActionResult("{}");
        }

        [HttpDelete]
        [Route("/api/product-templates/{id}")]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult DeleteProductTemplate(int id)
        {
            if (id <= 0)
            {
                return Error(HttpStatusCode.BadRequest, "id", "invalid id");
            }

            var productTemplateToDelete = _productTemplateService.GetProductTemplateById(id);

            if (productTemplateToDelete == null)
            {
                return Error(HttpStatusCode.NotFound, "producttemplate", "product template not found");
            }

            _productTemplateService.DeleteProductTemplate(productTemplateToDelete);

            //activity log
            _customerActivityService.InsertActivity("DeleteProductTemplate", _localizationService.GetResource("ActivityLog.DeleteProductTemplate"), productTemplateToDelete.Name);

            return new RawJsonActionResult("{}");
        }
        #endregion
    }
}