﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Nop.Plugin.Api.Services;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.Controllers
{
    public class OAuthController : Controller
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IClientService _clientService;

        public OAuthController(IHttpContextAccessor httpContextAccessor, IClientService clientService)
        {
            _httpContextAccessor = httpContextAccessor;
            _clientService = clientService;
        }

        [Route("/api/oauth/callback")]
        [HttpGet]
        public async Task<IActionResult> Authorize(string code)
        {
            if (string.IsNullOrEmpty(code))
            {
                return BadRequest("code is empty");
            }
            var clientId = _httpContextAccessor.HttpContext.User.FindFirst("client_id")?.Value;
            if (!string.IsNullOrEmpty(clientId))
            {
                return BadRequest("Cannot find specified client id");
            }
            var client = _clientService.FindClientByClientId(clientId);
            if (client == null)
            {
                return BadRequest("Cannot find specified client");
            }
            using (var http = new HttpClient())
            {
                http.BaseAddress = new Uri("http://localhost:64817/api/test");
                var content = new FormUrlEncodedContent(new[]{
                    new KeyValuePair<string, string>("code", code),
                    new KeyValuePair<string, string>("client_id", clientId),
                    new KeyValuePair<string, string>("client_secret", client.ClientSecret),
                    new KeyValuePair<string, string>("redirect_uri", client.RedirectUrl),
                    new KeyValuePair<string, string>("grant_type", "authorization_code")
                });
                http.DefaultRequestHeaders.Add("Content-Type", "application/x-www-form-urlencoded");
                var result = await http.PostAsync("/oauth/token", content);
                string resultContent = await result.Content.ReadAsStringAsync();
            }

            return Ok();
        }
    }
}
