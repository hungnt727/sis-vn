﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Discounts;
using Nop.Core.Domain.Media;
using Nop.Plugin.Api.Attributes;
using Nop.Plugin.Api.Constants;
using Nop.Plugin.Api.DTOs.Categories;
using Nop.Plugin.Api.Models.CategoriesParameters;
using Nop.Plugin.Api.Services;
using Nop.Services.Catalog;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Seo;
using Nop.Plugin.Api.Delta;
using Nop.Plugin.Api.DTOs.Images;
using Nop.Plugin.Api.Factories;
using Nop.Plugin.Api.JSON.ActionResults;
using Nop.Plugin.Api.ModelBinders;
using Nop.Services.Customers;
using Nop.Services.Discounts;
using Nop.Services.Media;
using Nop.Services.Security;
using Nop.Services.Stores;
using Nop.Plugin.Api.Helpers;
using Microsoft.AspNetCore.Mvc;

namespace Nop.Plugin.Api.Controllers
{
    using Microsoft.AspNetCore.Authentication;
    using Microsoft.AspNetCore.Authentication.JwtBearer;
    using Microsoft.AspNetCore.Mvc;
    using Nop.Core.Domain.Catalog;
    using Nop.Plugin.Api.Attributes;
    using Nop.Plugin.Api.DTOs.Errors;
    using Nop.Plugin.Api.DTOs.SISVN_ReceiptPayment;
    using Nop.Plugin.Api.Factories;
    using Nop.Plugin.Api.Helpers;
    using Nop.Plugin.Api.JSON.Serializers;
    using Nop.Plugin.Api.Models.SISVN_ReceiptPaymentParameters;
    using Nop.Plugin.Api.Services;
    using Nop.Services.Catalog;
    using Nop.Services.Localization;
    using Nop.Services.Logging;
    using Nop.Services.Seo;
    using Nop.Services.SISVN_ReceiptPayment;
    using System.Net;

    /// <summary>
    /// [HienNguyen][Api Thu Chi]
    /// </summary>
    [ApiAuthorize(Policy = JwtBearerDefaults.AuthenticationScheme, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class SISVN_ReceiptPaymentController : BaseApiController
    {
        private readonly ISISVN_ReceiptPaymentService _SISVN_ReceiptPaymentService;
        private readonly ICategoryApiService _categoryApiService;
        private readonly ICategoryService _categoryService;
        private readonly IUrlRecordService _urlRecordService;
        private readonly IFactory<Category> _factory;
        private readonly IDTOHelper _dtoHelper;
        private readonly IAuthenticationService _authenticationService;

        public SISVN_ReceiptPaymentController(ICategoryApiService categoryApiService,
            IJsonFieldsSerializer jsonFieldsSerializer,
            ICategoryService categoryService,
            IUrlRecordService urlRecordService,
            ICustomerActivityService customerActivityService,
            ILocalizationService localizationService,
            IPictureService pictureService,
            IStoreMappingService storeMappingService,
            IStoreService storeService,
            IDiscountService discountService,
            IAclService aclService,
            ICustomerService customerService,
            IFactory<Category> factory,
            ISISVN_ReceiptPaymentService SISVN_ReceiptPaymentService,
            IDTOHelper dtoHelper,
            IAuthenticationService authenticationService)
        {
            _categoryApiService = categoryApiService;
            _categoryService = categoryService;
            _urlRecordService = urlRecordService;
            _factory = factory;
            _dtoHelper = dtoHelper;
            _SISVN_ReceiptPaymentService = SISVN_ReceiptPaymentService;
            _SISVN_ReceiptPaymentService = SISVN_ReceiptPaymentService;
            _authenticationService = authenticationService;
        }

        #region Custom Method

        [HttpGet]
        [Route("/api/get_receipt_payment_type/")]
        [ProducesResponseType(typeof(SISVN_ReceiptPaymentTypeRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult GetReceiptPaymentType()
        {
            var receiptPaymentTypes = _SISVN_ReceiptPaymentService.GetReceiptPaymentType();

            var receiptPaymentTypeRootObject = new SISVN_ReceiptPaymentTypeRootObject()
            {
                SISVN_ReceiptPaymentType = receiptPaymentTypes.Select(m => _dtoHelper.PrepareSISVN_ReceiptPaymentTypeDto(m)).ToList()
            };

            var json = _jsonFieldsSerializer.Serialize(receiptPaymentTypeRootObject, string.Empty);

            return new RawJsonActionResult(json);
        }

        [HttpGet]
        [Route("/api/get_expense_type/")]
        [ProducesResponseType(typeof(SISVN_ExpenseTypeRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult GetExpenseType()
        {
            var expenseTypes = _SISVN_ReceiptPaymentService.GetExpenseType();

            var expenseTypeRootObject = new SISVN_ExpenseTypeRootObject()
            {
                SISVN_ExpenseType = expenseTypes.Select(m => _dtoHelper.PrepareSISVN_ExpenseTypeDto(m)).ToList()
            };

            var json = _jsonFieldsSerializer.Serialize(expenseTypeRootObject, string.Empty);

            return new RawJsonActionResult(json);
        }

        /// <summary>
        /// [HienNguyen][Tìm kiếm phiếu thu chi]
        /// </summary>
        /// <response code="200">OK</response>
        /// <response code="400">Bad Request</response>
        /// <response code="401">Unauthorized</response>
        [HttpGet]
        [Route("/api/get_receipt_payments")]
        [ProducesResponseType(typeof(SISVN_ReceiptPaymentRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult GetReceiptPayments(SISVN_ReceiptPaymentParametersModel parameters)
        {
            var listReceiptPayments = _SISVN_ReceiptPaymentService.GetReceiptPayment(
                parameters.Keyword,
                parameters.FromDate,
                parameters.ToDate,
                parameters.ReceiptPaymentTypeId,
                parameters.PaymentMethodId,
                parameters.ExpenseTypeId,
                false,
                parameters.PageIndex,
                parameters.PageSize
                );


            List<SISVN_ReceiptPaymentDto> listReceiptPaymentsDto = listReceiptPayments.Select(m => _dtoHelper.PrepareSISVN_ReceiptPaymentDto(m)).ToList();

            var receiptPaymentRootObject = new SISVN_ReceiptPaymentRootObject()
            {
                SISVN_ReceiptPayment = listReceiptPaymentsDto
            };

            var json = _jsonFieldsSerializer.Serialize(receiptPaymentRootObject, string.Empty);

            return new RawJsonActionResult(json);
        }

        /// <summary>
        /// [HienNguyen][Tìm kiếm phiếu theo id phiếu]
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("/api/receipt_payment/{id}")]
        [ProducesResponseType(typeof(SISVN_ReceiptPaymentRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult GetReceiptPaymentById(int id)
        {
            var receiptPayment = _SISVN_ReceiptPaymentService.GetReceiptPaymentById(id);

            var receiptPaymentRootObject = new SISVN_ReceiptPaymentRootObject()
            {
                SISVN_ReceiptPayment = new List<SISVN_ReceiptPaymentDto>() { _dtoHelper.PrepareSISVN_ReceiptPaymentDto(receiptPayment) }
            };

            var json = _jsonFieldsSerializer.Serialize(receiptPaymentRootObject, string.Empty);

            return new RawJsonActionResult(json);
        }


        /// <summary>
        /// [HienNguyen][Tạo phiếu thu chi]
        /// </summary>
        /// <param name="orderDelta"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("/api/create_receipt_payment")]
        [ProducesResponseType(typeof(SISVN_ReceiptPaymentRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ErrorsRootObject), 422)]
        public IActionResult CreateReceiptPayment([ModelBinder(typeof(JsonModelBinder<SISVN_ReceiptPaymentDto>))] Delta<SISVN_ReceiptPaymentDto> orderNoteDelta)
        {
            var receiptPayment = _SISVN_ReceiptPaymentService.CreateReceiptPayment(new Core.Domain.SISVN_ReceiptPayment.SISVN_ReceiptPayment()
            {
                Amount = orderNoteDelta.Dto.Amount,
                CreateUserId = orderNoteDelta.Dto.CustomerId,
                CustomerId = orderNoteDelta.Dto.CustomerId,
                Descriptions = orderNoteDelta.Dto.Descriptions,
                ExpenseTypeId = orderNoteDelta.Dto.ExpenseTypeId,
                PaymentMethodId = orderNoteDelta.Dto.PaymentMethodId,
                ReceiptPaymentCode = orderNoteDelta.Dto.ReceiptPaymentCode,
                ReceiptPaymentTypeId = orderNoteDelta.Dto.ReceiptPaymentTypeId,
                StoreId = orderNoteDelta.Dto.StoreId,
                TransactionDate = DateTime.Now
            });

            var receiptPaymentRootObject = new SISVN_ReceiptPaymentRootObject()
            {
                SISVN_ReceiptPayment = new List<SISVN_ReceiptPaymentDto>() { _dtoHelper.PrepareSISVN_ReceiptPaymentDto(receiptPayment) }
            };

            var json = _jsonFieldsSerializer.Serialize(receiptPaymentRootObject, string.Empty);

            return new RawJsonActionResult(json);
        }

        /// <summary>
        /// [HienNguyen][Xóa phiếu]
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("/api/delete_receipt_payment/{id}")]
        [ProducesResponseType(typeof(SISVN_ReceiptPaymentRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ErrorsRootObject), 422)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult DeleteReceiptPayment(int id)
        {
            if (id <= 0)
            {
                return Error(HttpStatusCode.BadRequest, "id", "invalid id");
            }

            var receiptPayment = _SISVN_ReceiptPaymentService.DeleteReceiptPayment(id, 1);
            var receiptPaymentRootObject = new SISVN_ReceiptPaymentRootObject()
            {
                SISVN_ReceiptPayment = new List<SISVN_ReceiptPaymentDto>() { _dtoHelper.PrepareSISVN_ReceiptPaymentDto(receiptPayment) }
            };

            var json = _jsonFieldsSerializer.Serialize(receiptPaymentRootObject, string.Empty);

            return new RawJsonActionResult(json);
        }
        #endregion
    }
}
