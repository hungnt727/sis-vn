﻿using FluentValidation.Attributes;
using Newtonsoft.Json;
using Nop.Plugin.Api.Constants;
using Nop.Plugin.Api.ModelBinders;
using Nop.Plugin.Api.Models.OrdersParameters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.ModelBinding;

namespace Nop.Plugin.Api.Controllers
{
    /// <summary>
    /// Hàm lấy dữ liệu đơn hàng
    /// </summary>
    public class ExampleOrderApi
    {
        //method: get
        //url: "/api/orders"

        /// <summary>
        /// Input Parameters
        /// </summary>
        public class OrdersParametersModel : BaseOrdersParametersModel
        {
            public OrdersParametersModel()
            {
                Ids = null;
                Keyword = null;
                OrderStatusIds = null;
                OrderTypeIds = null;
                FromDate = null;
                ToDate = null;
                PageSize = Configurations.DefaultLimit;
                PageIndex = Configurations.DefaultPageValue;
                SinceId = Configurations.DefaultSinceId;
                Fields = string.Empty;
            }

            /// <summary>
            /// A comma-separated list of order ids
            /// </summary>
            [JsonProperty("ids")]
            public List<int> Ids { get; set; }

            /// <summary>
            /// Tạo từ ngày
            /// </summary>
            [JsonProperty("fromdate")]
            public DateTime? FromDate { get; set; }

            /// <summary>
            /// Tạo đến ngày
            /// </summary>
            [JsonProperty("todate")]
            public DateTime? ToDate { get; set; }

            /// <summary>
            /// Tìm kiếm theo  mã đơn hoặc mã/họ tên/SĐT khách hàng/NCC
            /// </summary>
            [JsonProperty("keyword")]
            public string Keyword { get; set; }

            /// <summary>
            /// Trạng thái đơn hàng
            /// </summary>
            [JsonProperty("order_status_ids")]
            public int[] OrderStatusIds { get; set; }

            /// <summary>
            /// Loại đơn hàng
            /// </summary>
            [JsonProperty("order_type_ids")]
            public int[] OrderTypeIds { get; set; }

            /// <summary>
            /// Amount of results (default: 50) (maximum: 250)
            /// </summary>
            [JsonProperty("limit")]
            public int PageSize { get; set; }

            /// <summary>
            /// Page to show (default: 1)
            /// </summary>
            [JsonProperty("page")]
            public int PageIndex { get; set; }

            /// <summary>
            /// Restrict results to after the specified ID
            /// </summary>
            [JsonProperty("since_id")]
            public int SinceId { get; set; }

            /// <summary>
            /// comma-separated list of fields to include in the response
            /// </summary>
            [JsonProperty("fields")]
            public string Fields { get; set; }
        }

        /// <summary>
        /// Output Parameters
        /// </summary>
        public class OrderResponseModel
        {
            public OrderResponseModel()
            {
                Orders = new List<OrderDto>();
            }

            /// <summary>
            /// Danh sách đơn hàng
            /// </summary>
            [JsonProperty("orders")]
            public IList<OrderDto> Orders { get; set; }

            /// <summary>
            /// Tổng tiền nợ
            /// </summary>
            [JsonProperty("total_debt_amount")]
            public double TotalDebtAmount { get; set; }

            /// <summary>
            /// Tổng tiền đơn hàng
            /// </summary>
            [JsonProperty("total_amount")]
            public double TotalAmount { get; set; }

            /// <summary>
            /// Tổng số đơn hàng
            /// </summary>

            [JsonProperty("total_count_order")]
            public int TotalCountOrder { get; set; }

            [JsonProperty("total_row")]
            public int TotalRow { get; set; }
        }

        public class OrderDto
        {
            private ICollection<OrderItemDto> _orderItemDtos;

            /// <summary>
            /// Gets or sets a value indicating the order id
            /// </summary>
            [JsonProperty("id")]
            public string Id { get; set; }

            /// <summary>
            /// Mã đơn hàng
            /// </summary>
            [JsonProperty("order_id")]
            public int OrderId { get; set; }

            [JsonProperty("store_id")]
            public int? StoreId { get; set; }

            [JsonProperty("store_name")]
            public string StoreName { get; set; }

            /// <summary>
            /// Gets or sets the date and time of order creation
            /// </summary>
            [JsonProperty("created_on")]
            public DateTime? CreatedOn { get; set; }

            /// <summary>
            /// Người tạo
            /// </summary>
            [JsonProperty("create_user")]
            public string CreateUser { get; set; }

            /// <summary>
            /// Tổng tiền thu
            /// </summary>
            [JsonProperty("total_amount")]
            public double TotalAmount { get; set; }

            /// <summary>
            /// Tổng tiền nợ
            /// </summary>
            [JsonProperty("total_debt_amount")]
            public double TotalDebtAmount { get; set; }

            /// <summary>
            /// Gets or sets the customer
            /// </summary>
            [JsonProperty("customer")]
            public OrderCustomerDto Customer { get; set; }

            /// <summary>
            /// Gets or sets order items
            /// </summary>
            [JsonProperty("order_items")]
            public ICollection<OrderItemDto> OrderItemDtos
            {
                get { return _orderItemDtos; }
                set { _orderItemDtos = value; }
            }

            /// <summary>
            /// Gets or sets the order status
            /// </summary>
            [JsonProperty("order_status")]
            public OrderStatus OrderStatus { get; set; }

            /// <summary>
            /// Loại đơn hàng
            /// </summary>
            [JsonProperty("order_type")]
            public OrderType OrderType { get; set; }
        }

        public class OrderItemDto
        {
            /// <summary>
            /// Gets or sets the id
            /// </summary>
            [JsonProperty("order_item_id")]
            public string Id { get; set; }

            /// <summary>
            /// Gets or sets the quantity
            /// </summary>
            [JsonProperty("quantity")]
            public int? Quantity { get; set; }

            /// <summary>
            /// Đơn giá đã tính thuế
            /// </summary>
            [JsonProperty("price_incl_tax")]
            public decimal? PriceInclTax { get; set; }

            [JsonProperty("product_id")]
            public int? ProductId { get; set; }

            [JsonProperty("product_name")]
            public string ProductName { get; set; }
        }

        public class OrderStatus
        {
            [JsonProperty("order_status_id")]
            public int OrderStatusId { get; set; }

            [JsonProperty("order_status_name")]
            public string OrderStatusName { get; set; }
        }

        public class OrderType
        {
            [JsonProperty("order_type_id")]
            public int OrderTypeId { get; set; }

            [JsonProperty("order_type_name")]
            public string OrderTypeName { get; set; }
        }

        public class OrderCustomerDto
        {
            [JsonProperty("customer_id")]
            public string Id { get; set; }

            [JsonProperty("username")]
            public string Username { get; set; }
            /// <summary>
            /// Gets or sets the email
            /// </summary>
            [JsonProperty("email")]
            public string Email { get; set; }

            [JsonProperty("phone_number")]
            public string PhoneNumber { get; set; }

            [JsonProperty("address")]
            public string Address { get; set; }

            [JsonProperty("first_name")]
            public string FirstName { get; set; }

            [JsonProperty("last_name")]
            public string LastName { get; set; }

            [JsonProperty("date_of_birth")]
            public DateTime? DateOfBirth { get; set; }

            [JsonProperty("gender")]
            public string Gender { get; set; }

            /// <summary>
            /// Gets or sets the date and time of entity creation
            /// </summary>
            [JsonProperty("created_on")]
            public DateTime? CreatedOn { get; set; }
        }
    }

    /// <summary>
    /// Hàm xuất excel đơn hàng
    /// </summary>
    public class ExampleExportExcelOrderApi
    {
        //method: get
        //url: "/api/exportexcelorders"

        /// <summary>
        /// Input Parameters
        /// </summary>
        public class OrdersParametersModel : BaseOrdersParametersModel
        {
            public OrdersParametersModel()
            {
                Ids = null;
                Keyword = null;
                OrderStatusIds = null;
                OrderTypeIds = null;
                FromDate = null;
                ToDate = null;
            }

            /// <summary>
            /// A comma-separated list of order ids
            /// </summary>
            [JsonProperty("ids")]
            public List<int> Ids { get; set; }

            /// <summary>
            /// Tạo từ ngày
            /// </summary>
            [JsonProperty("fromdate")]
            public DateTime? FromDate { get; set; }

            /// <summary>
            /// Tạo đến ngày
            /// </summary>
            [JsonProperty("todate")]
            public DateTime? ToDate { get; set; }

            /// <summary>
            /// Tìm kiếm theo  mã đơn hoặc mã/họ tên/SĐT khách hàng/NCC
            /// </summary>
            [JsonProperty("keyword")]
            public string Keyword { get; set; }

            /// <summary>
            /// Trạng thái đơn hàng
            /// </summary>
            [JsonProperty("order_status_ids")]
            public int[] OrderStatusIds { get; set; }

            /// <summary>
            /// Loại đơn hàng
            /// </summary>
            [JsonProperty("order_type_ids")]
            public int[] OrderTypeIds { get; set; }
        }

        /// <summary>
        /// Đối tượng Response
        /// </summary>
        public class ResultObjectModel
        {
            public ResultObjectModel()
            {
                IsSuccess = true;
                Data = null;
                Message = null;
            }

            /// <summary>
            /// Đường dẫn download file excel
            /// </summary>
            public string Data { get; set; }
            public bool IsSuccess { get; set; }
            public string Message { get; set; }
        }

    }

    /// <summary>
    /// Hàm lấy dữ liệu trạng thái đơn hàng
    /// </summary>
    public class ExampleOrderStatusApi
    {
        //method: get
        //url: "/api/getorderstatus"

        public class OrderStatusParametersModel
        {
            public OrderStatusParametersModel()
            {
                OrderStatusIds = null;
            }

            /// <summary>
            /// Mã trạng thái đơn hàng nếu có
            /// </summary>
            [JsonProperty("order_status_ids")]
            public List<int> OrderStatusIds { get; set; }
        }

        public class OrderStatusResponseModel
        {
            public OrderStatusResponseModel()
            {
                OrderStatus = new List<OrderStatus>();
            }

            /// <summary>
            /// Danh sách đơn hàng
            /// </summary>
            [JsonProperty("order_status")]
            public IList<OrderStatus> OrderStatus { get; set; }

            [JsonProperty("total_row")]
            public int TotalRow { get; set; }
        }

        public class OrderStatus
        {
            [JsonProperty("order_status_id")]
            public int OrderStatusId { get; set; }

            [JsonProperty("order_status_name")]
            public string OrderStatusName { get; set; }
        }
    }

    /// <summary>
    /// Hàm lấy dữ liệu loại đơn hàng
    /// </summary>
    public class ExampleOrderTypeApi
    {
        //method: get
        //url: "/api/getordertype"

        public class OrderTypeParametersModel
        {
            public OrderTypeParametersModel()
            {
                OrderTypeIds = null;
            }

            /// <summary>
            /// Mã trạng thái đơn hàng nếu có
            /// </summary>
            [JsonProperty("order_type_ids")]
            public List<int> OrderTypeIds { get; set; }
        }

        public class OrderTypeResponseModel
        {
            public OrderTypeResponseModel()
            {
                OrderType = new List<OrderType>();
            }

            /// <summary>
            /// Danh sách đơn hàng
            /// </summary>
            [JsonProperty("order_Type")]
            public IList<OrderType> OrderType { get; set; }

            [JsonProperty("total_row")]
            public int TotalRow { get; set; }
        }

        public class OrderType
        {
            [JsonProperty("order_type_id")]
            public int OrderTypeId { get; set; }

            [JsonProperty("order_type_name")]
            public string OrderTypeName { get; set; }
        }
    }

    /// <summary>
    /// Các hàm cập nhật trạng thái đơn hàng
    /// </summary>
    public class ExampleUpdateStausOrder
    {
        //method: post
        //url: "/api/cancelorder/{id}"
        //url: "/api/deleteorder/{id}"
        //url: "/api/restoreorder/{id}"

        /// <summary>
        /// Đối tượng Response
        /// </summary>
        public class ResultObjectModel
        {
            public ResultObjectModel()
            {
                IsSuccess = true;
                Data = null;
                Message = null;
            }

            public object Data { get; set; }
            public bool IsSuccess { get; set; }
            public string Message { get; set; }
        }
    }
}
