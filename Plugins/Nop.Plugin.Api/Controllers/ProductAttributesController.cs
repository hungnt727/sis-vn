﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Mvc;
using Nop.Core.Domain.Catalog;
using Nop.Plugin.Api.Attributes;
using Nop.Plugin.Api.Constants;
using Nop.Plugin.Api.Delta;
using Nop.Plugin.Api.DTOs.Errors;
using Nop.Plugin.Api.DTOs.ProductAttributes;
using Nop.Plugin.Api.Helpers;
using Nop.Plugin.Api.JSON.ActionResults;
using Nop.Plugin.Api.ModelBinders;
using Nop.Plugin.Api.Models.ProductAttributes;
using Nop.Plugin.Api.JSON.Serializers;
using Nop.Plugin.Api.Services;
using Nop.Services.Catalog;
using Nop.Services.Customers;
using Nop.Services.Discounts;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Media;
using Nop.Services.Security;
using Nop.Services.Stores;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace Nop.Plugin.Api.Controllers
{
    [ApiAuthorize(Policy = JwtBearerDefaults.AuthenticationScheme, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class ProductAttributesController : BaseApiController
    {
        private readonly IProductAttributeService _productAttributeService;
        private readonly IProductAttributesApiService _productAttributesApiService;
        private readonly IDTOHelper _dtoHelper;

        public ProductAttributesController(IJsonFieldsSerializer jsonFieldsSerializer,
                                  ICustomerActivityService customerActivityService,
                                  ILocalizationService localizationService,
                                  IAclService aclService,
                                  IStoreMappingService storeMappingService,
                                  IStoreService storeService,
                                  ICustomerService customerService,
                                  IDiscountService discountService,
                                  IPictureService pictureService,
                                  IProductAttributeService productAttributeService,
                                  IProductAttributesApiService productAttributesApiService,
                                  IDTOHelper dtoHelper)
        {
            _productAttributeService = productAttributeService;
            _productAttributesApiService = productAttributesApiService;
            _dtoHelper = dtoHelper;
        }

        #region Custom Method

        /// <summary>
        ///  [HienNguyen][20/08/2018][Tạo mới danh mục thuộc tính]
        /// </summary>
        /// <param name="productAttributeDelta"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("/api/create_product_attribute")]
        [ProducesResponseType(typeof(ProductAttributesRootObjectDto), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ErrorsRootObject), 422)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult CreateProductAttribute([ModelBinder(typeof(JsonModelBinder<ProductAttributeDto>))] Delta<ProductAttributeDto> productAttributeDelta)
        {
            // Here we display the errors if the validation has failed at some point.
            if (!ModelState.IsValid)
            {
                return Error();
            }

            // Inserting the new product
            ProductAttribute productAttribute = new ProductAttribute();
            productAttributeDelta.Merge(productAttribute);

            _productAttributeService.InsertProductAttribute(productAttribute);

            _customerActivityService.InsertActivity("AddNewProductAttribute",
                _localizationService.GetResource("ActivityLog.AddNewProductAttribute"), productAttribute.Name);

            // Preparing the result dto of the new product
            ProductAttributeDto productAttributeDto = _dtoHelper.PrepareProductAttributeDTO(productAttribute);

            var productAttributesRootObjectDto = new ProductAttributesRootObjectDto();

            productAttributesRootObjectDto.ProductAttributes.Add(productAttributeDto);

            var json = _jsonFieldsSerializer.Serialize(productAttributesRootObjectDto, string.Empty);

            return new RawJsonActionResult(json);
        }

        /// <summary>
        ///  [HienNguyen][20/08/2018][Cập nhật danh mục thuộc tính]
        /// </summary>
        /// <param name="productAttributeDelta"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("/api/update_product_attribute/{id}")]
        [ProducesResponseType(typeof(ProductAttributesRootObjectDto), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ErrorsRootObject), 422)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult UpdateProductAttribute([ModelBinder(typeof(JsonModelBinder<ProductAttributeDto>))] Delta<ProductAttributeDto> productAttributeDelta)
        {
            // Here we display the errors if the validation has failed at some point.
            if (!ModelState.IsValid)
            {
                return Error();
            }

            // We do not need to validate the product attribute id, because this will happen in the model binder using the dto validator.
            int productAttributeId = int.Parse(productAttributeDelta.Dto.Id);

            ProductAttribute productAttribute = _productAttributesApiService.GetById(productAttributeId);

            if (productAttribute == null)
            {
                return Error(HttpStatusCode.NotFound, "product attribute", "not found");
            }

            productAttributeDelta.Merge(productAttribute);


            _productAttributeService.UpdateProductAttribute(productAttribute);

            _customerActivityService.InsertActivity("EditProductAttribute",
               _localizationService.GetResource("ActivityLog.EditProductAttribute"), productAttribute.Name);

            // Preparing the result dto of the new product attribute
            ProductAttributeDto productAttributeDto = _dtoHelper.PrepareProductAttributeDTO(productAttribute);

            var productAttributesRootObjectDto = new ProductAttributesRootObjectDto();

            productAttributesRootObjectDto.ProductAttributes.Add(productAttributeDto);

            var json = _jsonFieldsSerializer.Serialize(productAttributesRootObjectDto, string.Empty);

            return new RawJsonActionResult(json);
        }

        /// <summary>
        /// [HienNguyen][20/08/2018][Xóa danh mục thuộc tính]
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("/api/delete_product_attribute/{id}")]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ErrorsRootObject), 422)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult DeleteProductAttribute(int id)
        {
            if (id <= 0)
            {
                return Error(HttpStatusCode.BadRequest, "id", "invalid id");
            }

            ProductAttribute productAttribute = _productAttributesApiService.GetById(id);

            if (productAttribute == null)
            {
                return Error(HttpStatusCode.NotFound, "product attribute", "not found");
            }

            _productAttributeService.DeleteProductAttribute(productAttribute);

            //activity log
            _customerActivityService.InsertActivity("DeleteProductAttribute", _localizationService.GetResource("ActivityLog.DeleteProductAttribute"), productAttribute.Name);

            return new RawJsonActionResult("{}");
        }

        /// <summary>
        /// HienNguyen-Hàm lấy dữ liệu thuốc tính sản phẩm
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("/api/get_attributes")]
        [ProducesResponseType(typeof(ProductAttributesRootObjectDto), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult GetAttributes(ProductAttributesParametersModel parameters)
        {
            if (parameters.PageSize < Configurations.MinLimit || parameters.PageSize > Configurations.MaxLimit)
            {
                return Error(HttpStatusCode.BadRequest, "limit", "invalid limit parameter");
            }

            if (parameters.PageIndex < Configurations.DefaultPageValue)
            {
                return Error(HttpStatusCode.BadRequest, "page", "invalid page parameter");
            }

            var allProductAttributes = _productAttributeService.GetAllProductAttributes(parameters.PageIndex, parameters.PageSize);

            IList<ProductAttributeDto> productAttributesAsDtos = allProductAttributes.Select(productAttribute =>
            {
                return _dtoHelper.PrepareProductAttributeDTO(productAttribute);

            }).ToList();

            var productAttributesRootObject = new ProductAttributesRootObjectDto()
            {
                ProductAttributes = productAttributesAsDtos
            };

            var json = _jsonFieldsSerializer.Serialize(productAttributesRootObject, parameters.Fields);

            return new RawJsonActionResult(json);
        }

        /// <summary>
        /// [HienNguyen][20/08/2018][Lấy danh mục sản thuộc tính của sản phẩm]
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("/api/get_product_attributes")]
        [ProducesResponseType(typeof(ProductAttributesRootObjectDto), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult GetProductAttributes(ProductAttributesParametersModel parameters)
        {
            if (parameters.PageSize < Configurations.MinLimit || parameters.PageSize > Configurations.MaxLimit)
            {
                return Error(HttpStatusCode.BadRequest, "limit", "invalid limit parameter");
            }

            if (parameters.PageIndex < Configurations.DefaultPageValue)
            {
                return Error(HttpStatusCode.BadRequest, "page", "invalid page parameter");
            }

            var allProductAttributes = _productAttributesApiService.GetProductAttributes(parameters.PageSize, parameters.PageIndex, parameters.SinceId);

            IList<ProductAttributeDto> productAttributesAsDtos = allProductAttributes.Select(productAttribute =>
            {
                return _dtoHelper.PrepareProductAttributeDTO(productAttribute);

            }).ToList();

            var productAttributesRootObject = new ProductAttributesRootObjectDto()
            {
                ProductAttributes = productAttributesAsDtos
            };

            var json = _jsonFieldsSerializer.Serialize(productAttributesRootObject, parameters.Fields);

            return new RawJsonActionResult(json);
        }

        /// <summary>
        /// [HienNguyen][21/08/2018][Lấy danh mục thuộc tính theo sản phẩm]
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("/api/get_product_attribute_values_by_product_id/{productid}")]
        [ProducesResponseType(typeof(ProductAttributesRootObjectDto), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult GetProductAttributeValuesByProductId(int productid)
        {
            //Data Mapping product attribute
            IList<ProductAttributeMapping> productAttributeMappings = _productAttributeService.GetProductAttributeMappingsByProductId(productid);

            //Data attribute values
            IList<ProductAttributeValue> productAttributeValues = _productAttributeService.GetProductAttributeValuesByMappingIds(productAttributeMappings.Select(m => m.Id).ToArray());

            var productAttributesAsDtos = from mapping in productAttributeMappings
                                          join value in productAttributeValues on mapping.Id equals value.ProductAttributeMappingId
                                          select new ProductAttributeDto()
                                          {
                                              ProductId = mapping.ProductId,
                                              Name = mapping.ProductAttribute.Name,
                                              ProductAttributeId = mapping.ProductAttributeId,
                                              Value = value.Name,
                                              Description = mapping.ProductAttribute.Description
                                          };




            var productAttributesRootObject = new ProductAttributesRootObjectDto()
            {
                ProductAttributes = productAttributesAsDtos.ToList()
            };

            var json = _jsonFieldsSerializer.Serialize(productAttributesRootObject, string.Empty);

            return new RawJsonActionResult(json);
        }

        /// <summary>
        /// [HienNguyen][23/08/2018][Hàm cập nhật value cho thuộc tính của sản phẩm]
        /// </summary>
        /// <param name="productAttribute"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("/api/update_product_attribute_value")]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult UpdateProductAttributeValue([ModelBinder(typeof(JsonModelBinder<ProductAttributeDto>))] Delta<ProductAttributeDto> productAttributeValueDelta)
        {
            if (productAttributeValueDelta.Dto.ProductId <= 0)
            {
                return Error(HttpStatusCode.BadRequest, "product_id", "invalid id");
            }

            if (productAttributeValueDelta.Dto.ProductAttributeId <= 0)
            {
                return Error(HttpStatusCode.BadRequest, "product_attribute_id", "invalid id");
            }

            if (string.IsNullOrEmpty(productAttributeValueDelta.Dto.Value))
            {
                return Error(HttpStatusCode.BadRequest, "Value", "is not null or empty");
            }

            _productAttributeService.UpdateProductAttributeValue(productAttributeValueDelta.Dto.ProductId, productAttributeValueDelta.Dto.ProductAttributeId, productAttributeValueDelta.Dto.Value);

            //activity log
            _customerActivityService.InsertActivity("UpdateProductAttributeValue", _localizationService.GetResource("ActivityLog.UpdateProductAttributeValue"), string.Empty);

            return new RawJsonActionResult("{}");
        }

        #endregion

        /// <summary>
        /// Receive a count of all product attributes
        /// </summary>
        /// <response code="200">OK</response>
        /// <response code="401">Unauthorized</response>        
        [HttpGet]
        [Route("/api/productattributes/count")]
        [ProducesResponseType(typeof(ProductAttributesCountRootObject), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult GetProductAttributesCount()
        {
            int allProductAttributesCount = _productAttributesApiService.GetProductAttributesCount();

            var productAttributesCountRootObject = new ProductAttributesCountRootObject()
            {
                Count = allProductAttributesCount
            };

            return Ok(productAttributesCountRootObject);
        }

        /// <summary>
        /// Retrieve product attribute by spcified id
        /// </summary>
        /// <param name="id">Id of the product attribute</param>
        /// <param name="fields">Fields from the product attribute you want your json to contain</param>
        /// <response code="200">OK</response>
        /// <response code="404">Not Found</response>
        /// <response code="401">Unauthorized</response>
        [HttpGet]
        [Route("/api/productattributes/{id}")]
        [ProducesResponseType(typeof(ProductAttributesRootObjectDto), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
        [GetRequestsErrorInterceptorActionFilter]
        public IActionResult GetProductAttributeById(int id, string fields = "")
        {
            if (id <= 0)
            {
                return Error(HttpStatusCode.BadRequest, "id", "invalid id");
            }

            ProductAttribute productAttribute = _productAttributesApiService.GetById(id);

            if (productAttribute == null)
            {
                return Error(HttpStatusCode.NotFound, "product attribute", "not found");
            }

            ProductAttributeDto productAttributeDto = _dtoHelper.PrepareProductAttributeDTO(productAttribute);

            var productAttributesRootObject = new ProductAttributesRootObjectDto();

            productAttributesRootObject.ProductAttributes.Add(productAttributeDto);

            var json = _jsonFieldsSerializer.Serialize(productAttributesRootObject, fields);

            return new RawJsonActionResult(json);
        }
    }
}