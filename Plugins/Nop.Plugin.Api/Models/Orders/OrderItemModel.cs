﻿using Newtonsoft.Json;
using Nop.Plugin.Api.Models.Common;
using Nop.Web.Framework.Mvc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.Models.Orders
{
    public class OrderItemModel
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("product")]
        public OrderProductModel Product { get; set; }

        [JsonProperty("discount")]
        public DecimalModel Discount { get; set; }

        [JsonProperty("quantity")]
        public int Quantity { get; set; }

        [JsonProperty("unit_price")]
        public DecimalModel UnitPrice { get; set; }

        [JsonProperty("total_amount")]
        public DecimalModel TotalAmount { get; set; }
    }
}
