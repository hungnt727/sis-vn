﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.Models.Orders
{
    public class OrderShippingModel
    {
        public OrderShippingModel()
        {
            this.Shipments = new List<OrderShipmentModel>();
        }

        [JsonProperty("system_name")]
        public string SystemName { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("shipments")]
        public IList<OrderShipmentModel> Shipments { get; set; }
    }
}
