﻿using Newtonsoft.Json;
using Nop.Plugin.Api.DTOs;
using Nop.Plugin.Api.Models.Common;
using Nop.Plugin.Api.Models.Customers;
using Nop.Plugin.Api.Models.Stores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.Models.Orders
{
    [JsonObject(Title = "order")]
    public class OrderSummaryModel : ISerializableObject
    {
        public OrderSummaryModel()
        {
            PaymentHistory = new List<OrderPaymentHistoryModel>();
            OrderItems = new List<OrderItemModel>();
        }

        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("order_items")]
        public IList<OrderItemModel> OrderItems { get; set; }

        [JsonProperty("payment_history")]
        public IList<OrderPaymentHistoryModel> PaymentHistory { get; set; }

        [JsonProperty("order_subtotal")]
        public DecimalModel OrderSubTotal { get; set; }

        [JsonProperty("order_total")]
        public DecimalModel OrderTotal { get; set; }

        [JsonProperty("order_paid")]
        public DecimalModel OrderPaid { get; set; }

        [JsonProperty("order_debt")]
        public DecimalModel OrderDebt { get; set; }

        public string GetPrimaryPropertyName()
        {
            return "order";
        }

        public Type GetPrimaryPropertyType()
        {
            return typeof(OrderSummaryModel);
        }
    }
}
