﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.Models.Orders
{
    public class OrderShipmentModel
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("tracking_number")]
        public string TrackingNumber { get; set; }

        [JsonProperty("weight")]
        public decimal? Weight { get; set; }

        [JsonProperty("date_shipped")]
        public string DateShipped { get; set; }

        [JsonProperty("date_deliveried")]
        public string DateDeliveried { get; set; }
    }
}
