﻿using Newtonsoft.Json;
using Nop.Plugin.Api.DTOs;
using Nop.Plugin.Api.Models.Common;
using Nop.Plugin.Api.Models.Customers;
using Nop.Plugin.Api.Models.Stores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.Models.Orders
{
    [JsonObject(Title = "order")]
    public class OrderCopyModel : ISerializableObject
    {
        public OrderCopyModel()
        {
            OrderItems = new List<OrderItemModel>();
        }

        [JsonProperty("customer")]
        public CustomerSimpleModel Customer { get; set; }

        [JsonProperty("seller")]
        public CustomerSimpleModel Seller { get; set; }

        [JsonProperty("store")]
        public StoreSimpleModel Store { get; set; }

        [JsonProperty("order_items")]
        public IList<OrderItemModel> OrderItems { get; set; }

        [JsonProperty("order_subtotal")]
        public DecimalModel OrderSubTotal { get; set; }

        [JsonProperty("order_total")]
        public DecimalModel OrderTotal { get; set; }

        [JsonProperty("order_tax")]
        public DecimalModel OrderTax { get; set; }

        [JsonProperty("order_shipping")]
        public DecimalModel OrderShippingAmount { get; set; }

        [JsonProperty("order_discount")]
        public DecimalModel OrderDiscount { get; set; }

        [JsonProperty("shipping")]
        public OrderShippingModel OrderShipping { get; set; }

        public string GetPrimaryPropertyName()
        {
            return "order";
        }

        public Type GetPrimaryPropertyType()
        {
            return typeof(OrderSummaryModel);
        }
    }
}
