﻿using Newtonsoft.Json;
using Nop.Plugin.Api.Models.Common;
using Nop.Web.Framework.Mvc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.Models.Orders
{
    public class OrderPaymentHistoryModel
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("payment")]
        public PaymentModel Payment { get; set; }

        [JsonProperty("amount")]
        public DecimalModel Amount { get; set; }

        [JsonProperty("date_paid")]
        public string DatePaid { get; set; }
    }
}
