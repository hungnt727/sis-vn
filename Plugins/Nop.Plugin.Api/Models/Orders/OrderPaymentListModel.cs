﻿using Newtonsoft.Json;
using Nop.Plugin.Api.DTOs;
using Nop.Plugin.Api.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.Models.Orders
{
    public class OrderPaymentListModel : BaseListModel, ISerializableObject
    {
        public OrderPaymentListModel()
        {
            this.Payments = new List<OrderPaymentHistoryModel>();
        }

        [JsonProperty("payments")]
        public IList<OrderPaymentHistoryModel> Payments { get; set; }

        public string GetPrimaryPropertyName()
        {
            return "history";
        }

        public Type GetPrimaryPropertyType()
        {
            return typeof(OrderPaymentListModel);
        }
    }
}