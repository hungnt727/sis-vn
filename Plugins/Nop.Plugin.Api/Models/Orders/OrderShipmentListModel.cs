﻿using Newtonsoft.Json;
using Nop.Plugin.Api.DTOs;
using Nop.Plugin.Api.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.Models.Orders
{
    public class OrderShipmentListModel : BaseListModel, ISerializableObject
    {
        public OrderShipmentListModel()
        {
            this.Shipments = new List<OrderShipmentModel>();
        }

        [JsonProperty("shipments")]
        public IList<OrderShipmentModel> Shipments { get; set; }

        public string GetPrimaryPropertyName()
        {
            return "history";
        }

        public Type GetPrimaryPropertyType()
        {
            return typeof(OrderShipmentListModel);
        }
    }
}