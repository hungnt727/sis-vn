﻿using Newtonsoft.Json;
using Nop.Web.Framework.Mvc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.Models.Orders
{
    public class PaymentModel
    {
        [JsonProperty("system_name")]
        public string SystemName { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }
}
