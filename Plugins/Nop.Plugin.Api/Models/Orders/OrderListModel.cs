﻿using Newtonsoft.Json;
using Nop.Plugin.Api.DTOs;
using Nop.Plugin.Api.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.Models.Orders
{
    public class OrderListModel : BaseListModel, ISerializableObject
    {
        public OrderListModel()
        {
            this.Orders = new List<OrderModel>();
        }

        [JsonProperty("orders")]
        public IList<OrderModel> Orders { get; set; }

        public string GetPrimaryPropertyName()
        {
            return "orders";
        }

        public Type GetPrimaryPropertyType()
        {
            return typeof(OrderListModel);
        }
    }
}
