﻿using FluentValidation.Attributes;
using Newtonsoft.Json;
using Nop.Plugin.Api.DTOs;
using Nop.Plugin.Api.Models.Common;
using Nop.Plugin.Api.Models.Customers;
using Nop.Plugin.Api.Models.Stores;
using Nop.Plugin.Api.Validators;
using Nop.Web.Framework.Mvc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.Models.Orders
{
    [JsonObject(Title = "order")]
    public class OrderModel : ISerializableObject
    {
        public OrderModel()
        {
            PaymentHistory = new List<OrderPaymentHistoryModel>();
            OrderItems = new List<OrderItemModel>();
        }

        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("order_code")]
        public string OrderCode { get; set; }

        [JsonProperty("created_on")]
        public string CreatedOn { get; set; }
                
        [JsonProperty("paid_on")]
        public string PaidOn { get; set; }

        [JsonProperty("customer")]
        public CustomerSimpleModel Customer { get; set; }

        [JsonProperty("seller")]
        public CustomerSimpleModel Seller { get; set; }

        [JsonProperty("store")]
        public StoreSimpleModel Store { get; set; }

        [JsonProperty("order_items")]
        public IList<OrderItemModel> OrderItems { get; set; }

        [JsonProperty("payment_history")]
        public IList<OrderPaymentHistoryModel> PaymentHistory { get; set; }

        [JsonProperty("shipping")]
        public OrderShippingModel OrderShipping { get; set; }

        [JsonProperty("order_status_id")]
        public int OrderStatusId { get; set; }

        [JsonProperty("order_payment_id")]
        public int OrderPaymentId { get; set; }

        [JsonProperty("order_shipment_id")]
        public int OrderShipmentId { get; set; }

        [JsonProperty("order_subtotal")]
        public DecimalModel OrderSubTotal { get; set; }

        [JsonProperty("order_total")]
        public DecimalModel OrderTotal { get; set; }

        [JsonProperty("order_discount")]
        public DecimalModel OrderDiscount { get; set; }

        [JsonProperty("order_paid")]
        public DecimalModel OrderPaid { get; set; }

        [JsonProperty("order_debt")]
        public DecimalModel OrderDebt { get; set; }

        [JsonProperty("order_tax")]
        public DecimalModel OrderTax { get; set; }

        [JsonProperty("order_shipping")]
        public DecimalModel OrderShippingAmount { get; set; }

        public string GetPrimaryPropertyName()
        {
            return "order";
        }

        public Type GetPrimaryPropertyType()
        {
            return typeof(OrderModel);
        }
    }
}
