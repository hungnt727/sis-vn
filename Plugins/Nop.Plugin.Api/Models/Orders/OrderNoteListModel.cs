﻿using Newtonsoft.Json;
using Nop.Plugin.Api.DTOs;
using Nop.Plugin.Api.DTOs.Orders;
using Nop.Plugin.Api.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.Models.Orders
{
    public class OrderNoteListModel : BaseListModel, ISerializableObject
    {
        public OrderNoteListModel()
        {
            this.Notes = new List<OrderNoteDto>();
        }

        [JsonProperty("notes")]
        public IList<OrderNoteDto> Notes { get; set; }

        public string GetPrimaryPropertyName()
        {
            return "notes";
        }

        public Type GetPrimaryPropertyType()
        {
            return typeof(OrderNoteListModel);
        }
    }
}
