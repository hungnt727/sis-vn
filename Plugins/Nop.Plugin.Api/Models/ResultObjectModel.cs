﻿using Newtonsoft.Json;
using Nop.Plugin.Api.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.Models
{
    public class ResultObjectModel<T> : ISerializableObject
    {
        public ResultObjectModel()
        {}

        [JsonProperty("data")]
        public T Data { get; set; }
        [JsonProperty("success")]
        public bool Success { get; set; }
        [JsonProperty("message")]
        public string Message { get; set; }

        public string GetPrimaryPropertyName()
        {
            return "result";
        }

        public Type GetPrimaryPropertyType()
        {
            return typeof(ResultObjectModel<>);
        }
    }
}
