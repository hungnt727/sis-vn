﻿using System;
using Newtonsoft.Json;
using Nop.Plugin.Api.Constants;
using Nop.Plugin.Api.ModelBinders;

namespace Nop.Plugin.Api.Models.DiscountsParameters
{
    using Microsoft.AspNetCore.Mvc;
    using Nop.Plugin.Api.DTOs.Products;
    using System.Collections.Generic;

    // JsonProperty is used only for swagger
    [ModelBinder(typeof(ParametersModelBinder<Discount_AppliedParametersModel>))]
    public class Discount_AppliedParametersModel
    {
        public Discount_AppliedParametersModel()
        {
            DiscountId = 0;
            AppliedToProductIds = null;
        }

        [JsonProperty("discountid")]
        public int DiscountId { get; set; }
    
        [JsonProperty("products_ids")]
        public ICollection<int> AppliedToProductIds { get; set; }

        [JsonProperty("categorie_ids")]
        public  ICollection<int> AppliedToCategorieIds { get; set; }

        [JsonProperty("isdeleteoldproductapplied")]
        public bool IsDeleteOldProductApplied { get; set; }

        [JsonProperty("isdeleteoldcategoryapplied")]
        public bool IsDeleteOldCategoryApplied { get; set; }

        
    }
}