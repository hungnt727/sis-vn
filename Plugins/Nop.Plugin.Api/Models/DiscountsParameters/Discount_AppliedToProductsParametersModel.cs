﻿using System;
using Newtonsoft.Json;
using Nop.Plugin.Api.Constants;
using Nop.Plugin.Api.ModelBinders;

namespace Nop.Plugin.Api.Models.DiscountsParameters
{
    using Microsoft.AspNetCore.Mvc;
    using System.Collections.Generic;

    // JsonProperty is used only for swagger
    [ModelBinder(typeof(ParametersModelBinder<Discount_AppliedToProductsParametersModel>))]
    public class Discount_AppliedToProductsParametersModel
    {
        public Discount_AppliedToProductsParametersModel()
        {
            DiscountId = 0;
            ProductIds = null;
        }

        public int DiscountId { get; set; }
        public int[] ProductIds { get; set; }

    }
}