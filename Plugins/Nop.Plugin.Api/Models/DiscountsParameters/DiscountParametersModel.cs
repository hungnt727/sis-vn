﻿using System;
using Newtonsoft.Json;
using Nop.Plugin.Api.Constants;
using Nop.Plugin.Api.ModelBinders;

namespace Nop.Plugin.Api.Models.DiscountsParameters
{
    using Microsoft.AspNetCore.Mvc;

    // JsonProperty is used only for swagger
    [ModelBinder(typeof(ParametersModelBinder<DiscountParametersModel>))]
    public class DiscountParametersModel
    {
        

        public DiscountParametersModel()
        {
            SearchDiscountCouponCode = null;
            SearchDiscountName = null;
            SearchDiscountTypeId = -1;
            PageSize = Configurations.DefaultLimit;
            PageIndex = Configurations.DefaultPageValue;
            StartDateUtc = null;
            EndDateUtc = null;
        }

        [JsonProperty("search_discount_couponcode")]

        public string SearchDiscountCouponCode { get; set; }

        [JsonProperty("search_discount_name")]
        public string SearchDiscountName { get; set; }

        [JsonProperty("search_discount_typeid")]
        public int SearchDiscountTypeId { get; set; }


        /// <summary>
        /// Amount of results (default: 50) (maximum: 250)
        /// </summary>
        [JsonProperty("page_size")]
        public int PageSize { get; set; }

        /// <summary>
        /// Page to show (default: 1)
        /// </summary>
        [JsonProperty("page_index")]
        public int PageIndex { get; set; }

       
        /// <summary>
        /// Show dicount start date (format: 2008-12-31 03:00)
        /// </summary>
        [JsonProperty("start_date_utc")]
        public DateTime? StartDateUtc { get; set; }

        /// <summary>
        /// Show dicount end date  (format: 2008-12-31 03:00)
        /// </summary>
        [JsonProperty("end_date_utc")]
        public DateTime? EndDateUtc { get; set; }
    }
}