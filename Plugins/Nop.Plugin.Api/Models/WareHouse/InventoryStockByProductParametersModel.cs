﻿using System;
using Newtonsoft.Json;
using Nop.Plugin.Api.Constants;
using Nop.Plugin.Api.ModelBinders;

namespace Nop.Plugin.Api.Models.InventoryStockParameters
{
    using Microsoft.AspNetCore.Mvc;

    // JsonProperty is used only for swagger
    [ModelBinder(typeof(ParametersModelBinder<WareHouseSearchParametersModel>))]
    public class WareHouseSearchParametersModel
    {
        public WareHouseSearchParametersModel()
        {
            KeySearch = string.Empty;
            PageIndex = 1;
            PageSize = 50;
        }
        [JsonProperty("key_search")]
        public string KeySearch { get; set; }

        [JsonProperty("page_index")]
        public int PageIndex { get; set; }

        [JsonProperty("page_size")]
        public int PageSize { get; set; }

    }
}