﻿using System;
using Newtonsoft.Json;
using Nop.Plugin.Api.Constants;
using Nop.Plugin.Api.ModelBinders;

namespace Nop.Plugin.Api.Models.InventoryStockParameters
{
    using Microsoft.AspNetCore.Mvc;

    // JsonProperty is used only for swagger
    [ModelBinder(typeof(ParametersModelBinder<InventoryStockByProductParametersModel>))]
    public class InventoryStockByProductParametersModel
    {
        public InventoryStockByProductParametersModel()
        {
            ProductId = 0;
            WarehouseId = 0;
        }
        [JsonProperty("product_id")]
        public int ProductId { get; set; }

        [JsonProperty("ware_house_id")]
        public int WarehouseId { get; set; }
    }
}