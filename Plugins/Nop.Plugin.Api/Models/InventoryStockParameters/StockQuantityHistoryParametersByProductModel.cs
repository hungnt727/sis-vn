﻿using System;
using Newtonsoft.Json;
using Nop.Plugin.Api.Constants;
using Nop.Plugin.Api.ModelBinders;

namespace Nop.Plugin.Api.Models.InventoryStockParameters
{
    using Microsoft.AspNetCore.Mvc;

    // JsonProperty is used only for swagger
    [ModelBinder(typeof(ParametersModelBinder<StockQuantityHistoryParametersByProductModel>))]
    public class StockQuantityHistoryParametersByProductModel
    {
        public StockQuantityHistoryParametersByProductModel()
        {
            ProductId = 0;
            WarehouseId = null;
            DateFrom = DateTime.Now.AddMonths(-1);
            DateTo = DateTime.Now;
            PageSize = Configurations.DefaultLimit;
            PageIndex = Configurations.DefaultPageValue;
        }

        [JsonProperty("product_id")]
        public int ProductId { get; set; }
        [JsonProperty("ware_house_id")]
        public int? WarehouseId { get; set; }
        [JsonProperty("date_from")]
        public DateTime? DateFrom { get; set; }
        [JsonProperty("date_to")]
        public DateTime? DateTo { get; set; }

        /// <summary>
        /// Amount of results (default: 50) (maximum: 250)
        /// </summary>
        [JsonProperty("page_size")]
        public int PageSize { get; set; }

        /// <summary>
        /// Page to show (default: 1)
        /// </summary>
        [JsonProperty("page_index")]
        public int PageIndex { get; set; }
    }
}