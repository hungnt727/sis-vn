﻿using System;
using Newtonsoft.Json;
using Nop.Plugin.Api.Constants;
using Nop.Plugin.Api.ModelBinders;

namespace Nop.Plugin.Api.Models.InventoryStockParameters
{
    using Microsoft.AspNetCore.Mvc;

    // JsonProperty is used only for swagger
    [ModelBinder(typeof(ParametersModelBinder<StockQuantityHistoryParametersModel>))]
    public class StockQuantityHistoryParametersModel
    {
        public StockQuantityHistoryParametersModel()
        {
            DateFrom = DateTime.Now.AddMonths(-1);
            DateTo = DateTime.Now;
            KeySearchProduct = string.Empty;
            ProductTypeId = null;
            VendorId = null;
            WarehouseId = null;
            PageSize = Configurations.DefaultLimit;
            PageIndex = Configurations.DefaultPageValue;
        }

        [JsonProperty("date_from")]
        public DateTime DateFrom { get; set; }
        [JsonProperty("date_to")]
        public DateTime DateTo { get; set; }

        [JsonProperty("key_search_product")]
        public string KeySearchProduct { get; set; }
        [JsonProperty("product_type_id")]
        public int? ProductTypeId { get; set; }
        [JsonProperty("vendor_id")]
        public int? VendorId { get; set; }
        [JsonProperty("ware_house_id")]
        public int? WarehouseId { get; set; }

        /// <summary>
        /// Amount of results (default: 50) (maximum: 250)
        /// </summary>
        [JsonProperty("page_size")]
        public int PageSize { get; set; }

        /// <summary>
        /// Page to show (default: 1)
        /// </summary>
        [JsonProperty("page_index")]
        public int PageIndex { get; set; }
    }
}