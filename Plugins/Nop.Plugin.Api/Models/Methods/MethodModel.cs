﻿using Newtonsoft.Json;
using Nop.Plugin.Api.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.Models.Methods
{
    public class MethodModel : ISerializableObject
    {
        public string GetPrimaryPropertyName()
        {
            throw new NotImplementedException();
        }

        public Type GetPrimaryPropertyType()
        {
            throw new NotImplementedException();
        }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("system_name")]
        public string SystemName { get; set; }
    }
}
