﻿using Newtonsoft.Json;
using Nop.Plugin.Api.DTOs;
using Nop.Plugin.Api.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.Models.Methods
{
    public class MethodListModel : ISerializableObject
    {
        public MethodListModel()
        {
            this.Methods = new List<MethodModel>();
        }

        [JsonProperty("methods")]
        public IList<MethodModel> Methods { get; set; }

        public string GetPrimaryPropertyName()
        {
            return "orders";
        }

        public Type GetPrimaryPropertyType()
        {
            return typeof(MethodListModel);
        }
    }
}
