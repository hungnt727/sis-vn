﻿using System.Collections.Generic;
using Newtonsoft.Json;
using Nop.Plugin.Api.Constants;
using Nop.Plugin.Api.ModelBinders;

namespace Nop.Plugin.Api.Models.ProductsParameters
{
    using Microsoft.AspNetCore.Mvc;
    using Nop.Core.Domain.Catalog;

    // JsonProperty is used only for swagger
    [ModelBinder(typeof(ParametersModelBinder<ProductsParametersModel>))]
    public class ProductsParametersModel : BaseProductsParametersModel
    {
        public ProductsParametersModel()
        {
            Ids = null;
            Keyword = null;
            PageSize = 10;
            PageNumber = Configurations.DefaultPageValue;
            SinceId = Configurations.DefaultSinceId;
            Fields = string.Empty;
            OrderBy = ProductSortingEnum.Position;
            ProductStatus = ProductStatusEnum.All;
        }

        /// <summary>
        /// A comma-separated list of order ids
        /// </summary>
        [JsonProperty("ids")]
        public List<int> Ids { get; set; }

        [JsonProperty("order_by")]
        public ProductSortingEnum OrderBy { get; set; }

        [JsonProperty("image_size")]
        public int ImageSize { get; set; }

        [JsonProperty("keyword")]
        public string Keyword { get; set; }

        /// <summary>
        /// [HienNguyen][08/08/2018][Trạng thái sản phẩm]
        /// 0 Hàng đang kinh doanh
        /// 1 Hàng đã ngừng kinh doanh
        /// 2 Hàng đã xóa
        /// 3 Hiển thị ra Website
        /// 4 Không hiển thị ra Website
        /// </summary>
        /// 
        [JsonProperty("product_status_id")]
        public ProductStatusEnum ProductStatus { get; set; }

        /// <summary>
        /// Amount of results (default: 50) (maximum: 250)
        /// </summary>
        [JsonProperty("page_size")]
        public int PageSize { get; set; }

        /// <summary>
        /// Page to show (default: 1)
        /// </summary>
        [JsonProperty("page_number")]
        public int PageNumber { get; set; }

        /// <summary>
        /// Restrict results to after the specified ID
        /// </summary>
        [JsonProperty("since_id")]
        public int SinceId { get; set; }

        /// <summary>
        /// comma-separated list of fields to include in the response
        /// </summary>
        [JsonProperty("fields")]
        public string Fields { get; set; }
    }


}