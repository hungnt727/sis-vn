﻿using Newtonsoft.Json;
using Nop.Plugin.Api.Constants;
using Nop.Plugin.Api.ModelBinders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.ModelBinding;

namespace Nop.Plugin.Api.Models.SISVN_ReceiptPaymentParameters
{
    [ModelBinder(typeof(ParametersModelBinder<SISVN_ReceiptPaymentParametersModel>))]
    public class SISVN_ReceiptPaymentParametersModel
    {
        public SISVN_ReceiptPaymentParametersModel()
        {
            Keyword = null;
            ReceiptPaymentCode = null;
            FromDate = null;
            ToDate = null;
            ReceiptPaymentTypeId = null;
            PaymentMethodId = null;
            ExpenseTypeId = null;
            PageSize = int.MaxValue;
            PageIndex = Configurations.DefaultPageValue;
            Fields = string.Empty;
        }

        [JsonProperty("keyword")]
        public string Keyword { get; set; }

        [JsonProperty("receipt_payment_code")]
        public string ReceiptPaymentCode { get; set; }

        [JsonProperty("from_date")]
        public DateTime? FromDate { get; set; }

        [JsonProperty("to_date")]
        public DateTime? ToDate { get; set; }

        [JsonProperty("receipt_payment_type_id")]
        public int? ReceiptPaymentTypeId { get; set; }

        [JsonProperty("payment_method_id")]
        public int? PaymentMethodId { get; set; }

        [JsonProperty("expense_type_id")]
        public int? ExpenseTypeId { get; set; }

        /// <summary>
        /// Amount of results (default: 50) (maximum: 250)
        /// </summary>
        [JsonProperty("page_size")]
        public int PageSize { get; set; }

        /// <summary>
        /// Page to show (default: 1)
        /// </summary>
        [JsonProperty("page_index")]
        public int PageIndex { get; set; }

        /// <summary>
        /// comma-separated list of fields to include in the response
        /// </summary>
        [JsonProperty("fields")]
        public string Fields { get; set; }
    }
}
