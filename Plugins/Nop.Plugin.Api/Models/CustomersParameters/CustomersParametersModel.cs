﻿using System;
using Newtonsoft.Json;
using Nop.Plugin.Api.Constants;
using Nop.Plugin.Api.ModelBinders;

namespace Nop.Plugin.Api.Models.CustomersParameters
{
    using Microsoft.AspNetCore.Mvc;
    using Nop.Core.Domain.Customers;
    using System.Collections.Generic;

    // JsonProperty is used only for swagger
    [ModelBinder(typeof(ParametersModelBinder<CustomersParametersModel>))]
    public class CustomersParametersModel
    {
        public CustomersParametersModel()
        {
            Keyword = null;
            PageIndex = Configurations.DefaultPageValue;
            SinceId = 0;
            Fields = string.Empty;
            CreatedAtMax = null;
            CreatedAtMin = null;

            Roles = new List<int>();
            PageNumber = 1;
            PageSize = 10;
            CustomerType = CustomerTypeEnum.All;
        }

        [JsonProperty("customer_type_id")]
        public CustomerTypeEnum CustomerType { get; set; }

        [JsonProperty("roles")]
        public List<int> Roles { get; set; }

        [JsonProperty("keyword")]
        public string Keyword { get; set; }

        /// <summary>
        /// Amount of results (default: 50) (maximum: 250)
        /// </summary>
        [JsonProperty("page_size")]
        public int PageSize { get; set; }

        /// <summary>
        /// Page to show (default: 1)
        /// </summary>
        [JsonProperty("page_index")]
        public int PageIndex { get; set; }

        [JsonProperty("page_number")]
        public int PageNumber { get; set; }

        /// <summary>
        /// Restrict results to after the specified ID
        /// </summary>
        [JsonProperty("since_id")]
        public int SinceId { get; set; }

        /// <summary>
        /// Comma-separated list of fields to include in the response
        /// </summary>
        [JsonProperty("fields")]
        public string Fields { get; set; }

        /// <summary>
        /// Show customers created after date (format: 2008-12-31 03:00)
        /// </summary>
        [JsonProperty("created_at_min")]
        public DateTime? CreatedAtMin { get; set; }

        /// <summary>
        /// Show customers created before date (format: 2008-12-31 03:00)
        /// </summary>
        [JsonProperty("created_at_max")]
        public DateTime? CreatedAtMax { get; set; }
    }
}