﻿using System.Collections.Generic;
using Newtonsoft.Json;
using Nop.Plugin.Api.Constants;
using Nop.Plugin.Api.ModelBinders;

namespace Nop.Plugin.Api.Models.OrdersParameters
{
    using Microsoft.AspNetCore.Mvc;
    using Nop.Services.Orders;

    // JsonProperty is used only for swagger
    [ModelBinder(typeof(ParametersModelBinder<OrdersParametersModel>))]
    public class OrdersParametersModel : BaseOrdersParametersModel
    {
        public OrdersParametersModel()
        {
            Ids = null;
            Keyword = null;
            OrderStatusIds = null;
            OrderTypeIds = null;
            SearchType = null;

            SinceId = Configurations.DefaultSinceId;
            Fields = string.Empty;

            PageIndex = 1;
            PageSize = 10;
            PageNumber = 1;
        }
        [JsonProperty("order_type_id")]
        public OrderTypeEnum OrderType { get; set; }

        [JsonProperty("store_id")]
        public int StoreId { get; set; }

        [JsonProperty("order_payment_ids")]
        public List<int> OrderPaymentIds { get; set; }

        [JsonProperty("order_shipment_ids")]
        public List<int> OrderShipmentIds { get; set; }

        /// <summary>
        /// A comma-separated list of order ids
        /// </summary>
        [JsonProperty("ids")]
        public List<int> Ids { get; set; }

        /// <summary>
        /// Tìm kiếm theo  mã đơn hoặc mã/họ tên/SĐT khách hàng/NCC
        /// </summary>
        [JsonProperty("keyword")]
        public string Keyword { get; set; }

        /// <summary>
        /// [HienNguyen][Loại tìm kiếm]
        /// </summary>
        [JsonProperty("search_type")]
        public int? SearchType { get; set; }

        /// <summary>
        /// Trạng thái đơn hàng
        /// </summary>
        [JsonProperty("order_status_ids")]
        public List<int> OrderStatusIds { get; set; }

        /// <summary>
        /// Loại đơn hàng
        /// </summary>
        [JsonProperty("order_type_ids")]
        public List<int> OrderTypeIds { get; set; }

        /// <summary>
        /// Amount of results (default: 50) (maximum: 250)
        /// </summary>
        [JsonProperty("page_size")]
        public int PageSize { get; set; }

        /// <summary>
        /// Page to show (default: 1)
        /// </summary>
        [JsonProperty("page_index")]
        public int PageIndex { get; set; }

        [JsonProperty("page_number")]
        public int PageNumber { get; set; }

        /// <summary>
        /// Restrict results to after the specified ID
        /// </summary>
        [JsonProperty("since_id")]
        public int SinceId { get; set; }

        /// <summary>
        /// comma-separated list of fields to include in the response
        /// </summary>
        [JsonProperty("fields")]
        public string Fields { get; set; }
    }


}