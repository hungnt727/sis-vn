﻿using System;
using Newtonsoft.Json;
using Nop.Plugin.Api.Constants;
using Nop.Plugin.Api.ModelBinders;

namespace Nop.Plugin.Api.Models.InputVoucherParameters
{
    using Microsoft.AspNetCore.Mvc;
    using Nop.Core.Domain.Orders;

    // JsonProperty is used only for swagger
    [ModelBinder(typeof(ParametersModelBinder<InputVoucherSearchParametersModel>))]
    public class InputVoucherSearchParametersModel
    {
        public InputVoucherSearchParametersModel()
        {
            ProductId = null;
            VendorId = null;
            WareHouseId = null;
            InputTypeId = null;
            InputStatusId = null;
            InputDateFrom = null;
            InputDateFrom = null;
            InputDateTo = null;
            PageIndex = Configurations.DefaultPageValue;
            PageSize = Configurations.DefaultLimit;

        }

        public int? ProductId { get; set; }
        public int? VendorId { get; set; }
        public int? WareHouseId { get; set; }
        public int? InputTypeId { get; set; }
        public int? InputStatusId { get; set; }
        public DateTime? InputDateFrom { get; set; }
        public DateTime? InputDateTo { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }


    }
}