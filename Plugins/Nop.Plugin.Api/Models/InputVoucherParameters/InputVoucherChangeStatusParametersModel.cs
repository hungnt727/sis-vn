﻿using System;
using Newtonsoft.Json;
using Nop.Plugin.Api.Constants;
using Nop.Plugin.Api.ModelBinders;

namespace Nop.Plugin.Api.Models.InputVoucherParameters
{
    using Microsoft.AspNetCore.Mvc;
    using Nop.Core.Domain.Orders;

    // JsonProperty is used only for swagger
    [ModelBinder(typeof(ParametersModelBinder<InputVoucherChangeStatusParametersModel>))]
    public class InputVoucherChangeStatusParametersModel
    {
        public InputVoucherChangeStatusParametersModel()
        {
            InputVoucherID = -1;
            InputStatusId = (int)OrderStatus.Temporary;
        } 

        public int InputVoucherID { get; set; }

        public int InputStatusId { get; set; }
    
    }
}