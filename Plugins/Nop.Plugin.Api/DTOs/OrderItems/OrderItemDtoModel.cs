﻿using System;
using System.Collections.Generic;
using FluentValidation.Attributes;
using Newtonsoft.Json;
using Nop.Core.Domain.Orders;
using Nop.Plugin.Api.Attributes;
using Nop.Plugin.Api.DTOs.Products;
using Nop.Plugin.Api.Validators;

namespace Nop.Plugin.Api.DTOs.OrderItems
{
    [Validator(typeof(OrderItemDtoValidator))]
    [JsonObject(Title = "order_item")]
    public class OrderItemDtoModel
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("order_id")]
        public int OrderId { get; set; }

        [JsonProperty("sku")]
        public string SKU { get; set; }

        [JsonProperty("quantity")]
        public int Quantity { get; set; }

        /// <summary>
        /// Đơn giá sản phẩm
        /// </summary>
        [JsonProperty("unit_price")]
        public decimal UnitPrice { get; set; }

        /// <summary>
        /// Giá bán của sản phẩm
        /// </summary>
        [JsonProperty("sale_price")]
        public decimal SalePrice { get; set; }

        /// <summary>
        /// Là giảm giá theo phần trăm
        /// </summary>
        [JsonProperty("is_discount_percent")]
        public bool IsDiscountPercent { get; set; }

        /// <summary>
        /// Số tiền giảm giá hoặc phần trăm giảm giá
        /// </summary>
        [JsonProperty("discount")]
        public decimal Discount { get; set; }

        /// <summary>
        /// [HienNguyen][Tổng tiền]
        /// </summary>
        [JsonProperty("total_amount")]
        public decimal TotalAmount { get; set; }

        [JsonProperty("product_id")]
        public int ProductId { get; set; }

        [JsonProperty("product_name")]
        public string ProductName { get; set; }

        /// <summary>
        /// [HienNguyen][30/07/2018][Hàm copy data]
        /// </summary>
        /// <param name="orderItem"></param>
        /// <returns></returns>
        public static OrderItemDtoModel CopyData(OrderItem orderItem)
        {
            OrderItemDtoModel orderItemCopy = new OrderItemDtoModel()
            {
                Id = orderItem.Id.ToString(),
                OrderId = orderItem.OrderId,
                SalePrice = orderItem.PriceInclTax,
                UnitPrice = orderItem.UnitPriceInclTax,
                ProductId = orderItem.ProductId,
                ProductName = orderItem.Product.Name,
                Quantity = orderItem.Quantity,
                SKU = orderItem.Product.Sku,
                Discount = orderItem.DiscountAmountInclTax,
                TotalAmount = (orderItem.PriceInclTax - orderItem.DiscountAmountInclTax) * orderItem.Quantity,
            };
            return orderItemCopy;
        }
    }
}