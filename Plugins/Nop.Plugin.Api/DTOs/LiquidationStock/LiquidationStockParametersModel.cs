﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Core.Domain.Catalog;

namespace Nop.Plugin.Api.DTOs.LiquidationStock
{
    [JsonObject(Title = "liquidation_stock")]
    public class LiquidationStockDto
    {
        /// <summary>
        /// Ngày kiểm kê
        /// </summary>
        [JsonProperty("liquidation_date")]
        public DateTime LiquidationDate { get; set; }
        /// <summary>
        /// Kho kiểm kê
        /// </summary>
        [JsonProperty("ware_house_id")]
        public int? WarehouseId { get; set; }
        /// <summary>
        /// Ghi chú
        /// </summary>
        [JsonProperty("note")]
        public string Note { get; set; }
        /// <summary>
        /// Người kiểm kê
        /// </summary>
        [JsonProperty("liquidation_user_id")]
        public int LiquidationUserId { get; set; }

        [JsonProperty("liquidation_stock_items")]
        public List<LiquidationStockItemDto> LiquidationStockItems { get; set; }
    }


    [JsonObject(Title = "liquidation_stock_item")]
    public class LiquidationStockItemDto
    {
        /// <summary>
        /// Mã sản phẩm
        /// </summary>
        [JsonProperty("product_id")]
        public int ProductId { get; set; }
        /// <summary>
        /// Tồn kho hệ thống
        /// </summary>
        [JsonProperty("stock_quantity")]
        public int StockQuantity { get; set; }
        /// <summary>
        /// Tồn kho thức tế
        /// </summary>
        [JsonProperty("current_stock_quantity")]
        public int CurrentStockQuantity { get; set; }
        /// <summary>
        /// Diễn giải
        /// </summary>
        [JsonProperty("description")]
        public string Description { get; set; }
    }
}