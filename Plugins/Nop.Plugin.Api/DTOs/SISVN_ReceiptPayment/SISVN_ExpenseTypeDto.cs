﻿using FluentValidation.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.DTOs.SISVN_ReceiptPayment
{
    //[Validator(typeof(ShoppingCartItemDtoValidator))]
    [JsonObject(Title = "sisvn_expense_type")]
    public class SISVN_ExpenseTypeDto
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("receipt_payment_type_id")]
        public int ReceiptPaymentTypeId { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("descriptions")]
        public string Descriptions { get; set; }
        [JsonProperty("sisvn_receipt_payment_type")]
        public virtual SISVN_ReceiptPaymentTypeDto SISVN_ReceiptPaymentType { get; set; }
    }
}
