﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.DTOs.SISVN_ReceiptPayment
{
    public class SISVN_ReceiptPaymentTypeRootObject : ISerializableObject
    {
        public SISVN_ReceiptPaymentTypeRootObject()
        {
            SISVN_ReceiptPaymentType = new List<SISVN_ReceiptPaymentTypeDto>();
        }

        [JsonProperty("sisvn_receipt_payment_types")]
        public IList<SISVN_ReceiptPaymentTypeDto> SISVN_ReceiptPaymentType { get; set; }

        public string GetPrimaryPropertyName()
        {
            return "sisvn_receipt_payment_types";
        }

        public Type GetPrimaryPropertyType()
        {
            return typeof(SISVN_ReceiptPaymentTypeDto);
        }
    }
}
