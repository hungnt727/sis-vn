﻿using FluentValidation.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.DTOs.SISVN_ReceiptPayment
{
    //[Validator(typeof(ShoppingCartItemDtoValidator))]
    [JsonObject(Title = "sisvn_receipt_payment")]
    public class SISVN_ReceiptPaymentDto
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("receipt_payment_code")]
        public string ReceiptPaymentCode { get; set; }
        [JsonProperty("receipt_payment_type_id")]
        public int ReceiptPaymentTypeId { get; set; }
        [JsonProperty("payment_method_id")]
        public int PaymentMethodId { get; set; }
        [JsonProperty("expense_type_id")]
        public int ExpenseTypeId { get; set; }
        [JsonProperty("customer_id")]
        public int CustomerId { get; set; }
        [JsonProperty("store_id")]
        public int StoreId { get; set; }
        [JsonProperty("amount")]
        public decimal Amount { get; set; }
        [JsonProperty("transaction_date")]
        public DateTime TransactionDate { get; set; }
        [JsonProperty("createuser_id")]
        public int CreateUserId { get; set; }
        [JsonProperty("is_delete")]
        public bool? IsDelete { get; set; }
        [JsonProperty("delete_user_id")]
        public int? DeleteUserId { get; set; }
        [JsonProperty("delete_date")]
        public DateTime? DeleteDate { get; set; }
        [JsonProperty("descriptions")]
        public string Descriptions { get; set; }
        //public SISVN_ReceiptPaymentTypeDto SISVN_ReceiptPaymentType { get; set; }
        //public SISVN_ExpenseTypeDto SISVN_ExpenseType { get; set; }
        //public Store Store { get; set; }

    }
}
