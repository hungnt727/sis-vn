﻿using FluentValidation.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.DTOs.SISVN_ReceiptPayment
{
    //[Validator(typeof(ShoppingCartItemDtoValidator))]
    [JsonObject(Title = "sisvn_receipt_payment_type")]
    public class SISVN_ReceiptPaymentTypeDto
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("descriptions")]
        public string Descriptions { get; set; }
    }
}
