﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.DTOs.SISVN_ReceiptPayment
{
    public class SISVN_ExpenseTypeRootObject : ISerializableObject
    {
        public SISVN_ExpenseTypeRootObject()
        {
            SISVN_ExpenseType = new List<SISVN_ExpenseTypeDto>();
        }

        [JsonProperty("sisvn_expense_type")]
        public IList<SISVN_ExpenseTypeDto> SISVN_ExpenseType { get; set; }

        public string GetPrimaryPropertyName()
        {
            return "sisvn_expense_type";
        }

        public Type GetPrimaryPropertyType()
        {
            return typeof(SISVN_ExpenseTypeDto);
        }
    }
}
