﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.DTOs.SISVN_ReceiptPayment
{
    public class SISVN_ReceiptPaymentRootObject : ISerializableObject
    {
        public SISVN_ReceiptPaymentRootObject()
        {
            SISVN_ReceiptPayment = new List<SISVN_ReceiptPaymentDto>();
        }

        [JsonProperty("sisvn_receipt_payments")]
        public IList<SISVN_ReceiptPaymentDto> SISVN_ReceiptPayment { get; set; }

        public string GetPrimaryPropertyName()
        {
            return "sisvn_receipt_payments";
        }

        public Type GetPrimaryPropertyType()
        {
            return typeof(SISVN_ReceiptPaymentDto);
        }
    }
}
