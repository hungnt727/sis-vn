﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Nop.Plugin.Api.DTOs.Orders
{
    public class OrderPaymentBalanceRootObject : ISerializableObject
    {
        public OrderPaymentBalanceRootObject()
        {
            OrderPaymentBalance = new List<OrderPaymentDto>();
        }

        /// <summary>
        /// Danh sách đơn hàng
        /// </summary>
        [JsonProperty("order_payment_balance")]
        public IList<OrderPaymentDto> OrderPaymentBalance { get; set; }

        public string GetPrimaryPropertyName()
        {
            return "order_payment_balance";
        }

        public Type GetPrimaryPropertyType()
        {
            return typeof(OrderPaymentDto);
        }
    }
}