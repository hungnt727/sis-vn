﻿using Newtonsoft.Json;
using Nop.Core;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Stores;
using Nop.Plugin.Api.DTOs.Customers;
using Nop.Plugin.Api.DTOs.OrderItems;
using Nop.Services.Customers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.DTOs.Orders
{
    /// <summary>
    /// [HienNguyen][03/08/2018][Model nhận dữ liệu trang tạo mới đơn hàng]
    /// </summary>
    [JsonObject(Title = "sale_order")]
    public class SaleOrderDtoModel : OrderDtoModel
    {
        private List<OrderItemDtoModel> _orderItems;
        private List<OrderNoteDtoModel> _orderNotes;

        /// <summary>
        /// Thông tin thanh toán
        /// </summary>
        [JsonProperty("payment_method")]
        public OrderPaymentDto PaymentMethod { get; set; }

        /// <summary>
        /// Thông tin đơn hàng chi tiết
        /// </summary>
        [JsonProperty("order_items")]
        public List<OrderItemDtoModel> OrderItems
        {
            get { return _orderItems; }
            set { _orderItems = value; }
        }

        /// <summary>
        /// Thông tin ghi chú đơn hàng
        /// </summary>
        [JsonProperty("order_notes")]
        public List<OrderNoteDtoModel> OrderNotes
        {
            get { return _orderNotes; }
            set { _orderNotes = value; }
        }
    }
}
