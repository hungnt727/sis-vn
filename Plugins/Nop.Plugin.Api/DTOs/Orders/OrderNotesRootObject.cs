﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Nop.Plugin.Api.DTOs.Orders
{
    public class OrderNotesRootObject : ISerializableObject
    {
        public OrderNotesRootObject()
        {
            OrderNotes = new List<OrderNoteDto>();
        }

        /// <summary>
        /// Danh sách đơn hàng
        /// </summary>
        [JsonProperty("order_notes")]
        public IList<OrderNoteDto> OrderNotes { get; set; }

        [JsonProperty("total_row")]
        public int TotalRow { get; set; }

        public string GetPrimaryPropertyName()
        {
            return "orders";
        }

        public Type GetPrimaryPropertyType()
        {
            return typeof(OrderDto);
        }
    }
}