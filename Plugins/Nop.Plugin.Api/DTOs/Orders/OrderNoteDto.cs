﻿using System;
using System.Collections.Generic;
using FluentValidation.Attributes;
using Newtonsoft.Json;
using Nop.Plugin.Api.DTOs.Customers;
using Nop.Plugin.Api.DTOs.OrderItems;
using Nop.Plugin.Api.DTOs.Shipment;
using Nop.Plugin.Api.Validators;

namespace Nop.Plugin.Api.DTOs.Orders
{
    [JsonObject(Title = "note")]
    //[Validator(typeof(OrderDtoValidator))]
    public class OrderNoteDto
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the order identifier
        /// </summary>
        /// 
        [JsonProperty("order_id")]
        public int OrderId { get; set; }

        /// <summary>
        /// Gets or sets the note
        /// </summary>
        /// 
        [JsonProperty("note")]
        public string Note { get; set; }

        /// <summary>
        /// Gets or sets the attached file (download) identifier
        /// </summary>
        /// 
        [JsonProperty("download_id")]
        public int DownloadId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether a customer can see a note
        /// </summary>
        /// 
        [JsonProperty("display_to_customer")]
        public bool DisplayToCustomer { get; set; }

        /// <summary>
        /// Gets or sets the date and time of order note creation
        /// </summary>.
        /// 
        [JsonProperty("created_on_utc")]
        public DateTime CreatedOnUtc { get; set; }

        [JsonProperty("created_on")]
        public string CreatedOn { get; set; }
    }
}