﻿using System;
using System.Collections.Generic;
using FluentValidation.Attributes;
using Newtonsoft.Json;
using Nop.Plugin.Api.DTOs.Customers;
using Nop.Plugin.Api.DTOs.OrderItems;
using Nop.Plugin.Api.DTOs.Shipment;
using Nop.Plugin.Api.Validators;

namespace Nop.Plugin.Api.DTOs.Orders
{
    [JsonObject(Title = "sisvn_transporter")]
    //[Validator(typeof(OrderDtoValidator))]
    public class SISVN_TransporterDto
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the order identifier
        /// </summary>
        /// 
        [JsonProperty("name")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the entity has been deleted
        /// </summary>
        /// 
        [JsonProperty("deleted")]
        public bool Deleted { get; set; }

        /// <summary>
        /// Gets or sets the display order
        /// </summary>
        /// 
        [JsonProperty("display_order")]
        public int DisplayOrder { get; set; }

        /// <summary>
        /// Gets or sets the date and time of instance creation
        /// </summary>
        /// 
        [JsonProperty("created_on_utc")]
        public DateTime CreatedOnUtc { get; set; }

        /// <summary>
        /// Gets or sets the date and time of instance update
        /// </summary>
        /// 
        [JsonProperty("updated_on_utc")]
        public DateTime UpdatedOnUtc { get; set; }
    }
}