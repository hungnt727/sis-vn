﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Nop.Plugin.Api.DTOs.Orders
{
    public class SISVN_TransporterRootObject : ISerializableObject
    {
        public SISVN_TransporterRootObject()
        {
            SISVN_Transporters = new List<SISVN_TransporterDto>();
        }

        /// <summary>
        /// Danh sách đơn hàng
        /// </summary>
        [JsonProperty("sisvn_transporters")]
        public IList<SISVN_TransporterDto> SISVN_Transporters { get; set; }

        [JsonProperty("total_row")]
        public int TotalRow { get; set; }

        public string GetPrimaryPropertyName()
        {
            return "orders";
        }

        public Type GetPrimaryPropertyType()
        {
            return typeof(SISVN_TransporterDto);
        }
    }
}