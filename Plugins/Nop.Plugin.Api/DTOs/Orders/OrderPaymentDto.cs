﻿using System;
using System.Collections.Generic;
using FluentValidation.Attributes;
using Newtonsoft.Json;
using Nop.Core;
using Nop.Core.Domain.Orders;
using Nop.Plugin.Api.DTOs.Customers;
using Nop.Plugin.Api.DTOs.OrderItems;
using Nop.Plugin.Api.Validators;

namespace Nop.Plugin.Api.DTOs.Orders
{
    /// <summary>
    /// [TuanDang][02/08/2017][]
    /// </summary>
    [JsonObject(Title = "payment")]
    public class OrderPaymentDto
    {
        [JsonProperty("payment_system_name")]
        public string PaymentSystemName { get; set; }

        [JsonProperty("date_paid")]
        public DateTime? PaymentDate { get; set; }

        [JsonProperty("amount")]
        public decimal PaymentAmount { get; set; }

        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("order_id")]
        public int OrderId { get; set; }

        [JsonProperty("payment_method_id")]
        public int PaymentMethodId { get; set; }

        [JsonProperty("payment_method")]
        public string PaymentMethod
        {
            get
            {
                return ((Nop.Core.Domain.Orders.PaymentMethod)PaymentMethodId).ToString();
            }
        }

        /// <summary>
        /// [HienNguyen][01/08/2018][Hàm copy data model]
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static OrderPaymentDto CopyData(SISVN_OrderPaymentBalance model)
        {
            return new OrderPaymentDto()
            {
                OrderId = model.OrderId,
                PaymentAmount = model.PaymentAmount,
                PaymentDate = model.PaymentDateUtc,
                PaymentMethodId = model.PaymentMethodId
            };
        }
    }
}