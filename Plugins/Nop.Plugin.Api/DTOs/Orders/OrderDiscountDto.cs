﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.DTOs.Orders
{
    public class OrderDiscountDto
    {
        [JsonProperty("value")]
        public decimal Value { get; set; }

        [JsonProperty("is_percent")]
        public bool IsPercent { get; set; }
    }
}
