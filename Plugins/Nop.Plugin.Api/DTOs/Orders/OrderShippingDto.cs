﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.DTOs.Orders
{
    public class OrderShippingDto
    {
        public OrderShippingDto()
        {
            this.Shipments = new List<OrderShipmentDto>();
        }
        [JsonProperty("system_name")]
        public string SystemName { get; set; }

        [JsonProperty("shipments")]
        public IList<OrderShipmentDto> Shipments { get; set; }
    }
}
