﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Nop.Plugin.Api.DTOs.Orders
{
    public class OrdersRootObjectModel : ISerializableObject
    {
        public OrdersRootObjectModel()
        {
            Orders = new List<OrderDto>();
        }

        /// <summary>
        /// Danh sách đơn hàng
        /// </summary>
        [JsonProperty("orders")]
        public IList<OrderDto> Orders { get; set; }

        /// <summary>
        /// Tổng tiền nợ
        /// </summary>
        [JsonProperty("total_debt_amount")]
        public decimal TotalDebtAmount { get; set; }

        /// <summary>
        /// Tổng tiền đơn hàng
        /// </summary>
        [JsonProperty("total_amount")]
        public decimal TotalAmount { get; set; }

        /// <summary>
        /// Tổng số đơn hàng
        /// </summary>

        [JsonProperty("total_count_order")]
        public int TotalCountOrder { get; set; }

        [JsonProperty("total_row")]
        public int TotalRow { get; set; }

        [JsonProperty("page_number")]
        public int PageNumber { get; set; }
        [JsonProperty("page_size")]
        public int PageSize { get; set; }
        [JsonProperty("total_page")]
        public int TotalPages { get; set; }

        public string GetPrimaryPropertyName()
        {
            return "orders";
        }

        public Type GetPrimaryPropertyType()
        {
            return typeof(OrderDtoModel);
        }
    }
}