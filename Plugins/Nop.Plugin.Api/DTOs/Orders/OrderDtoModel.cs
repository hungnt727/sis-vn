﻿using Newtonsoft.Json;
using Nop.Core;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Stores;
using Nop.Services.Customers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.DTOs.Orders
{
    [JsonObject(Title = "order")]
    public class OrderDtoModel : BaseEntity
    {

        [JsonProperty("custom_order_number")]
        public string CustomOrderNumber { get; set; }

        [JsonProperty("store_id")]
        public int StoreId { get; set; }

        [JsonProperty("store_name")]
        public string StoreName { get; set; }

        /// <summary>
        /// Là giảm giá theo phần trăm
        /// </summary>
        [JsonProperty("is_discount_percent")]
        public bool? IsDiscountPercent { get; set; }

        /// <summary>
        /// Số tiền giảm giá
        /// </summary>
        [JsonProperty("order_discount")]
        public decimal OrderDiscount { get; set; }

        [JsonProperty("payment_balance")]
        public decimal PaymentBalance { get; set; }

        [JsonProperty("order_total")]
        public decimal OrderTotal { get; set; }

        /// <summary>
        /// [HienNguyen][Tiền còn nợ]
        /// </summary>
        [JsonProperty("order_debt")]
        public decimal OrderDebt { get; set; }

        [JsonProperty("created_on")]
        public DateTime CreatedOn { get; set; }

        [JsonProperty("customer_id")]
        public int CustomerId { get; set; }

        [JsonProperty("customer_name")]
        public string CustomerName { get; set; }

        [JsonProperty("customer_create_id")]
        public int CustomerCreateId { get; set; }

        [JsonProperty("customer_create_name")]
        public string CustomerCreateName { get; set; }

        [JsonProperty("order_status_id")]
        public int OrderStatusId { get; set; }

        [JsonProperty("order_status")]
        public string OrderStatus { get; set; }

        [JsonProperty("order_type_id")]
        public int OrderTypeId { get; set; }

        [JsonProperty("order_type")]
        public string OrderType { get; set; }

        [JsonProperty("shipping_address_id")]
        public int? ShippingAddressId { get; set; }

        [JsonProperty("shipping_address1")]
        public string ShippingAddress1 { get; set; }

        [JsonProperty("shipping_address2")]
        public string ShippingAddress2 { get; set; }

        /// <summary>
        /// Ngày giao hàng
        /// </summary>
        [JsonProperty("shipped_date")]
        public DateTime? ShippedDate { get; set; }

        /// <summary>
        /// Ngày khách nhận hàng
        /// </summary>
        [JsonProperty("delivery_date")]
        public DateTime? DeliveryDate { get; set; }

        public static OrderDtoModel CopyData(Order order, IList<Store> listStore = null)
        {
            return new OrderDtoModel()
            {
                Id = order.Id,
                CreatedOn = order.CreatedOnUtc,
                CustomerId = order.CustomerId,
                CustomerName = order.Customer.GetFullName(),
                OrderDiscount = order.OrderDiscount,
                OrderStatusId = order.OrderStatusId,
                OrderStatus = CommonHelper.GetEnumDescription(order.OrderStatus),
                OrderTypeId = order.OrderTypeId ?? (int)Core.Domain.Orders.OrderType.ORDER,
                OrderType = CommonHelper.GetEnumDescription(order.OrderType ?? Core.Domain.Orders.OrderType.ORDER),
                OrderTotal = order.OrderTotal,
                PaymentBalance = 0,
                StoreId = order.StoreId,
                StoreName = listStore != null ? (listStore.FirstOrDefault(s => s.Id == order.StoreId) != null ? listStore.FirstOrDefault(s => s.Id == order.StoreId).Name : string.Empty) : null,
                ShippingAddressId = order.ShippingAddressId,
                ShippingAddress1 = order.ShippingAddress != null ? order.ShippingAddress.Address1 : string.Empty,
                ShippingAddress2 = order.ShippingAddress != null ? order.ShippingAddress.Address2 : string.Empty,
                ShippedDate = order.Shipments.Any() ? order.Shipments.FirstOrDefault().ShippedDateUtc : null,
                DeliveryDate = order.Shipments.Any() ? order.Shipments.FirstOrDefault().DeliveryDateUtc : null,
            };
        }
    }

    public class OrderStatusModel : ISerializableObject
    {
        public OrderStatusModel()
        {
            OrderStatus = new List<OrderStatus>();
        }

        /// <summary>
        /// Danh sách đơn hàng
        /// </summary>
        [JsonProperty("orderstatus")]
        public IList<OrderStatus> OrderStatus { get; set; }

        public string GetPrimaryPropertyName()
        {
            return "orderstatus";
        }

        public Type GetPrimaryPropertyType()
        {
            return typeof(OrderStatus);
        }
    }

    public class OrderTypeModel : ISerializableObject
    {
        public OrderTypeModel()
        {
            OrderType = new List<OrderType>();
        }

        /// <summary>
        /// Danh sách đơn hàng
        /// </summary>
        [JsonProperty("ordertype")]
        public IList<OrderType> OrderType { get; set; }

        public string GetPrimaryPropertyName()
        {
            return "ordertype";
        }

        public Type GetPrimaryPropertyType()
        {
            return typeof(OrderType);
        }
    }

    public class PaymentMethodModel : ISerializableObject
    {
        public PaymentMethodModel()
        {
            PaymentMethod = new List<PaymentMethod>();
        }

        /// <summary>
        /// Danh sách đơn hàng
        /// </summary>
        [JsonProperty("paymentmethod")]
        public IList<PaymentMethod> PaymentMethod { get; set; }

        public string GetPrimaryPropertyName()
        {
            return "paymentmethod";
        }

        public Type GetPrimaryPropertyType()
        {
            return typeof(PaymentMethod);
        }
    }

    public class OrderStatus
    {
        [JsonProperty("order_status_id")]
        public int OrderStatusId { get; set; }

        [JsonProperty("order_status_name")]
        public string OrderStatusName { get; set; }
    }

    public class OrderType
    {
        [JsonProperty("order_type_id")]
        public int OrderTypeId { get; set; }

        [JsonProperty("order_type_name")]
        public string OrderTypeName { get; set; }
    }

    public class PaymentMethod
    {
        [JsonProperty("payment_method_id")]
        public int PaymentMethodId { get; set; }

        [JsonProperty("payment_method_name")]
        public string PaymentMethodName { get; set; }
    }
}
