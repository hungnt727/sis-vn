﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.DTOs.Orders
{
    [JsonObject(Title = "shipment")]
    public class OrderShipmentDto
    {
        public OrderShipmentDto()
        {
            this.Items = new List<OrderShipmentItemDto>();
        }

        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("items")]
        public IList<OrderShipmentItemDto> Items { get; set; }

        [JsonProperty("tracking_number")]
        public string TrackingNumber { get; set; }

        [JsonProperty("weight")]
        public decimal? Weight { get; set; }

        [JsonProperty("date_shipped")]
        public DateTime? DateShipped { get; set; }

        [JsonProperty("date_deliveried")]
        public DateTime? DateDeliveried { get; set; }
    }
}
