﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Nop.Plugin.Api.DTOs.Categories
{
    public class CategoriesRootObjectModel : ISerializableObject
    {
        public CategoriesRootObjectModel()
        {
            Categories = new List<CategoryDtoModel>();
        }

        [JsonProperty("categories")]
        public IList<CategoryDtoModel> Categories { get; set; }

        public string GetPrimaryPropertyName()
        {
            return "categories";
        }

        public Type GetPrimaryPropertyType()
        {
            return typeof(VendorDtoModel);
        }
    }
}