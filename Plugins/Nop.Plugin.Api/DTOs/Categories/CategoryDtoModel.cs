﻿using System;
using System.Collections.Generic;
using FluentValidation.Attributes;
using Newtonsoft.Json;
using Nop.Plugin.Api.DTOs.Images;
using Nop.Plugin.Api.DTOs.Languages;
using Nop.Plugin.Api.Validators;

namespace Nop.Plugin.Api.DTOs.Categories
{
    [JsonObject(Title = "category")]
    public class CategoryDtoModel
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the parent category identifier
        /// </summary>
        [JsonProperty("parent_category_id")]
        public int ParentCategoryId { get; set; }

    }
}