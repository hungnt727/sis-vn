﻿using Newtonsoft.Json;
using Nop.Core.Domain.Catalog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.DTOs.ProductWarehouseInventorys
{
    [JsonObject(Title = "product_ware_house_inventory")]
    class ProductWarehouseInventoryDto
    {
        [JsonProperty("product_id")]
        public int ProductId { get; set; }

        [JsonProperty("product_name")]
        public string ProductName { get; set; }

        [JsonProperty("ware_house_id")]
        public int WarehouseId { get; set; }

        [JsonProperty("ware_house_name")]
        public string WarehouseName { get; set; }

        [JsonProperty("stock_quantity")]
        public int StockQuantity { get; set; }

        [JsonProperty("reserved_quantity")]
        public int ReservedQuantity { get; set; }


        public static ProductWarehouseInventoryDto CopyData(ProductWarehouseInventory productWarehouseInventory)
        {
            ProductWarehouseInventoryDto productWarehouseInventoryCopy = new ProductWarehouseInventoryDto()
            {
                ProductId = productWarehouseInventory.ProductId,
                ProductName = productWarehouseInventory.Product.Name,


                WarehouseId = productWarehouseInventory.WarehouseId,
                WarehouseName = productWarehouseInventory.Warehouse.Name,

                StockQuantity = productWarehouseInventory.StockQuantity,
                ReservedQuantity = productWarehouseInventory.ReservedQuantity,


            };
            return productWarehouseInventoryCopy;
        }
    }

    class ProductWarehouseInventoryDtoRootObject : ISerializableObject
    {
        public ProductWarehouseInventoryDtoRootObject()
        {
            ProductWarehouseInventorys = new List<ProductWarehouseInventoryDto>();
        }

        [JsonProperty("product_ware_house_inventory")]
        public IList<ProductWarehouseInventoryDto> ProductWarehouseInventorys { get; set; }

        [JsonProperty("totalrow")]
        public int TotalRow { get; set; }

        public string GetPrimaryPropertyName()
        {
            return "product_ware_house_inventory";
        }

        public Type GetPrimaryPropertyType()
        {
            return typeof(ProductWarehouseInventoryDto);
        }
    }
    

}
