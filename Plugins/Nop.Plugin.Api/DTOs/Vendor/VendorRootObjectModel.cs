﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Nop.Plugin.Api.DTOs.Categories;

namespace Nop.Plugin.Api.DTOs.Vendor
{
    public class VendorRootObjectModel : ISerializableObject
    {
        public VendorRootObjectModel()
        {
            Vendors = new List<VendorDtoModel>();
        }

        [JsonProperty("vendors")]
        public IList<VendorDtoModel> Vendors { get; set; }

        public string GetPrimaryPropertyName()
        {
            return "vendors";
        }

        public Type GetPrimaryPropertyType()
        {
            return typeof(VendorDtoModel);
        }
    }
}