﻿using Newtonsoft.Json;

namespace Nop.Plugin.Api.DTOs.InputVouchers
{
    public class InputVoucherOrderRootObject
    {
        [JsonProperty("inputvoucher")]
        public InputVoucherDto InputVoucher { get; set; }
    }
}