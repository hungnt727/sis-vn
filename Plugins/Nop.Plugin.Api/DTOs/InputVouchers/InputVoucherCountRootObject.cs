﻿using Newtonsoft.Json;

namespace Nop.Plugin.Api.DTOs.InputVouchers
{
    public class InputVoucherCountRootObject
    {
        [JsonProperty("count")]
        public int Count { get; set; }
    }
}