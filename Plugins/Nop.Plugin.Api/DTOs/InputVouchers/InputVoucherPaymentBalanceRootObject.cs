﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Nop.Plugin.Api.DTOs.InputVouchers
{
    public class OrderPaymentBalanceRootObject : ISerializableObject
    {
        public OrderPaymentBalanceRootObject()
        {
            OrderPaymentBalance = new List<SISVN_InputVoucherPaymentBalanceModel>();
        }

        /// <summary>
        /// Danh sách đơn hàng
        /// </summary>
        [JsonProperty("order_payment_balance")]
        public IList<SISVN_InputVoucherPaymentBalanceModel> OrderPaymentBalance { get; set; }

        public string GetPrimaryPropertyName()
        {
            return "order_payment_balance";
        }

        public Type GetPrimaryPropertyType()
        {
            return typeof(SISVN_InputVoucherPaymentBalanceModel);
        }
    }
}