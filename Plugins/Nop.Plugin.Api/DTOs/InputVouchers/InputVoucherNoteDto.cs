﻿using Newtonsoft.Json;
using Nop.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.DTOs.InputVouchers
{
    [JsonObject(Title = "input_voucher_note")]
    public class InputVoucherNoteDto : BaseEntity
    {
        [JsonProperty("input_voucher_id")]
        public int InputVoucherId { get; set; }

        [JsonProperty("note")]
        public string Note { get; set; }

        [JsonProperty("display_to_customer")]
        public bool DisplayToCustomer { get; set; }

        [JsonProperty("created_on")]
        public DateTime CreatedOnUtc { get; set; }
    }
}
