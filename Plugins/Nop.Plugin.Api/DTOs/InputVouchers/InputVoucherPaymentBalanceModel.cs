﻿using System;
using System.Collections.Generic;
using FluentValidation.Attributes;
using Newtonsoft.Json;
using Nop.Core;
using Nop.Core.Domain.Orders;
using Nop.Plugin.Api.DTOs.Customers;
using Nop.Plugin.Api.DTOs.OrderItems;
using Nop.Plugin.Api.DTOs.Orders;
using Nop.Plugin.Api.Validators;

namespace Nop.Plugin.Api.DTOs.InputVouchers
{
    /// <summary>
    /// [HienNguyen][02/08/2017][]
    /// </summary>
    [JsonObject(Title = "order_payment_balance")]
    public class SISVN_InputVoucherPaymentBalanceModel
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("order_id")]
        public int OrderId { get; set; }

        [JsonProperty("payment_date")]
        public DateTime PaymentDateUtc { get; set; }

        [JsonProperty("payment_amount")]
        public decimal PaymentAmount { get; set; }

        [JsonProperty("payment_method_id")]
        public int PaymentMethodId { get; set; }

        [JsonProperty("payment_method")]
        public string PaymentMethod
        {
            get
            {
                return CommonHelper.GetEnumDescription((Nop.Core.Domain.Orders.PaymentMethod)PaymentMethodId);
            }
        }

        /// <summary>
        /// [HienNguyen][01/08/2018][Hàm copy data model]
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static OrderPaymentDto CopyData(SISVN_OrderPaymentBalance model)
        {
            return new OrderPaymentDto()
            {
                OrderId = model.OrderId,
                PaymentAmount = model.PaymentAmount,
                PaymentDate = model.PaymentDateUtc,
                PaymentMethodId = model.PaymentMethodId
            };
        }
    }
}