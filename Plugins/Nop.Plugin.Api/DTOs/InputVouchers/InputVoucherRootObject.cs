﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Nop.Plugin.Api.DTOs.InputVouchers
{
    public class InputVoucherRootObject : ISerializableObject
    {
        public InputVoucherRootObject()
        {
            InputVouchers = new List<InputVoucherDto>();
        }

        /// <summary>
        /// Danh sách phiếu nhập
        /// </summary>
        [JsonProperty("input_vouchers")]
        public IList<InputVoucherDto> InputVouchers { get; set; }

        /// <summary>
        /// Tổng tiền nợ
        /// </summary>
        [JsonProperty("total_debt_amount")]
        public decimal TotalDebtAmount { get; set; }

        /// <summary>
        /// Tổng tiền đơn hàng
        /// </summary>
        [JsonProperty("total_amount")]
        public decimal TotalAmount { get; set; }

        /// <summary>
        /// Tổng số đơn hàng
        /// </summary>

        [JsonProperty("total_count_order")]
        public int TotalCountOrder { get; set; }

        [JsonProperty("total_row")]
        public int TotalRow { get; set; }

        public string GetPrimaryPropertyName()
        {
            return "orders";
        }

        public Type GetPrimaryPropertyType()
        {
            return typeof(object);
        }
    }
}