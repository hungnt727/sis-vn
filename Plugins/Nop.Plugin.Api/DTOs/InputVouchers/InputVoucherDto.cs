﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentValidation.Attributes;
using Newtonsoft.Json;
using Nop.Core;
using Nop.Core.Domain.Orders;
using Nop.Plugin.Api.DTOs.Customers;
using Nop.Plugin.Api.DTOs.InputVoucherItems;
using Nop.Plugin.Api.DTOs.OrderItems;
using Nop.Plugin.Api.Validators;

namespace Nop.Plugin.Api.DTOs.InputVouchers
{
    [JsonObject(Title = "input_voucher")]
    public class InputVoucherDto
    {
        /// <summary>
        /// Gets or sets a value indicating the order id
        /// </summary>
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("input_date")]
        public DateTime InputDate { get; set; }

        [JsonProperty("input_user")]
        public string InputUser { get; set; }


        [JsonProperty("vendor_id")]
        public int VendorId { get; set; }
        [JsonProperty("vendor_name")]
        public string VendorName { get; set; }


        /// <summary>
        /// Mã siêu thị đặt hàng.
        /// </summary>
        [JsonProperty("store_id")]
        public int StoreId { get; set; }
        [JsonProperty("store_name")]
        public string StoreName { get; set; }
        [JsonProperty("ware_house_id")]
        public int WareHouseId { get; set; }
        [JsonProperty("ware_house_name")]
        public string WareHouseName { get; set; }
        [JsonProperty("payment_method_id")]
        public int PaymentMethodId { get; set; }
        [JsonProperty("payment_method_name")]
        public string PaymentMethodName { get; set; }
        [JsonProperty("payment_amount")]
        public decimal PaymentAmount { get; set; }
        [JsonProperty("vat_number")]
        public string VatNumber { get; set; }

        [JsonProperty("input_voucher_total")]
        public decimal InputVoucherTotal { get; set; }



        [JsonProperty("input_notes")]
        public List<InputVoucherNoteDto> InputNotes { get; set; }
        [JsonProperty("input_voucher_items")]
        public List<InputVoucherItemDto> InputVoucherItems  { get; set; }

        [JsonProperty("customer_create_id")]
        public int CustomerCreateId { get; set; }
        [JsonProperty("customer_create_name")]
        public string CustomerCreateName { get; set; }
        [JsonProperty("input_type_id")]
        public int? InputTypeId { get; set; }

        [JsonProperty("input_type_Name")]
        public string InputTypeName { get; set; }

        [JsonProperty("input_status_id")]
        public int? InputStatusId { get; set; }
        [JsonProperty("input_status_name")]
        public string InputStatusName { get; set; }
        


        public static InputVoucherDto CopyData(Order _order)
        {
            InputVoucherDto inputVoucherDto = new InputVoucherDto();
            inputVoucherDto.Id = _order.Id;
            inputVoucherDto.InputDate = _order.CreatedOnUtc;
           
            if (_order.OrderType != null )
            {
                inputVoucherDto.InputTypeId = _order.OrderTypeId ?? (int)Core.Domain.Orders.OrderType.INVENTORY_INPUT;
                inputVoucherDto.InputTypeName = CommonHelper.GetEnumDescription(_order.OrderType ?? Core.Domain.Orders.OrderType.INVENTORY_INPUT);
            }

            if (_order.Vendor != null)
            {
                inputVoucherDto.VendorId = _order.Vendor.Id;
                inputVoucherDto.VendorName = _order.Vendor.Name;
            }

            if (_order.WareHouse != null)
            {
                inputVoucherDto.WareHouseId = _order.WareHouse.Id;
                inputVoucherDto.WareHouseName = _order.WareHouse.Name;
            }

            inputVoucherDto.InputTypeId = _order.OrderStatusId;
            inputVoucherDto.InputTypeName = CommonHelper.GetEnumDescription(_order.OrderStatus);

            if (_order.CustomerCreate != null)
            {
                inputVoucherDto.InputUser = _order.CustomerCreate.Username;
            }

            if (_order.SISVN_OrderPaymentBalances != null )
            {
                SISVN_OrderPaymentBalance objPayment = _order.SISVN_OrderPaymentBalances.FirstOrDefault();
                if (objPayment != null )
                {
                    inputVoucherDto.PaymentMethodId = objPayment.PaymentMethodId;
                    inputVoucherDto.PaymentMethodName = CommonHelper.GetEnumDescription((Nop.Core.Domain.Orders.PaymentMethod)inputVoucherDto.PaymentMethodId);
                    inputVoucherDto.PaymentAmount = objPayment.PaymentAmount;
                }
            }
            if (_order.OrderItems != null )
            {
                inputVoucherDto.InputVoucherItems = new List<InputVoucherItemDto>();
                inputVoucherDto.InputVoucherItems = _order.OrderItems.Select(s => InputVoucherItemDto.CopyData(s)).ToList();
            }

            return inputVoucherDto;
        }
    }


    [JsonObject(Title = "input_voucher_type")]
    public class InputVoucherType
    {
        [JsonProperty("input_voucher_type_id")]
        public int InputVoucherTypeId { get; set; }

        [JsonProperty("input_voucher_type_name")]
        public string InputVoucherTypeName { get; set; }
    }


    public class InputVoucherTypeModel : ISerializableObject
    {
        public InputVoucherTypeModel()
        {
            InputVoucherTypes = new List<InputVoucherType>();
        }

        /// <summary>
        /// Danh sách đơn hàng
        /// </summary>
        [JsonProperty("input_voucher_types")]
        public IList<InputVoucherType> InputVoucherTypes { get; set; }

        public string GetPrimaryPropertyName()
        {
            return "input_voucher_types";
        }

        public Type GetPrimaryPropertyType()
        {
            return typeof(InputVoucherType);
        }
    }

}