﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Core.Domain.Catalog;

namespace Nop.Plugin.Api.DTOs.StockQuantityHistorys
{
    [JsonObject(Title = "stock_quantity_history")]
    public class StockQuantityHistoryDto
    {
        [JsonProperty("quantity_adjustment")]
        public int QuantityAdjustment { get; set; }

        [JsonProperty("stock_quantity")]
        public int StockQuantity { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("created_on_utc")]
        public DateTime CreatedOnUtc { get; set; }

        [JsonProperty("product_id")]
        public int ProductId { get; set; }

        [JsonProperty("warehouse_id")]
        public int? WarehouseId { get; set; }

        [JsonProperty("product_name")]
        public string ProductName { get; set; }

        [JsonProperty("ware_house_name")]
        public string WarehouseName { get; set; }

        [JsonProperty("first_stock_quantity")]
        public int FirstStockQuantity { get; set; }

        [JsonProperty("last_stock_quantity")]
        public int LastStockQuantity { get; set; }

        [JsonProperty("input_quantity")]
        public int InputQuantity { get; set; }

        [JsonProperty("output_quantity")]
        public int OutputQuantity { get; set; }



        public static StockQuantityHistoryDto CopyData(StockQuantityHistory stockQuantityHistory)
        {
            StockQuantityHistoryDto stockQuantityHistoryCopy = new StockQuantityHistoryDto()
            {
                ProductId = stockQuantityHistory.ProductId,
                ProductName = stockQuantityHistory.Product.Name,

                WarehouseId = stockQuantityHistory.WarehouseId,

                StockQuantity = stockQuantityHistory.StockQuantity,

                QuantityAdjustment = stockQuantityHistory.QuantityAdjustment,

                Message = stockQuantityHistory.Message,

                CreatedOnUtc = stockQuantityHistory.CreatedOnUtc

            };
            return stockQuantityHistoryCopy;
        }
    }
    public class StockQuantityHistoryDtoRootObject : ISerializableObject
    {
        public StockQuantityHistoryDtoRootObject()
        {
            StockQuantityHistorys = new List<StockQuantityHistoryDto>();
        }

        [JsonProperty("stock_quantity_history")]
        public IList<StockQuantityHistoryDto> StockQuantityHistorys { get; set; }

        [JsonProperty("totalrow")]
        public int TotalRow { get; set; }

        public string GetPrimaryPropertyName()
        {
            return "stock_quantity_history";
        }

        public Type GetPrimaryPropertyType()
        {
            return typeof(StockQuantityHistoryDto);
        }
    }

    
}
