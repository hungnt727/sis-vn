﻿using Newtonsoft.Json;
using Nop.Core.Domain.Catalog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.DTOs.StockQuantityHistorys
{
    [JsonObject(Title = "stock_quantity_history")]
    public class StockQuantityHistoryDtoModel
    {
        [JsonProperty("quantity_adjustment")]
        public int QuantityAdjustment { get; set; }

        [JsonProperty("stock_quantity")]
        public int StockQuantity { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("created_on_utc")]
        public DateTime CreatedOnUtc { get; set; }

        [JsonProperty("product_id")]
        public int ProductId { get; set; }

        [JsonProperty("product_name")]
        public string ProductName { get; set; }


        [JsonProperty("ware_house_id")]
        public int? WarehouseId { get; set; }

        public static StockQuantityHistoryDtoModel CopyData(StockQuantityHistory stockQuantityHistory)
        {
            StockQuantityHistoryDtoModel stockQuantityHistoryCopy = new StockQuantityHistoryDtoModel()
            {
                ProductId = stockQuantityHistory.ProductId,
                ProductName = stockQuantityHistory.Product.Name,

                WarehouseId = stockQuantityHistory.WarehouseId,

                StockQuantity = stockQuantityHistory.StockQuantity,

                QuantityAdjustment = stockQuantityHistory.QuantityAdjustment,

                Message = stockQuantityHistory.Message,

                CreatedOnUtc = stockQuantityHistory.CreatedOnUtc

            };
            return stockQuantityHistoryCopy;
        }
    }
    public class StockQuantityHistoryDtoModelRootObject : ISerializableObject
    {
        public StockQuantityHistoryDtoModelRootObject()
        {
            StockQuantityHistorys = new List<StockQuantityHistoryDtoModel>();
        }

        [JsonProperty("stock_quantity_history")]
        public IList<StockQuantityHistoryDtoModel> StockQuantityHistorys { get; set; }

        [JsonProperty("totalrow")]
        public int TotalRow { get; set; }

        public string GetPrimaryPropertyName()
        {
            return "stock_quantity_history";
        }
        public Type GetPrimaryPropertyType()
        {
            return typeof(StockQuantityHistoryDto);
        }
    }
}
