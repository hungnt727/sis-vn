﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Nop.Plugin.Api.DTOs.Categories
{
    public class TaxRootObject : ISerializableObject
    {
        public TaxRootObject()
        {
            Categories = new List<TaxDto>();
        }

        [JsonProperty("categories")]
        public IList<TaxDto> Categories { get; set; }

        public string GetPrimaryPropertyName()
        {
            return "categories";
        }

        public Type GetPrimaryPropertyType()
        {
            return typeof(VendorDtoModel);
        }
    }
}