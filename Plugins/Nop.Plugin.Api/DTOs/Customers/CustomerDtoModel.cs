﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using Nop.Core.Domain.Customers;
using Nop.Plugin.Api.Models.Common;
using Nop.Services.Common;
using Nop.Services.Customers;

namespace Nop.Plugin.Api.DTOs.Customers
{
    [JsonObject(Title = "customer")]
    public class CustomerDtoModel : ISerializableObject
    {
        public CustomerDtoModel()
        {
            this.Roles = new List<int>();
        }

        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("customer_code")]
        public string CustomerCode { get; set; }

        [JsonProperty("roles")]
        public IList<int> Roles { get; set; }

        /// <summary>
        /// Gets or sets the email
        /// </summary>
        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("full_name")]
        public string FullName { get; set; }

        [JsonProperty("last_order_on")]
        public string LastOrderOn { get; set; }

        [JsonProperty("product_amount")]
        public DecimalModel ProductAmount { get; set; }

        [JsonProperty("paid_amount")]
        public DecimalModel PaidAmount { get; set; }

        [JsonProperty("debt_amount")]
        public DecimalModel DebtAmount { get; set; }

        [JsonProperty("birthday")]
        public DateTime? DateOfBirth { get; set; }

        [JsonProperty("gender")]
        public string Gender { get; set; }

        [JsonProperty("phone")]
        public string Phone { get; set; }

        [JsonProperty("address")]
        public string Address { get; set; }

        [JsonProperty("admin_comment")]
        public string AdminComment { get; set; }

        //*TODO* load customer orders
        [JsonProperty("orders")]
        public object Orders { get; set; }

        /// <summary>
        /// [Tuandang][13-12-2018][Hàm copy customer]
        /// [HienNguyen][30/07/2018][Hàm copy customer]
        /// </summary>
        /// <param name="customer"></param>
        /// <returns></returns>
        public static CustomerDtoModel CopyData(Customer customer)
        {
            CustomerDtoModel customerCopy = new CustomerDtoModel()
            {
                Id = customer.Id,
                AdminComment = customer.AdminComment,
                DateOfBirth = customer.GetAttribute<DateTime?>(SystemCustomerAttributeNames.DateOfBirth),
                Email = customer.Email,
                Gender = customer.GetAttribute<string>(SystemCustomerAttributeNames.Gender),
                Phone = customer.GetAttribute<string>(SystemCustomerAttributeNames.Phone),
                Address = customer.BillingAddress?.Address1,
                FullName = customer.GetFullName(),
                Roles = customer.GetCustomerRoleIds()
            };

            return customerCopy;
        }
        public static CustomerDtoModel CopyDataFromDto(CustomerDto customer)
        {
            CustomerDtoModel customerCopy = new CustomerDtoModel()
            {
                Id = customer.Id,
                AdminComment = customer.AdminComment,
                DateOfBirth = customer.DateOfBirth,
                Email = customer.Email,
                Gender = customer.Gender,
                Phone = customer.Phone,
            };

            return customerCopy;
        }

        public string GetPrimaryPropertyName()
        {
            return "customer";
        }

        public Type GetPrimaryPropertyType()
        {
            return typeof(CustomerDtoModel);
        }
    }

    public class CustomerTypeModel : ISerializableObject
    {
        public CustomerTypeModel()
        {
            CustomerType = new List<CustomerType>();
        }

        /// <summary>
        /// Danh sách đơn hàng
        /// </summary>
        [JsonProperty("customer_type")]
        public IList<CustomerType> CustomerType { get; set; }

        public string GetPrimaryPropertyName()
        {
            return "orderstatus";
        }

        public Type GetPrimaryPropertyType()
        {
            return typeof(CustomerType);
        }
    }

    public class CustomerType
    {
        [JsonProperty("customer_type_id")]
        public int CustomerTypeId { get; set; }

        [JsonProperty("customer_type_name")]
        public string CustomerTypeName { get; set; }
    }
}
