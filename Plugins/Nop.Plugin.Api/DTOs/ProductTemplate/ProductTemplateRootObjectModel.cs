﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Nop.Plugin.Api.DTOs.ProductTemplate;

namespace Nop.Plugin.Api.DTOs.ProductTemplate
{
    public class ProductTemplateRootObjectModel : ISerializableObject
    {
        public ProductTemplateRootObjectModel()
        {
            ProductTemplates = new List<ProductTemplateDtoModel>();
        }

        [JsonProperty("product_templates")]
        public IList<ProductTemplateDtoModel> ProductTemplates { get; set; }

        public string GetPrimaryPropertyName()
        {
            return "product_templates";
        }

        public Type GetPrimaryPropertyType()
        {
            return typeof(ProductTemplateDtoModel);
        }
    }
}