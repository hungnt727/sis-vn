﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.DTOs.ProductTemplate
{
    [JsonObject(Title = "product_template")]
    public class ProductTemplateDtoModel
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("view_path")]
        public string ViewPath { get; set; }

        [JsonProperty("display_order")]
        public int DisplayOrder { get; set; }

        [JsonProperty("ignored_product_types")]
        public string IgnoredProductTypes { get; set; }
    }
}
