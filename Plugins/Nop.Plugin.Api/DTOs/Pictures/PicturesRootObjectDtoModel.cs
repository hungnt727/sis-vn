﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Nop.Plugin.Api.DTOs.Pictures;

namespace Nop.Plugin.Api.DTOs.Pictures
{
    public class PicturesRootObjectDtoModel : ISerializableObject
    {
        public PicturesRootObjectDtoModel()
        {
            Pictures = new List<PictureDtoModel>();
        }

        [JsonProperty("pictures")]
        public IList<PictureDtoModel> Pictures { get; set; }

        [JsonProperty("total_row")]
        public int TotalRow { get; set; }


        public string GetPrimaryPropertyName()
        {
            return "pictures";
        }

        public Type GetPrimaryPropertyType()
        {
            return typeof(PictureDtoModel);
        }
    }
}