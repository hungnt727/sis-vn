﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Nop.Plugin.Api.DTOs.InputVoucherItems
{
    public class InputVoucherItemRootObject : ISerializableObject
    {
        public InputVoucherItemRootObject()
        {
            InputVoucherItems = new List<InputVoucherItemDtoModel>();
        }

        [JsonProperty("input_voucher_items")]
        public IList<InputVoucherItemDtoModel> InputVoucherItems { get; set; }

        /// <summary>
        /// [HienNguyen][31/07/2018]
        /// Tổng tiền hàng
        /// </summary>
        [JsonProperty("sub_total_amount")]
        public decimal SubTotalAmount { get; set; }

        /// <summary>
        /// [HienNguyen][31/07/2018]
        /// Tổng tiền KM
        /// </summary>
        [JsonProperty("total_discount")]
        public decimal TotalDiscount { get; set; }

        /// <summary>
        /// [HienNguyen][31/07/2018]
        /// Tổng tiền đả trừ km
        /// </summary>
        [JsonProperty("total_amount")]
        public decimal TotalAmount { get; set; }

        /// <summary>
        /// [HienNguyen][31/07/2018]
        /// Tổng tiền đã thanh toán
        /// </summary>
        [JsonProperty("total_amount_paid")]
        public decimal TotalAmountPaid { get; set; }

        [JsonProperty("total_row")]
        public int TotalRow { get; set; }

        public string GetPrimaryPropertyName()
        {
            return "input_voucher_items";
        }

        public Type GetPrimaryPropertyType()
        {
            return typeof(InputVoucherItemDtoModel);
        }
    }
}