﻿using System;
using System.Collections.Generic;
using FluentValidation.Attributes;
using Newtonsoft.Json;
using Nop.Core.Domain.Orders;
using Nop.Plugin.Api.Attributes;
using Nop.Plugin.Api.DTOs.Products;
using Nop.Plugin.Api.Validators;

namespace Nop.Plugin.Api.DTOs.InputVoucherItems
{
    [Validator(typeof(OrderItemDtoValidator))]
    [JsonObject(Title = "input_voucher_item")]
    public class InputVoucherItemDtoModel
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("input_voucher_id")]
        public int InputVoucherId { get; set; }

        [JsonProperty("sku")]
        public string SKU { get; set; }

        [JsonProperty("quantity")]
        public int Quantity { get; set; }

        /// <summary>
        /// Đơn giá sản phẩm
        /// </summary>
        [JsonProperty("unit_price")]
        public decimal UnitPrice { get; set; }

        /// <summary>
        /// Giá bán của sản phẩm
        /// </summary>
        [JsonProperty("SalePrice")]
        public decimal SalePrice { get; set; }

        /// <summary>
        /// Là giảm giá theo phần trăm
        /// </summary>
        [JsonProperty("is_discount_percent")]
        public bool IsDiscountPercent { get; set; }

        /// <summary>
        /// Số tiền giảm giá
        /// </summary>
        [JsonProperty("discount_amount")]
        public decimal? DiscountAmount { get; set; }

        [JsonProperty("product_id")]
        public int ProductId { get; set; }

        [JsonProperty("product_name")]
        public string ProductName { get; set; }

        /// <summary>
        /// [HienNguyen][30/07/2018][Hàm copy data]
        /// </summary>
        /// <param name="orderItem"></param>
        /// <returns></returns>
        public static InputVoucherItemDtoModel CopyData(OrderItem orderItem)
        {
            InputVoucherItemDtoModel orderItemCopy = new InputVoucherItemDtoModel()
            {
                Id = orderItem.Id.ToString(),
                //OrderId = orderItem.OrderId,
                SalePrice = orderItem.PriceInclTax,
                UnitPrice = orderItem.UnitPriceInclTax,
                ProductId = orderItem.ProductId,
                ProductName = orderItem.Product.Name,
                Quantity = orderItem.Quantity,
                SKU = orderItem.Product.Sku,
                DiscountAmount = orderItem.DiscountAmountInclTax
            };
            return orderItemCopy;
        }
    }
}