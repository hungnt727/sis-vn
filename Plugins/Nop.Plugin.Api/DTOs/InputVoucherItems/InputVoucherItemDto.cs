﻿using System;
using System.Collections.Generic;
using FluentValidation.Attributes;
using Newtonsoft.Json;
using Nop.Core.Domain.Orders;
using Nop.Plugin.Api.Attributes;
using Nop.Plugin.Api.DTOs.Products;
using Nop.Plugin.Api.Validators;

namespace Nop.Plugin.Api.DTOs.InputVoucherItems
{
    [Validator(typeof(OrderItemDtoValidator))]
    [JsonObject(Title = "input_voucher_item")]
    public class InputVoucherItemDto
    {
        /// <summary>
        /// Gets or sets the id
        /// </summary>
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("input_voucher_id")]
        public int InputVoucherId { get; set; }


        /// <summary>
        /// Gets or sets the quantity
        /// </summary>
        [JsonProperty("quantity")]
        public int Quantity { get; set; }

        /// <summary>
        /// Số tiền trên 1 sản phẩm chưa tính thuế.
        /// </summary>
        [JsonProperty("unit_price_excl_tax")]
        public decimal UnitPriceExclTax { get; set; }

        /// <summary>
        /// Tổng tiền trên 1 sản phẩm chưa tính thuế.
        /// </summary>
        [JsonProperty("price_excl_tax")]
        public decimal PriceExclTax { get; set; }

        /// <summary>
        /// Số tiền trên 1 sản phẩm đã tính thuế.
        /// </summary>
        [JsonProperty("unit_price_incl_tax")]
        public decimal UnitPriceInclTax { get; set; }


        /// <summary>
        /// Tổng tiền trên 1 sản phẩm đã tính thuế.
        /// </summary>
        [JsonProperty("price_incl_tax")]
        public decimal PriceInclTax { get; set; }

        /// <summary>
        /// Gets the product name
        /// </summary>
        [JsonProperty("product_name")]
        public string ProductName { get; set; }

        /// <summary>
        /// Gets the product id
        /// </summary>
        [JsonProperty("product_id")]
        public int ProductId { get; set; }

        public static InputVoucherItemDto CopyData(OrderItem _orderItem)
        {
            InputVoucherItemDto objReturn = new InputVoucherItemDto();
            objReturn.Id = _orderItem.Id;
            objReturn.Quantity = _orderItem.Quantity;
            objReturn.UnitPriceExclTax = _orderItem.UnitPriceExclTax;
            objReturn.UnitPriceInclTax = _orderItem.UnitPriceInclTax;
            objReturn.PriceExclTax = _orderItem.PriceExclTax;
            objReturn.PriceInclTax = _orderItem.PriceInclTax;

            if (_orderItem.Product != null )
            {
                objReturn.ProductId  = _orderItem.Product.Id;
                objReturn.ProductName = _orderItem.Product.Name;
            }
            return objReturn;
        }


    }
}