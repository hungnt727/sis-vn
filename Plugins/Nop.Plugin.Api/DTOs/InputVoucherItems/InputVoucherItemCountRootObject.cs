﻿using Newtonsoft.Json;

namespace Nop.Plugin.Api.DTOs.InputVoucherItems
{
    public class InputVoucherItemCountRootObject
    {
        [JsonProperty("count")]
        public int Count { get; set; }
    }
}