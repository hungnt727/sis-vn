﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Nop.Core.Domain.Discounts;
using Nop.Plugin.Api.DTOs.Categories;
using Nop.Plugin.Api.DTOs.Products;

namespace Nop.Plugin.Api.DTOs.Discounts
{
    [JsonObject(Title = "discount")]
    public class DiscountDto 
    {
        private string _discountlimitation { get; set; }
        private string _discountlimitationid { get; set; }


        /// <summary>
        /// Gets or sets the entity identifier
        /// </summary>
        [JsonProperty("id")]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the name
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the discount type identifier
        /// </summary>
        [JsonProperty("discount_typeid")]
        public int DiscountTypeId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to use percentage
        /// </summary>
        [JsonProperty("use_percentage")]
        public bool UsePercentage { get; set; }

        /// <summary>
        /// Gets or sets the discount percentage
        /// </summary>
        [JsonProperty("discount_percentage")]
        public decimal DiscountPercentage { get; set; }

        /// <summary>
        /// Gets or sets the discount amount
        /// </summary>
        [JsonProperty("discount_amount")]
        public decimal DiscountAmount { get; set; }

        /// <summary>
        /// Gets or sets the maximum discount amount (used with "UsePercentage")
        /// </summary>
        [JsonProperty("maximum_discount_amount")]
        public decimal? MaximumDiscountAmount { get; set; }

        /// <summary>
        /// Gets or sets the discount start date and time
        /// </summary>
        [JsonProperty("start_date_utc")]
        public DateTime? StartDateUtc { get; set; }

        /// <summary>
        /// Gets or sets the discount end date and time
        /// </summary>
        [JsonProperty("end_date_utc")]
        public DateTime? EndDateUtc { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether discount requires coupon code
        /// </summary>
        [JsonProperty("requires_coupon_code")]
        public bool RequiresCouponCode { get; set; }

        /// <summary>
        /// Gets or sets the coupon code
        /// </summary>
        [JsonProperty("coupon_code")]
        public string CouponCode { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether discount can be used simultaneously with other discounts (with the same discount type)
        /// </summary>
        [JsonProperty("is_cumulative")]
        public bool IsCumulative { get; set; }

        /// <summary>
        /// Gets or sets the discount limitation identifier
        /// </summary>
        [JsonProperty("discount_limitation_id")]
        public int DiscountLimitationId { get; set; }

        /// <summary>
        /// Gets or sets the discount limitation times (used when Limitation is set to "N Times Only" or "N Times Per Customer")
        /// </summary>
        [JsonProperty("limitation_times")]
        public int LimitationTimes { get; set; }

        /// <summary>
        /// Gets or sets the maximum product quantity which could be discounted
        /// Used with "Assigned to products" or "Assigned to categories" type
        /// </summary>
        [JsonProperty("maximum_discounted_quantity")]
        public int? MaximumDiscountedQuantity { get; set; }

        [JsonProperty("selected_productids")]
        public List<int> SelectedProductIds { get; set; }


        /// <summary>
        /// Gets or sets the discount limitation
        /// </summary>
        [JsonProperty("discount_limitation")]
        public string DiscountLimitation
        {
            get
            {
                return this._discountlimitation;
            }
            set
            {
                DiscountLimitationType discountLimitationId;
                if (Enum.TryParse(value, out discountLimitationId))
                {
                    this.DiscountLimitationId = (int)discountLimitationId;
                }
                else
                {
                    this.DiscountLimitationId = (int)DiscountLimitationType.Unlimited;
                }
                _discountlimitation = value;
            }
        }

        
        [JsonProperty("category_ids")]
        public virtual ICollection<int> AppliedToCategoryIds { get; set; }

     
        [JsonProperty("product_ids")]
        public virtual ICollection<int> AppliedToProductIds { get; set; }

    }


    public class DiscountTypeModel : ISerializableObject
    {
        public DiscountTypeModel()
        {
            DiscountType = new List<DiscountType>();
        }

        /// <summary>
        /// Danh sách đơn hàng
        /// </summary>
        [JsonProperty("customer_type")]
        public IList<DiscountType> DiscountType { get; set; }

        public string GetPrimaryPropertyName()
        {
            return "discounttype";
        }

        public Type GetPrimaryPropertyType()
        {
            return typeof(DiscountType);
        }
    }

    public class DiscountLimitationTypeRootModel : ISerializableObject
    {
        public DiscountLimitationTypeRootModel()
        {
            DiscountLimitationTypes = new List<DiscountLimitationTypeModel>();
        }

        /// <summary>
        /// Danh sách đơn hàng
        /// </summary>
        [JsonProperty("discount_limitation_types")]
        public IList<DiscountLimitationTypeModel> DiscountLimitationTypes { get; set; }

        public string GetPrimaryPropertyName()
        {
            return "discounttype";
        }

        public Type GetPrimaryPropertyType()
        {
            return typeof(DiscountType);
        }
    }


    public class DiscountLimitationTypeModel
    {
        [JsonProperty("discount_limitation_type_id")]
        public int DiscountLimitationTypeId { get; set; }

        [JsonProperty("discount_limitation_type_name")]
        public string DiscountLimitationTypeName { get; set; }
    }

    public class DiscountType
    {
        [JsonProperty("discount_type_id")]
        public int DiscountTypeId { get; set; }

        [JsonProperty("discount_type_name")]
        public string DiscountTypeName { get; set; }
    }

    public class AppliedToCategoryModel 
    {
        [JsonProperty("category_id")]
        public int CategoryId { get; set; }

        [JsonProperty("category_name")]
        public string CategoryName { get; set; }
    }

    public  class AppliedToProductModel 
    {
        [JsonProperty("product_id")]
        public int ProductId { get; set; }

        [JsonProperty("product_name")]
        public string ProductName { get; set; }
    }

}