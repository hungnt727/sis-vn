﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.DTOs.Discounts
{
    [JsonObject(Title = "applied_to_categories")]
    public class AddDiscountAppliedToCategoriesDto
    {
        [JsonProperty("discount_id")]
        public int DiscountId { get; set; }

        [JsonProperty("category_ids")]
        public ICollection<int> AppliedToCategorieIds { get; set; }

        [JsonProperty("is_delete_old_category_applied")]
        public bool IsDeleteOldCategoryApplied { get; set; }

    }
}
