﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.DTOs.Discounts
{
    [JsonObject(Title = "applied_to_categories")]
    public class AddDiscountAppliedToProductsDto
    {
        [JsonProperty("discount_id")]
        public int DiscountId { get; set; }

        [JsonProperty("product_ids")]
        public ICollection<int> AppliedToProductIds { get; set; }

        [JsonProperty("is_delete_old_product_applied")]
        public bool IsDeleteOldProductApplied { get; set; }

    }
}
