﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Nop.Plugin.Api.DTOs.Discounts
{
    public class DiscountsRootObject : ISerializableObject
    {
        public DiscountsRootObject()
        {
            Discounts = new List<DiscountDtoModel>();    
        }
        
        [JsonProperty("discounts")]
        public IList<DiscountDtoModel> Discounts { get; set; }

        [JsonProperty("totalrow")]
        public int TotalRow { get; set; }

        public string GetPrimaryPropertyName()
        {
            return "discounts";
        }

        public Type GetPrimaryPropertyType()
        {
            return typeof (DiscountDto);
        }
    }

}