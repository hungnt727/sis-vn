﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Nop.Core.Domain.Discounts;

namespace Nop.Plugin.Api.DTOs.Discounts
{

    [JsonObject(Title = "DiscountRequirement")]

    public partial class DiscountRequirementDto
    {
        private ICollection<DiscountRequirementDto> _childRequirements;

        /// <summary>
        /// Gets or sets the discount identifier
        /// </summary>
        [JsonProperty("discountid")]
        public int DiscountId { get; set; }

        /// <summary>
        /// Gets or sets the discount requirement rule system name
        /// </summary>
        [JsonProperty("discount_requirement_rule_system_name")]
        public string DiscountRequirementRuleSystemName { get; set; }

        /// <summary>
        /// Gets or sets the parent requirement identifier
        /// </summary>
        [JsonProperty("parent_id")]
        public int? ParentId { get; set; }

        /// <summary>
        /// Gets or sets an interaction type identifier (has a value for the group and null for the child requirements)
        /// </summary>
        [JsonProperty("interaction_typeid")]
        public int? InteractionTypeId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this requirement has any child requirements
        /// </summary>
        [JsonProperty("is_group")]
        public bool IsGroup { get; set; }

        /// <summary>
        /// Gets or sets an interaction type
        /// </summary>
        [JsonProperty("interaction_type")]
        public RequirementGroupInteractionType? InteractionType
        {
            get { return (RequirementGroupInteractionType?)InteractionTypeId; }
            set { InteractionTypeId = (int?)value; }
        }

        /// <summary>
        /// Gets or sets the discount
        /// </summary>
        [JsonProperty("discount")]
        public virtual DiscountDto Discount { get; set; }

        /// <summary>
        /// Gets or sets the child discount requirements
        /// </summary>
        [JsonProperty("totalrow")]
        public virtual ICollection<DiscountRequirementDto> ChildRequirements
        {
            get { return _childRequirements ?? (_childRequirements = new List<DiscountRequirementDto>()); }
            protected set { _childRequirements = value; }
        }
    }
}