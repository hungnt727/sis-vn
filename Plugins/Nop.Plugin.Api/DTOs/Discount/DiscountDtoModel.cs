﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using Nop.Core.Domain.Discounts;
using Nop.Plugin.Api.DTOs.Categories;
using Nop.Plugin.Api.DTOs.Products;

namespace Nop.Plugin.Api.DTOs.Discounts
{
    public class DiscountDtoModel: DiscountDto 
    {
        [JsonProperty("discount_type")]
        public string DiscountType
        {
            get
            {
                try
                {
                    return Nop.Core.CommonHelper.GetEnumDescription((Nop.Core.Domain.Discounts.DiscountType)DiscountTypeId);
                }
                catch (Exception)
                {
                    return null;
                }
                
            }
        }
        [JsonProperty("primary_store_currency_code")]
        public string PrimaryStoreCurrencyCode { get; set; }
        public static DiscountDtoModel CopyData(Discount discount)
        {
            DiscountDtoModel discountCopy = new DiscountDtoModel()
            {
                Id = discount.Id,
                Name = discount.Name,
                UsePercentage = discount.UsePercentage,
                DiscountPercentage = discount.DiscountPercentage,
                DiscountAmount = discount.DiscountAmount,
                MaximumDiscountAmount = discount.MaximumDiscountAmount,
                StartDateUtc = discount.StartDateUtc,
                EndDateUtc = discount.EndDateUtc,
                RequiresCouponCode = discount.RequiresCouponCode,
                CouponCode = discount.CouponCode,
                IsCumulative = discount.IsCumulative,
                DiscountLimitationId = discount.DiscountLimitationId,
                DiscountTypeId = discount.DiscountTypeId,
                MaximumDiscountedQuantity = discount.MaximumDiscountedQuantity,
                AppliedToProductIds = discount.AppliedToProducts.Select(s => s.Id).ToList(),
                AppliedToCategoryIds = discount.AppliedToCategories.Select(s => s.Id).ToList(),

            };
            return discountCopy;
        }
    }
}