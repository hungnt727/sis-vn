﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.DTOs.Products
{
    /// <summary>
    /// [HienNguyen][07/08/2018]
    /// </summary>
    /// 
    [JsonObject(Title = "product_picture")]
    public class ProductPictureDtoModel : ISerializableObject
    {
        public ProductPictureDtoModel()
        {

        }

        [JsonProperty("product_id")]
        public int ProductId { get; set; }

        [JsonProperty("picture_id")]
        public int PictureId { get; set; }

        [JsonProperty("display_order")]
        public int DisplayOrder { get; set; }

        [JsonProperty("picture_binary")]
        public byte[] PictureBinary { get; set; }

        [JsonProperty("picture_url")]
        public string PictureUrl { get; set; }

        public string GetPrimaryPropertyName()
        {
            return "product_picture";
        }

        public Type GetPrimaryPropertyType()
        {
            return typeof(ProductPictureDtoModel);
        }
    }
}
