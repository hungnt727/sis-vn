﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Nop.Plugin.Api.DTOs.Products
{
    public class ProductsRootObjectDtoModel : ISerializableObject
    {
        public ProductsRootObjectDtoModel()
        {
            Products = new List<ProductDtoModel>();
        }

        [JsonProperty("products")]
        public IList<ProductDtoModel> Products { get; set; }

        [JsonProperty("total_row")]
        public int TotalRow { get; set; }
        [JsonProperty("page_index")]
        public int PageIndex { get; set; }
        [JsonProperty("page_size")]
        public int PageSize { get; set; }
        [JsonProperty("total_page")]
        public int TotalPage { get; set; }


        public string GetPrimaryPropertyName()
        {
            return "products";
        }

        public Type GetPrimaryPropertyType()
        {
            return typeof(ProductDtoModel);
        }
    }
}