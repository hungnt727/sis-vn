﻿using System;
using System.Collections.Generic;
using FluentValidation.Attributes;
using Newtonsoft.Json;
using Nop.Core.Domain.Catalog;
using Nop.Plugin.Api.Attributes;
using Nop.Plugin.Api.DTOs.Images;
using Nop.Plugin.Api.DTOs.Languages;
using Nop.Plugin.Api.Validators;

namespace Nop.Plugin.Api.DTOs.Products
{
    /// <summary>
    /// 
    /// </summary>
    [JsonObject(Title = "product")]
    public class ProductDtoModel
    {
        [JsonProperty("id")]
        public int ProductId { get; set; }

        [JsonProperty("published")]
        public bool Published { get; set; }

        [JsonProperty("name")]
        public string ProductName { get; set; }

        [JsonProperty("sku")]
        public string Sku { get; set; }

        [JsonProperty("category_id")]
        public int CategoryId { get; set; }

        [JsonProperty("category_name")]
        public string CategoryName { get; set; }

        [JsonProperty("vendor_id")]
        public int VendorId { get; set; }

        [JsonProperty("vendor_name")]
        public string VendorName { get; set; }

        [JsonProperty("stock_quantity")]
        public int? StockQuantity { get; set; }

        [JsonProperty("sale_price")]
        public decimal SalePrice { get; set; }

        [JsonProperty("sale_price_text")]
        public string SalePriceText { get; set; }

        [JsonProperty("whole_sale_price")]
        public decimal WholeSalePrice { get; set; }

        [JsonProperty("vip_sale_price")]
        public decimal VipSalePrice { get; set; }
        
        [JsonProperty("images")]
        public IList<ImageMappingDto> Images { get; set; }
    }
}