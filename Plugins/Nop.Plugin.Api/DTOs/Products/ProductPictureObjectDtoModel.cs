﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Nop.Plugin.Api.DTOs.Products
{
    public class ProductPictureObjectDtoModel : ISerializableObject
    {
        public ProductPictureObjectDtoModel()
        {
            ProductPicture = new List<ProductPictureDtoModel>();
        }

        [JsonProperty("product_picture")]
        public IList<ProductPictureDtoModel> ProductPicture { get; set; }

        public string GetPrimaryPropertyName()
        {
            return "product_picture";
        }

        public Type GetPrimaryPropertyType()
        {
            return typeof(ProductPictureDtoModel);
        }
    }
}