﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Nop.Plugin.Api.DTOs.WareHouses
{
    public class WareHousesRootObject : ISerializableObject
    {
        public WareHousesRootObject()
        {
            WareHouses = new List<WareHouseDto>();
        }

        [JsonProperty("ware_houses")]
        public IList<WareHouseDto> WareHouses { get; set; }

        [JsonProperty("totalrow")]
        public int TotalRow { get; set; }

        public string GetPrimaryPropertyName()
        {
            return "ware_houses";
        }

        public Type GetPrimaryPropertyType()
        {
            return typeof(WareHouseDto);
        }
    }
}
