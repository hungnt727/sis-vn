﻿using System;
using System.Collections.Generic;
using FluentValidation.Attributes;
using Newtonsoft.Json;
using Nop.Plugin.Api.DTOs.Images;
using Nop.Plugin.Api.DTOs.Languages;
using Nop.Plugin.Api.Validators;

namespace Nop.Plugin.Api.DTOs.WareHouses
{
    [JsonObject(Title = "ware_house")]
    public class WareHouseDto
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the warehouse name
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the admin comment
        /// </summary>
        [JsonProperty("note")]
        public string Note { get; set; }
    }

}