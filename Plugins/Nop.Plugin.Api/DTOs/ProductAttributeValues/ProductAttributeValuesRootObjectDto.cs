﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Nop.Plugin.Api.DTOs.ProductAttributeValue
{
    public class ProductAttributeValuesRootObjectDto : ISerializableObject
    {
        public ProductAttributeValuesRootObjectDto()
        {
            ProductAttributeValues = new List<ProductAttributeValueDtoModel>();
        }

        [JsonProperty("product_attribute_values")]
        public IList<ProductAttributeValueDtoModel> ProductAttributeValues { get; set; }

        public string GetPrimaryPropertyName()
        {
            return "product_attribute_values";
        }

        public Type GetPrimaryPropertyType()
        {
            return typeof(ProductAttributeValueDtoModel);
        }
    }
}