﻿using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Localization;
using Nop.Core.Domain.Media;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.SISVN_ReceiptPayment;
using Nop.Core.Domain.Stores;
using Nop.Core.Domain.Vendors;
using Nop.Plugin.Api.DTOs.Categories;
using Nop.Plugin.Api.DTOs.Customers;
using Nop.Plugin.Api.DTOs.InputVoucherItems;
using Nop.Plugin.Api.DTOs.InputVouchers;
using Nop.Plugin.Api.DTOs.Languages;
using Nop.Plugin.Api.DTOs.OrderItems;
using Nop.Plugin.Api.DTOs.Orders;
using Nop.Plugin.Api.DTOs.Pictures;
using Nop.Plugin.Api.DTOs.ProductAttributes;
using Nop.Plugin.Api.DTOs.Products;
using Nop.Plugin.Api.DTOs.ProductTemplate;
using Nop.Plugin.Api.DTOs.ShoppingCarts;
using Nop.Plugin.Api.DTOs.SISVN_ReceiptPayment;
using Nop.Plugin.Api.DTOs.Stores;
using System.Collections.Generic;

namespace Nop.Plugin.Api.Helpers
{
    public interface IDTOHelper
    {
        #region Custom Method
        /// <summary>
        /// [TuanDang][13-12-2018]
        /// [HienNguyen][28/07/2018][Hàm Prepare customer]
        /// </summary>
        /// <param name="customer"></param>
        /// <returns></returns>
        CustomerDtoModel PrepareCustomerDTOModel(Customer customer, bool details = false);

        /// <summary>
        /// [HienNguyen][28/07/2018][Hàm Prepare customer]
        /// </summary>
        /// <param name="customer"></param>
        /// <returns></returns>
        CustomerDto PrepareCustomerDTO(Customer customer);

        /// <summary>
        /// [HienNguyen][04/08/2018][Hàm convert model entity]
        /// </summary>
        /// <param name="orderModel"></param>
        /// <returns></returns>
        Order PrepareSaleOrderModelEntity(SaleOrderDtoModel orderModel);

        /// <summary>
        /// [HienNguyen][04/08/2018]
        /// </summary>
        /// <param name="orderPaymentModel"></param>
        /// <returns></returns>
        SISVN_OrderPaymentBalance PrepareOrderPaymentBalanceEntity(OrderPaymentDto orderPaymentModel);

        /// <summary>
        /// [HienNguyen][04/08/2018]
        /// </summary>
        /// <param name="orderItem"></param>
        /// <returns></returns>
        OrderItem PrepareOrderItemEntity(OrderItemDtoModel orderItem);

        /// <summary>
        /// [HienNguyen][04/08/2018]
        /// </summary>
        /// <param name="orderItem"></param>
        /// <returns></returns>
        List<OrderItem> PrepareOrderItemEntity(List<OrderItemDtoModel> orderItems);

        /// <summary>
        /// [HienNguyen][04/08/2018]
        /// </summary>
        /// <param name="orderNote"></param>
        /// <returns></returns>
        OrderNote PrepareOrderNoteEntity(OrderNoteDtoModel orderNote);

        /// <summary>
        /// [HienNguyen][04/08/2018]
        /// </summary>
        /// <param name="orderNote"></param>
        /// <returns></returns>
        List<OrderNote> PrepareOrderNoteEntity(List<OrderNoteDtoModel> orderNotes);

        /// <summary>
        /// [HienNguyen][08/08/2018]
        /// </summary>
        /// <param name="product"></param>
        /// <returns></returns>
        ProductDtoModel PrepareProductDTOModel(Product product, bool prepareImage = true, int imageSize = 0);

        /// <summary>
        /// [HienNguyen][08/08/2018]
        /// </summary>
        /// <param name="picture"></param>
        /// <returns></returns>
        ProductPictureDtoModel PrepareProductPictureDTOModel(ProductPicture picture);

        /// <summary>
        /// [HienNguyen][08/08/2018]
        /// </summary>
        /// <param name="category"></param>
        /// <returns></returns>
        CategoryDtoModel PrepareCategoryDTOModel(Category category);

        /// <summary>
        /// [HienNguyen][08/08/2018]
        /// </summary>
        /// <param name="vendor"></param>
        /// <returns></returns>
        VendorDtoModel PrepareVendorDTOModel(Vendor vendor);

        /// <summary>
        /// [HienNguyen][15/08/2018]
        /// </summary>
        /// <param name="productTemplate"></param>
        /// <returns></returns>
        ProductTemplateDtoModel PrepareProductTemplateDTOModel(ProductTemplate productTemplate);

        /// <summary>
        /// [HienNguyen][21/08/2018]
        /// </summary>
        /// <param name="picture"></param>
        /// <returns></returns>
        PictureDtoModel PreparePictureDTOModel(Picture picture);

        /// <summary>
        /// [HienNguyen][]
        /// </summary>
        /// <param name="receiptPayment"></param>
        /// <returns></returns>
        SISVN_ReceiptPaymentDto PrepareSISVN_ReceiptPaymentDto(SISVN_ReceiptPayment receiptPayment);

        /// <summary>
        /// [HienNguyen][]
        /// </summary>
        /// <param name="receiptPayment"></param>
        /// <returns></returns>
        SISVN_ReceiptPaymentTypeDto PrepareSISVN_ReceiptPaymentTypeDto(SISVN_ReceiptPaymentType receiptPaymentType);

        /// <summary>
        /// [HienNguyen][]
        /// </summary>
        /// <param name="receiptPayment"></param>
        /// <returns></returns>
        SISVN_ExpenseTypeDto PrepareSISVN_ExpenseTypeDto(SISVN_ExpenseType expenseType);

        #endregion


        ProductDto PrepareProductDTO(Product product);
        CategoryDto PrepareCategoryDTO(Category category);
        OrderDto PrepareOrderDTO(Order order);

        ShoppingCartItemDto PrepareShoppingCartItemDTO(ShoppingCartItem shoppingCartItem);
        OrderItemDto PrepareOrderItemDTO(OrderItem orderItem);
        StoreDto PrepareStoreDTO(Store store);
        LanguageDto PrepateLanguageDto(Language language);
        ProductAttributeDto PrepareProductAttributeDTO(ProductAttribute productAttribute);

        InputVoucherDto PrepareInputVoucherDTO(Order order);
        Order PrepareInputVoucher(InputVoucherDto inputVoucherDto);
        List<OrderItem> PrepareListInputVoucherItem(List<InputVoucherItemDto> listInputVoucherItems);
        OrderItem PrepareInputVoucherItem(InputVoucherItemDto orderItem);

        List<OrderNote> PrepareListInputVoucherNote(List<InputVoucherNoteDto> listInputVoucherNoteDto);

        OrderNote PrepareInputVoucherItem(InputVoucherNoteDto inputVoucherNoteDto);


    }
}
