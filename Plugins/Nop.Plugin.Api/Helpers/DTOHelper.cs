﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Directory;
using Nop.Core.Domain.Localization;
using Nop.Plugin.Api.DTOs.Products;
using Nop.Services.Catalog;
using Nop.Services.Seo;
using Nop.Services.Security;
using Nop.Services.Stores;
using Nop.Plugin.Api.DTOs.Images;
using Nop.Core.Domain.Media;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Stores;
using Nop.Plugin.Api.MappingExtensions;
using Nop.Plugin.Api.DTOs.Categories;
using Nop.Plugin.Api.DTOs.Customers;
using Nop.Plugin.Api.DTOs.Languages;
using Nop.Plugin.Api.DTOs.Orders;
using Nop.Plugin.Api.Services;
using Nop.Services.Media;
using Nop.Plugin.Api.DTOs.ShoppingCarts;
using Nop.Plugin.Api.DTOs.Stores;
using Nop.Services.Directory;
using Nop.Services.Localization;
using Nop.Plugin.Api.DTOs.ProductAttributes;
using Nop.Plugin.Api.DTOs.OrderItems;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Vendors;
using Nop.Plugin.Api.DTOs.ProductTemplate;
using Nop.Plugin.Api.AutoMapper;
using Nop.Plugin.Api.DTOs.Pictures;
using Nop.Plugin.Api.DTOs.InputVouchers;
using Nop.Plugin.Api.DTOs.InputVoucherItems;
using Nop.Plugin.Api.DTOs.SISVN_ReceiptPayment;
using Nop.Core.Domain.SISVN_ReceiptPayment;
using Nop.Services.Vendors;
using Nop.Services.Customers;
using Nop.Services.Orders;
using OrderStatus = Nop.Core.Domain.Orders.OrderStatus;
using Nop.Services.Helpers;
using Nop.Plugin.Api.DTOs;
using Nop.Plugin.Api.Models.Common;

namespace Nop.Plugin.Api.Helpers
{
    public class DTOHelper : IDTOHelper
    {
        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly IPriceFormatter _priceFormatter;
        private readonly IWorkContext _workContext;
        private readonly IUrlRecordService _urlRecordService;
        private readonly ICategoryService _categoryService;
        private readonly ICustomerService _customerService;
        private readonly IOrderService _orderService;
        private readonly IStoreContext _storeContext;
        private readonly IVendorService _vendorService;
        private readonly IProductService _productService;
        private readonly IAclService _aclService;
        private readonly IStoreMappingService _storeMappingService;
        private readonly IPictureService _pictureService;
        private readonly IProductAttributeService _productAttributeService;
        private readonly ILanguageService _languageService;
        private readonly ICurrencyService _currencyService;
        private readonly CurrencySettings _currencySettings;
        private readonly IStoreService _storeService;
        private readonly ICustomerApiService _customerApiService;
        private readonly IProductAttributeConverter _productAttributeConverter;

        public DTOHelper(
            IDateTimeHelper dateTimeHelper,
            IOrderService orderService,
            ICustomerService customerService,
            IPriceFormatter _priceFormatter,
            IWorkContext workContext,
            IUrlRecordService urlRecordService,
            ICategoryService categoryService,
            IVendorService vendorService,
            IStoreContext storeContext,
            IProductService productService,
            IAclService aclService,
            IStoreMappingService storeMappingService,
            IPictureService pictureService,
            IProductAttributeService productAttributeService,
            ICustomerApiService customerApiService,
            IProductAttributeConverter productAttributeConverter,
            ILanguageService languageService,
            ICurrencyService currencyService,
            CurrencySettings currencySettings,
            IStoreService storeService)
        {
            this._dateTimeHelper = dateTimeHelper;
            this._priceFormatter = _priceFormatter;
            _customerService = customerService;
            _orderService = orderService;
            _workContext = workContext;
            _urlRecordService = urlRecordService;
            _categoryService = categoryService;
            _vendorService = vendorService;
            _productService = productService;
            _aclService = aclService;
            _storeMappingService = storeMappingService;
            _pictureService = pictureService;
            _productAttributeService = productAttributeService;
            _customerApiService = customerApiService;
            _productAttributeConverter = productAttributeConverter;
            _languageService = languageService;
            _currencyService = currencyService;
            _currencySettings = currencySettings;
            _storeService = storeService;
            _storeContext = storeContext;
        }

        #region Custom Method

        /// <summary>
        /// [HienNguyen][15/08/2018]
        /// </summary>
        /// <param name="productTemplate"></param>
        /// <returns></returns>
        public PictureDtoModel PreparePictureDTOModel(Picture picture)
        {
            return picture.MapTo<Picture, PictureDtoModel>();
        }

        /// <summary>
        /// [HienNguyen]
        /// </summary>
        /// <param name="receiptPayment"></param>
        /// <returns></returns>
        public SISVN_ReceiptPaymentDto PrepareSISVN_ReceiptPaymentDto(SISVN_ReceiptPayment receiptPayment)
        {
            return new SISVN_ReceiptPaymentDto()
            {
                Amount = receiptPayment.Amount,
                CreateUserId = receiptPayment.CustomerId,
                CustomerId = receiptPayment.CustomerId,
                Descriptions = receiptPayment.Descriptions,
                ExpenseTypeId = receiptPayment.ExpenseTypeId,
                PaymentMethodId = receiptPayment.PaymentMethodId,
                ReceiptPaymentCode = receiptPayment.ReceiptPaymentCode,
                ReceiptPaymentTypeId = receiptPayment.ReceiptPaymentTypeId,
                StoreId = receiptPayment.StoreId,
                TransactionDate = DateTime.Now,
                DeleteDate = receiptPayment.DeleteDate,
                DeleteUserId = receiptPayment.DeleteUserId,
                IsDelete = receiptPayment.IsDelete,
                Id = receiptPayment.Id
            };
        }

        /// <summary>
        /// [HienNguyen][]
        /// </summary>
        /// <param name="receiptPayment"></param>
        /// <returns></returns>
        public SISVN_ReceiptPaymentTypeDto PrepareSISVN_ReceiptPaymentTypeDto(SISVN_ReceiptPaymentType receiptPaymentType)
        {
            return new SISVN_ReceiptPaymentTypeDto()
            {
                Id = receiptPaymentType.Id,
                Descriptions = receiptPaymentType.Descriptions,
                Name = receiptPaymentType.Name
            };
        }

        /// <summary>
        /// [HienNguyen][]
        /// </summary>
        /// <param name="receiptPayment"></param>
        /// <returns></returns>
        public SISVN_ExpenseTypeDto PrepareSISVN_ExpenseTypeDto(SISVN_ExpenseType expenseType)
        {
            return new SISVN_ExpenseTypeDto()
            {
                Descriptions = expenseType.Descriptions,
                Name = expenseType.Name,
                Id = expenseType.Id,
                ReceiptPaymentTypeId = expenseType.ReceiptPaymentTypeId,
                SISVN_ReceiptPaymentType = this.PrepareSISVN_ReceiptPaymentTypeDto(expenseType.SISVN_ReceiptPaymentType)
            };
        }

        /// <summary>
        /// [HienNguyen][15/08/2018]
        /// </summary>
        /// <param name="productTemplate"></param>
        /// <returns></returns>
        public ProductTemplateDtoModel PrepareProductTemplateDTOModel(ProductTemplate productTemplate)
        {
            return productTemplate.MapTo<ProductTemplate, ProductTemplateDtoModel>();
        }

        /// <summary>
        /// [HienNguyen][28/07/2018][Hàm Prepare customer]
        /// </summary>
        /// <param name="customer"></param>
        /// <returns></returns>
        public CustomerDto PrepareCustomerDTO(Customer customer)
        {
            var dto = customer.ToDto();
            return dto;
        }

        /// <summary>
        /// [TuanDang][13-12-2018][Prepare customer model]
        /// </summary>
        /// <param name="customer"></param>
        /// <returns></returns>
        public CustomerDtoModel PrepareCustomerDTOModel(Customer customer, bool details = false)
        {
            var dto = CustomerDtoModel.CopyData(customer);
            var orders = _orderService.SearchOrders(customerId: customer.Id, osIds: new List<int> { (int)OrderStatus.Complete, (int)OrderStatus.Processing, (int)OrderStatus.Shipping, (int)OrderStatus.Temporary, (int)OrderStatus.Pending });
            if (orders.Count > 0)
            {
                var lastOrder = orders.First();
                dto.LastOrderOn = _dateTimeHelper.ConvertToUserTime(lastOrder.CreatedOnUtc).ToString("dd/MM/yyyy");

                var productAmount = orders.Sum(o => o.OrderSubtotalExclTax);
                dto.ProductAmount = new DecimalModel
                {
                    Value = productAmount,
                    Text = _priceFormatter.FormatPrice(productAmount)
                };

                var paidAmount = orders.Sum(o => o.OrderPaid ?? 0);
                dto.PaidAmount = new DecimalModel
                {
                    Value = paidAmount,
                    Text = _priceFormatter.FormatPrice(paidAmount)
                };

                var debtAmount = orders.Sum(o => o.OrderDebt ?? 0);
                dto.DebtAmount = new DecimalModel
                {
                    Value = debtAmount,
                    Text = _priceFormatter.FormatPrice(debtAmount)
                };

                if (details)
                {
                    //*TODO* load orders of customer
                }
            }
            return dto;
        }

        /// <summary>
        /// [HienNguyen][04/08/2018][Hàm convert model entity]
        /// </summary>
        /// <param name="orderModel"></param>
        /// <returns></returns>
        public Order PrepareSaleOrderModelEntity(SaleOrderDtoModel orderModel)
        {
            return new Order()
            {
                Id = orderModel.Id,
                CustomOrderNumber = orderModel.CustomOrderNumber,
                StoreId = orderModel.StoreId,
                CreatedOnUtc = orderModel.CreatedOn,
                CustomerId = orderModel.CustomerId,
                CustomerCreateId = orderModel.CustomerCreateId,
                OrderDiscount = orderModel.OrderDiscount,
                OrderTotal = orderModel.OrderTotal
            };
        }

        /// <summary>
        /// [HienNguyen][04/08/2018]
        /// </summary>
        /// <param name="orderPaymentModel"></param>
        /// <returns></returns>
        public SISVN_OrderPaymentBalance PrepareOrderPaymentBalanceEntity(OrderPaymentDto orderPaymentModel)
        {
            return new SISVN_OrderPaymentBalance()
            {
                Id = orderPaymentModel.Id,
                OrderId = orderPaymentModel.OrderId,
                PaymentAmount = orderPaymentModel.PaymentAmount,
                PaymentMethodId = orderPaymentModel.PaymentMethodId,
                PaymentDateUtc = orderPaymentModel.PaymentDate ?? DateTime.UtcNow
            };
        }

        /// <summary>
        /// [HienNguyen][04/08/2018]
        /// </summary>
        /// <param name="orderPaymentModel"></param>
        /// <returns></returns>
        public OrderItem PrepareOrderItemEntity(OrderItemDtoModel orderItem)
        {
            return new OrderItem()
            {
                Id = int.Parse(orderItem.Id),
                OrderId = orderItem.OrderId,
                ProductId = orderItem.ProductId,
                Quantity = orderItem.Quantity,
                PriceInclTax = orderItem.SalePrice,
                UnitPriceInclTax = orderItem.UnitPrice,
                DiscountAmountInclTax = orderItem.Discount,
                OrderItemGuid = Guid.NewGuid(),

            };
        }

        /// <summary>
        /// [HienNguyen][04/08/2018]
        /// </summary>
        /// <param name="orderItem"></param>
        /// <returns></returns>
        public List<OrderItem> PrepareOrderItemEntity(List<OrderItemDtoModel> orderItems)
        {
            return orderItems.Select(m => PrepareOrderItemEntity(m)).ToList();
        }

        /// <summary>
        /// [HienNguyen][04/08/2018]
        /// </summary>
        /// <param name="orderNote"></param>
        /// <returns></returns>
        public OrderNote PrepareOrderNoteEntity(OrderNoteDtoModel orderNote)
        {
            return new OrderNote()
            {
                Id = orderNote.Id,
                OrderId = orderNote.OrderId,
                CreatedOnUtc = orderNote.CreatedOnUtc,
                Note = orderNote.Note
            };
        }

        /// <summary>
        /// [HienNguyen][04/08/2018]
        /// </summary>
        /// <param name="orderNote"></param>
        /// <returns></returns>
        public List<OrderNote> PrepareOrderNoteEntity(List<OrderNoteDtoModel> orderNotes)
        {
            return orderNotes.Select(m => PrepareOrderNoteEntity(m)).ToList();
        }

        /// <summary>
        /// [HienNguyen][08/08/2018]
        /// </summary>
        /// <param name="product"></param>
        /// <returns></returns>
        public ProductDtoModel PrepareProductDTOModel(Product product, bool prepareImage = true, int imageSize = 0)
        {
            ProductDtoModel productDto = new ProductDtoModel()
            {
                CategoryId = product.ProductCategories.Count > 0 ? product.ProductCategories.First().CategoryId : 0,
                CategoryName = product.ProductCategories.Count > 0 ? product.ProductCategories.First().Category.Name : string.Empty,
                ProductId = product.Id,
                ProductName = product.Name,
                SalePrice = product.Price,
                Sku = product.Sku,
                StockQuantity = product.StockQuantity,
                VendorId = product.VendorId,
                VipSalePrice = product.SpecialPrice ?? 0,
                WholeSalePrice = product.WholePrice ?? 0,
                Published = product.Published,
                SalePriceText = _priceFormatter.FormatPrice(product.Price)
            };
            productDto.VendorName = _vendorService.GetVendorById(product.VendorId)?.GetLocalized(l => l.Name);
            if (prepareImage)
            {
                PrepareProductImages(product.ProductPictures, productDto, imageSize);
            }

            return productDto;
        }

        /// <summary>
        /// [HienNguyen][08/08/2018]
        /// </summary>
        /// <param name="product"></param>
        /// <returns></returns>
        public ProductPictureDtoModel PrepareProductPictureDTOModel(ProductPicture picture)
        {
            ProductPictureDtoModel productPicture = new ProductPictureDtoModel();

            return productPicture;
        }

        /// <summary>
        /// [HienNguyen][08/08/2018]
        /// </summary>
        /// <param name="category"></param>
        /// <returns></returns>
        public CategoryDtoModel PrepareCategoryDTOModel(Category category)
        {
            CategoryDtoModel categoryModel = new CategoryDtoModel()
            {
                Id = category.Id,
                Name = category.GetFormattedBreadCrumb(_categoryService),
                ParentCategoryId = category.ParentCategoryId
            };

            return categoryModel;
        }

        /// <summary>
        /// [HienNguyen][08/08/2018]
        /// </summary>
        /// <param name="category"></param>
        /// <returns></returns>
        public VendorDtoModel PrepareVendorDTOModel(Vendor vendor)
        {
            VendorDtoModel vendorModel = new VendorDtoModel()
            {
                Id = vendor.Id,
                Name = vendor.Name,
            };

            return vendorModel;
        }

        #endregion

        public ProductDto PrepareProductDTO(Product product)
        {
            ProductDto productDto = product.ToDto();

            PrepareProductImages(product.ProductPictures, productDto);
            PrepareProductAttributes(product.ProductAttributeMappings, productDto);

            productDto.SeName = product.GetSeName();
            productDto.DiscountIds = product.AppliedDiscounts.Select(discount => discount.Id).ToList();
            productDto.ManufacturerIds = product.ProductManufacturers.Select(pm => pm.ManufacturerId).ToList();
            productDto.RoleIds = _aclService.GetAclRecords(product).Select(acl => acl.CustomerRoleId).ToList();
            productDto.StoreIds = _storeMappingService.GetStoreMappings(product).Select(mapping => mapping.StoreId).ToList();
            productDto.Tags = product.ProductTags.Select(tag => tag.Name).ToList();

            //category
            productDto.CategoryId = product.ProductCategories.Count > 0 ? product.ProductCategories.ElementAt(0).CategoryId : 0;

            productDto.AssociatedProductIds =
                _productService.GetAssociatedProducts(product.Id, showHidden: true)
                    .Select(associatedProduct => associatedProduct.Id)
                    .ToList();

            IList<Language> allLanguages = _languageService.GetAllLanguages();

            productDto.LocalizedNames = new List<LocalizedNameDto>();

            foreach (var language in allLanguages)
            {
                var localizedNameDto = new LocalizedNameDto
                {
                    LanguageId = language.Id,
                    LocalizedName = product.GetLocalized(x => x.Name, language.Id)
                };

                productDto.LocalizedNames.Add(localizedNameDto);
            }

            return productDto;
        }

        public CategoryDto PrepareCategoryDTO(Category category)
        {
            CategoryDto categoryDto = category.ToDto();

            Picture picture = _pictureService.GetPictureById(category.PictureId);
            ImageDto imageDto = PrepareImageDto(picture);

            if (imageDto != null)
            {
                categoryDto.Image = imageDto;
            }

            categoryDto.SeName = category.GetSeName();
            categoryDto.DiscountIds = category.AppliedDiscounts.Select(discount => discount.Id).ToList();
            categoryDto.RoleIds = _aclService.GetAclRecords(category).Select(acl => acl.CustomerRoleId).ToList();
            categoryDto.StoreIds = _storeMappingService.GetStoreMappings(category).Select(mapping => mapping.StoreId).ToList();

            IList<Language> allLanguages = _languageService.GetAllLanguages();

            categoryDto.LocalizedNames = new List<LocalizedNameDto>();

            foreach (var language in allLanguages)
            {
                var localizedNameDto = new LocalizedNameDto
                {
                    LanguageId = language.Id,
                    LocalizedName = category.GetLocalized(x => x.Name, language.Id)
                };

                categoryDto.LocalizedNames.Add(localizedNameDto);
            }

            return categoryDto;
        }

        public OrderDto PrepareOrderDTO(Order order)
        {
            OrderDto orderDto = order.ToDto();

            orderDto.OrderItemDtos = order.OrderItems.Select(orderItem => PrepareOrderItemDTO(orderItem)).ToList();

            CustomerDto customerDto = _customerApiService.GetCustomerById(order.Customer.Id);

            if (customerDto != null)
            {
                orderDto.Customer = customerDto.ToOrderCustomerDto();
            }

            return orderDto;
        }
        public InputVoucherDto PrepareInputVoucherDTO(Order order)
        {
            InputVoucherDto inputVoucherDto = order.ToInputVoucherDto();

            inputVoucherDto.InputVoucherItems = order.OrderItems.Select(orderItem => PrepareInputVocucherItemDTO(orderItem)).ToList();

            CustomerDto customerCreateDto = _customerApiService.GetCustomerById(order.CustomerCreateId ?? -1);
            if (customerCreateDto != null)
            {
                inputVoucherDto.CustomerCreateName = customerCreateDto.SystemName;
            }

            return inputVoucherDto;
        }


        public ShoppingCartItemDto PrepareShoppingCartItemDTO(ShoppingCartItem shoppingCartItem)
        {
            var dto = shoppingCartItem.ToDto();
            dto.ProductDto = PrepareProductDTO(shoppingCartItem.Product);
            dto.CustomerDto = shoppingCartItem.Customer.ToCustomerForShoppingCartItemDto();
            dto.Attributes = _productAttributeConverter.Parse(shoppingCartItem.AttributesXml);
            return dto;
        }

        public OrderItemDto PrepareOrderItemDTO(OrderItem orderItem)
        {
            var dto = orderItem.ToDto();
            dto.Product = PrepareProductDTO(orderItem.Product);
            dto.Attributes = _productAttributeConverter.Parse(orderItem.AttributesXml);
            return dto;
        }
        public InputVoucherItemDto PrepareInputVocucherItemDTO(OrderItem orderItem)
        {
            var dto = orderItem.ToInputVoucherItemDto();
            //dto.Product = PrepareProductDTO(orderItem.Product);
            return dto;
        }

        public StoreDto PrepareStoreDTO(Store store)
        {
            StoreDto storeDto = store.ToDto();

            Currency primaryCurrency = _currencyService.GetCurrencyById(_currencySettings.PrimaryStoreCurrencyId);

            if (!String.IsNullOrEmpty(primaryCurrency.DisplayLocale))
            {
                storeDto.PrimaryCurrencyDisplayLocale = primaryCurrency.DisplayLocale;
            }

            storeDto.LanguageIds = _languageService.GetAllLanguages(false, store.Id).Select(x => x.Id).ToList();

            return storeDto;
        }

        public LanguageDto PrepateLanguageDto(Language language)
        {
            LanguageDto languageDto = language.ToDto();

            languageDto.StoreIds = _storeMappingService.GetStoreMappings(language).Select(mapping => mapping.StoreId).ToList();

            if (languageDto.StoreIds.Count == 0)
            {
                languageDto.StoreIds = _storeService.GetAllStores().Select(s => s.Id).ToList();
            }

            return languageDto;
        }

        private void PrepareProductImages(IEnumerable<ProductPicture> productPictures, ProductDto productDto)
        {
            if (productDto.Images == null)
                productDto.Images = new List<ImageMappingDto>();

            // Here we prepare the resulted dto image.
            foreach (var productPicture in productPictures)
            {
                ImageDto imageDto = PrepareImageDto(productPicture.Picture);

                if (imageDto != null)
                {
                    ImageMappingDto productImageDto = new ImageMappingDto();
                    productImageDto.Id = productPicture.Id;
                    productImageDto.Position = productPicture.DisplayOrder;
                    productImageDto.Src = imageDto.Src;
                    productImageDto.Attachment = imageDto.Attachment;

                    productDto.Images.Add(productImageDto);
                }
            }
        }

        private void PrepareProductImages(IEnumerable<ProductPicture> productPictures, ProductDtoModel productDto, int imageSize = 0)
        {
            if (productDto.Images == null)
                productDto.Images = new List<ImageMappingDto>();

            // Here we prepare the resulted dto image.
            foreach (var productPicture in productPictures)
            {
                ImageDto imageDto = PrepareImageDto(productPicture.Picture, imageSize);

                if (imageDto != null)
                {
                    ImageMappingDto productImageDto = new ImageMappingDto();
                    productImageDto.Id = productPicture.Id;
                    productImageDto.Position = productPicture.DisplayOrder;
                    productImageDto.Src = imageDto.Src;
                    productImageDto.Attachment = imageDto.Attachment;

                    productDto.Images.Add(productImageDto);
                }
            }
        }

        protected ImageDto PrepareImageDto(Picture picture, int imageSize = 0)
        {
            ImageDto image = null;

            if (picture != null)
            {
                // We don't use the image from the passed dto directly 
                // because the picture may be passed with src and the result should only include the base64 format.
                image = new ImageDto()
                {
                    //Attachment = Convert.ToBase64String(picture.PictureBinary),
                    Src = _pictureService.GetPictureUrl(picture, imageSize)
                };
            }

            return image;
        }

        private void PrepareProductAttributes(IEnumerable<ProductAttributeMapping> productAttributeMappings, ProductDto productDto)
        {
            if (productDto.ProductAttributeMappings == null)
                productDto.ProductAttributeMappings = new List<ProductAttributeMappingDto>();

            foreach (var productAttributeMapping in productAttributeMappings)
            {
                ProductAttributeMappingDto productAttributeMappingDto = PrepareProductAttributeMappingDto(productAttributeMapping);

                if (productAttributeMappingDto != null)
                {
                    productDto.ProductAttributeMappings.Add(productAttributeMappingDto);
                }
            }
        }

        private ProductAttributeMappingDto PrepareProductAttributeMappingDto(ProductAttributeMapping productAttributeMapping)
        {
            ProductAttributeMappingDto productAttributeMappingDto = null;

            if (productAttributeMapping != null)
            {
                productAttributeMappingDto = new ProductAttributeMappingDto()
                {
                    Id = productAttributeMapping.Id,
                    ProductAttributeId = productAttributeMapping.ProductAttributeId,
                    ProductAttributeName = _productAttributeService.GetProductAttributeById(productAttributeMapping.ProductAttributeId).Name,
                    TextPrompt = productAttributeMapping.TextPrompt,
                    DefaultValue = productAttributeMapping.DefaultValue,
                    AttributeControlTypeId = productAttributeMapping.AttributeControlTypeId,
                    DisplayOrder = productAttributeMapping.DisplayOrder,
                    IsRequired = productAttributeMapping.IsRequired,
                    ProductAttributeValues = productAttributeMapping.ProductAttributeValues.Select(x => PrepareProductAttributeValueDto(x, productAttributeMapping.Product)).ToList()
                };
            }

            return productAttributeMappingDto;
        }

        private ProductAttributeValueDto PrepareProductAttributeValueDto(ProductAttributeValue productAttributeValue, Product product)
        {
            ProductAttributeValueDto productAttributeValueDto = null;

            if (productAttributeValue != null)
            {
                productAttributeValueDto = productAttributeValue.ToDto();
                if (productAttributeValue.ImageSquaresPictureId > 0)
                {
                    Picture imageSquaresPicture = _pictureService.GetPictureById(productAttributeValue.ImageSquaresPictureId);
                    ImageDto imageDto = PrepareImageDto(imageSquaresPicture);
                    productAttributeValueDto.ImageSquaresImage = imageDto;
                }

                if (productAttributeValue.PictureId > 0)
                {
                    // make sure that the picture is mapped to the product
                    // This is needed since if you delete the product picture mapping from the nopCommerce administrationthe
                    // then the attribute value is not updated and it will point to a picture that has been deleted
                    var productPicture = product.ProductPictures.FirstOrDefault(pp => pp.PictureId == productAttributeValue.PictureId);
                    if (productPicture != null)
                    {
                        productAttributeValueDto.ProductPictureId = productPicture.Id;
                    }
                }
            }

            return productAttributeValueDto;
        }


        public ProductAttributeDto PrepareProductAttributeDTO(ProductAttribute productAttribute)
        {
            return productAttribute.ToDto();
        }


        #region InputVoucher
        public Order PrepareInputVoucher(InputVoucherDto inputVoucherDto)
        {
            return new Order()
            {
                Id = inputVoucherDto.Id,
                OrderTypeId = inputVoucherDto.InputTypeId,
                StoreId = inputVoucherDto.StoreId,
                CreatedOnUtc = inputVoucherDto.InputDate,
                VendorId = inputVoucherDto.VendorId,
                CustomerCreateId = inputVoucherDto.CustomerCreateId,
                OrderTotal = inputVoucherDto.InputVoucherTotal
            };
        }

        public List<OrderItem> PrepareListInputVoucherItem(List<InputVoucherItemDto> listInputVoucherItems)
        {
            return listInputVoucherItems.Select(m => PrepareInputVoucherItem(m)).ToList();
        }
        public OrderItem PrepareInputVoucherItem(InputVoucherItemDto inputVoucherItem)
        {
            return new OrderItem()
            {
                Id = inputVoucherItem.Id,
                OrderId = inputVoucherItem.InputVoucherId,
                ProductId = inputVoucherItem.ProductId,
                Quantity = inputVoucherItem.Quantity,
                PriceInclTax = inputVoucherItem.PriceExclTax,
                UnitPriceInclTax = inputVoucherItem.UnitPriceInclTax,
                OrderItemGuid = Guid.NewGuid(),
            };
        }

        public List<OrderNote> PrepareListInputVoucherNote(List<InputVoucherNoteDto> listInputVoucherNotes)
        {
            return listInputVoucherNotes.Select(m => PrepareInputVoucherItem(m)).ToList();
        }
        public OrderNote PrepareInputVoucherItem(InputVoucherNoteDto inputVoucherNote)
        {
            return new OrderNote()
            {
                Id = inputVoucherNote.Id,
                OrderId = inputVoucherNote.InputVoucherId,
                CreatedOnUtc = inputVoucherNote.CreatedOnUtc,
                Note = inputVoucherNote.Note
            };
        }
        #endregion


    }
}
