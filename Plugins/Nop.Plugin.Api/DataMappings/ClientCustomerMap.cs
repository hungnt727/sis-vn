﻿using Nop.Plugin.Api.Domain;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.DataMappings
{
    public class ClientCustomerMap : EntityTypeConfiguration<ClientCustomer>
    {
        public ClientCustomerMap()
        {
            this.ToTable("ClientCustomer");
            this.HasKey(c => c.Id);
            this.HasRequired(c => c.Client).WithMany().HasForeignKey(c => c.ClientId);
            //this.HasRequired(c => c.Customer).WithMany().HasForeignKey(c => c.CustomerId);
        }
    }
}
