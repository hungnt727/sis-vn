﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Core;
using Nop.Core.Domain.Orders;
using Nop.Plugin.Api.Models.Customers;
using Nop.Plugin.Api.Models.Orders;
using Nop.Services.Catalog;
using Nop.Services.Helpers;
using Nop.Services.Customers;
using Nop.Services.Localization;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Stores;
using Nop.Plugin.Api.Models.Stores;
using Nop.Services.Stores;
using Nop.Core.Domain.Catalog;
using Nop.Services.Media;
using Nop.Core.Domain.Media;
using Nop.Services.Payments;
using Nop.Core.Domain.Shipping;

namespace Nop.Plugin.Api.Converters
{
    public class ModelConverter : IModelConverter
    {
        private readonly IPaymentService _paymentService;
        private readonly ICustomerService _customerService;
        private readonly MediaSettings _mediaSettings;
        private readonly IPictureService _pictureService;
        private readonly IStoreService _storeService;
        private readonly IPriceFormatter _priceFormatter;
        private readonly IDateTimeHelper _dateTimeHelper;

        public ModelConverter(
            IPaymentService paymentService,
            ICustomerService customerService,
            MediaSettings mediaSettings,
            IStoreService storeService,
             IPictureService pictureService,
            IPriceFormatter priceFormatter, IDateTimeHelper dateTimeHelper)
        {
            _paymentService = paymentService;
            _customerService = customerService;
            _mediaSettings = mediaSettings;
            _pictureService = pictureService;
            _storeService = storeService;
            _priceFormatter = priceFormatter;
            _dateTimeHelper = dateTimeHelper;
        }

        public OrderModel ToModel(Order o)
        {
            if (o == null) throw new ArgumentNullException("o");
            var model = new OrderModel
            {
                Id = o.Id,
                OrderTotal = o.OrderTotal.ToModel(_priceFormatter),
                OrderDiscount = o.OrderDiscount.ToModel(_priceFormatter),
                OrderTax = o.OrderTax.ToModel(_priceFormatter),
                OrderShippingAmount = o.OrderShippingInclTax.ToModel(_priceFormatter),
                OrderSubTotal = o.OrderSubTotalDiscountInclTax.ToModel(_priceFormatter),
                OrderPaid = o.OrderPaid?.ToModel(_priceFormatter),
                OrderDebt = o.OrderDebt?.ToModel(_priceFormatter),
                OrderPaymentId = o.PaymentStatusId,
                OrderShipmentId = o.ShippingStatusId,
                OrderStatusId = o.OrderStatusId,
                CreatedOn = o.CreatedOnUtc.Format(_dateTimeHelper, "dd/MM/yyyy"),
                PaidOn = o.PaidDateUtc?.Format(_dateTimeHelper, "dd/MM/yyyy"),
                Customer = ToSimpleModel(o.Customer),
                OrderItems = o.OrderItems.Select(ToModel).ToList(),
                PaymentHistory = o.SISVN_OrderPaymentBalances.Select(ToModel).ToList(),
                OrderCode = o.CustomOrderNumber,
                OrderShipping = new OrderShippingModel
                {
                    //*TODO* test
                    SystemName = o.ShippingRateComputationMethodSystemName,
                    Name = o.ShippingMethod,
                    Shipments = o.Shipments.Select(ToModel).ToList()
                }
            };
            //store
            var store = _storeService.GetStoreById(o.StoreId);
            if (store != null)
            {
                model.Store = ToSimpleModel(store);
            }
            //seller
            var seller = _customerService.GetCustomerById(o.SellerId ?? 0);
            if(seller != null)
            {
                model.Seller = ToSimpleModel(seller);
            }
            return model;
        }

        public OrderSummaryModel ToSummaryModel(Order o)
        {
            if (o == null) throw new ArgumentNullException("o");
            var model = new OrderSummaryModel
            {
                Id = o.Id,
                OrderTotal = o.OrderTotal.ToModel(_priceFormatter),
                OrderSubTotal = o.OrderSubTotalDiscountInclTax.ToModel(_priceFormatter),
                OrderPaid = o.OrderPaid?.ToModel(_priceFormatter),
                OrderDebt = o.OrderDebt?.ToModel(_priceFormatter),
                OrderItems = o.OrderItems.Select(ToModel).ToList(),
                PaymentHistory = o.SISVN_OrderPaymentBalances.Select(ToModel).ToList()
            };
            return model;
        }

        public OrderListModel ToModel(IPagedList<Order> orders)
        {
            if (orders == null) throw new ArgumentNullException("orders");

            var model = new OrderListModel
            {
                Orders = orders.Select(ToModel).ToList(),
                PageNumber = orders.PageIndex + 1,
                PageSize = orders.PageSize,
                TotalPage = orders.TotalPages,
                TotalRow = orders.TotalCount
            };
            return model;
        }

        public OrderItemModel ToModel(OrderItem item)
        {
            if (item == null) throw new ArgumentNullException("item");
            var model = new OrderItemModel
            {
                Id = item.Id,
                UnitPrice = item.UnitPriceInclTax.ToModel(_priceFormatter),
                Quantity = item.Quantity,
                Discount = item.DiscountAmountInclTax.ToModel(_priceFormatter),
                TotalAmount = item.PriceInclTax.ToModel(_priceFormatter),
                Product = ToOrderModel(item.Product)
            };
            return model;
        }

        public OrderPaymentHistoryModel ToModel(SISVN_OrderPaymentBalance p)
        {
            if (p == null) throw new ArgumentNullException("p");
            var model = new OrderPaymentHistoryModel
            {
                Amount = p.PaymentAmount.ToModel(_priceFormatter),
                DatePaid = p.PaymentDateUtc.Format(_dateTimeHelper, "dd/MM/yyyy"),
                Id = p.Id
            };

            model.Payment = new PaymentModel
            {
                SystemName = p.PaymentMethodSystemName
            };
            var pm = _paymentService.LoadPaymentMethodBySystemName(p.PaymentMethodSystemName);
            if(pm != null)
            {
                model.Payment.Name = pm.PluginDescriptor.FriendlyName;
            }

            return model;
        }

        public OrderProductModel ToOrderModel(Product product)
        {
            if (product == null) throw new ArgumentNullException("product");
            var model = new OrderProductModel
            {
                Id = product.Id,
                Name = product.GetLocalized(l => l.Name),
                Sku = product.Sku
            };
            var defaultPicture = _pictureService.GetPicturesByProductId(product.Id, 1).FirstOrDefault();
            if(defaultPicture != null)
            {
                model.ImageUrl = _pictureService.GetPictureUrl(defaultPicture, _mediaSettings.ProductThumbPictureSize);
            }
            return model;
        }

        public CustomerSimpleModel ToSimpleModel(Customer c)
        {
            if (c == null) throw new ArgumentNullException("customer");
            return new CustomerSimpleModel
            {
                Id = c.Id,
                Name = c.GetFullName()
            };
        }

        public StoreSimpleModel ToSimpleModel(Store store)
        {
            if (store == null) throw new ArgumentNullException("store");
            return new StoreSimpleModel
            {
                Id = store.Id,
                Name = store.GetLocalized(l => l.Name)
            };
        }

        public OrderShipmentModel ToModel(Shipment s)
        {
            return new OrderShipmentModel
            {
                Id = s.Id,
                TrackingNumber = s.TrackingNumber,
                Weight = s.TotalWeight,
                DateShipped = s.ShippedDateUtc?.Format(_dateTimeHelper),
                DateDeliveried = s.DeliveryDateUtc?.Format(_dateTimeHelper),
                
            };
        }

        public OrderCopyModel Copy(Order o)
        {
            if (o == null) throw new ArgumentNullException("o");
            var model = new OrderCopyModel
            {
                OrderTotal = o.OrderTotal.ToModel(_priceFormatter),
                OrderDiscount = o.OrderDiscount.ToModel(_priceFormatter),
                OrderTax = o.OrderTax.ToModel(_priceFormatter),
                OrderShippingAmount = o.OrderShippingInclTax.ToModel(_priceFormatter),
                OrderSubTotal = o.OrderSubTotalDiscountInclTax.ToModel(_priceFormatter),
                Customer = ToSimpleModel(o.Customer),
                OrderItems = o.OrderItems.Select(ToModel).ToList(),
                OrderShipping = new OrderShippingModel
                {
                    //*TODO* test
                    SystemName = o.ShippingRateComputationMethodSystemName,
                    Name = o.ShippingMethod
                }
            };
            //store
            var store = _storeService.GetStoreById(o.StoreId);
            if (store != null)
            {
                model.Store = ToSimpleModel(store);
            }
            //seller
            var seller = _customerService.GetCustomerById(o.SellerId ?? 0);
            if (seller != null)
            {
                model.Seller = ToSimpleModel(seller);
            }
            return model;
        }
    }
}
