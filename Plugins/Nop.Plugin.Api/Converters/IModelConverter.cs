﻿using Nop.Core;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Shipping;
using Nop.Core.Domain.Stores;
using Nop.Plugin.Api.Models.Customers;
using Nop.Plugin.Api.Models.Orders;
using Nop.Plugin.Api.Models.Stores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.Converters
{
    public interface IModelConverter
    {
        CustomerSimpleModel ToSimpleModel(Customer c);

        StoreSimpleModel ToSimpleModel(Store store);

        OrderModel ToModel(Order o);

        OrderCopyModel Copy(Order o);

        OrderSummaryModel ToSummaryModel(Order o);

        OrderShipmentModel ToModel(Shipment s);

        OrderPaymentHistoryModel ToModel(SISVN_OrderPaymentBalance p);

        OrderListModel ToModel(IPagedList<Order> orders);

        OrderItemModel ToModel(OrderItem item);

        OrderProductModel ToOrderModel(Product product);
    }
}
