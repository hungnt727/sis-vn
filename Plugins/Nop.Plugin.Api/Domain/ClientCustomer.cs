﻿using IdentityServer4.EntityFramework.Entities;
using Nop.Core;
using Nop.Core.Domain.Customers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.Domain
{
    public partial class ClientCustomer : BaseEntity
    {
        public int ClientId { get; set; }

        public Client Client { get; set; }

        public int CustomerId { get; set; }
        
        //public virtual Customer Customer { get; set; }
    }
}
