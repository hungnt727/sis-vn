﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core;
using Nop.Core.Data;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Payments;
using Nop.Core.Domain.Shipping;
using Nop.Core.Domain.Vendors;
using Nop.Plugin.Api.Constants;
using Nop.Plugin.Api.DataStructures;
using Nop.Plugin.Api.DTOs.InputVouchers;

namespace Nop.Plugin.Api.Services
{
    public class InputVoucherApiService : IInputVoucherApiService
    {
        private readonly IRepository<Order> _orderRepository;
        private readonly IRepository<Vendor> _vendorRepository;

        public InputVoucherApiService(IRepository<Order> orderRepository)
        {
            _orderRepository = orderRepository;
        }

        /// <summary>
        /// [TuanAnh][28/09/2018][Lấy danh sách loại phiếu nhập].
        /// </summary>
        /// <returns></returns>
        public List<InputVoucherType> GetListInputVoucherType()
        {
            return Enum.GetValues(typeof(Core.Domain.Orders.OrderType))
                                .Cast<Core.Domain.Orders.OrderType>()
                                .Where(s => s.ToString().StartsWith("INVENTORY_INPUT"))
                                .OrderBy(s => (int)s)
                                .Select(v => new DTOs.InputVouchers.InputVoucherType()
                                {
                                    InputVoucherTypeId = (int)v,
                                    InputVoucherTypeName = CommonHelper.GetEnumDescription(v)
                                })
                                .ToList();
        }

        /// <summary>
        /// [TuanAnh][08/09/2018][Load danh sách phiếu nhập theo nhà cung cấp].
        /// </summary>
        /// <param name="VendorId">Mã nhà cung cấp</param>
        /// <param name="PageIndex"></param>
        /// <param name="PageSize"></param>
        /// <returns></returns>
        public IPagedList<Order> GetInputVouchersByVendorId(int VendorId, int PageIndex, int PageSize)
        {
            var ListInputType = GetListInputVoucherType();
            var ListInputTypeId = ListInputType.Select(s => s.InputVoucherTypeId).ToList();

            var query = _orderRepository.Table;
            query = query.Where(s => s.OrderTypeId != null && ListInputTypeId.Contains(s.OrderTypeId.Value));

            query = query.Where(s => s.VendorId == VendorId);

            query = query.OrderByDescending(s => s.CreatedOnUtc);

            return new PagedList<Order>(query, PageIndex, PageSize);

        }

        /// <summary>
        /// [27/09/2018][TuanAnh][Lấy danh sách phiếu nhập theo điều kiện tìm kiếm]
        /// </summary>
        /// <param name="ProductId">Mã sản phẩm</param>
        /// <param name="VendorId">Nhà cung cấp</param>
        /// <param name="WareHouseId">Mã kho nhận hàng</param>
        /// <param name="InputTypeId">Mã loại phiếu nhập</param>
        /// <param name="InputStatusId">Mã trạng thái phiếu nhập</param>
        /// <param name="InputDateFrom">Ngày bắt đầu nhập hàng</param>
        /// <param name="InputDateTo">Ngày kết thúc nhập hàng</param>
        /// <param name="PageIndex"></param>
        /// <param name="PageSize"></param>
        /// <returns></returns>
        public IPagedList<Order> GetListInputVouchers(int? ProductId, int? VendorId,int? WareHouseId,int? InputTypeId, int? InputStatusId, DateTime? InputDateFrom, DateTime? InputDateTo , int PageIndex, int PageSize)
        {
            var query = _orderRepository.Table;

            query = query.Where(s => s.VendorId == VendorId);
            query = query.Where(s => s.WareHouseId == WareHouseId);

            if (InputTypeId != null)
            {
                query = query.Where(s => s.OrderTypeId != InputTypeId);
            }
            else
            {
                var ListInputType = GetListInputVoucherType();
                var ListInputTypeId = ListInputType.Select(s => s.InputVoucherTypeId).ToList();
                query = query.Where(s => s.OrderTypeId != null && ListInputTypeId.Contains(s.OrderTypeId.Value));
            }


            if (InputDateFrom != null )
            {
                InputDateFrom = InputDateFrom.Value.Date;
                query = query.Where(s => s.CreatedOnUtc >= InputDateFrom);
            }
            if (InputDateTo != null )
            {
                InputDateTo = InputDateTo.Value.Date.AddDays(1);
                query = query.Where(s => s.CreatedOnUtc < InputDateTo);
            }
            if (ProductId != null )
            {
                query = query.Where(s => s.OrderItems !=  null && s.OrderItems.Count > 0 && s.OrderItems.Any(i=> i.ProductId == ProductId.Value));
            }

            if (InputStatusId != null )
            {
                query = query.Where(s => s.OrderStatusId == InputStatusId);
            }
            query = query.OrderByDescending(s => s.CreatedOnUtc);

            return new PagedList<Order>(query, PageIndex, PageSize);

        }



    }
}