﻿using System.Collections.Generic;

namespace Nop.Plugin.Api.Services
{
    using Nop.Plugin.Api.Domain;
    using Nop.Plugin.Api.Models;
    using System.Threading.Tasks;

    public interface IClientService
    {
        IList<ClientApiModel> GetAllClients();
        void DeleteClient(int id);
        int InsertClient(ClientApiModel model);
        void UpdateClient(ClientApiModel model);
        ClientApiModel FindClientByIdAsync(int id);
        ClientApiModel FindClientByClientId(string clientId);


        IList<ClientCustomer> GetAllClientCustomers();
        void DeleteClientCustomer(int id);
        int InsertClientCustomer(ClientCustomer entity);
        void UpdateCustomer(ClientCustomer entity);
    }
}