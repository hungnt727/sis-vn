﻿using Nop.Core;
using Nop.Core.Data;
using Nop.Core.Domain.Catalog;
using Nop.Plugin.Api.DTOs.StockQuantityHistorys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.Services
{
    public class StockQuantityHistoryApiService : IStockQuantityHistoryApiService
    {
        private readonly IRepository<StockQuantityHistory> _stockQuantityHistoryRepository;


        public StockQuantityHistoryApiService(IRepository<StockQuantityHistory> stockQuantityHistoryRepository)
        {
            _stockQuantityHistoryRepository = stockQuantityHistoryRepository;
        }

        /// <summary>
        /// [TuanAnh][03/09/2018][Lấy thông tin tồn kho sản phẩm theo time line]
        /// </summary>
        /// <param name="DateFrom">Ngày bắt đầu</param>
        /// <param name="DateTo">Ngày kết thúc</param>
        /// <param name="KeySearchProduct">Thông tin sản phẩm (Mã hoặc tên sản phẩm)</param>
        /// <param name="ProductTypeId">Mã loại sản phẩm</param>
        /// <param name="VendorId">Mã nhà cung cấp</param>
        /// <param name="WarehouseId">Mã kho</param>
        /// <param name="PageIndex"></param>
        /// <param name="PageSize"></param>
        /// <returns></returns>
        public IPagedList<StockQuantityHistoryDto> GetInventoryStockByTimeLine(DateTime DateFrom, DateTime DateTo, string KeySearchProduct, int? ProductTypeId, int? WarehouseId, int? VendorId, int PageIndex = 1, int PageSize = 50)
        {
            var query = _stockQuantityHistoryRepository.Table;
            query = query.Where(d => d.Product != null);
            
            query = query.Where(d => DateFrom.Date <= d.CreatedOnUtc && d.CreatedOnUtc <= DateTo);
            //Tìm theo key search product
            if (!string.IsNullOrEmpty(KeySearchProduct))
            {
                query = query.Where(d => d.Product.Name.ToUpper().Contains(KeySearchProduct.ToUpper()) || 
                                d.ProductId.ToString() == KeySearchProduct.Trim());
            }
            if (ProductTypeId != null)
            {
                query = query.Where(d => d.Product.ProductTypeId == ProductTypeId);
            }
            
            //Nhà cung cấp
            if (VendorId != null)
            {
                query = query.Where(d => d.Product.VendorId == VendorId);
            }
            if (WarehouseId != null)
            {
                query = query.Where(d => d.WarehouseId == WarehouseId);
            }
            var listStockQuantityHistory = query.ToList();

            var listData = listStockQuantityHistory.GroupBy(d => new { d.Product , d.WarehouseId }).Select(d => new StockQuantityHistoryDto()
            {
                ProductId = d.Key.Product.Id,
                WarehouseId = d.Key.WarehouseId,
                ProductName = d.Key.Product.Name,
                FirstStockQuantity = d != null && d.Count() > 0 ? d.OrderBy(s => s.CreatedOnUtc).FirstOrDefault().StockQuantity : 0,
                LastStockQuantity = d != null && d.Count() > 0 ? d.OrderByDescending(s => s.CreatedOnUtc).FirstOrDefault().StockQuantity: 0,
                InputQuantity = d.Where(i => i.QuantityAdjustment > 0).Sum(i => i.QuantityAdjustment),
                OutputQuantity = Math.Abs(d.Where(i => i.QuantityAdjustment < 0).Sum(i => i.QuantityAdjustment)),
            }).OrderBy(s => s.ProductId).ToList();
            var lstInventoryStock = new PagedList<StockQuantityHistoryDto>(listData, PageIndex, PageSize);
            return lstInventoryStock;
        }


        /// <summary>
        /// [TuanAnh][03/09/2018][Lấy thông tin thay đổi tồn kho của sản phẩm]
        /// </summary>
        /// <param name="ProductID">Mã sản phẩm</param>
        /// <param name="WarehouseId">Mã kho</param>
        /// <param name="DateFrom">Ngày bắt đầu</param>
        /// <param name="DateTo">Ngày kết thúc</param>
        /// <param name="PageIndex"></param>
        /// <param name="PageSize"></param>
        /// <returns></returns>
        public IPagedList<StockQuantityHistory> GetStockQuantityHistoryByProduct(int ProductID, int? WarehouseId, DateTime? DateFrom, DateTime? DateTo, int PageIndex = 1, int PageSize = 50)
        {
            var query = _stockQuantityHistoryRepository.Table;
            query = query.Where(d => d.ProductId == ProductID);
            if (DateFrom != null )
            {
                query = query.Where(d => DateFrom <= d.CreatedOnUtc);
            }
            if (DateTo != null)
            {
                query = query.Where(d => d.CreatedOnUtc <= DateTo);
            }
            if (WarehouseId != null)
            {
                query = query.Where(d => d.WarehouseId == WarehouseId);
            }
            query = query.OrderByDescending(d => d.CreatedOnUtc);
            var lstInventoryStock = new PagedList<StockQuantityHistory>(query, PageIndex, PageSize);
            return lstInventoryStock;
        }

        /// <summary>
        /// [TuanAnh][20/09/2018][Tạo mới thông tin thay đổi tồn kho]
        /// </summary>
        /// <param name="stockQuantityHistory"></param>
        /// <returns></returns>
        public StockQuantityHistory CreateStockQuantityHistory(StockQuantityHistory stockQuantityHistory ) 
        {
            var query = _stockQuantityHistoryRepository.Table;
            _stockQuantityHistoryRepository.Insert(stockQuantityHistory);
            return stockQuantityHistory;
        }



    }
}
