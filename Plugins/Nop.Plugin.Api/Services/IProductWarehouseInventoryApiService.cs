﻿using Nop.Core;
using Nop.Core.Domain.Catalog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.Services
{
    public interface IProductWarehouseInventoryApiService
    {
        /// <summary>
        /// Lấy thông tin tồn kho hiện tại.
        /// </summary>
        /// <param name="KeySearchProduct">Thông tin sản phẩm (tên hoặc mã sản phẩm)</param>
        /// <param name="ProductTypeId">Loại sản phẩm</param>
        /// <param name="VendorId">Mã nhà cung cấp</param>
        /// <param name="WarehouseId">Mã kho cần kiểm tra</param>
        /// <param name="PageIndex"></param>
        /// <param name="PageSize"></param>
        /// <returns>IPagedList<ProductWarehouseInventory></returns>
        IPagedList<ProductWarehouseInventory> GetInventoryStock(string KeySearchProduct, int? ProductTypeId, int? VendorId,
            int? WarehouseId, int PageIndex = 1, int PageSize = 50);

        /// <summary>
        /// Lấy thông tin tồn kho hiện tại theo đích danh sản phẩm.
        /// </summary>
        /// <param name="ProductID">Mã sả phẩm</param>
        /// <param name="WarehouseId">Mã kho</param>
        /// <returns>ProductWarehouseInventory</returns>
        ProductWarehouseInventory GetInventoryStockByProduct(int ProductID, int WarehouseId);

        /// <summary>
        /// Hàm lấy thông tin tồn kho dưới mức tồn min
        /// </summary>
        /// <param name="WarehouseId">Mã kho</param>
        /// <param name="PageIndex"></param>
        /// <param name="PageSize"></param>
        /// <returns></returns>
        IPagedList<ProductWarehouseInventory> GetInventoryStockUnderMinStock(int ? WarehouseId, int PageIndex = 1, int PageSize = 50);

        /// <summary>
        /// [TuanAnh][20/09/2018][Cập nhập thông tin tồn kho theo sản phẩm]
        /// </summary>
        /// <param name="productWarehouseInventory"></param>
        /// <returns></returns>
        ProductWarehouseInventory UpdateProductWarehouseInventory(ProductWarehouseInventory productWarehouseInventory);

    }
}
