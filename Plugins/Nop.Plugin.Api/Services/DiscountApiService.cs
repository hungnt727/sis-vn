﻿using System;
using System.Collections.Generic;
using Nop.Core.Data;
using Nop.Core.Domain.Customers;
using Nop.Plugin.Api.DTOs.Customers;
using System.Linq;
using System.Linq.Dynamic;
using System.Text.RegularExpressions;
using Nop.Core;
using Nop.Core.Domain.Common;
using Nop.Plugin.Api.Constants;
using Nop.Plugin.Api.DataStructures;
using Nop.Plugin.Api.Helpers;
using Nop.Plugin.Api.MappingExtensions;
using Nop.Services.Localization;
using Nop.Services.Stores;
using Nop.Core.Domain.Messages;
using Nop.Core.Caching;
using Nop.Core.Domain.Discounts;

namespace Nop.Plugin.Api.Services
{
    public class DiscountApiService : IDiscountApiService
    {
        private readonly IStoreContext _storeContext;
        private readonly ILanguageService _languageService;
        private readonly IRepository<Discount> _discountRepository;
        private readonly IRepository<GenericAttribute> _genericAttributeRepository;
        private readonly IStaticCacheManager _cacheManager;

        public DiscountApiService(IRepository<Discount> discountRepository,
            IStoreContext storeContext,
            ILanguageService languageService,
            IStoreMappingService storeMappingService,
            IStaticCacheManager staticCacheManager,
            IRepository<GenericAttribute> genericAttributeRepository)
        {
            _discountRepository = discountRepository;
            _genericAttributeRepository = genericAttributeRepository;
            _storeContext = storeContext;
            _languageService = languageService;
            _cacheManager = staticCacheManager;
        }

        public virtual IPagedList<Discount> SearchDiscount(DiscountType? discountType = null, string couponCode = "", string discountName = "",
            bool showHidden = false, DateTime? dtFrom = null, DateTime? dtTo = null, int pageIndex = 0, int pageSize = int.MaxValue)
        {
            var query = _discountRepository.Table;
            if (!showHidden)
            {
                //The function 'CurrentUtcDateTime' is not supported by SQL Server Compact. 
                //That's why we pass the date value
                var nowUtc = DateTime.UtcNow;
                query = query.Where(d =>
                    (!d.StartDateUtc.HasValue || d.StartDateUtc <= nowUtc)
                    && (!d.EndDateUtc.HasValue || d.EndDateUtc >= nowUtc));
            }
            if (!string.IsNullOrEmpty(couponCode))
            {
                query = query.Where(d => d.CouponCode == couponCode);
            }
            if (!string.IsNullOrEmpty(discountName))
            {
                query = query.Where(d => d.Name.Contains(discountName));
            }
            if (discountType.HasValue)
            {
                var discountTypeId = (int)discountType.Value;
                query = query.Where(d => d.DiscountTypeId == discountTypeId);
            }
            if (dtFrom != null)
            {
                dtFrom = new DateTime(dtFrom.Value.Year, dtFrom.Value.Month, dtFrom.Value.Day);
                query = query.Where(d => dtFrom <= d.EndDateUtc);
            }
            if (dtTo != null)
            {
                dtTo = new DateTime(dtTo.Value.Year, dtTo.Value.Month, dtTo.Value.Day).AddDays(1);
                query = query.Where(d => dtTo > d.StartDateUtc);
            }
            query = query.OrderBy(d => d.Name);

            var discounts = new PagedList<Discount>(query, pageIndex, pageSize);
            return discounts;
        }


    }
}