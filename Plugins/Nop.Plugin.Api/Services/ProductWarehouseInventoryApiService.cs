﻿using Nop.Core;
using Nop.Core.Data;
using Nop.Core.Domain.Catalog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.Services
{
    public class ProductWarehouseInventoryApiService : IProductWarehouseInventoryApiService
    {
        private readonly IRepository<ProductWarehouseInventory> _productWarehouseInventory;
        private readonly IRepository<Product> _productRepository;

        public ProductWarehouseInventoryApiService(IRepository<ProductWarehouseInventory> productWarehouseInventory, IRepository<Product> productRepository)
        {
            _productWarehouseInventory = productWarehouseInventory;
            _productRepository = productRepository;
        }

        /// <summary>
        /// [TuanAnh][03/09/2018]Lấy thông tin tồn kho hiện tại.
        /// </summary>
        /// <param name="KeySearchProduct">Thông tin sản phẩm (tên hoặc mã sản phẩm)</param>
        /// <param name="ProductTypeId">Loại sản phẩm</param>
        /// <param name="VendorId">Mã nhà cung cấp</param>
        /// <param name="WarehouseId">Mã kho cần kiểm tra</param>
        /// <param name="PageIndex"></param>
        /// <param name="PageSize"></param>
        /// <returns></returns>
        public IPagedList<ProductWarehouseInventory> GetInventoryStock(string KeySearchProduct, int? ProductTypeId, int? VendorId,
            int? WarehouseId, int PageIndex = 1, int PageSize = 50)
        {
            var query = _productWarehouseInventory.Table;
            query = query.Where(d => d.Product != null);

            //Tìm theo key search product
            if (!string.IsNullOrEmpty(KeySearchProduct))
            {
                query = query.Where(d => d.Product.Name.ToUpper().Contains(KeySearchProduct.ToUpper()) ||
                                d.ProductId.ToString() == KeySearchProduct.Trim());
            }
            if (ProductTypeId != null)
            {
                query = query.Where(d => d.Product.ProductTypeId == ProductTypeId);
            }
            //Nhà cung cấp
            if (VendorId != null)
            {
                query = query.Where(d => d.Product.VendorId == VendorId);
            }

            if (WarehouseId != null)
            {
                query = query.Where(d => d.WarehouseId == WarehouseId);
            }

            query = query.OrderBy(d => d.Product.Name);

            var lstInventoryStock = new PagedList<ProductWarehouseInventory>(query, PageIndex, PageSize);
            return lstInventoryStock;
        }

        /// <summary>
        /// [TuanAnh][03/09/2018]Lấy thông tin tồn kho hiện tại theo đích danh sản phẩm.
        /// </summary>
        /// <param name="ProductID">Mã sả phẩm</param>
        /// <param name="WarehouseId">Mã kho</param>
        /// <returns></returns>
        public ProductWarehouseInventory GetInventoryStockByProduct(int ProductID, int WarehouseId)
        {
            var query = _productWarehouseInventory.Table;
            query = query.Where(d => d.ProductId == ProductID);
            query = query.Where(d => d.WarehouseId == WarehouseId);
            var objProductWarehouseInventory = query.FirstOrDefault();
            return objProductWarehouseInventory;
        }

        /// <summary>
        /// [TuanAnh][03/09/2018]Hàm lấy thông tin tồn kho dưới mức tồn min
        /// </summary>
        /// <param name="WarehouseId">Mã kho</param>
        /// <param name="PageIndex"></param>
        /// <param name="PageSize"></param>
        /// <returns></returns>
        public IPagedList<ProductWarehouseInventory> GetInventoryStockUnderMinStock(int? WarehouseId, int PageIndex = 1, int PageSize = 50)
        {
            var query = _productWarehouseInventory.Table;
            query = query.Where(d => d.Product != null);
            if (WarehouseId != null)
            {
                query = query.Where(d => d.WarehouseId == WarehouseId);
            }
            query = query.Where(s => (s.StockQuantity - s.ReservedQuantity) < s.Product.MinStockQuantity);
            query = query.OrderBy(d => d.Product.Name);

            var lstInventoryStock = new PagedList<ProductWarehouseInventory>(query, PageIndex, PageSize);
            return lstInventoryStock;
        }

        /// <summary>
        /// [TuanAnh][20/09/2018][Cập nhập thông tin tồn kho theo sản phẩm]
        /// </summary>
        /// <param name="productWarehouseInventory"></param>
        /// <returns></returns>
        public ProductWarehouseInventory UpdateProductWarehouseInventory(ProductWarehouseInventory productWarehouseInventory)
        {
            var query = _productWarehouseInventory.Table;
            query = query.Where(d => d.ProductId == productWarehouseInventory.ProductId && 
                                     d.WarehouseId == productWarehouseInventory.WarehouseId);

            ProductWarehouseInventory objEnity = query.First();
            objEnity.StockQuantity = productWarehouseInventory.StockQuantity;
            _productWarehouseInventory.Update(objEnity);
            return objEnity;
        }

    }
}
