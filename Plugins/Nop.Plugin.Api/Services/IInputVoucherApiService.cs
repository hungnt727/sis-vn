﻿using System;
using System.Collections.Generic;
using Nop.Core;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Payments;
using Nop.Core.Domain.Shipping;
using Nop.Plugin.Api.Constants;
using Nop.Plugin.Api.DTOs.InputVouchers;

namespace Nop.Plugin.Api.Services
{
    public interface IInputVoucherApiService
    {

        /// <summary>
        /// [08/09/2018][TuanAnh]Lấy phiếu nhập theo nhà cung cấp.
        /// </summary>
        /// <param name="VendorId">Mã nhà cung cấp</param>
        /// <param name="PageIndex"></param>
        /// <param name="PageSize"></param>
        /// <returns></returns>
        IPagedList<Order> GetInputVouchersByVendorId(int VendorId, int PageIndex, int PageSize);

        /// <summary>
        /// [27/09/2018][TuanAnh][Lấy danh sách phiếu nhập theo điều kiện tìm kiếm]
        /// </summary>
        /// <param name="ProductId">Mã sản phẩm</param>
        /// <param name="VendorId">Nhà cung cấp</param>
        /// <param name="WareHouseId">Mã kho nhận hàng</param>
        /// <param name="InputTypeId">Mã loại phiếu nhập</param>
        /// <param name="InputStatusId">Mã trạng thái phiếu nhập</param>
        /// <param name="InputDateFrom">Ngày bắt đầu nhập hàng</param>
        /// <param name="InputDateTo">Ngày kết thúc nhập hàng</param>
        /// <param name="PageIndex"></param>
        /// <param name="PageSize"></param>
        /// <returns></returns>
        IPagedList<Order> GetListInputVouchers(int? ProductId, int? VendorId, int? WareHouseId, int? InputTypeId, int? InputStatusId, DateTime? InputDateFrom, DateTime? InputDateTo, int PageIndex, int PageSize);


        /// <summary>
        /// [TuanAnh][28/09/2018][Lấy danh sách loại phiếu nhập].
        /// </summary>
        /// <returns></returns>
        List<InputVoucherType> GetListInputVoucherType();
    }
}