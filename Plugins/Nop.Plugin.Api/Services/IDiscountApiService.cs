﻿using System;
using System.Collections.Generic;
using Nop.Core;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Discounts;
using Nop.Plugin.Api.Constants;
using Nop.Plugin.Api.DTOs.Customers;

namespace Nop.Plugin.Api.Services
{
    public interface IDiscountApiService
    {
        IPagedList<Discount> SearchDiscount(DiscountType? discountType = null, string couponCode = "", string discountName = "",
            bool showHidden = false, DateTime? dtFrom = null, DateTime? dtTo = null, int pageIndex = 0, int pageSize = int.MaxValue);
    }
}