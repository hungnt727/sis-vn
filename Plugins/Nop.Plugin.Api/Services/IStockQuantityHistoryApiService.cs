﻿using Nop.Core;
using Nop.Core.Domain.Catalog;
using Nop.Plugin.Api.DTOs.StockQuantityHistorys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Api.Services
{
    public interface IStockQuantityHistoryApiService
    {
        /// <summary>
        /// [TuanAnh][03/09/2018]Lấy thông tin tồn kho sản phẩm theo time line.
        /// </summary>
        /// <param name="DateFrom">Ngày bắt đầu</param>
        /// <param name="DateTo">Ngày kết thúc</param>
        /// <param name="KeySearchProduct">Thông tin sản phẩm (Mã hoặc tên sản phẩm)</param>
        /// <param name="ProductTypeId">Mã loại sản phẩm</param>
        /// <param name="VendorId">Mã nhà cung cấp</param>
        /// <param name="WarehouseId">Mã kho</param>
        /// <param name="PageIndex"></param>
        /// <param name="pPageSize"></param>
        /// <returns></returns>
        IPagedList<StockQuantityHistoryDto> GetInventoryStockByTimeLine(DateTime DateFrom,DateTime DateTo, string KeySearchProduct, int? ProductTypeId, int? WarehouseId, int? VendorId, int PageIndex = 1, int PageSize = 50);

        /// <summary>
        /// [TuanAnh][03/09/2018][Hàm lấy lịch sử tồn kho của sản phẩm.]
        /// </summary>
        /// <param name="ProductID">Mã sản phẩm</param>
        /// <param name="WarehouseId">Mã kho</param>
        /// <param name="DateFrom">Ngày bắt đầu</param>
        /// <param name="DateTo">Ngày kết thúc</param>
        /// <param name="PageIndex"></param>
        /// <param name="PageSize"></param>
        /// <returns></returns>
        IPagedList<StockQuantityHistory> GetStockQuantityHistoryByProduct(int ProductID, int? WarehouseId, DateTime? DateFrom, DateTime? DateTo,  int PageIndex = 1, int PageSize = 50);


        /// <summary>
        /// [TuanAnh][20/09/2018][Tạo mới thông tin thay đổi tồn kho]
        /// </summary>
        /// <param name="stockQuantityHistory"></param>
        /// <returns></returns>
        StockQuantityHistory CreateStockQuantityHistory(StockQuantityHistory stockQuantityHistory);


    }
}
