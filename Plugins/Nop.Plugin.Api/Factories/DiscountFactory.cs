﻿using System;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Discounts;

namespace Nop.Plugin.Api.Factories
{
    public class DiscountFactory : IFactory<Discount>
    {
        public Discount Initialize()
        {
            var defaultDiscount = new Discount()
            {
               
            };

            return defaultDiscount;
        }
    }
}