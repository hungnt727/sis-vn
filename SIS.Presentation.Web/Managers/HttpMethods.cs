﻿namespace SIS.Presentation.Web.Managers
{
    public enum HttpMethods
    {
        Get,
        Post,
        Put,
        Delete,
    }
}