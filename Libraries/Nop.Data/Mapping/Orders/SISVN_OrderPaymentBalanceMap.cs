﻿using Nop.Core.Domain.Orders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Data.Mapping.Orders
{
    public class SISVN_OrderPaymentBalanceMap : NopEntityTypeConfiguration<SISVN_OrderPaymentBalance>
    {
        public SISVN_OrderPaymentBalanceMap()
        {
            this.Ignore(p => p.PaymentMethod);
        }
    }
}
