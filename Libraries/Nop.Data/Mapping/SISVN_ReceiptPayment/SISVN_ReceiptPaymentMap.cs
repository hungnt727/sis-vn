﻿using Nop.Core.Domain.Shipping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Data.Mapping.SISVN_ReceiptPayment
{

    public class SISVN_ReceiptPaymentMap : NopEntityTypeConfiguration<Nop.Core.Domain.SISVN_ReceiptPayment.SISVN_ReceiptPayment>
    {
        public SISVN_ReceiptPaymentMap()
        {
            this.ToTable("SISVN_ReceiptPayment");
            this.HasKey(pr => pr.Id);

            this.HasRequired(pr => pr.SISVN_ExpenseType)
                .WithMany(m => m.SISVN_ReceiptPayments)
                .HasForeignKey(pr => pr.ExpenseTypeId);

            this.HasRequired(pr => pr.SISVN_ReceiptPaymentType)
          .WithMany(m => m.SISVN_ReceiptPayments)
          .HasForeignKey(pr => pr.ReceiptPaymentTypeId);

            this.HasRequired(pr => pr.Store)
        .WithMany()
        .HasForeignKey(pr => pr.StoreId);

            //this.HasRequired(pr => pr.Customer)
            //    .WithMany()
            //    .HasForeignKey(pr => pr.CustomerId);

            //this.HasRequired(pr => pr.Store)
            //    .WithMany()
            //    .HasForeignKey(pr => pr.StoreId);
        }
    }

    public class SISVN_ReceiptPaymentTypeMap : NopEntityTypeConfiguration<Nop.Core.Domain.SISVN_ReceiptPayment.SISVN_ReceiptPaymentType>
    {
        public SISVN_ReceiptPaymentTypeMap()
        {
            this.ToTable("SISVN_ReceiptPaymentType");
            this.HasKey(pr => pr.Id);

            //this.HasRequired(pr => pr.SISVN_ReceiptPayments)
            //    .WithMany(m=>m.)
            //    .HasForeignKey(pr => pr.ReceiptPaymentTypeId);

            //this.HasRequired(pr => pr.Customer)
            //    .WithMany()
            //    .HasForeignKey(pr => pr.CustomerId);

            //this.HasRequired(pr => pr.Store)
            //    .WithMany()
            //    .HasForeignKey(pr => pr.StoreId);
        }
    }

    public class SISVN_ExpenseTypeMap : NopEntityTypeConfiguration<Nop.Core.Domain.SISVN_ReceiptPayment.SISVN_ExpenseType>
    {
        public SISVN_ExpenseTypeMap()
        {
            this.ToTable("SISVN_ExpenseType");
            this.HasKey(pr => pr.Id);

            this.HasRequired(pr => pr.SISVN_ReceiptPaymentType)
                .WithMany(m => m.SISVN_ExpenseTypes)
                .HasForeignKey(pr => pr.ReceiptPaymentTypeId);

            //this.HasRequired(pr => pr.Customer)
            //    .WithMany()
            //    .HasForeignKey(pr => pr.CustomerId);

            //this.HasRequired(pr => pr.Store)
            //    .WithMany()
            //    .HasForeignKey(pr => pr.StoreId);
        }
    }

    public class SISVN_TransporterMap : NopEntityTypeConfiguration<SISVN_Transporter>
    {

    }
}
