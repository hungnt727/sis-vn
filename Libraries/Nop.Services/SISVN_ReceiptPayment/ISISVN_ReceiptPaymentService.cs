﻿using Nop.Core;
using Nop.Core.Domain.SISVN_ReceiptPayment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Services.SISVN_ReceiptPayment
{
    public partial interface ISISVN_ReceiptPaymentService
    {
        /// <summary>
        /// [HienNguyen][Loại phiếu]
        /// </summary>
        /// <returns></returns>
        IList<SISVN_ExpenseType> GetExpenseType();

        /// <summary>
        /// [HienNguyen][Loại thu chi]
        /// </summary>
        /// <returns></returns>
        IList<SISVN_ReceiptPaymentType> GetReceiptPaymentType();

        /// <summary>
        /// [HienNguyen][Get phiếu thu chi by id]
        /// </summary>
        /// <param name="receiptPaymentId"></param>
        /// <returns></returns>
        Nop.Core.Domain.SISVN_ReceiptPayment.SISVN_ReceiptPayment GetReceiptPaymentById(int receiptPaymentId);

        /// <summary>
        ///  [HienNguyen][Tìm kiếm phiếu]
        /// </summary>
        /// <param name="keyword"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="receiptPaymentTypeIds"></param>
        /// <param name="paymentMethodIds"></param>
        /// <param name="expenseTypeIds"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        IPagedList<Nop.Core.Domain.SISVN_ReceiptPayment.SISVN_ReceiptPayment> GetReceiptPayments(
            string keyword = null,
            DateTime? startDate = null,
            DateTime? endDate = null,
            int[] receiptPaymentTypeIds = null,
            int[] paymentMethodIds = null,
            int[] expenseTypeIds = null,
            bool isDelete = false,
            int pageIndex = 0,
            int pageSize = int.MaxValue);

        /// <summary>
        /// [HienNguyen][Tìm kiếm phiếu]
        /// </summary>
        /// <param name="keyword"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="receiptPaymentTypeId"></param>
        /// <param name="paymentMethodId"></param>
        /// <param name="expenseTypeId"></param>
        /// <param name="isDelete"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        IPagedList<Nop.Core.Domain.SISVN_ReceiptPayment.SISVN_ReceiptPayment> GetReceiptPayment(
                string keyword = null,
                DateTime? startDate = null,
                DateTime? endDate = null,
                int? receiptPaymentTypeId = null,
                int? paymentMethodId = null,
                int? expenseTypeId = null,
                bool isDelete = false,
                int pageIndex = 0,
                int pageSize = int.MaxValue);

        /// <summary>
        ///  [HienNguyen][Tạo phiếu]
        /// </summary>
        /// <param name="receiptPaymentModel"></param>
        /// <returns></returns>
        Nop.Core.Domain.SISVN_ReceiptPayment.SISVN_ReceiptPayment CreateReceiptPayment(Nop.Core.Domain.SISVN_ReceiptPayment.SISVN_ReceiptPayment receiptPaymentModel);

        /// <summary>
        ///  [HienNguyen][Xóa phiếu]
        /// </summary>
        /// <param name="receiptPaymentId"></param>
        /// <returns></returns>
        Nop.Core.Domain.SISVN_ReceiptPayment.SISVN_ReceiptPayment DeleteReceiptPayment(int receiptPaymentId, int deleteUserId);
    }
}
