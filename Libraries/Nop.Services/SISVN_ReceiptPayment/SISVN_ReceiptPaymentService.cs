﻿using Nop.Core.Caching;
using Nop.Core.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Core.Domain.SISVN_ReceiptPayment;
using Nop.Core;

namespace Nop.Services.SISVN_ReceiptPayment
{
    /// <summary>
    /// [HienNguyen][Xử lý nghiệp vụ thu chi]
    /// </summary>
    public partial class SISVN_ReceiptPaymentService : ISISVN_ReceiptPaymentService
    {
        #region Fields

        private readonly IRepository<Nop.Core.Domain.SISVN_ReceiptPayment.SISVN_ReceiptPayment> _SISVN_ReceiptPaymentRepository;
        private readonly IRepository<Nop.Core.Domain.SISVN_ReceiptPayment.SISVN_ReceiptPaymentType> _SISVN_ReceiptPaymentTypeRepository;
        private readonly IRepository<Nop.Core.Domain.SISVN_ReceiptPayment.SISVN_ExpenseType> _SISVN_ExpenseTypeRepository;
        private readonly ICacheManager _cacheManager;

        #endregion

        #region Ctor

        /// <summary>
        /// Ctor
        /// </summary>
        public SISVN_ReceiptPaymentService(IRepository<Nop.Core.Domain.SISVN_ReceiptPayment.SISVN_ReceiptPayment> SISVN_ReceiptPaymentRepository,
            IRepository<Nop.Core.Domain.SISVN_ReceiptPayment.SISVN_ReceiptPaymentType> SISVN_ReceiptPaymentTypeRepository,
            IRepository<Nop.Core.Domain.SISVN_ReceiptPayment.SISVN_ExpenseType> SISVN_ExpenseTypeRepository,
            ICacheManager cacheManager)
        {
            this._SISVN_ReceiptPaymentRepository = SISVN_ReceiptPaymentRepository;
            this._SISVN_ExpenseTypeRepository = SISVN_ExpenseTypeRepository;
            this._SISVN_ReceiptPaymentTypeRepository = SISVN_ReceiptPaymentTypeRepository;
            this._cacheManager = cacheManager;
        }

        #endregion

        #region Custom Method

        /// <summary>
        /// [HienNguyen][Loại phiếu]
        /// </summary>
        /// <returns></returns>
        public virtual IList<SISVN_ExpenseType> GetExpenseType()
        {
            return _SISVN_ExpenseTypeRepository.Table.ToList();
        }

        /// <summary>
        /// [HienNguyen][Loại thu chi]
        /// </summary>
        /// <returns></returns>
        public virtual IList<SISVN_ReceiptPaymentType> GetReceiptPaymentType()
        {
            return _SISVN_ReceiptPaymentTypeRepository.Table.ToList();
        }

        public virtual Nop.Core.Domain.SISVN_ReceiptPayment.SISVN_ReceiptPayment GetReceiptPaymentById(int receiptPaymentId)
        {
            return _SISVN_ReceiptPaymentRepository.GetById(receiptPaymentId);
        }

        /// <summary>
        ///  [HienNguyen][Tìm kiếm phiếu]
        /// </summary>
        /// <param name="keyword"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="receiptPaymentTypeIds"></param>
        /// <param name="paymentMethodIds"></param>
        /// <param name="expenseTypeIds"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public virtual IPagedList<Nop.Core.Domain.SISVN_ReceiptPayment.SISVN_ReceiptPayment> GetReceiptPayments(
            string keyword = null,
            DateTime? startDate = null,
            DateTime? endDate = null,
            int[] receiptPaymentTypeIds = null,
            int[] paymentMethodIds = null,
            int[] expenseTypeIds = null,
            bool isDelete = false,
            int pageIndex = 0,
            int pageSize = int.MaxValue)
        {
            var receiptPayments = _SISVN_ReceiptPaymentRepository.Table.Where(m => m.IsDelete == isDelete);

            if (!string.IsNullOrEmpty(keyword))
            {
                receiptPayments = receiptPayments.Where(m => m.ReceiptPaymentCode.Contains(keyword));
            }

            if (startDate != null)
            {
                receiptPayments = receiptPayments.Where(m => m.TransactionDate >= startDate);
            }

            if (endDate != null)
            {
                receiptPayments = receiptPayments.Where(m => m.TransactionDate <= endDate);
            }

            if (receiptPaymentTypeIds != null && receiptPaymentTypeIds.Count() > 0)
            {
                receiptPayments = receiptPayments.Where(m => receiptPaymentTypeIds.Contains(m.ReceiptPaymentTypeId));
            }

            if (paymentMethodIds != null && paymentMethodIds.Count() > 0)
            {
                receiptPayments = receiptPayments.Where(m => paymentMethodIds.Contains(m.PaymentMethodId));
            }

            if (expenseTypeIds != null && expenseTypeIds.Count() > 0)
            {
                receiptPayments = receiptPayments.Where(m => expenseTypeIds.Contains(m.ExpenseTypeId));
            }

            return new PagedList<Nop.Core.Domain.SISVN_ReceiptPayment.SISVN_ReceiptPayment>(receiptPayments, pageIndex, pageSize);
        }

        /// <summary>
        /// [HienNguyen][Tìm kiếm phiếu]
        /// </summary>
        /// <param name="keyword"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="receiptPaymentTypeId"></param>
        /// <param name="paymentMethodId"></param>
        /// <param name="expenseTypeId"></param>
        /// <param name="isDelete"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public virtual IPagedList<Nop.Core.Domain.SISVN_ReceiptPayment.SISVN_ReceiptPayment> GetReceiptPayment(
                string keyword = null,
                DateTime? startDate = null,
                DateTime? endDate = null,
                int? receiptPaymentTypeId = null,
                int? paymentMethodId = null,
                int? expenseTypeId = null,
                bool isDelete = false,
                int pageIndex = 0,
                int pageSize = int.MaxValue)

        {
            var receiptPayments = _SISVN_ReceiptPaymentRepository.Table.Where(m => m.IsDelete == isDelete || m.IsDelete == null);

            if (!string.IsNullOrEmpty(keyword))
            {
                receiptPayments = receiptPayments.Where(m => m.ReceiptPaymentCode.Contains(keyword));
            }

            if (startDate != null)
            {
                receiptPayments = receiptPayments.Where(m => m.TransactionDate >= startDate);
            }

            if (endDate != null)
            {
                receiptPayments = receiptPayments.Where(m => m.TransactionDate <= endDate);
            }

            if (receiptPaymentTypeId != null)
            {
                receiptPayments = receiptPayments.Where(m => receiptPaymentTypeId.Value == m.ReceiptPaymentTypeId);
            }

            if (paymentMethodId != null)
            {
                receiptPayments = receiptPayments.Where(m => paymentMethodId.Value == m.PaymentMethodId);
            }

            if (expenseTypeId != null)
            {
                receiptPayments = receiptPayments.Where(m => expenseTypeId.Value == m.ExpenseTypeId);
            }

            receiptPayments = receiptPayments.OrderByDescending(m => m.Id);

            return new PagedList<Nop.Core.Domain.SISVN_ReceiptPayment.SISVN_ReceiptPayment>(receiptPayments, pageIndex, pageSize);
        }

        /// <summary>
        ///  [HienNguyen][Tạo phiếu]
        /// </summary>
        /// <param name="receiptPaymentModel"></param>
        /// <returns></returns>
        public virtual Nop.Core.Domain.SISVN_ReceiptPayment.SISVN_ReceiptPayment CreateReceiptPayment(Nop.Core.Domain.SISVN_ReceiptPayment.SISVN_ReceiptPayment receiptPaymentModel)
        {
            _SISVN_ReceiptPaymentRepository.Insert(receiptPaymentModel);
            return receiptPaymentModel;
        }

        /// <summary>
        ///  [HienNguyen][Xóa phiếu]
        /// </summary>
        /// <param name="receiptPaymentId"></param>
        /// <returns></returns>
        public virtual Nop.Core.Domain.SISVN_ReceiptPayment.SISVN_ReceiptPayment DeleteReceiptPayment(int receiptPaymentId, int deleteUserId)
        {
            var receiptPayment = _SISVN_ReceiptPaymentRepository.GetById(receiptPaymentId);
            receiptPayment.IsDelete = true;
            receiptPayment.DeleteDate = DateTime.Now;
            receiptPayment.DeleteUserId = deleteUserId;
            _SISVN_ReceiptPaymentRepository.Update(receiptPayment);
            return receiptPayment;
        }
        #endregion
    }
}
