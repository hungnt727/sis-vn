﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core;
using Nop.Core.Data;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Shipping;
using Nop.Services.Events;

namespace Nop.Services.Orders
{
    /// <summary>
    /// Order service
    /// </summary>
    public partial class OrderService : IOrderService
    {
        #region Fields

        private readonly IRepository<Order> _orderRepository;
        private readonly IRepository<OrderItem> _orderItemRepository;
        private readonly IRepository<SISVN_OrderPaymentBalance> _orderPaymentRepository;
        private readonly IRepository<OrderNote> _orderNoteRepository;
        private readonly IRepository<SISVN_Transporter> _SISVN_Transporter;
        private readonly IRepository<Product> _productRepository;
        private readonly IRepository<RecurringPayment> _recurringPaymentRepository;
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<SISVN_OrderPaymentBalance> _SISVN_OrderPaymentBalanceRepository;
        private readonly IEventPublisher _eventPublisher;

        #endregion

        #region Ctorc

        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="orderRepository">Order repository</param>
        /// <param name="orderItemRepository">Order item repository</param>
        /// <param name="orderNoteRepository">Order note repository</param>
        /// <param name="productRepository">Product repository</param>
        /// <param name="recurringPaymentRepository">Recurring payment repository</param>
        /// <param name="customerRepository">Customer repository</param>
        /// <param name="eventPublisher">Event published</param>
        public OrderService(IRepository<Order> orderRepository,
            IRepository<SISVN_OrderPaymentBalance> orderPaymentRepository,
            IRepository<OrderItem> orderItemRepository,
            IRepository<OrderNote> orderNoteRepository,
            IRepository<Product> productRepository,
            IRepository<RecurringPayment> recurringPaymentRepository,
            IRepository<Customer> customerRepository,
            IRepository<SISVN_Transporter> SISVN_Transporter,
            IEventPublisher eventPublisher)
        {
            this._orderPaymentRepository = orderPaymentRepository;
            this._orderRepository = orderRepository;
            this._orderItemRepository = orderItemRepository;
            this._orderNoteRepository = orderNoteRepository;
            this._productRepository = productRepository;
            this._recurringPaymentRepository = recurringPaymentRepository;
            this._customerRepository = customerRepository;
            this._eventPublisher = eventPublisher;
            this._SISVN_Transporter = SISVN_Transporter;
        }

        #endregion

        #region Methods

        #region Custom Method

        /// <summary>
        /// [HienNguyen][Tìm kiếm đơn vị vận chuyển]
        /// </summary>
        /// <param name="keyword"></param>
        /// <returns></returns>
        public IList<SISVN_Transporter> SearchSISVN_Transporter(string keyword)
        {
            var transporters = _SISVN_Transporter.Table;
            if (!string.IsNullOrEmpty(keyword))
            {
                transporters = transporters.Where(m => m.Name.Contains(keyword) || m.Id.ToString() == keyword);
            }

            return transporters.ToList();
        }

        /// <summary>
        /// [HienNguyen][Tạo mới đơn vị vận chuyển]
        /// </summary>
        /// <param name="keyword"></param>
        /// <returns></returns>
        public void CreateSISVN_Transporter(SISVN_Transporter transporter)
        {
            _SISVN_Transporter.Insert(transporter);
        }

        /// <summary>
        /// [HienNguyen][Xóa  đơn vị vận chuyển]
        /// </summary>
        /// <param name="keyword"></param>
        /// <returns></returns>
        public void DeleteSISVN_Transporter(int transporterId)
        {
            var transporterById = _SISVN_Transporter.GetById(transporterId);
            transporterById.Deleted = true;
            _SISVN_Transporter.Update(transporterById);
        }

        /// <summary>
        /// [HienNguyen][Cập nhật đơn vị vận chuyển]
        /// </summary>
        /// <param name="keyword"></param>
        /// <returns></returns>
        public SISVN_Transporter UpdateSISVN_Transporter(SISVN_Transporter transporter)
        {
            _SISVN_Transporter.Update(transporter);
            return transporter;
        }

        /// <summary>
        /// [HienNguyen][Get Note by OrderId]
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns></returns>
        public virtual IList<OrderNote> GetOrderNoteByOrderId(int orderId)
        {
            if (orderId == 0)
                return null;

            return _orderNoteRepository.Table.Where(m => m.OrderId == orderId).ToList();
        }

        /// <summary>
        /// [HienNguyen][Create Order Note]
        /// </summary>
        /// <param name="orderNote"></param>
        public virtual void CreateOrderNote(OrderNote orderNote)
        {
            _orderNoteRepository.Insert(orderNote);
        }

        /// <summary>
        /// [HienNguyen][31/07/2018][Hàm lấy dữ liệu thanh toán đơn hàng]
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns></returns>
        public virtual IPagedList<SISVN_OrderPaymentBalance> GetOrderPaymentBalance(
            int orderId,
            int pageIndex = 0,
            int pageSize = int.MaxValue)
        {
            var query = _SISVN_OrderPaymentBalanceRepository.Table;
            query = query.Where(m => m.OrderId == orderId);

            return new PagedList<SISVN_OrderPaymentBalance>(query, pageIndex, pageSize);
        }

        /// <summary>
        /// [HienNguyen][22/07/2018][Hàm đếm số lượng đơn hàng]
        /// </summary>
        /// <param name="orderTypeIds"></param>
        /// <returns></returns>
        public virtual int GetTotalCountOrder(List<int> orderTypeIds = null)
        {
            var query = _orderRepository.Table;
            query = query.Where(o => !o.Deleted);

            return query.Count();
        }

        /// <summary>
        /// [HienNguyen][22/07/2018][Hàm tạo đơn hàng bán hàng]
        /// </summary>
        /// <param name="order"></param>
        /// <param name="orderItems"></param>
        /// <param name="orderPaymentBalances"></param>
        /// <param name="orderNotes"></param>
        /// <returns></returns>
        public virtual Order CreateSaleOrder(Order order, List<OrderItem> orderItems, List<SISVN_OrderPaymentBalance> orderPaymentBalances, List<OrderNote> orderNotes)
        {
            InsertSaleOrder(order, orderItems, orderPaymentBalances, orderNotes);
            return order;
        }

        /// <summary>
        /// [HienNguyen][22/07/2018][Hàm tạo đơn hàng bán hàng]
        /// </summary>
        /// <param name="order"></param>
        /// <param name="orderItems"></param>
        /// <param name="orderPaymentBalances"></param>
        /// <param name="orderNotes"></param>
        public virtual void InsertSaleOrder(Order order, List<OrderItem> orderItems, List<SISVN_OrderPaymentBalance> orderPaymentBalances, List<OrderNote> orderNotes)
        {
            if (order == null)
                throw new ArgumentNullException(nameof(order));

            _orderRepository.Insert(order);

            orderItems.ForEach(m => m.OrderId = order.Id);
            _orderItemRepository.Insert(orderItems);

            orderPaymentBalances.ForEach(m => m.OrderId = order.Id);
            _SISVN_OrderPaymentBalanceRepository.Insert(orderPaymentBalances);

            if (orderNotes != null && orderNotes.Count > 0)
            {
                orderNotes.ForEach(m => m.OrderId = order.Id);
                _orderNoteRepository.Insert(orderNotes);
            }

            //event notification
            _eventPublisher.EntityInserted(order);
        }

        /// <summary>
        /// [HienNguyen][20/07/2018][Hàm tìm kiếm đơn hàng]
        /// </summary>
        /// <param name="keyword"></param>
        /// <param name="storeId"></param>
        /// <param name="statusIds"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public virtual IPagedList<Order> SearchOrdersCustom(string keyword = null,
            int storeId = 0,
            List<int> statusIds = null,
            List<int> orderTypeIds = null,
            DateTime? fromDate = null,
            DateTime? toDate = null,
            int pageIndex = 0,
            int pageSize = int.MaxValue)
        {
            var query = _orderRepository.Table;
            if (storeId > 0)
                query = query.Where(o => o.StoreId == storeId);
            if (statusIds != null && statusIds.Any())
                query = query.Where(o => statusIds.Contains(o.OrderStatusId));
            if (fromDate.HasValue)
                query = query.Where(o => fromDate.Value <= o.CreatedOnUtc);
            if (toDate.HasValue)
                query = query.Where(o => toDate.Value >= o.CreatedOnUtc);

            //[HienNguyen][20/07/2018][Tìm kiếm theo mã đơn hoặc mã/họ tên/SĐT khách hàng/NCC]
            if (!string.IsNullOrEmpty(keyword))
            {
                query = query.Where(m =>
                                        m.Id.ToString().Contains(keyword) ||
                                        m.CustomOrderNumber.Contains(keyword) ||
                                        m.CustomOrderNumber.Contains(keyword) ||
                                        m.BillingAddress.LastName.Contains(keyword) ||
                                        m.BillingAddress.FirstName.Contains(keyword) ||
                                        m.BillingAddress.PhoneNumber.Contains(keyword)
                );
            }

            query = query.Where(o => !o.Deleted);
            query = query.OrderByDescending(o => o.CreatedOnUtc);

            return new PagedList<Order>(query, pageIndex, pageSize);
        }

        /// <summary>
        /// [HienNguyen][20/07/2018][Hàm lấy dữ liệu đơn hàng chi tiết]
        /// </summary>
        /// <param name="orderId"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public virtual IPagedList<OrderItem> GetOrderItemByOrderId(int orderId,
            int pageIndex = 0,
            int pageSize = int.MaxValue)
        {
            var query = _orderItemRepository.Table;
            query = query.Where(m => m.OrderId == orderId);

            query = query.OrderByDescending(m => m.Quantity);

            return new PagedList<OrderItem>(query, pageIndex, pageSize);
        }

        /// <summary>
        /// [HienNguyen][20/07/2018][Xóa đơn hàng]
        /// </summary>
        /// <param name="orderId"></param>
        public virtual void DeleteOrder(int orderId)
        {
            Order order = _orderRepository.GetById(orderId);
            order.Deleted = true;
            UpdateOrder(order);
        }

        /// <summary>
        /// [HienNguyen][20/07/2018][Hủy đơn hàng]
        /// </summary>
        /// <param name="orderId"></param>
        public virtual void CancelOrder(int orderId)
        {
            Order order = _orderRepository.GetById(orderId);
            order.OrderStatusId = (int)(OrderStatus.Cancelled);
            UpdateOrder(order);
        }

        /// <summary>
        /// [HienNguyen][20/07/2018][Khôi phục đơn hàng]
        /// </summary>
        /// <param name="orderId"></param>
        public virtual void RestorOrder(int orderId)
        {
            Order order = _orderRepository.GetById(orderId);
            order.OrderStatusId = (int)(OrderStatus.Pending);
            UpdateOrder(order);
        }

        #endregion

        #region Orders

        /// <summary>
        /// Gets an order
        /// </summary>
        /// <param name="orderId">The order identifier</param>
        /// <returns>Order</returns>
        public virtual Order GetOrderById(int orderId)
        {
            if (orderId == 0)
                return null;

            return _orderRepository.GetById(orderId);
        }

        /// <summary>
        /// Gets an order
        /// </summary>
        /// <param name="customOrderNumber">The custom order number</param>
        /// <returns>Order</returns>
        public virtual Order GetOrderByCustomOrderNumber(string customOrderNumber)
        {
            if (string.IsNullOrEmpty(customOrderNumber))
                return null;

            return _orderRepository.Table.FirstOrDefault(o => o.CustomOrderNumber == customOrderNumber);
        }

        /// <summary>
        /// Get orders by identifiers
        /// </summary>
        /// <param name="orderIds">Order identifiers</param>
        /// <returns>Order</returns>
        public virtual IList<Order> GetOrdersByIds(int[] orderIds)
        {
            if (orderIds == null || orderIds.Length == 0)
                return new List<Order>();

            var query = from o in _orderRepository.Table
                        where orderIds.Contains(o.Id) && !o.Deleted
                        select o;
            var orders = query.ToList();
            //sort by passed identifiers
            var sortedOrders = new List<Order>();
            foreach (var id in orderIds)
            {
                var order = orders.Find(x => x.Id == id);
                if (order != null)
                    sortedOrders.Add(order);
            }
            return sortedOrders;
        }

        /// <summary>
        /// Gets an order
        /// </summary>
        /// <param name="orderGuid">The order identifier</param>
        /// <returns>Order</returns>
        public virtual Order GetOrderByGuid(Guid orderGuid)
        {
            if (orderGuid == Guid.Empty)
                return null;

            var query = from o in _orderRepository.Table
                        where o.OrderGuid == orderGuid
                        select o;
            var order = query.FirstOrDefault();
            return order;
        }

        /// <summary>
        /// Deletes an order
        /// </summary>
        /// <param name="order">The order</param>
        public virtual void DeleteOrder(Order order)
        {
            if (order == null)
                throw new ArgumentNullException(nameof(order));

            order.Deleted = true;
            UpdateOrder(order);

            //event notification
            _eventPublisher.EntityDeleted(order);
        }

        /// <summary>
        /// Search orders
        /// </summary>
        /// <param name="storeId">Store identifier; 0 to load all orders</param>
        /// <param name="vendorId">Vendor identifier; null to load all orders</param>
        /// <param name="customerId">Customer identifier; 0 to load all orders</param>
        /// <param name="productId">Product identifier which was purchased in an order; 0 to load all orders</param>
        /// <param name="affiliateId">Affiliate identifier; 0 to load all orders</param>
        /// <param name="billingCountryId">Billing country identifier; 0 to load all orders</param>
        /// <param name="warehouseId">Warehouse identifier, only orders with products from a specified warehouse will be loaded; 0 to load all orders</param>
        /// <param name="paymentMethodSystemName">Payment method system name; null to load all records</param>
        /// <param name="createdFromUtc">Created date from (UTC); null to load all records</param>
        /// <param name="createdToUtc">Created date to (UTC); null to load all records</param>
        /// <param name="osIds">Order status identifiers; null to load all orders</param>
        /// <param name="psIds">Payment status identifiers; null to load all orders</param>
        /// <param name="ssIds">Shipping status identifiers; null to load all orders</param>
        /// <param name="billingEmail">Billing email. Leave empty to load all records.</param>
        /// <param name="billingLastName">Billing last name. Leave empty to load all records.</param>
        /// <param name="orderNotes">Search in order notes. Leave empty to load all records.</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>Orders</returns>
        public virtual IPagedList<Order> SearchOrders(string keyword = null, int storeId = 0,
            int vendorId = 0, int customerId = 0,
            int productId = 0, int affiliateId = 0, int warehouseId = 0,
            int billingCountryId = 0, string paymentMethodSystemName = null,
            DateTime? createdFromUtc = null, DateTime? createdToUtc = null,
            List<int> osIds = null, List<int> psIds = null, List<int> ssIds = null,
            string billingEmail = null, string billingLastName = "",
            string orderNotes = null, int pageIndex = 0, int pageSize = int.MaxValue, OrderTypeEnum type = OrderTypeEnum.All)
        {
            var query = _orderRepository.Table;
            if (storeId > 0)
                query = query.Where(o => o.StoreId == storeId);
            if (vendorId > 0)
            {
                query = query
                    .Where(o => o.OrderItems
                    .Any(orderItem => orderItem.Product.VendorId == vendorId));
            }
            if (customerId > 0)
                query = query.Where(o => o.CustomerId == customerId);
            if (productId > 0)
            {
                query = query
                    .Where(o => o.OrderItems
                    .Any(orderItem => orderItem.Product.Id == productId));
            }
            if (warehouseId > 0)
            {
                var manageStockInventoryMethodId = (int)ManageInventoryMethod.ManageStock;
                query = query
                    .Where(o => o.OrderItems
                    .Any(orderItem =>
                        //"Use multiple warehouses" enabled
                        //we search in each warehouse
                        (orderItem.Product.ManageInventoryMethodId == manageStockInventoryMethodId &&
                        orderItem.Product.UseMultipleWarehouses &&
                        orderItem.Product.ProductWarehouseInventory.Any(pwi => pwi.WarehouseId == warehouseId))
                        ||
                        //"Use multiple warehouses" disabled
                        //we use standard "warehouse" property
                        ((orderItem.Product.ManageInventoryMethodId != manageStockInventoryMethodId ||
                        !orderItem.Product.UseMultipleWarehouses) &&
                        orderItem.Product.WarehouseId == warehouseId))
                        );
            }
            if (billingCountryId > 0)
                query = query.Where(o => o.BillingAddress != null && o.BillingAddress.CountryId == billingCountryId);
            if (!string.IsNullOrEmpty(paymentMethodSystemName))
                query = query.Where(o => o.PaymentMethodSystemName == paymentMethodSystemName);
            if (affiliateId > 0)
                query = query.Where(o => o.AffiliateId == affiliateId);
            if (createdFromUtc.HasValue)
                query = query.Where(o => createdFromUtc.Value <= o.CreatedOnUtc);
            if (createdToUtc.HasValue)
                query = query.Where(o => createdToUtc.Value >= o.CreatedOnUtc);
            if (osIds != null && osIds.Any())
                query = query.Where(o => osIds.Contains(o.OrderStatusId));
            if (psIds != null && psIds.Any())
                query = query.Where(o => psIds.Contains(o.PaymentStatusId));
            if (ssIds != null && ssIds.Any())
                query = query.Where(o => ssIds.Contains(o.ShippingStatusId));
            if (!string.IsNullOrEmpty(billingEmail))
                query = query.Where(o => o.BillingAddress != null && !string.IsNullOrEmpty(o.BillingAddress.Email) && o.BillingAddress.Email.Contains(billingEmail));
            if (!string.IsNullOrEmpty(billingLastName))
                query = query.Where(o => o.BillingAddress != null && !string.IsNullOrEmpty(o.BillingAddress.LastName) && o.BillingAddress.LastName.Contains(billingLastName));
            if (!string.IsNullOrEmpty(orderNotes))
                query = query.Where(o => o.OrderNotes.Any(on => on.Note.Contains(orderNotes)));

            //[HienNguyen][20/07/2018][Tìm kiếm theo mã đơn hoặc mã/họ tên/SĐT khách hàng/NCC]
            if (!string.IsNullOrEmpty(keyword))
            {
                query = query.Where(m =>
                                        m.Id.ToString().Contains(keyword) ||
                                        m.CustomOrderNumber.Contains(keyword) ||
                                        m.BillingAddress.LastName.Contains(keyword) ||
                                        m.BillingAddress.FirstName.Contains(keyword) ||
                                        m.BillingAddress.PhoneNumber.Contains(keyword)
                );
            }
            switch (type)
            {
                case OrderTypeEnum.Deleted:
                    query = query.Where(o => o.Deleted);
                    break;
                case OrderTypeEnum.Debt:
                    query = query.Where(o => o.OrderTotal > o.OrderPaid);
                    break;
                case OrderTypeEnum.Return:
                    //*TODO*
                    break;
                default:
                    query = query.Where(o => !o.Deleted);
                    break;

            }
            query = query.OrderByDescending(o => o.CreatedOnUtc);

            //database layer paging
            return new PagedList<Order>(query, pageIndex, pageSize);
        }

        /// <summary>
        /// Inserts an order
        /// </summary>
        /// <param name="order">Order</param>
        public virtual void InsertOrder(Order order)
        {
            if (order == null)
                throw new ArgumentNullException(nameof(order));

            _orderRepository.Insert(order);

            //event notification
            _eventPublisher.EntityInserted(order);
        }

        /// <summary>
        /// Updates the order
        /// </summary>
        /// <param name="order">The order</param>
        public virtual void UpdateOrder(Order order)
        {
            if (order == null)
                throw new ArgumentNullException(nameof(order));

            _orderRepository.Update(order);

            //event notification
            _eventPublisher.EntityUpdated(order);
        }

        /// <summary>
        /// Get an order by authorization transaction ID and payment method system name
        /// </summary>
        /// <param name="authorizationTransactionId">Authorization transaction ID</param>
        /// <param name="paymentMethodSystemName">Payment method system name</param>
        /// <returns>Order</returns>
        public virtual Order GetOrderByAuthorizationTransactionIdAndPaymentMethod(string authorizationTransactionId,
            string paymentMethodSystemName)
        {
            var query = _orderRepository.Table;
            if (!string.IsNullOrWhiteSpace(authorizationTransactionId))
                query = query.Where(o => o.AuthorizationTransactionId == authorizationTransactionId);

            if (!string.IsNullOrWhiteSpace(paymentMethodSystemName))
                query = query.Where(o => o.PaymentMethodSystemName == paymentMethodSystemName);

            query = query.OrderByDescending(o => o.CreatedOnUtc);
            var order = query.FirstOrDefault();
            return order;
        }

        /// <summary>
        /// [AnhLe][15/09/2018][Hàm cập nhập phiếu nhập hàng]
        /// </summary>
        /// <param name="order"></param>
        /// <param name="orderItems"></param>
        /// <param name="orderPaymentBalances"></param>
        /// <param name="orderNotes"></param>
        /// <returns></returns>
        public virtual Order UpdateInputVouchcer(Order order, List<OrderItem> orderItems, List<SISVN_OrderPaymentBalance> orderPaymentBalances, List<OrderNote> orderNotes)
        {
            if (order == null)
                throw new ArgumentNullException(nameof(order));

            //Xóa những dòng dữ liệu liên quan cũ.
            order.OrderItems.Clear();
            order.SISVN_OrderPaymentBalances.Clear();

            _orderRepository.Update(order);

            orderItems.ForEach(m => m.OrderId = order.Id);
            _orderItemRepository.Insert(orderItems);
            orderPaymentBalances.ForEach(m => m.OrderId = order.Id);
            _SISVN_OrderPaymentBalanceRepository.Insert(orderPaymentBalances);

            if (orderNotes != null && orderNotes.Count > 0)
            {
                orderNotes.ForEach(m => m.OrderId = order.Id);
                _orderNoteRepository.Insert(orderNotes);
            }

            //event notification
            _eventPublisher.EntityInserted(order);
            return order;
        }


        #endregion

        #region Orders items

        /// <summary>
        /// Gets an order item
        /// </summary>
        /// <param name="orderItemId">Order item identifier</param>
        /// <returns>Order item</returns>
        public virtual OrderItem GetOrderItemById(int orderItemId)
        {
            if (orderItemId == 0)
                return null;

            return _orderItemRepository.GetById(orderItemId);
        }

        /// <summary>
        /// Gets an item
        /// </summary>
        /// <param name="orderItemGuid">Order identifier</param>
        /// <returns>Order item</returns>
        public virtual OrderItem GetOrderItemByGuid(Guid orderItemGuid)
        {
            if (orderItemGuid == Guid.Empty)
                return null;

            var query = from orderItem in _orderItemRepository.Table
                        where orderItem.OrderItemGuid == orderItemGuid
                        select orderItem;
            var item = query.FirstOrDefault();
            return item;
        }

        /// <summary>
        /// Gets all downloadable order items
        /// </summary>
        /// <param name="customerId">Customer identifier; null to load all records</param>
        /// <returns>Order items</returns>
        public virtual IList<OrderItem> GetDownloadableOrderItems(int customerId)
        {
            if (customerId == 0)
                throw new ArgumentOutOfRangeException("customerId");

            var query = from orderItem in _orderItemRepository.Table
                        join o in _orderRepository.Table on orderItem.OrderId equals o.Id
                        join p in _productRepository.Table on orderItem.ProductId equals p.Id
                        where customerId == o.CustomerId &&
                        p.IsDownload &&
                        !o.Deleted
                        orderby o.CreatedOnUtc descending, orderItem.Id
                        select orderItem;

            var orderItems = query.ToList();
            return orderItems;
        }

        /// <summary>
        /// Delete an order item
        /// </summary>
        /// <param name="orderItem">The order item</param>
        public virtual void DeleteOrderItem(OrderItem orderItem)
        {
            if (orderItem == null)
                throw new ArgumentNullException(nameof(orderItem));

            _orderItemRepository.Delete(orderItem);

            //event notification
            _eventPublisher.EntityDeleted(orderItem);
        }
        public virtual void DeleteOrderPaymentItem(SISVN_OrderPaymentBalance payment)
        {
            if (payment == null)
                throw new ArgumentNullException(nameof(payment));

            _orderPaymentRepository.Delete(payment);
        }


        #endregion

        #region Orders notes

        /// <summary>
        /// Gets an order note
        /// </summary>
        /// <param name="orderNoteId">The order note identifier</param>
        /// <returns>Order note</returns>
        public virtual OrderNote GetOrderNoteById(int orderNoteId)
        {
            if (orderNoteId == 0)
                return null;

            return _orderNoteRepository.GetById(orderNoteId);
        }

        /// <summary>
        /// Deletes an order note
        /// </summary>
        /// <param name="orderNote">The order note</param>
        public virtual void DeleteOrderNote(OrderNote orderNote)
        {
            if (orderNote == null)
                throw new ArgumentNullException(nameof(orderNote));

            _orderNoteRepository.Delete(orderNote);

            //event notification
            _eventPublisher.EntityDeleted(orderNote);
        }

        #endregion

        #region Recurring payments

        /// <summary>
        /// Deletes a recurring payment
        /// </summary>
        /// <param name="recurringPayment">Recurring payment</param>
        public virtual void DeleteRecurringPayment(RecurringPayment recurringPayment)
        {
            if (recurringPayment == null)
                throw new ArgumentNullException(nameof(recurringPayment));

            recurringPayment.Deleted = true;
            UpdateRecurringPayment(recurringPayment);

            //event notification
            _eventPublisher.EntityDeleted(recurringPayment);
        }

        /// <summary>
        /// Gets a recurring payment
        /// </summary>
        /// <param name="recurringPaymentId">The recurring payment identifier</param>
        /// <returns>Recurring payment</returns>
        public virtual RecurringPayment GetRecurringPaymentById(int recurringPaymentId)
        {
            if (recurringPaymentId == 0)
                return null;

            return _recurringPaymentRepository.GetById(recurringPaymentId);
        }

        /// <summary>
        /// Inserts a recurring payment
        /// </summary>
        /// <param name="recurringPayment">Recurring payment</param>
        public virtual void InsertRecurringPayment(RecurringPayment recurringPayment)
        {
            if (recurringPayment == null)
                throw new ArgumentNullException(nameof(recurringPayment));

            _recurringPaymentRepository.Insert(recurringPayment);

            //event notification
            _eventPublisher.EntityInserted(recurringPayment);
        }

        /// <summary>
        /// Updates the recurring payment
        /// </summary>
        /// <param name="recurringPayment">Recurring payment</param>
        public virtual void UpdateRecurringPayment(RecurringPayment recurringPayment)
        {
            if (recurringPayment == null)
                throw new ArgumentNullException(nameof(recurringPayment));

            _recurringPaymentRepository.Update(recurringPayment);

            //event notification
            _eventPublisher.EntityUpdated(recurringPayment);
        }

        /// <summary>
        /// Search recurring payments
        /// </summary>
        /// <param name="storeId">The store identifier; 0 to load all records</param>
        /// <param name="customerId">The customer identifier; 0 to load all records</param>
        /// <param name="initialOrderId">The initial order identifier; 0 to load all records</param>
        /// <param name="initialOrderStatus">Initial order status identifier; null to load all records</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Recurring payments</returns>
        public virtual IPagedList<RecurringPayment> SearchRecurringPayments(int storeId = 0,
            int customerId = 0, int initialOrderId = 0, OrderStatus? initialOrderStatus = null,
            int pageIndex = 0, int pageSize = int.MaxValue, bool showHidden = false)
        {
            int? initialOrderStatusId = null;
            if (initialOrderStatus.HasValue)
                initialOrderStatusId = (int)initialOrderStatus.Value;

            var query1 = from rp in _recurringPaymentRepository.Table
                         join c in _customerRepository.Table on rp.InitialOrder.CustomerId equals c.Id
                         where
                         (!rp.Deleted) &&
                         (showHidden || !rp.InitialOrder.Deleted) &&
                         (showHidden || !c.Deleted) &&
                         (showHidden || rp.IsActive) &&
                         (customerId == 0 || rp.InitialOrder.CustomerId == customerId) &&
                         (storeId == 0 || rp.InitialOrder.StoreId == storeId) &&
                         (initialOrderId == 0 || rp.InitialOrder.Id == initialOrderId) &&
                         (!initialOrderStatusId.HasValue || initialOrderStatusId.Value == 0 ||
                          rp.InitialOrder.OrderStatusId == initialOrderStatusId.Value)
                         select rp.Id;

            var query2 = from rp in _recurringPaymentRepository.Table
                         where query1.Contains(rp.Id)
                         orderby rp.StartDateUtc, rp.Id
                         select rp;

            var recurringPayments = new PagedList<RecurringPayment>(query2, pageIndex, pageSize);
            return recurringPayments;
        }

        #endregion

        #endregion
    }
}
