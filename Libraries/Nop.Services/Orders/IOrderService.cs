﻿using System;
using System.Collections.Generic;
using Nop.Core;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Shipping;

namespace Nop.Services.Orders
{
    /// <summary>
    /// Order service interface
    /// </summary>
    public partial interface IOrderService
    {
        #region Custom Method
        void DeleteOrderPaymentItem(SISVN_OrderPaymentBalance payment);

        /// <summary>
        /// [HienNguyen][Tìm kiếm đơn vị vận chuyển]
        /// </summary>
        /// <param name="keyword"></param>
        /// <returns></returns>
        IList<SISVN_Transporter> SearchSISVN_Transporter(string keyword);

        /// <summary>
        /// [HienNguyen][Tạo mới đơn vị vận chuyển]
        /// </summary>
        /// <param name="keyword"></param>
        /// <returns></returns>
        void CreateSISVN_Transporter(SISVN_Transporter transporter);

        /// <summary>
        /// [HienNguyen][Xóa  đơn vị vận chuyển]
        /// </summary>
        /// <param name="keyword"></param>
        /// <returns></returns>
        void DeleteSISVN_Transporter(int transporterId);

        /// <summary>
        /// [HienNguyen][Cập nhật đơn vị vận chuyển]
        /// </summary>
        /// <param name="keyword"></param>
        /// <returns></returns>
        SISVN_Transporter UpdateSISVN_Transporter(SISVN_Transporter transporter);

        /// <summary>
        /// [HienNguyen][Get Note by OrderId]
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns></returns>
        IList<OrderNote> GetOrderNoteByOrderId(int orderId);

        /// <summary>
        /// [HienNguyen][Create Order Note]
        /// </summary>
        /// <param name="orderNote"></param>
        void CreateOrderNote(OrderNote orderNote);

        /// <summary>
        /// [HienNguyen][22/07/2018][Hàm tạo đơn hàng bán hàng]
        /// </summary>
        /// <param name="order"></param>
        /// <param name="orderItems"></param>
        /// <param name="orderPaymentBalances"></param>
        /// <param name="orderNotes"></param>
        /// <returns></returns>
        Order CreateSaleOrder(Order order, List<OrderItem> orderItems, List<SISVN_OrderPaymentBalance> orderPaymentBalances, List<OrderNote> orderNotes);

        /// <summary>
        /// [HienNguyen][22/07/2018][Hàm tạo đơn hàng bán hàng]
        /// </summary>
        /// <param name="order"></param>
        /// <param name="orderItems"></param>
        /// <param name="orderPaymentBalances"></param>
        /// <param name="orderNotes"></param>
        void InsertSaleOrder(Order order, List<OrderItem> orderItems, List<SISVN_OrderPaymentBalance> orderPaymentBalances, List<OrderNote> orderNotes);

        /// <summary>
        /// [HienNguyen][31/07/2018][Hàm lấy dữ liệu thanh toán đơn hàng]
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns></returns>
        IPagedList<SISVN_OrderPaymentBalance> GetOrderPaymentBalance(
            int orderId,
            int pageIndex = 0,
            int pageSize = int.MaxValue);

        /// <summary>
        /// [HienNguyen][22/07/2018][Hàm đếm số lượng đơn hàng]
        /// </summary>
        /// <param name="orderTypeIds"></param>
        /// <returns></returns>
        int GetTotalCountOrder(List<int> orderTypeIds = null);

        /// <summary>
        /// [HienNguyen][20/07/2018][Hàm tìm kiếm đơn hàng]
        /// </summary>
        /// <param name="keyword"></param>
        /// <param name="storeId"></param>
        /// <param name="statusIds"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        IPagedList<Order> SearchOrdersCustom(string keyword = null,
            int storeId = 0,
            List<int> statusIds = null,
            List<int> orderTypeIds = null,
            DateTime? fromDate = null,
            DateTime? toDate = null,
            int pageIndex = 0,
            int pageSize = int.MaxValue);

        /// <summary>
        /// [HienNguyen][20/07/2018][Hàm lấy dữ liệu đơn hàng chi tiết]
        /// </summary>
        /// <param name="orderId"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        IPagedList<OrderItem> GetOrderItemByOrderId(int orderId,
            int pageIndex = 0,
            int pageSize = int.MaxValue);

        /// <summary>
        /// [HienNguyen][20/07/2018][Xóa đơn hàng]
        /// </summary>
        /// <param name="orderId"></param>
        void DeleteOrder(int orderId);

        /// <summary>
        /// [HienNguyen][20/07/2018][Hủy đơn hàng]
        /// </summary>
        /// <param name="orderId"></param>
        void CancelOrder(int orderId);

        /// <summary>
        /// [HienNguyen][20/07/2018][Khôi phục đơn hàng]
        /// </summary>
        /// <param name="orderId"></param>
        void RestorOrder(int orderId);

        #endregion

        #region Orders

        /// <summary>
        /// Gets an order
        /// </summary>
        /// <param name="orderId">The order identifier</param>
        /// <returns>Order</returns>
        Order GetOrderById(int orderId);

        /// <summary>
        /// Gets an order
        /// </summary>
        /// <param name="customOrderNumber">The custom order number</param>
        /// <returns>Order</returns>
        Order GetOrderByCustomOrderNumber(string customOrderNumber);

        /// <summary>
        /// Get orders by identifiers
        /// </summary>
        /// <param name="orderIds">Order identifiers</param>
        /// <returns>Order</returns>
        IList<Order> GetOrdersByIds(int[] orderIds);

        /// <summary>
        /// Gets an order
        /// </summary>
        /// <param name="orderGuid">The order identifier</param>
        /// <returns>Order</returns>
        Order GetOrderByGuid(Guid orderGuid);

        /// <summary>
        /// Deletes an order
        /// </summary>
        /// <param name="order">The order</param>
        void DeleteOrder(Order order);

        /// <summary>
        /// Search orders
        /// </summary>
        /// <param name="storeId">Store identifier; null to load all orders</param>
        /// <param name="vendorId">Vendor identifier; null to load all orders</param>
        /// <param name="customerId">Customer identifier; null to load all orders</param>
        /// <param name="productId">Product identifier which was purchased in an order; 0 to load all orders</param>
        /// <param name="affiliateId">Affiliate identifier; 0 to load all orders</param>
        /// <param name="billingCountryId">Billing country identifier; 0 to load all orders</param>
        /// <param name="warehouseId">Warehouse identifier, only orders with products from a specified warehouse will be loaded; 0 to load all orders</param>
        /// <param name="paymentMethodSystemName">Payment method system name; null to load all records</param>
        /// <param name="createdFromUtc">Created date from (UTC); null to load all records</param>
        /// <param name="createdToUtc">Created date to (UTC); null to load all records</param>
        /// <param name="osIds">Order status identifiers; null to load all orders</param>
        /// <param name="psIds">Payment status identifiers; null to load all orders</param>
        /// <param name="ssIds">Shipping status identifiers; null to load all orders</param>
        /// <param name="billingEmail">Billing email. Leave empty to load all records.</param>
        /// <param name="billingLastName">Billing last name. Leave empty to load all records.</param>
        /// <param name="orderNotes">Search in order notes. Leave empty to load all records.</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>Orders</returns>
        IPagedList<Order> SearchOrders(string keyword = null, int storeId = 0,
            int vendorId = 0, int customerId = 0,
            int productId = 0, int affiliateId = 0, int warehouseId = 0,
            int billingCountryId = 0, string paymentMethodSystemName = null,
            DateTime? createdFromUtc = null, DateTime? createdToUtc = null,
            List<int> osIds = null, List<int> psIds = null, List<int> ssIds = null,
            string billingEmail = null, string billingLastName = "",
            string orderNotes = null, int pageIndex = 0, int pageSize = int.MaxValue, OrderTypeEnum type = OrderTypeEnum.All);

        /// <summary>
        /// Inserts an order
        /// </summary>
        /// <param name="order">Order</param>
        void InsertOrder(Order order);

        /// <summary>
        /// Updates the order
        /// </summary>
        /// <param name="order">The order</param>
        void UpdateOrder(Order order);

        /// <summary>
        /// Get an order by authorization transaction ID and payment method system name
        /// </summary>
        /// <param name="authorizationTransactionId">Authorization transaction ID</param>
        /// <param name="paymentMethodSystemName">Payment method system name</param>
        /// <returns>Order</returns>
        Order GetOrderByAuthorizationTransactionIdAndPaymentMethod(string authorizationTransactionId, string paymentMethodSystemName);



        /// <summary>
        /// [AnhLe][15/09/2018][Hàm cập nhập phiếu nhập hàng]
        /// </summary>
        /// <param name="order"></param>
        /// <param name="orderItems"></param>
        /// <param name="orderPaymentBalances"></param>
        /// <param name="orderNotes"></param>
        /// <returns></returns>
        Order UpdateInputVouchcer(Order order, List<OrderItem> orderItems, List<SISVN_OrderPaymentBalance> orderPaymentBalances, List<OrderNote> orderNotes);



        #endregion

        #region Orders items

        /// <summary>
        /// Gets an order item
        /// </summary>
        /// <param name="orderItemId">Order item identifier</param>
        /// <returns>Order item</returns>
        OrderItem GetOrderItemById(int orderItemId);

        /// <summary>
        /// Gets an order item
        /// </summary>
        /// <param name="orderItemGuid">Order item identifier</param>
        /// <returns>Order item</returns>
        OrderItem GetOrderItemByGuid(Guid orderItemGuid);

        /// <summary>
        /// Gets all downloadable order items
        /// </summary>
        /// <param name="customerId">Customer identifier; null to load all records</param>
        /// <returns>Order items</returns>
        IList<OrderItem> GetDownloadableOrderItems(int customerId);

        /// <summary>
        /// Delete an order item
        /// </summary>
        /// <param name="orderItem">The order item</param>
        void DeleteOrderItem(OrderItem orderItem);

        #endregion

        #region Order notes

        /// <summary>
        /// Gets an order note
        /// </summary>
        /// <param name="orderNoteId">The order note identifier</param>
        /// <returns>Order note</returns>
        OrderNote GetOrderNoteById(int orderNoteId);

        /// <summary>
        /// Deletes an order note
        /// </summary>
        /// <param name="orderNote">The order note</param>
        void DeleteOrderNote(OrderNote orderNote);

        #endregion

        #region Recurring payments

        /// <summary>
        /// Deletes a recurring payment
        /// </summary>
        /// <param name="recurringPayment">Recurring payment</param>
        void DeleteRecurringPayment(RecurringPayment recurringPayment);

        /// <summary>
        /// Gets a recurring payment
        /// </summary>
        /// <param name="recurringPaymentId">The recurring payment identifier</param>
        /// <returns>Recurring payment</returns>
        RecurringPayment GetRecurringPaymentById(int recurringPaymentId);

        /// <summary>
        /// Inserts a recurring payment
        /// </summary>
        /// <param name="recurringPayment">Recurring payment</param>
        void InsertRecurringPayment(RecurringPayment recurringPayment);

        /// <summary>
        /// Updates the recurring payment
        /// </summary>
        /// <param name="recurringPayment">Recurring payment</param>
        void UpdateRecurringPayment(RecurringPayment recurringPayment);

        /// <summary>
        /// Search recurring payments
        /// </summary>
        /// <param name="storeId">The store identifier; 0 to load all records</param>
        /// <param name="customerId">The customer identifier; 0 to load all records</param>
        /// <param name="initialOrderId">The initial order identifier; 0 to load all records</param>
        /// <param name="initialOrderStatus">Initial order status identifier; null to load all records</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Recurring payments</returns>
        IPagedList<RecurringPayment> SearchRecurringPayments(int storeId = 0,
            int customerId = 0, int initialOrderId = 0, OrderStatus? initialOrderStatus = null,
            int pageIndex = 0, int pageSize = int.MaxValue, bool showHidden = false);

        #endregion
    }
}
