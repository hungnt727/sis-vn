﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Services.Orders
{
    public enum OrderTypeEnum
    {
        All,
        Deleted,
        Debt,
        Return
    }
}
