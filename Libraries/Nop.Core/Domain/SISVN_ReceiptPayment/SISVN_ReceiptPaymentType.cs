﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.SISVN_ReceiptPayment
{
    /// <summary>
    /// [HienNguyen][Loại phiếu thi hoặc chi]
    /// </summary>
    public partial class SISVN_ReceiptPaymentType : BaseEntity
    {
        private ICollection<SISVN_ExpenseType> _SISVN_ExpenseTypes;
        private ICollection<SISVN_ReceiptPayment> _SISVN_ReceiptPayments;

        public string Name { get; set; }
        public string Descriptions { get; set; }

        public virtual ICollection<SISVN_ExpenseType> SISVN_ExpenseTypes
        {
            get { return _SISVN_ExpenseTypes ?? (_SISVN_ExpenseTypes = new List<SISVN_ExpenseType>()); }
            protected set { _SISVN_ExpenseTypes = value; }
        }

        public virtual ICollection<SISVN_ReceiptPayment> SISVN_ReceiptPayments
        {
            get { return _SISVN_ReceiptPayments ?? (_SISVN_ReceiptPayments = new List<SISVN_ReceiptPayment>()); }
            protected set { _SISVN_ReceiptPayments = value; }
        }
    }
}
