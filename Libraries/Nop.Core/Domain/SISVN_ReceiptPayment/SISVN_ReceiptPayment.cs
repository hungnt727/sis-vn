﻿using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Stores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.SISVN_ReceiptPayment
{
    public partial class SISVN_ReceiptPayment : BaseEntity
    {
        public string ReceiptPaymentCode { get; set; }
        public int ReceiptPaymentTypeId { get; set; }
        public int PaymentMethodId { get; set; }
        public int ExpenseTypeId { get; set; }
        public int CustomerId { get; set; }
        public int StoreId { get; set; }
        public decimal Amount { get; set; }
        public DateTime TransactionDate { get; set; }
        public int CreateUserId { get; set; }
        public bool? IsDelete { get; set; }
        public int? DeleteUserId { get; set; }
        public DateTime? DeleteDate { get; set; }
        public string Descriptions { get; set; }

        public SISVN_ReceiptPaymentType SISVN_ReceiptPaymentType { get; set; }
        public SISVN_ExpenseType SISVN_ExpenseType { get; set; }
        public Store Store { get; set; }
    }
}
