﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.SISVN_ReceiptPayment
{
    public partial class SISVN_ExpenseType : BaseEntity
    {
        private ICollection<SISVN_ReceiptPayment> _SISVN_ReceiptPayments;

        public int ReceiptPaymentTypeId { get; set; }
        public string Name { get; set; }
        public string Descriptions { get; set; }
        public virtual SISVN_ReceiptPaymentType SISVN_ReceiptPaymentType { get; set; }

        public virtual ICollection<SISVN_ReceiptPayment> SISVN_ReceiptPayments
        {
            get { return _SISVN_ReceiptPayments ?? (_SISVN_ReceiptPayments = new List<SISVN_ReceiptPayment>()); }
            protected set { _SISVN_ReceiptPayments = value; }
        }
    }
}
