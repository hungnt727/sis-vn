﻿using System.ComponentModel;

namespace Nop.Core.Domain.Orders
{
    /// <summary>
    /// Represents an order status enumeration
    /// </summary>
    public enum PaymentMethod
    {
        [Description("Tiền mặt")]
        Money = 10,
        [Description("Chuyển khoản")]
        Tranfer = 10,
        [Description("Thẻ")]
        Card = 10,
    }
}
