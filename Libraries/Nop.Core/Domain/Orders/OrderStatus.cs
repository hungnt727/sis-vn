﻿using System.ComponentModel;

namespace Nop.Core.Domain.Orders
{
    /// <summary>
    /// Represents an order status enumeration
    /// </summary>
    public enum OrderStatus
    {
        /// <summary>
        /// Đã đặt
        /// </summary>
        /// 
        [Description("Đã đặt")]
        Pending = 10,
        /// <summary>
        /// Xác nhận
        /// </summary>
        /// 
        [Description("Xác nhận")]
        Processing = 20,
        /// <summary>
        /// Hoàn thành
        /// </summary>
        /// 
        [Description("Hoàn thành")]
        Complete = 30,
        /// <summary>
        /// Hủy
        /// </summary>
        /// 
        [Description("Hủy")]
        Cancelled = 40,
        /// <summary>
        /// Đang giao hàng
        /// </summary>
        /// 
        [Description("Đang giao hàng")]
        Shipping = 50,
        /// <summary>
        /// Lưu tạm
        /// </summary>
        /// 
        [Description("Lưu tạm")]
        Temporary = 60
    }    
}
