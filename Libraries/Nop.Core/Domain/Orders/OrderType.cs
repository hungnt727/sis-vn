﻿using System.ComponentModel;

namespace Nop.Core.Domain.Orders
{
    /// <summary>
    /// Represents an order status enumeration
    /// Tiết đầu ngữ "INVENTORY_" đánh dùng cho module kho.
    /// </summary>
    public enum OrderType
    {
        /// <summary>
        /// POS bán hàng
        /// </summary>
        /// 
        [Description("POS")]
        POS = 10,

        /// <summary>
        /// Đặt hàng
        /// </summary>
        [Description("Đặt hàng")]
        ORDER = 20,

        /// <summary>
        /// Nhập hàng
        /// </summary>
        [Description("Nhập hàng")]
        INVENTORY_INPUT = 30,

        /// <summary>
        /// Nhập bù
        /// </summary>
        [Description("Nhập bù")]
        INVENTORY_INPUTOFFSET = 40,

        /// <summary>
        /// Nhập bù
        /// </summary>
        [Description("Nhập trả hàng khách đặt")]
        INVENTORY_INPUTRETUTNCUSTOMER = 50,

        /// <summary>
        /// Xuất thiếu
        /// </summary>
        [Description("Xuất thiếu")]
        INVENTORY_OUTPUTMISSING = 60,

        /// <summary>
        /// "Nhập bù
        /// </summary>
        [Description("Xuất trả NCC")]
        INVENTORY_OUTPUTRETURNVENDOR = 70,
    }
}
