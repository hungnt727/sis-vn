﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.Orders
{
    public partial class SISVN_OrderPaymentBalance : BaseEntity
    {
        /// <summary>
        /// Gets or sets the order identifier
        /// </summary>
        public int OrderId { get; set; }

        public DateTime PaymentDateUtc { get; set; }

        public decimal PaymentAmount { get; set; }

        public int PaymentMethodId { get; set; }

        /// <summary>
        /// Gets the order
        /// </summary>
        public virtual Order Order { get; set; }

        /// <summary>
        /// Gets or sets the order status
        /// </summary>
        public PaymentMethod PaymentMethod
        {
            get
            {
                return (PaymentMethod)PaymentMethodId;
            }
            set
            {
                PaymentMethodId = (int)value;
            }
        }

        public string PaymentMethodSystemName { get; set; }
    }
}
