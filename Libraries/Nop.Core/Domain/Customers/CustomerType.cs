﻿using System.ComponentModel;

namespace Nop.Core.Domain.Customers
{
    /// <summary>
    /// Represents an order status enumeration
    /// </summary>
    public enum CustomerType
    {

        [Description("Khách lẻ")]
        GUEST = 10,

        [Description("Khách sỉ")]
        WHOLESALE = 20,

        [Description("Khách Vip")]
        VIP = 30
    }
}
